import 'package:atoma_cash/models/api/error/error.dart' as mc2;
import 'package:equatable/equatable.dart';

class Action<T> extends Equatable {
  final T result;
  final mc2.Error error;

  Action({this.result, this.error});

  @override
  List<Object> get props => [result, error];
}
