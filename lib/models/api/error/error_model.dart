// To parse this JSON data, do
//
//     final errorResponseModel = errorResponseModelFromJson(jsonString);

import 'dart:convert';

ErrorResponse errorResponseModelFromJson(String str) =>
    ErrorResponse.fromJson(json.decode(str));

String errorResponseModelToJson(ErrorResponse data) =>
    json.encode(data.toJson());

class ErrorResponse {
  String type;
  String title;
  int status;
  String detail;
  String instance;

  ErrorResponse({
    this.type,
    this.title,
    this.status,
    this.detail,
    this.instance
  });

  ErrorResponse copyWith({
    String type,
    String title,
    int status,
    String detail,
    String instance,
  }) =>
      ErrorResponse(
        type: type ?? this.type,
        title: title ?? this.title,
        status: status ?? this.status,
        detail: detail ?? this.detail,
        instance: instance ?? this.instance,
      );

  factory ErrorResponse.fromJson(Map<String, dynamic> json) =>
      ErrorResponse(
    type: json["type"] == null ? null : json["type"],
    title: json["title"] == null ? null : json["title"],
    status: json["status"] == null ? null : json["status"],
    detail: json["detail"] == null ? null : json["detail"],
    instance: json["instance"] == null ? null : json["instance"],
  );

  Map<String, dynamic> toJson() => {
    "type": type == null ? null : type,
    "title": title == null ? null : title,
    "status": status == null ? null : status,
    "detail": detail == null ? null : detail,
    "instance": instance == null ? null : instance,
  };

  @override
  String toString() {
    return 'ErrorResponse{title: $title, status: $status}';
  }


}
