import 'error_model.dart';

enum ErrorType { TIMEOUT, RESPONSE, CANCEL, NO_INTERNET_CONNECTION, UNEXPECTED }

class Error {
  int code;
  ErrorType type = ErrorType.UNEXPECTED;
  ErrorResponse errorDetails;

  Error({this.type, this.errorDetails, this.code = 500});

  @override
  String toString() {
    return 'Error{code: $code, type: $type, errorDetails: $errorDetails}';
  }


}
