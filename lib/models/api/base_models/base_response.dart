import 'dart:collection';

import 'package:atoma_cash/models/api/error/error.dart' as mc2;
import 'package:atoma_cash/models/api/error/error_model.dart';
import 'package:dio/dio.dart';

typedef S GenericsCreator<S>(Map<String, dynamic> json);

typedef S DynamicGenericsCreator<S>(dynamic data);

class BaseResponse<T> {
  T result;
  mc2.Error error;
  bool nullableResult = false;

  GenericsCreator<T> creator;
  DynamicGenericsCreator<T> dynamicGenericsCreator;

  BaseResponse(Response response,
      {GenericsCreator<T> this.creator, this.nullableResult = false}) {
    if (response.statusCode == 200 || response.statusCode == 204) {
      if (!nullableResult) {
        result = creator(response.data);
      }
    } else {
      error = _handleError(null);
    }
  }

  BaseResponse.dynamic(Response response,
      {DynamicGenericsCreator<T> this.dynamicGenericsCreator,
        this.nullableResult = false}) {
    if (response.statusCode == 200) {
      if (!nullableResult) {
        result = dynamicGenericsCreator(response.data);
      }
    } else {
      error = _handleError(null);
    }
  }

  BaseResponse.withError(this.error);

  BaseResponse.withDioError(DioError error) {
    this.error = _handleError(error);
  }

  mc2.Error _handleError(dynamic dioError) {
    error = mc2.Error(type: mc2.ErrorType.UNEXPECTED);

    if (dioError is DioError) {
      switch (dioError.type) {
        case DioErrorType.CANCEL:
          error.type = mc2.ErrorType.CANCEL;
          break;
        case DioErrorType.CONNECT_TIMEOUT:
          error.type = mc2.ErrorType.TIMEOUT;
          break;
        case DioErrorType.DEFAULT:
          error.type = mc2.ErrorType.NO_INTERNET_CONNECTION;
          break;
        case DioErrorType.RECEIVE_TIMEOUT:
          error.type = mc2.ErrorType.TIMEOUT;
          break;
        case DioErrorType.RESPONSE:
          error.type = mc2.ErrorType.RESPONSE;
          error.code = dioError.response?.statusCode;
          try {
            var responseData = dioError.response?.data;
            print("response data - ${responseData.toString()}");
            if (responseData is LinkedHashMap) {
              error.errorDetails =
                  ErrorResponse.fromJson(dioError.response?.data);
            } else if (responseData is String) {
              error.errorDetails =
                  errorResponseModelFromJson(dioError.response?.data);
            }
          } catch (e) {
            print("Failed json serialization on response - ${e.toString()}");
          }
          break;
        case DioErrorType.SEND_TIMEOUT:
          error.type = mc2.ErrorType.TIMEOUT;
          break;
      }
    }

    return error;
  }

  bool isSuccess() => (result != null) || (nullableResult && error == null);
}
