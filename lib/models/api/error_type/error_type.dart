const String kBankingUnauthenticated = "/api/Errors/banking/unauthenticated";
const String kTokenExpired = "/api/errors/auth/token_expired";
const String kRetryLater = "/Errors/auth/retry_later";
const String kPasscodeLimitExceeded = "/Errors/auth/limit_exceeded";
