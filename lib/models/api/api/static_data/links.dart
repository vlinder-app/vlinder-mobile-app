class Links {
  final String voteFeaturesUrl;
  final String requestFeatureUrl;
  final String helpEmail;
  final String infoEmail;

  Links(
      {this.voteFeaturesUrl,
      this.requestFeatureUrl,
      this.helpEmail,
      this.infoEmail});

  Links copyWith(
          {String voteFeaturesUrl,
          String requestFeatureUrl,
          String helpEmail,
          String infoEmail}) =>
      Links(
        voteFeaturesUrl: voteFeaturesUrl ?? this.voteFeaturesUrl,
        requestFeatureUrl: requestFeatureUrl ?? this.requestFeatureUrl,
        helpEmail: helpEmail ?? this.helpEmail,
        infoEmail: infoEmail ?? this.infoEmail,
      );

  factory Links.fromJson(Map<String, dynamic> json) => Links(
      voteFeaturesUrl: json['voteFeaturesUrl'] ?? json['voteFeaturesUrl'],
      requestFeatureUrl: json['requestFeatureUrl'] ?? json['requestFeatureUrl'],
      helpEmail: json['helpEmail'] ?? json['helpEmail'],
      infoEmail: json['infoEmail'] ?? json['infoEmail']);

  Map<String, dynamic> toJson() => {
        'voteFeaturesUrl': voteFeaturesUrl == null ? null : voteFeaturesUrl,
        'requestFeatureUrl':
            requestFeatureUrl == null ? null : requestFeatureUrl,
        'helpEmail': helpEmail == null ? null : helpEmail,
        'infoEmail': infoEmail == null ? null : infoEmail,
      };
}
