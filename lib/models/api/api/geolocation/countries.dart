// To parse this JSON data, do
//
//     final countries = countriesFromJson(jsonString);

import 'dart:convert';

List<Country> countriesFromJsonList(List<dynamic> countriesList) {
  return new List<Country>.from(countriesList.map((x) => Country.fromJson(x)));
}

List<Country> countriesFromJson(Map<String, dynamic> jsonMap) {
  return new List<Country>.from(jsonMap.values.map((x) => Country.fromJson(x)));
}

List<Country> countriesFromJsonRaw(String str) =>
    new List<Country>.from(json.decode(str).map((x) => Country.fromJson(x)));

String countriesToJson(List<Country> data) =>
    json.encode(new List<dynamic>.from(data.map((x) => x.toJson())));

class Country {
  List<Region> regions;
  String phoneCode;
  String iso2Code;
  String name;
  List<String> countryCodes;

  Country({
    this.regions,
    this.phoneCode,
    this.iso2Code,
    this.name,
    this.countryCodes,
  });

  factory Country.fromJson(Map<String, dynamic> json) => new Country(
    regions: json["regions"] == null
        ? null
        : new List<Region>.from(
        json["regions"].map((x) => Region.fromJson(x))),
    phoneCode: json["phoneCode"] == null ? null : json["phoneCode"],
    iso2Code: json["iso2Code"] == null ? null : json["iso2Code"],
    name: json["name"] == null ? null : json["name"],
    countryCodes: json["countryCodes"] == null
        ? null
        : new List<String>.from(json["countryCodes"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "regions": regions == null
        ? null
        : new List<dynamic>.from(regions.map((x) => x.toJson())),
    "phoneCode": phoneCode == null ? null : phoneCode,
    "iso2Code": iso2Code == null ? null : iso2Code,
    "name": name == null ? null : name,
    "countryCodes": countryCodes == null
        ? null
        : new List<dynamic>.from(countryCodes.map((x) => x)),
  };
}

class Region {
  String iso2Code;
  String name;

  Region({
    this.iso2Code,
    this.name,
  });

  @override
  String toString() {
    return 'Region{iso2Code: $iso2Code, name: $name}';
  }

  factory Region.fromJson(Map<String, dynamic> json) =>
      new Region(
        iso2Code: json["iso2Code"],
        name: json["name"],
      );

  Map<String, dynamic> toJson() =>
      {
        "iso2Code": iso2Code,
        "name": name,
      };
}
