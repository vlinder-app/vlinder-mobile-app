import 'dart:convert';

Geolocation geolocationFromJson(String str) =>
    Geolocation.fromJson(json.decode(str));

String geolocationToJson(Geolocation data) => json.encode(data.toJson());

class Geolocation {
  String iso2Code;
  String region;
  String city;

  Geolocation({
    this.iso2Code,
    this.region,
    this.city,
  });

  factory Geolocation.fromJson(Map<String, dynamic> json) => new Geolocation(
        iso2Code: json["iso2Code"] == null ? null : json["iso2Code"],
        region: json["region"] == null ? null : json["region"],
        city: json["city"] == null ? null : json["city"],
      );

  Map<String, dynamic> toJson() => {
        "iso2Code": iso2Code == null ? null : iso2Code,
        "region": region == null ? null : region,
        "city": city == null ? null : city,
      };
}
