import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class IBannerModel implements Equatable {
  String get id;

  DateTime get appearsAt;

  DateTime get expiresAt;

  String get imageUrl;

  String get assetId;

  String get action;

  DateTime get readAt;

  String titleForContext(BuildContext context);

  String bodyForContext(BuildContext context);
}

class BannerModel extends IBannerModel {
  BannerModel({
    this.id,
    this.userId,
    this.appearsAt,
    this.expiresAt,
    this.title,
    this.imageUrl,
    this.body,
    this.link,
    this.readAt,
  });

  final String id;
  final String userId;
  final DateTime appearsAt;
  final DateTime expiresAt;
  final String title;
  final String imageUrl;
  final String body;
  final String link;
  final DateTime readAt;

  BannerModel copyWith({
    String id,
    String userId,
    DateTime appearsAt,
    DateTime expiresAt,
    String title,
    String imageUrl,
    String body,
    String link,
    DateTime readAt,
  }) =>
      BannerModel(
        id: id ?? this.id,
        userId: userId ?? this.userId,
        appearsAt: appearsAt ?? this.appearsAt,
        expiresAt: expiresAt ?? this.expiresAt,
        title: title ?? this.title,
        imageUrl: imageUrl ?? this.imageUrl,
        body: body ?? this.body,
        link: link ?? this.link,
        readAt: readAt ?? this.readAt,
      );

  factory BannerModel.fromJson(Map<String, dynamic> json) => BannerModel(
        id: json["id"] == null ? null : json["id"],
        userId: json["userId"] == null ? null : json["userId"],
        appearsAt: json["appearsAt"] == null
            ? null
            : DateTime.parse(json["appearsAt"]),
        expiresAt: json["expiresAt"] == null
            ? null
            : DateTime.parse(json["expiresAt"]),
        title: json["title"] == null ? null : json["title"],
        imageUrl: json["imageUrl"] == null ? null : json["imageUrl"],
        body: json["body"] == null ? null : json["body"],
        link: json["link"] == null ? null : json["link"],
        readAt: json["readAt"] == null ? null : DateTime.parse(json["readAt"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "userId": userId == null ? null : userId,
        "appearsAt": appearsAt == null ? null : appearsAt.toIso8601String(),
        "expiresAt": expiresAt == null ? null : expiresAt.toIso8601String(),
        "title": title == null ? null : title,
        "imageUrl": imageUrl == null ? null : imageUrl,
        "body": body == null ? null : body,
        "link": link == null ? null : link,
        "readAt": readAt == null ? null : readAt.toIso8601String(),
      };

  @override
  List<Object> get props => [
        id,
        userId,
        appearsAt,
        expiresAt,
        title,
        imageUrl,
        body,
        link,
        readAt,
      ];

  @override
  String get action => link;

  @override
  String get assetId => null;

  String titleForContext(BuildContext context) => title;

  String bodyForContext(BuildContext context) => body;

  @override
  bool get stringify => true;
}
