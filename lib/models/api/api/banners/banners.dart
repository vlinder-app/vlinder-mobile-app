// To parse this JSON data, do
//
//     final banners = bannersFromJson(jsonString);

import 'dart:convert';

import 'package:atoma_cash/models/api/api/banners/banner_model.dart';

Banners bannersFromJson(String str) => Banners.fromJson(json.decode(str));

String bannersToJson(Banners data) => json.encode(data.toJson());

class Banners {
  Banners({
    this.banners,
    this.totalCount,
  });

  final List<BannerModel> banners;
  final int totalCount;

  Banners copyWith({
    List<BannerModel> banners,
    int totalCount,
  }) =>
      Banners(
        banners: banners ?? this.banners,
        totalCount: totalCount ?? this.totalCount,
      );

  factory Banners.fromJson(Map<String, dynamic> json) => Banners(
        banners: json["banners"] == null
            ? null
            : List<BannerModel>.from(
                json["banners"].map((x) => BannerModel.fromJson(x))),
        totalCount: json["totalCount"] == null ? null : json["totalCount"],
      );

  Map<String, dynamic> toJson() => {
        "banners": banners == null
            ? null
            : List<dynamic>.from(banners.map((x) => x.toJson())),
        "totalCount": totalCount == null ? null : totalCount,
      };
}
