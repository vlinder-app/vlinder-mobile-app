// To parse this JSON data, do
//
//     final reports = reportsFromJson(jsonString);

import 'dart:convert';

import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/categories/transactions_type.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/providers/currency_provider.dart';
import 'package:atoma_cash/src/reports/reports_list/list/filters.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

Reports reportsFromJson(String str) => Reports.fromJson(json.decode(str));

String reportsToJson(Reports data) => json.encode(data.toJson());

class Reports {
  Amount totalAmount;
  double totalCarbonFootprint;
  List<Report> reports;
  Transaction lastTransaction;

  List<Report> get incomeReports =>
      Filters.reportsForCategoryType(reports, TransactionsType.INCOME);

  Amount get totalIncome {
    double totalIncomeValue = 0.0;
    incomeReports.forEach((report) => totalIncomeValue += report.amount.value);
    return BaseCurrencyProvider()
        .baseCurrency
        .copyWith(value: totalIncomeValue);
  }

  Reports({
    this.lastTransaction,
    this.totalAmount,
    this.totalCarbonFootprint,
    this.reports,
  });

  Reports copyWith({
    Amount totalIncome,
    Amount amount,
    double totalCarbonFootprint,
    Transaction lastTransaction,
    List<Report> reports,
  }) =>
      Reports(
        lastTransaction: lastTransaction ?? this.lastTransaction,
        totalAmount: amount ?? this.totalAmount,
        totalCarbonFootprint: totalCarbonFootprint ?? this.totalCarbonFootprint,
        reports: reports ?? this.reports,
      );

  factory Reports.fromJson(Map<String, dynamic> json) => Reports(
    lastTransaction: json["lastTransaction"] == null
        ? null
        : Transaction.fromJson(json["lastTransaction"]),
    totalAmount: json["totalAmount"] == null
        ? null
        : Amount.fromJson(json["totalAmount"]),
    totalCarbonFootprint: json["totalCarbonFootprint"] == null
        ? null
        : json["totalCarbonFootprint"].toDouble(),
    reports: json["reports"] == null
        ? null
        : List<Report>.from(json["reports"].map((x) => Report.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "amount": totalAmount == null ? null : totalAmount.toJson(),
        "totalCarbonFootprint":
            totalCarbonFootprint == null ? null : totalCarbonFootprint,
        "reports": reports == null
            ? null
            : List<dynamic>.from(reports.map((x) => x.toJson())),
      };

  @override
  String toString() {
    return 'Reports{totalIncome: $totalIncome, totalAmount: $totalAmount, totalCarbonFootprint: $totalCarbonFootprint, reports: $reports, lastTransaction: $lastTransaction}';
  }
}

@immutable
class Report extends Equatable {
  final Category category;
  final Amount amount;
  final double amountPercentage;
  final double carbonFootprint;
  final double carbonFootprintPercentage;
  final List<Transaction> transactions;

  Report({
    this.category,
    this.amount,
    this.amountPercentage,
    this.carbonFootprint,
    this.carbonFootprintPercentage,
    this.transactions,
  });

  Report copyWith({
    Category category,
    Amount amount,
    double amountPercentage,
    double carbonFootprint,
    double carbonFootprintPercentage,
    List<Transaction> transactions,
  }) =>
      Report(
        category: category ?? this.category,
        amount: amount ?? this.amount,
        amountPercentage: amountPercentage ?? this.amountPercentage,
        carbonFootprint: carbonFootprint ?? this.carbonFootprint,
        carbonFootprintPercentage:
            carbonFootprintPercentage ?? this.carbonFootprintPercentage,
        transactions: transactions ?? this.transactions,
      );

  factory Report.fromJson(Map<String, dynamic> json) => Report(
        category: json["category"] == null
            ? null
            : Category.fromJson(json["category"]),
        amount: json["amount"] == null ? null : Amount.fromJson(json["amount"]),
        amountPercentage: json["amountPercentage"] == null
            ? null
            : json["amountPercentage"].toDouble(),
        carbonFootprint: json["carbonFootprint"] == null
            ? null
            : json["carbonFootprint"].toDouble(),
        carbonFootprintPercentage: json["carbonFootprintPercentage"] == null
            ? null
            : json["carbonFootprintPercentage"].toDouble(),
        transactions: json["transactions"] == null
            ? null
            : List<Transaction>.from(
                json["transactions"].map((x) => Transaction.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "category": category == null ? null : category.toJson(),
        "amount": amount == null ? null : amount.toJson(),
        "amountPercentage": amountPercentage == null ? null : amountPercentage,
        "carbonFootprint": carbonFootprint == null ? null : carbonFootprint,
        "carbonFootprintPercentage": carbonFootprintPercentage == null
            ? null
            : carbonFootprintPercentage,
        "transactions": transactions == null
            ? null
            : List<dynamic>.from(transactions.map((x) => x.toJson())),
      };

  @override
  List<Object> get props =>
      [
        category,
        amount,
        amountPercentage,
        carbonFootprint,
        carbonFootprintPercentage,
        transactions
      ];

  @override
  String toString() {
    return 'Report{category: $category, amount: $amount, amountPercentage: $amountPercentage, carbonFootprint: $carbonFootprint, carbonFootprintPercentage: $carbonFootprintPercentage, transactions: $transactions}';
  }
}
