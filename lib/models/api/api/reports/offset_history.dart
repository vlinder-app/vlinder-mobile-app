// To parse this JSON data, do
//
//     final offsetHistory = offsetHistoryFromJson(jsonString);

import 'dart:convert';

OffsetHistory offsetHistoryFromJson(String str) =>
    OffsetHistory.fromJson(json.decode(str));

String offsetHistoryToJson(OffsetHistory data) => json.encode(data.toJson());

class OffsetHistory {
  OffsetHistory({
    this.offsetHistory,
  });

  List<OffsetHistoryItem> offsetHistory;

  OffsetHistory copyWith({
    List<OffsetHistoryItem> offsetHistory,
  }) =>
      OffsetHistory(
        offsetHistory: offsetHistory ?? this.offsetHistory,
      );

  factory OffsetHistory.fromJson(Map<String, dynamic> json) => OffsetHistory(
        offsetHistory: json["offsetHistory"] == null
            ? null
            : List<OffsetHistoryItem>.from(json["offsetHistory"]
                .map((x) => OffsetHistoryItem.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "offsetHistory": offsetHistory == null
            ? null
            : List<dynamic>.from(offsetHistory.map((x) => x.toJson())),
      };
}

class OffsetHistoryItem {
  OffsetHistoryItem({
    this.carbonFootprint,
    this.startDate,
    this.endDate,
  });

  double carbonFootprint;
  DateTime startDate;
  DateTime endDate;

  factory OffsetHistoryItem.placeholder(DateTime startDate, DateTime endDate) {
    return OffsetHistoryItem(
        carbonFootprint: 0.0, startDate: startDate, endDate: endDate);
  }

  OffsetHistoryItem copyWith({
    double carbonFootprint,
    dynamic offset,
    DateTime startDate,
    DateTime endDate,
  }) =>
      OffsetHistoryItem(
        carbonFootprint: carbonFootprint ?? this.carbonFootprint,
        startDate: startDate ?? this.startDate,
        endDate: endDate ?? this.endDate,
      );

  factory OffsetHistoryItem.fromJson(Map<String, dynamic> json) =>
      OffsetHistoryItem(
        carbonFootprint: json["carbonFootprint"] == null
            ? null
            : json["carbonFootprint"].toDouble(),
        startDate: json["startDate"] == null
            ? null
            : DateTime.parse(json["startDate"]),
        endDate:
            json["endDate"] == null ? null : DateTime.parse(json["endDate"]),
      );

  Map<String, dynamic> toJson() => {
        "carbonFootprint": carbonFootprint == null ? null : carbonFootprint,
        "startDate": startDate == null ? null : startDate.toIso8601String(),
        "endDate": endDate == null ? null : endDate.toIso8601String(),
      };
}
