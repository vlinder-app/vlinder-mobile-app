import 'package:atoma_cash/extensions/double_extensions.dart';
import 'package:equatable/equatable.dart';

class Amount extends Equatable {
  final String currency;
  final String symbol;
  final double value;
  final String displayName;

  Amount({
    this.currency,
    this.displayName,
    this.symbol,
    this.value,
  });

  Amount copyWith({
    String displayName,
    String currency,
    String symbol,
    double value,
  }) =>
      Amount(
        displayName: displayName ?? this.displayName,
        currency: currency ?? this.currency,
        symbol: symbol ?? this.symbol,
        value: value ?? this.value,
      );

  Amount resetAmount() => Amount(
      currency: this.currency,
      symbol: this.symbol,
      displayName: this.displayName);

  factory Amount.fromJson(Map<String, dynamic> json) => Amount(
        displayName: json["displayName"] == null ? null : json["displayName"],
        currency: json["currency"] == null ? null : json["currency"],
        symbol: json["symbol"] == null ? null : json["symbol"],
        value: json["value"] == null ? null : json["value"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "displayName": displayName == null ? null : displayName,
        "currency": currency == null ? null : currency,
        "symbol": symbol == null ? null : symbol,
        "value": value == null ? null : value,
      };

  String _ticker() => symbol ?? currency;

  String currencyFormatted() {
    return value?.currencyFormatted(_ticker()) ?? "...";
  }

  String currencyShortFormatted() {
    return value?.currencyAbridgedString(_ticker()) ?? "...";
  }

  @override
  String toString() {
    return 'Amount{currency: $currency, symbol: $symbol, value: $value, displayName: $displayName}';
  }

  @override
  List<Object> get props => [currency, symbol, value, displayName];
}
