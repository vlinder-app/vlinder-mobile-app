// To parse this JSON data, do
//
//     final syncStatus = syncStatusFromJson(jsonString);

import 'dart:convert';

SyncStatus syncStatusFromJson(String str) =>
    SyncStatus.fromJson(json.decode(str));

String syncStatusToJson(SyncStatus data) => json.encode(data.toJson());

class SyncStatus {
  static const statusOk = "Ok";
  static const statusAuthRequired = "BankingAuthRequired";
  static const statusSyncRequired = "BankingSyncRequired";

  final String status;

  SyncStatus({
    this.status,
  });

  factory SyncStatus.fromJson(Map<String, dynamic> json) => SyncStatus(
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "status": status == null ? null : status,
      };
}
