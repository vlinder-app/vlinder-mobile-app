// To parse this JSON data, do
//
//     final balances = balancesFromJson(jsonString);

import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'amount.dart';

Balances balancesFromJson(Map<String, dynamic> jsonMap) =>
    Balances.fromJson(jsonMap);

Balances balancesFromJsonString(String str) =>
    Balances.fromJson(json.decode(str));

String balancesToJson(Balances data) => json.encode(data.toJson());

class Balances extends Equatable {
  final Amount totalBaseCurrencyBalance;
  final List<Balance> balances;

  Balances({
    this.totalBaseCurrencyBalance,
    this.balances,
  });

  Balances copyWith({
    Amount totalBaseCurrencyBalance,
    List<Balance> balances,
  }) =>
      Balances(
        totalBaseCurrencyBalance:
        totalBaseCurrencyBalance ?? this.totalBaseCurrencyBalance,
        balances: balances ?? this.balances,
      );

  factory Balances.fromJson(Map<String, dynamic> json) => Balances(
    totalBaseCurrencyBalance: json["totalBaseCurrencyBalance"] == null
        ? null
        : Amount.fromJson(json["totalBaseCurrencyBalance"]),
    balances: json["balances"] == null
        ? null
        : List<Balance>.from(
        json["balances"].map((x) => Balance.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "totalBaseCurrencyBalance": totalBaseCurrencyBalance == null
        ? null
        : totalBaseCurrencyBalance.toJson(),
    "balances": balances == null
        ? null
        : List<dynamic>.from(balances.map((x) => x.toJson())),
  };

  @override
  List<Object> get props => [totalBaseCurrencyBalance, balances];
}

class Balance extends Equatable {
  static const String _bitcoinAccountIssuer = "bitcoin";

  final String accountId;
  final String accountPublicId;
  final String accountIssuer;
  final String accountIconUrl;
  final String accountName;
  final Amount baseCurrencyBalance;
  final int sortOrder;
  final String financeProviderType;
  final DateTime relevantAt;
  final String currency;
  final String symbol;
  final double value;

  Balance({
    this.accountId,
    this.accountPublicId,
    this.accountIssuer,
    this.accountIconUrl,
    this.accountName,
    this.baseCurrencyBalance,
    this.sortOrder,
    this.financeProviderType,
    this.relevantAt,
    this.currency,
    this.symbol,
    this.value,
  });

  Balance copyWith({
    String accountId,
    String accountPublicId,
    String accountIssuer,
    String accountIconUrl,
    String accountName,
    Amount baseCurrencyBalance,
    int sortOrder,
    String financeProviderType,
    DateTime relevantAt,
    String currency,
    String symbol,
    double value,
  }) =>
      Balance(
        accountId: accountId ?? this.accountId,
        accountPublicId: accountPublicId ?? this.accountPublicId,
        accountIssuer: accountIssuer ?? this.accountIssuer,
        accountIconUrl: accountIconUrl ?? this.accountIconUrl,
        accountName: accountName ?? this.accountName,
        baseCurrencyBalance: baseCurrencyBalance ?? this.baseCurrencyBalance,
        sortOrder: sortOrder ?? this.sortOrder,
        financeProviderType: financeProviderType ?? this.financeProviderType,
        relevantAt: relevantAt ?? this.relevantAt,
        currency: currency ?? this.currency,
        symbol: symbol ?? this.symbol,
        value: value ?? this.value,
      );

  factory Balance.fromJson(Map<String, dynamic> json) => Balance(
    accountId: json["accountId"] == null ? null : json["accountId"],
    accountPublicId:
    json["accountPublicId"] == null ? null : json["accountPublicId"],
    accountIssuer:
    json["accountIssuer"] == null ? null : json["accountIssuer"],
    accountIconUrl:
    json["accountIconUrl"] == null ? null : json["accountIconUrl"],
    accountName: json["accountName"] == null ? null : json["accountName"],
    baseCurrencyBalance: json["baseCurrencyBalance"] == null
        ? null
        : Amount.fromJson(json["baseCurrencyBalance"]),
    sortOrder: json["sortOrder"] == null ? null : json["sortOrder"],
    financeProviderType: json["financeProviderType"] == null
        ? null
        : json["financeProviderType"],
    relevantAt: json["relevantAt"] == null
        ? null
        : DateTime.parse(json["relevantAt"]),
    currency: json["currency"] == null ? null : json["currency"],
    symbol: json["symbol"] == null ? null : json["symbol"],
    value: json["value"] == null ? null : json["value"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "accountId": accountId == null ? null : accountId,
    "accountPublicId": accountPublicId == null ? null : accountPublicId,
    "accountIssuer": accountIssuer == null ? null : accountIssuer,
    "accountIconUrl": accountIconUrl == null ? null : accountIconUrl,
    "accountName": accountName == null ? null : accountName,
    "baseCurrencyBalance":
    baseCurrencyBalance == null ? null : baseCurrencyBalance.toJson(),
    "sortOrder": sortOrder == null ? null : sortOrder,
    "financeProviderType":
    financeProviderType == null ? null : financeProviderType,
    "relevantAt": relevantAt == null ? null : relevantAt.toIso8601String(),
    "currency": currency == null ? null : currency,
    "symbol": symbol == null ? null : symbol,
    "value": value == null ? null : value,
  };

  bool get isCryptoBalance =>
      accountIssuer != null &&
          accountIssuer.toLowerCase() == _bitcoinAccountIssuer;

  @override
  List<Object> get props =>
      [
        accountId,
        accountPublicId,
        accountIssuer,
        accountIconUrl,
        accountName,
        baseCurrencyBalance,
        sortOrder,
        financeProviderType,
        relevantAt,
        currency,
        symbol,
        value
      ];
}


