// To parse this JSON data, do
//
//     final patchModel = patchModelFromJson(jsonString);

import 'dart:convert';

List<PatchModel> patchModelFromJson(String str) => List<PatchModel>.from(json.decode(str).map((x) => PatchModel.fromJson(x)));

String patchModelToJson(List<PatchModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class PatchModel {
  String path;
  String op;
  String from;

  PatchModel({
    this.path,
    this.op,
    this.from,
  });

  PatchModel copyWith({
    String path,
    String op,
    String from,
  }) =>
      PatchModel(
        path: path ?? this.path,
        op: op ?? this.op,
        from: from ?? this.from,
      );

  factory PatchModel.moveBalances(int from, int to) => PatchModel(
    from: "/balances/$from",
    path: "/balances/$to",
    op: "move"
  );

  factory PatchModel.fromJson(Map<String, dynamic> json) => PatchModel(
    path: json["path"] == null ? null : json["path"],
    op: json["op"] == null ? null : json["op"],
    from: json["from"] == null ? null : json["from"],
  );

  Map<String, dynamic> toJson() => {
    "path": path == null ? null : path,
    "op": op == null ? null : op,
    "from": from == null ? null : from,
  };
}
