import 'dart:convert';

import 'package:atoma_cash/models/api/error/error_model.dart';

SyncResult syncResultFromJson(Map<String, dynamic> jsonMap) =>
    SyncResult.fromJson(jsonMap);

SyncResult syncResultFromJsonString(String str) =>
    SyncResult.fromJson(json.decode(str));

String syncResultToJson(SyncResult data) => json.encode(data.toJson());

class SyncResult {
  ErrorResponse bank;
  ErrorResponse bitcoin;

  SyncResult({
    this.bank,
    this.bitcoin,
  });

  SyncResult copyWith({
    ErrorResponse bank,
    ErrorResponse bitcoin,
  }) =>
      SyncResult(
        bank: bank ?? this.bank,
        bitcoin: bitcoin ?? this.bitcoin,
      );

  factory SyncResult.fromJson(Map<String, dynamic> json) => SyncResult(
        bank:
            json["Bank"] == null ? null : ErrorResponse.fromJson(json["Bank"]),
        bitcoin: json["Bitcoin"] == null
            ? null
            : ErrorResponse.fromJson(json["Bitcoin"]),
      );

  Map<String, dynamic> toJson() => {
        "Bank": bank == null ? null : bank.toJson(),
        "Bitcoin": bitcoin == null ? null : bitcoin.toJson(),
      };
}
