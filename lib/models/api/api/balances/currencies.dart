import 'dart:convert';

import 'package:atoma_cash/models/api/api/balances/amount.dart';

Currencies currenciesFromJson(String str) =>
    Currencies.fromJson(json.decode(str));

String currenciesToJson(Currencies data) => json.encode(data.toJson());

class Currencies {
  Currencies({
    this.currencies,
  });

  List<Amount> currencies;

  Currencies copyWith({
    List<Amount> currencies,
  }) =>
      Currencies(
        currencies: currencies ?? this.currencies,
      );

  factory Currencies.fromJson(Map<String, dynamic> json) => Currencies(
        currencies: json["currencies"] == null
            ? null
            : List<Amount>.from(
                json["currencies"].map((x) => Amount.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "currencies": currencies == null
            ? null
            : List<dynamic>.from(currencies.map((x) => x.toJson())),
      };
}
