import 'package:json_annotation/json_annotation.dart';

part 'connection_sync_model.g.dart';

@JsonSerializable(includeIfNull: false)
class ConnectionSyncModel {
  final String connectionId;
  final String accountId;

  ConnectionSyncModel({this.connectionId, this.accountId});

  factory ConnectionSyncModel.fromJson(Map<String, dynamic> json) =>
      _$ConnectionSyncModelFromJson(json);

  Map<String, dynamic> toJson() => _$ConnectionSyncModelToJson(this);
}
