// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'connection_sync_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ConnectionSyncModel _$ConnectionSyncModelFromJson(Map<String, dynamic> json) {
  return ConnectionSyncModel(
    connectionId: json['connectionId'] as String,
    accountId: json['accountId'] as String,
  );
}

Map<String, dynamic> _$ConnectionSyncModelToJson(ConnectionSyncModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('connectionId', instance.connectionId);
  writeNotNull('accountId', instance.accountId);
  return val;
}
