// To parse this JSON data, do
//
//     final uriResponse = uriResponseFromJson(jsonString);

import 'dart:convert';

UriModel uriResponseFromJson(String str) => UriModel.fromJson(json.decode(str));

String uriResponseToJson(UriModel data) => json.encode(data.toJson());

class UriModel {
  String failureUri;
  String successUri;
  String uri;

  UriModel({
    this.failureUri,
    this.successUri,
    this.uri,
  });

  UriModel copyWith({
    String failureUri,
    String successUri,
    String uri,
  }) =>
      UriModel(
        failureUri: failureUri ?? this.failureUri,
        successUri: successUri ?? this.successUri,
        uri: uri ?? this.uri,
      );

  factory UriModel.fromJson(Map<String, dynamic> json) => UriModel(
    failureUri: json["failureUri"] == null ? null : json["failureUri"],
    successUri: json["successUri"] == null ? null : json["successUri"],
    uri: json["uri"] == null ? null : json["uri"],
  );

  Map<String, dynamic> toJson() => {
    "failureUri": failureUri == null ? null : failureUri,
    "successUri": successUri == null ? null : successUri,
    "uri": uri == null ? null : uri,
  };

  @override
  String toString() {
    return 'UriModel{failureUri: $failureUri, successUri: $successUri, uri: $uri}';
  }


}
