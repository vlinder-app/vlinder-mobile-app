import 'dart:convert';

import 'package:flutter/cupertino.dart';

Connections connectionsFromJson(String str) =>
    Connections.fromJson(json.decode(str));

String connectionsToJson(Connections data) => json.encode(data.toJson());

class Connections {
  final List<Connection> connections;

  Connections({@required this.connections});

  Connections copyWith({
    List<Connection> connections,
  }) =>
      Connections(
        connections: connections ?? this.connections,
      );

  factory Connections.fromJson(Map<String, dynamic> json) => Connections(
        connections: json["connections"] == null
            ? null
            : List<Connection>.from(
                json["connections"].map((x) => Connection.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "connections": connections == null
            ? null
            : List<dynamic>.from(connections.map((x) => x.toJson())),
      };
}

class Connection {
  final String connectionId;
  final String providerName;
  final String providerIconUrl;
  final DateTime refreshedAt;
  final DateTime nextRefreshPossibleAt;

  Connection(
      {@required this.connectionId,
      @required this.providerName,
      @required this.providerIconUrl,
      @required this.refreshedAt,
      @required this.nextRefreshPossibleAt});

  Connection copyWith(
      String connectionId,
      String providerName,
      String providerIconUrl,
      DateTime refreshedAt,
      DateTime nextRefreshPossibleAt) {
    return Connection(
      connectionId: connectionId ?? this.connectionId,
      providerName: providerName ?? this.providerName,
      providerIconUrl: providerIconUrl ?? this.providerIconUrl,
      refreshedAt: refreshedAt ?? this.refreshedAt,
      nextRefreshPossibleAt:
          nextRefreshPossibleAt ?? this.nextRefreshPossibleAt,
    );
  }

  factory Connection.fromJson(Map<String, dynamic> json) => Connection(
        connectionId:
            json['connectionId'] == null ? null : json['connectionId'],
        providerName:
            json['providerName'] == null ? null : json['providerName'],
        providerIconUrl:
            json['providerIconUrl'] == null ? null : json['providerIconUrl'],
        refreshedAt: json['refreshedAt'] == null
            ? null
            : DateTime.parse(json['refreshedAt']),
        nextRefreshPossibleAt: json['nextRefreshPossibleAt'] == null
            ? null
            : DateTime.parse(json['nextRefreshPossibleAt']),
      );

  Map<String, dynamic> toJson() => {
        'connectionId': connectionId == null ? null : connectionId,
        'providerName': providerName == null ? null : providerName,
        'providerIconUrl': providerIconUrl == null ? null : providerIconUrl,
        'refreshedAt':
            refreshedAt == null ? null : refreshedAt.toIso8601String(),
        'nextRefreshPossibleAt': nextRefreshPossibleAt == null
            ? null
            : nextRefreshPossibleAt.toIso8601String(),
      };
}
