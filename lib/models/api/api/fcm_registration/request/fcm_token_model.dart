import 'package:json_annotation/json_annotation.dart';

part 'fcm_token_model.g.dart';

@JsonSerializable(includeIfNull: false)
class FcmTokenModel {
  String registrationToken;

  FcmTokenModel({
    this.registrationToken,
  });

  factory FcmTokenModel.fromJson(Map<String, dynamic> json) =>
      _$FcmTokenModelFromJson(json);

  Map<String, dynamic> toJson() => _$FcmTokenModelToJson(this);
}