// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fcm_token_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FcmTokenModel _$FcmTokenModelFromJson(Map<String, dynamic> json) {
  return FcmTokenModel(
    registrationToken: json['registrationToken'] as String,
  );
}

Map<String, dynamic> _$FcmTokenModelToJson(FcmTokenModel instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('registrationToken', instance.registrationToken);
  return val;
}
