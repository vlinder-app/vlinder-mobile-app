import 'package:json_annotation/json_annotation.dart';

part 'profile_model.g.dart';

@JsonSerializable(includeIfNull: false)
class ProfileModel {
  String email;
  String firstName;
  String lastName;

  ProfileModel({
    this.email,
    this.firstName,
    this.lastName,
  });

  factory ProfileModel.fromJson(Map<String, dynamic> json) =>
      _$ProfileModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProfileModelToJson(this);
}
