import 'package:equatable/equatable.dart';

class Profile extends Equatable {
  String email;
  bool emailConfirmed;
  String firstName;
  String lastName;
  String phoneNumber;

  Profile({
    this.email,
    this.emailConfirmed,
    this.firstName,
    this.lastName,
    this.phoneNumber,
  });

  factory Profile.fromJson(Map<String, dynamic> json) => new Profile(
        email: json["email"],
        emailConfirmed: json["emailConfirmed"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        phoneNumber: json["phoneNumber"],
      );

  Map<String, dynamic> toJson() => {
        "email": email,
        "emailConfirmed": emailConfirmed,
        "firstName": firstName,
        "lastName": lastName,
        "phoneNumber": phoneNumber,
      };

  @override
  String toString() {
    return 'Profile{email: $email, emailConfirmed: $emailConfirmed, firstName: $firstName, lastName: $lastName, phoneNumber: $phoneNumber}';
  }

  @override
  // TODO: implement props
  List<Object> get props =>
      [email, emailConfirmed, firstName, lastName, phoneNumber];
}
