// To parse this JSON data, do
//
//     final spending = spendingFromJson(jsonString);

import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'spendings.dart';

Spending spendingFromJson(String str) => Spending.fromJson(json.decode(str));


String spendingToJson(Spending data) => json.encode(data.toJson());

class Spending extends Equatable {
  final DateTime relevantAt;
  final Period period;
  final Budget budget;
  final Income income;
  final Savings savings;
  final List<Goal> goals;
  final List<SpendingByCategory> spendingByCategories;

  Spending({
    this.relevantAt,
    this.period,
    this.budget,
    this.income,
    this.savings,
    this.goals,
    this.spendingByCategories,
  });

  Spending copyWith({
    DateTime relevantAt,
    Period period,
    Budget budget,
    Income income,
    Savings savings,
    List<Goal> goals,
    List<SpendingByCategory> spendingByCategories,
  }) =>
      Spending(
        relevantAt: relevantAt ?? this.relevantAt,
        period: period ?? this.period,
        budget: budget ?? this.budget,
        income: income ?? this.income,
        savings: savings ?? this.savings,
        goals: goals ?? this.goals,
        spendingByCategories: spendingByCategories ?? this.spendingByCategories,
      );

  factory Spending.fromJson(Map<String, dynamic> json) => Spending(
        relevantAt: json["relevantAt"] == null ? null : DateTime.parse(json["relevantAt"]),
        period: json["period"] == null ? null : Period.fromJson(json["period"]),
        budget: json["budget"] == null ? null : Budget.fromJson(json["budget"]),
        income: json["income"] == null ? null : Income.fromJson(json["income"]),
        savings:
            json["savings"] == null ? null : Savings.fromJson(json["savings"]),
        goals: json["goals"] == null
            ? null
            : List<Goal>.from(json["goals"].map((x) => Goal.fromJson(x))),
        spendingByCategories: json["spendingByCategories"] == null
            ? null
            : List<SpendingByCategory>.from(json["spendingByCategories"]
                .map((x) => SpendingByCategory.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "relevantAt": relevantAt == null ? null : relevantAt.toIso8601String(),
        "period": period == null ? null : period.toJson(),
        "budget": budget == null ? null : budget.toJson(),
        "income": income == null ? null : income.toJson(),
        "savings": savings == null ? null : savings.toJson(),
        "goals": goals == null
            ? null
            : List<dynamic>.from(goals.map((x) => x.toJson())),
        "spendingByCategories": spendingByCategories == null
            ? null
            : List<dynamic>.from(spendingByCategories.map((x) => x.toJson())),
      };

  @override
  List<Object> get props =>
      [period, budget, income, savings, goals, spendingByCategories, relevantAt];
}
