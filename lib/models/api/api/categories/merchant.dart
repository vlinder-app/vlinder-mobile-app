class Merchant {
  String name;
  String iconUrl;
  String color;

  Merchant({
    this.name,
    this.iconUrl,
    this.color,
  });

  Merchant copyWith({
    String name,
    String iconUrl,
    String color,
  }) =>
      Merchant(
        name: name ?? this.name,
        iconUrl: iconUrl ?? this.iconUrl,
        color: color ?? this.color,
      );

  factory Merchant.fromJson(Map<String, dynamic> json) => Merchant(
        name: json["name"] == null ? null : json["name"],
        iconUrl: json["iconUrl"] == null ? null : json["iconUrl"],
        color: json["color"] == null ? null : json["color"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "iconUrl": iconUrl == null ? null : iconUrl,
        "color": color == null ? null : color,
      };
}
