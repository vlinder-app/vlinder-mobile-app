import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/categories/period.dart';

class Income {
  PeriodType periodType;
  Amount currentPeriodAmount;

  Income({this.currentPeriodAmount, this.periodType});

  Income copyWith({
    Amount currentPeriodAmount,
    PeriodType periodType,
  }) =>
      Income(
          currentPeriodAmount: currentPeriodAmount ?? this.currentPeriodAmount,
          periodType: periodType ?? this.periodType);

  factory Income.fromJson(Map<String, dynamic> json) => Income(
    periodType: json["periodType"] == null
        ? null
        : periodTypeValues.map[json["periodType"]],
    currentPeriodAmount: json["currentPeriodAmount"] == null
        ? null
        : Amount.fromJson(json["currentPeriodAmount"]),
  );

  Map<String, dynamic> toJson() => {
    "periodType":
    periodType == null ? null : periodTypeValues.reverse[periodType],
    "currentPeriodAmount":
    currentPeriodAmount == null ? null : currentPeriodAmount.toJson(),
  };
}
