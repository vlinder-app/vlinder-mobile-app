import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/categories/period.dart';

class Savings {
  Amount totalAmount;
  Amount currentPeriodAmount;
  Amount previousPeriodAmount;
  PeriodType periodType;

  Savings({
    this.currentPeriodAmount,
    this.previousPeriodAmount,
    this.periodType,
    this.totalAmount,
  });

  Savings copyWith({
    Amount totalAmount,
    Amount currentPeriodAmount,
    Amount previousPeriodAmount,
    PeriodType periodType,
  }) =>
      Savings(
        periodType: periodType ?? this.periodType,
        totalAmount: totalAmount ?? this.totalAmount,
        currentPeriodAmount: currentPeriodAmount ?? this.currentPeriodAmount,
        previousPeriodAmount: previousPeriodAmount ?? this.previousPeriodAmount,
      );

  factory Savings.fromJson(Map<String, dynamic> json) => Savings(
    totalAmount: json["totalAmount"] == null
        ? null
        : Amount.fromJson(json["totalAmount"]),
    periodType: json["periodType"] == null
        ? null
        : periodTypeValues.map[json["periodType"]],
    currentPeriodAmount: json["currentPeriodAmount"] == null
        ? null
        : Amount.fromJson(json["currentPeriodAmount"]),
    previousPeriodAmount: json["previousPeriodAmount"] == null
        ? null
        : Amount.fromJson(json["previousPeriodAmount"]),
  );

  Map<String, dynamic> toJson() => {
    "totalAmount": totalAmount == null ? null : totalAmount.toJson(),
    "periodType":
    periodType == null ? null : periodTypeValues.reverse[periodType],
    "currentPeriodAmount":
    currentPeriodAmount == null ? null : currentPeriodAmount.toJson(),
    "previousPeriodAmount":
    previousPeriodAmount == null ? null : previousPeriodAmount.toJson(),
  };
}
