import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/balances/balances.dart';

class Fee {
  String item1;
  Amount item2;

  Fee({
    this.item1,
    this.item2,
  });

  Fee copyWith({
    String item1,
    Amount item2,
  }) =>
      Fee(
        item1: item1 ?? this.item1,
        item2: item2 ?? this.item2,
      );

  factory Fee.fromJson(Map<String, dynamic> json) => Fee(
        item1: json["item1"] == null ? null : json["item1"],
        item2: json["item2"] == null ? null : Amount.fromJson(json["item2"]),
      );

  Map<String, dynamic> toJson() => {
        "item1": item1 == null ? null : item1,
        "item2": item2 == null ? null : item2.toJson(),
      };
}
