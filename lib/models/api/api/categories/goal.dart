import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/balances/balances.dart';

class Goal {
  String name;
  Amount totalAmount;
  Amount savingsAmount;
  double percentCompeted;

  Goal({
    this.name,
    this.totalAmount,
    this.savingsAmount,
    this.percentCompeted,
  });

  Goal copyWith({
    String name,
    Amount totalAmount,
    Amount savingsAmount,
    double percentCompeted,
  }) =>
      Goal(
        name: name ?? this.name,
        totalAmount: totalAmount ?? this.totalAmount,
        savingsAmount: savingsAmount ?? this.savingsAmount,
        percentCompeted: percentCompeted ?? this.percentCompeted,
      );

  factory Goal.fromJson(Map<String, dynamic> json) => Goal(
        name: json["name"] == null ? null : json["name"],
        totalAmount: json["totalAmount"] == null
            ? null
            : Amount.fromJson(json["totalAmount"]),
        savingsAmount: json["savingsAmount"] == null
            ? null
            : Amount.fromJson(json["savingsAmount"]),
        percentCompeted:
            json["percentCompeted"] == null ? null : json["percentCompeted"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "totalAmount": totalAmount == null ? null : totalAmount.toJson(),
        "savingsAmount": savingsAmount == null ? null : savingsAmount.toJson(),
        "percentCompeted": percentCompeted == null ? null : percentCompeted,
      };
}
