import 'package:atoma_cash/models/api/api/categories/goals.dart';
import 'package:equatable/equatable.dart';

class SummaryGoals extends Equatable {
  final Goals personalGoals;
  final Goals communityGoals;

  SummaryGoals({this.personalGoals, this.communityGoals});

  @override
  List<Object> get props => [personalGoals, communityGoals];
}
