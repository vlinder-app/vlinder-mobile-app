import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/categories/period.dart';

class DateRangeCollections {
  List<DateRange> months;
  List<DateRange> days;
  List<DateRange> weeks;

  DateRangeCollections() {
    _initDays();
    _initWeeks();
    _initMonths();
  }

  void _initDays() {
    DateTime now = DateTime.now();

    days = List.generate(14, (index) {
      DateTime daysBefore = now.subtract(Duration(days: index));
      return DateRange(
          period: Period.dayOfMonth(daysBefore.day, daysBefore.month),
          periodType: PeriodType.TODAY);
    }, growable: true)
        .toList();
  }

  void _initWeeks() {
    DateTime now = DateTime.now();

    weeks = List.generate(6, (index) {
      return DateRange(
          period: Period.week(now.subtract(Duration(days: index * 7))),
          periodType: PeriodType.WEEK);
    }, growable: false);
  }

  void _initMonths() {
    DateTime now = DateTime.now();

    months = List.generate(12, (m) {
      return DateRange(
          period: Period.month(now.month - m), periodType: PeriodType.MONTH);
    });
    /*
    months = List.generate(now.month, (m) {
      return DateRangeItem(Period.month(m + 1), PeriodType.MONTH);
    }, growable: true)
        .reversed
        .toList();
    }
    */
  }
}
