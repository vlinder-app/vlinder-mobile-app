import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/balances/balances.dart';

class ExchangeRate {
  Amount base;
  Amount quote;

  ExchangeRate({
    this.base,
    this.quote,
  });

  ExchangeRate copyWith({
    Amount base,
    Amount quote,
  }) =>
      ExchangeRate(
        base: base ?? this.base,
        quote: quote ?? this.quote,
      );

  factory ExchangeRate.fromJson(Map<String, dynamic> json) => ExchangeRate(
        base: json["base"] == null ? null : Amount.fromJson(json["base"]),
        quote: json["quote"] == null ? null : Amount.fromJson(json["quote"]),
      );

  Map<String, dynamic> toJson() => {
        "base": base == null ? null : base.toJson(),
        "quote": quote == null ? null : quote.toJson(),
      };
}
