import 'package:atoma_cash/models/api/api/categories/period.dart';
import 'package:equatable/equatable.dart';
import 'package:intl/intl.dart';

class DateRange extends Equatable {
  static const _kDayPattern = 'd MMM';
  static const _kDayMonthYearPattern = 'd MMM y';

  final Period period;
  final PeriodType periodType;

  DateRange({this.period, this.periodType});

  @override
  List<Object> get props => [period, periodType];

  factory DateRange.month() =>
      DateRange(period: Period.currentMonth(), periodType: PeriodType.MONTH);

  factory DateRange.week() =>
      DateRange(period: Period.currentWeek(), periodType: PeriodType.WEEK);

  factory DateRange.day() =>
      DateRange(period: Period.today(), periodType: PeriodType.TODAY);

  factory DateRange.custom() =>
      DateRange(period: Period.currentMonth(), periodType: PeriodType.CUSTOM);

  @override
  String toString() {
    return 'DateRangeFilter{period: $period, periodType: $periodType}';
  }

  DateRange fromDateTime(DateTime dateTime) {
    DateTime now = DateTime.now();
    if (dateTime.year != now.year) {
      return DateRange(
          periodType: PeriodType.CUSTOM,
          period: Period(
              startDate: DateTime(dateTime.year, dateTime.month, dateTime.day),
              endDate: now));
    } else {
      return DateRange(
          periodType: this.periodType,
          period: _periodForDateTime(this.periodType, dateTime));
    }
  }

  DateRange copyWith({Period period, PeriodType periodType}) =>
      DateRange(
          period: period ?? this.period,
          periodType: periodType ?? this.periodType);

  String _formatByPattern(String pattern, DateTime dateTime) {
    return DateFormat(pattern).format(dateTime);
  }

  Period _periodForDateTime(PeriodType type, DateTime dateTime) {
    switch (type) {
      case PeriodType.TODAY:
        return Period.dayOfMonth(dateTime.day, dateTime.month);
      case PeriodType.WEEK:
        return Period.week(dateTime);
      case PeriodType.CUSTOM:
      case PeriodType.MONTH:
        return Period.month(dateTime.month);
    }
  }

  String _dayString(DateTime dateTime) =>
      _formatByPattern(_kDayPattern, dateTime);

  String _dayMonthYearString(DateTime dateTime) =>
      _formatByPattern(_kDayMonthYearPattern, dateTime);

  String _rangeString() =>
      "${_dayString(period.startDate)} - ${_dayString(period.endDate)}";

  String _rangeWithYearString() =>
      "${_dayMonthYearString(period.startDate)} - ${_dayMonthYearString(
          period.endDate)}";

  String rangeString() {
    switch (periodType) {
      case PeriodType.TODAY:
        return _dayString(period.startDate);
        break;
      case PeriodType.WEEK:
        return _rangeString();
        break;
      case PeriodType.MONTH:
        return DateFormat.MMMM().format(period.startDate);
        break;
      case PeriodType.WEEK:
        return _rangeString();
      case PeriodType.CUSTOM:
        return _rangeWithYearString();
      default:
        return "Undefined period";
    }
  }
}
