import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/categories/period.dart';
import 'package:equatable/equatable.dart';

class Budget extends Equatable {
  final Period period;
  final Amount limit;
  final Amount spending;
  final Amount amountLeft;
  final Amount amountOverExpense;
  final double carbonFootprint;

  Budget({
    this.period,
    this.limit,
    this.spending,
    this.amountLeft,
    this.amountOverExpense,
    this.carbonFootprint
  });

  double get spentPercentage => (spending.value * 100 / limit.value).abs();

  double get overExpensePercentage =>
      (amountOverExpense.value * 100 / limit.value).abs();

  Budget copyWith({
    Period period,
    Amount amount,
    Amount spending,
    Amount amountLeft,
    Amount amountOverExpense,
    double carbonFootprint
  }) =>
      Budget(
        carbonFootprint: carbonFootprint ?? this.carbonFootprint,
        period: period ?? this.period,
        limit: amount ?? this.limit,
        spending: spending ?? this.spending,
        amountLeft: amountLeft ?? this.amountLeft,
        amountOverExpense: amountOverExpense ?? this.amountOverExpense,
      );

  factory Budget.fromJson(Map<String, dynamic> json) => Budget(
    carbonFootprint: json["carbonFootprint"] == null
        ? null
        : json["carbonFootprint"].toDouble(),
    period: json["period"] == null ? null : Period.fromJson(json["period"]),
    limit: json["limit"] == null ? null : Amount.fromJson(json["limit"]),
    spending:
    json["spending"] == null ? null : Amount.fromJson(json["spending"]),
    amountLeft: json["amountLeft"] == null
        ? null
        : Amount.fromJson(json["amountLeft"]),
    amountOverExpense: json["amountOverExpense"] == null
        ? null
        : Amount.fromJson(json["amountOverExpense"]),
      );

  Map<String, dynamic> toJson() => {
    "carbonFootprint": carbonFootprint == null ? null : carbonFootprint,
        "period": period == null ? null : period.toJson(),
        "limit": limit == null ? null : limit.toJson(),
        "spending": spending == null ? null : spending.toJson(),
        "amountLeft": amountLeft == null ? null : amountLeft.toJson(),
        "amountOverExpense":
            amountOverExpense == null ? null : amountOverExpense.toJson(),
      };


  @override
  String toString() {
    return 'Budget{period: $period, limit: $limit, spending: $spending, amountLeft: $amountLeft, amountOverExpense: $amountOverExpense, carbonFootprint: $carbonFootprint}';
  }

  @override
  List<Object> get props =>
      [period, limit, spending, amountLeft, amountOverExpense, carbonFootprint];
}
