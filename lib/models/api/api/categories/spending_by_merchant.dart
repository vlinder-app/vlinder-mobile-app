import 'package:atoma_cash/models/api/api/transactions/transaction.dart';

import 'budget.dart';
import 'merchant.dart';

class SpendingByMerchant {
  Merchant merchant;
  Budget budget;
  List<Transaction> transactions;

  SpendingByMerchant({
    this.merchant,
    this.budget,
    this.transactions,
  });

  SpendingByMerchant copyWith({
    Merchant merchant,
    Budget budget,
    List<Transaction> transactions,
  }) =>
      SpendingByMerchant(
        merchant: merchant ?? this.merchant,
        budget: budget ?? this.budget,
        transactions: transactions ?? this.transactions,
      );

  factory SpendingByMerchant.fromJson(Map<String, dynamic> json) =>
      SpendingByMerchant(
        merchant: json["merchant"] == null
            ? null
            : Merchant.fromJson(json["merchant"]),
        budget: json["budget"] == null ? null : Budget.fromJson(json["budget"]),
        transactions: json["transactions"] == null
            ? null
            : List<Transaction>.from(
                json["transactions"].map((x) => Transaction.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "merchant": merchant == null ? null : merchant.toJson(),
        "budget": budget == null ? null : budget.toJson(),
        "transactions": transactions == null
            ? null
            : List<dynamic>.from(transactions.map((x) => x.toJson())),
      };
}
