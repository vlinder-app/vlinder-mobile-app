import 'dart:convert';

import 'package:atoma_cash/models/api/api/categories/goal.dart';

Goals goalsFromJson(String str) => Goals.fromJson(json.decode(str));

String goalsToJson(Goals data) => json.encode(data.toJson());

class Goals {
  List<Goal> goals;
  int goalsCount;
  double percentCompleted;

  Goals({
    this.goals,
    this.goalsCount,
    this.percentCompleted,
  });

  Goals copyWith({
    List<Goal> goals,
    int goalsCount,
    double percentCompleted,
  }) =>
      Goals(
        goals: goals ?? this.goals,
        goalsCount: goalsCount ?? this.goalsCount,
        percentCompleted: percentCompleted ?? this.percentCompleted,
      );

  factory Goals.fromJson(Map<String, dynamic> json) => Goals(
        goals: json["goals"] == null
            ? null
            : List<Goal>.from(json["goals"].map((x) => Goal.fromJson(x))),
        goalsCount:
            json["goalsCount"] == null ? null : json["goalsCount"].toInt(),
        percentCompleted: json["percentCompleted"] == null
            ? null
            : json["percentCompleted"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "goals": goals == null
            ? null
            : List<dynamic>.from(goals.map((x) => x.toJson())),
        "goalsCount": goalsCount == null ? null : goalsCount,
        "percentCompleted": percentCompleted == null ? null : percentCompleted,
      };
}
