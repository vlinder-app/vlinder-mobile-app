import 'package:atoma_cash/extensions/enum.dart';

enum EcoRating { AWFUL, POOR, NORMAL, GOOD, EXCELLENT }

final ecoRatingValues = EnumValues({
  "Awful": EcoRating.AWFUL,
  "Poor": EcoRating.POOR,
  "Normal": EcoRating.NORMAL,
  "Good": EcoRating.GOOD,
  "Excellent": EcoRating.EXCELLENT
});
