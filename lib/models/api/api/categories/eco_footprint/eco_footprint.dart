import 'dart:convert';

import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_rating.dart';
import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_trend.dart';

EcoFootprint ecoFootprintFromJson(String str) =>
    EcoFootprint.fromJson(json.decode(str));

String ecoFootprintToJson(EcoFootprint data) => json.encode(data.toJson());

class EcoFootprint {
  EcoRating rating;
  EcoTrend trend;
  double carbonFootprint;

  EcoFootprint({
    this.rating,
    this.trend,
    this.carbonFootprint,
  });

  EcoFootprint copyWith({
    EcoRating rating,
    EcoTrend trend,
    double carbonFootprint,
  }) =>
      EcoFootprint(
        rating: rating ?? this.rating,
        trend: trend ?? this.trend,
        carbonFootprint: carbonFootprint ?? this.carbonFootprint,
      );

  factory EcoFootprint.fromJson(Map<String, dynamic> json) => EcoFootprint(
        rating:
            json["rating"] == null ? null : ecoRatingValues.map[json["rating"]],
        trend: json["trend"] == null ? null : ecoTrendValues.map[json["trend"]],
        carbonFootprint: json["carbonFootprint"] == null
            ? null
            : json["carbonFootprint"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "rating": rating == null ? null : ecoRatingValues.reverse[rating],
        "trend": trend == null ? null : ecoTrendValues.reverse[trend],
        "carbonFootprint": carbonFootprint == null ? null : carbonFootprint,
      };
}
