import 'package:atoma_cash/models/api/api/geolocation/countries.dart';

class EcoFootprintInfo {
  final CountryEcoFootprintInfo countryFootprintInfo;
  final CarbonFootprint worldCarbonFootprint;

  EcoFootprintInfo({this.countryFootprintInfo, this.worldCarbonFootprint});

  EcoFootprintInfo copyWith(
          {CountryEcoFootprintInfo countryFootprintInfo,
          CarbonFootprint worldCarbonFootprint}) =>
      EcoFootprintInfo(
          countryFootprintInfo:
              countryFootprintInfo ?? this.countryFootprintInfo,
          worldCarbonFootprint:
              worldCarbonFootprint ?? this.worldCarbonFootprint);

  factory EcoFootprintInfo.fromJson(Map<String, dynamic> json) =>
      EcoFootprintInfo(
        countryFootprintInfo:
            CountryEcoFootprintInfo.fromJson(json["countryFootprint"]),
        worldCarbonFootprint: CarbonFootprint.fromJson(json["worldFootprint"]),
      );

  Map<String, dynamic> toJson() => {
        "countryFootprintInfo":
            countryFootprintInfo == null ? null : countryFootprintInfo.toJson(),
        "worldCarbonFootprint":
            worldCarbonFootprint == null ? null : worldCarbonFootprint.toJson(),
      };

  @override
  String toString() {
    return 'EcoFootprintInfo{countryFootprintInfo: $countryFootprintInfo, worldCarbonFootprint: $worldCarbonFootprint}';
  }
}

class CountryEcoFootprintInfo {
  final Country country;
  final CarbonFootprint carbonFootprint;

  CountryEcoFootprintInfo({this.country, this.carbonFootprint});

  CountryEcoFootprintInfo copyWith(
          {Country country, CarbonFootprint carbonFootprint}) =>
      CountryEcoFootprintInfo(
          country: country ?? this.country,
          carbonFootprint: carbonFootprint ?? this.carbonFootprint);

  factory CountryEcoFootprintInfo.fromJson(Map<String, dynamic> json) =>
      CountryEcoFootprintInfo(
          country: Country.fromJson(json["country"]),
          carbonFootprint: CarbonFootprint.fromJson(json["carbonFootprint"]));

  Map<String, dynamic> toJson() => {
        "country": country == null ? null : country.toJson(),
        "carbonFootprint":
            carbonFootprint == null ? null : carbonFootprint.toJson()
      };

  @override
  String toString() {
    return 'CountryEcoFootprintInfo{country: $country, carbonFootprint: $carbonFootprint}';
  }
}

class CarbonFootprint {
  final double value;

  CarbonFootprint({this.value});

  CarbonFootprint copyWith({double value}) =>
      CarbonFootprint(value: value ?? this.value);

  factory CarbonFootprint.fromJson(Map<String, dynamic> json) =>
      CarbonFootprint(
        value: json["value"] == null ? null : json["value"].toDouble(),
      );

  Map<String, dynamic> toJson() => {
        "value": value == null ? null : value,
      };

  @override
  String toString() {
    return 'CarbonFootprint{value: $value}';
  }
}
