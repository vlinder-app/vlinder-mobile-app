import 'package:atoma_cash/extensions/enum.dart';

enum EcoTrend { DOWNWARD, NOCHANGES, UPWARD }

final ecoTrendValues = EnumValues({
  "Downward": EcoTrend.DOWNWARD,
  "NoChanges": EcoTrend.NOCHANGES,
  "Upward": EcoTrend.UPWARD
});
