import 'package:atoma_cash/models/api/api/categories/budget.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/categories/spending_by_merchant.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';

class SpendingByCategory {
  Category category;
  Budget budget;
  List<SpendingByMerchant> spendingByMerchants;
  List<Transaction> transactions;

  SpendingByCategory({
    this.category,
    this.budget,
    this.spendingByMerchants,
    this.transactions,
  });

  SpendingByCategory copyWith({
    Category category,
    Budget budget,
    List<SpendingByMerchant> spendingByMerchants,
    List<Transaction> transactions,
  }) =>
      SpendingByCategory(
        category: category ?? this.category,
        budget: budget ?? this.budget,
        spendingByMerchants: spendingByMerchants ?? this.spendingByMerchants,
        transactions: transactions ?? this.transactions,
      );

  factory SpendingByCategory.fromJson(Map<String, dynamic> json) =>
      SpendingByCategory(
        category: json["category"] == null
            ? null
            : Category.fromJson(json["category"]),
        budget: json["budget"] == null ? null : Budget.fromJson(json["budget"]),
        spendingByMerchants: json["spendingByMerchants"] == null
            ? null
            : List<SpendingByMerchant>.from(json["spendingByMerchants"]
                .map((x) => SpendingByMerchant.fromJson(x))),
        transactions: json["transactions"] == null
            ? null
            : List<Transaction>.from(
                json["transactions"].map((x) => Transaction.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "category": category == null ? null : category.toJson(),
        "budget": budget == null ? null : budget.toJson(),
        "spendingByMerchants": spendingByMerchants == null
            ? null
            : List<dynamic>.from(spendingByMerchants.map((x) => x.toJson())),
        "transactions": transactions == null
            ? null
            : List<dynamic>.from(transactions.map((x) => x.toJson())),
      };
}
