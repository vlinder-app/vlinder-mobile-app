import 'package:atoma_cash/extensions/enum.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:equatable/equatable.dart';

enum PeriodType { TODAY, WEEK, MONTH, CUSTOM }

final periodTypeValues = EnumValues({
  "Today": PeriodType.TODAY,
  "Week": PeriodType.WEEK,
  "Month": PeriodType.MONTH,
  "Custom": PeriodType.CUSTOM,
});

class Period extends Equatable {
  final DateTime startDate;
  final DateTime endDate;

  Period({
    this.startDate,
    this.endDate,
  });

  Period copyWith({
    DateTime startDate,
    DateTime endDate,
  }) =>
      Period(
        startDate: startDate ?? this.startDate,
        endDate: endDate ?? this.endDate,
      );

  factory Period.currentWeek() {
    return Period.week(DateTime.now());
  }

  factory Period.currentMonth() {
    return Period.month(DateTime
        .now()
        .month);
  }

  factory Period.today() {
    return Period.day(DateTime
        .now()
        .day);
  }

  factory Period.month(int month) {
    DateTime now = new DateTime.now();
    return Period(
        startDate: DateTime(now.year, month, 1),
        endDate: DateTime(now.year, month + 1, 0));
  }

  factory Period.day(int day) =>
      Period.dayOfMonth(day, DateTime
          .now()
          .month);

  factory Period.dayOfMonth(int day, int month) {
    DateTime daily = DateTime(DateTime
        .now()
        .year, month, day);
    return Period(startDate: daily, endDate: DateHelper.endOfADay(daily));
  }

  factory Period.week(DateTime dateTime) {
    return Period(
        startDate: DateHelper.weekStart(dateTime),
        endDate: DateHelper.weekEnd(dateTime));
  }

  factory Period.fromJson(Map<String, dynamic> json) => Period(
    startDate: json["startDate"] == null
        ? null
        : DateTime.parse(json["startDate"]),
    endDate:
    json["endDate"] == null ? null : DateTime.parse(json["endDate"]),
  );

  Map<String, dynamic> toJson() => {
    "startDate": startDate == null ? null : startDate.toIso8601String(),
    "endDate": endDate == null ? null : endDate.toIso8601String(),
  };

  @override
  String toString() {
    return 'Period{startDate: $startDate, endDate: $endDate}';
  }

  @override
  List<Object> get props => [startDate, endDate];
}
