import 'package:atoma_cash/extensions/enum.dart';
import 'package:flutter/material.dart';

enum TransactionsType { INCOME, EXPENSES }

final transactionsTypeValues = EnumValues(
    {"Income": TransactionsType.INCOME, "Expense": TransactionsType.EXPENSES});

extension TransactionTypeLocalization on TransactionsType {
  String toLocalizedString(BuildContext context) {
    switch (this) {
      case TransactionsType.INCOME:
        return "";
      case TransactionsType.EXPENSES:
        return "−";
    }
  }
}
