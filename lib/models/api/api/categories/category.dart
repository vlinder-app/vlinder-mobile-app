import 'package:atoma_cash/models/api/api/categories/transactions_type.dart';
import 'package:equatable/equatable.dart';


class Category extends Equatable {
  final String id;
  final String name;
  final String parentId;
  final String iconUrl;
  final String color;
  final TransactionsType transactionsType;

  Category({this.id,
    this.name,
    this.parentId,
    this.iconUrl,
    this.color,
    this.transactionsType});

  Category copyWith({String id,
    String name,
    String parentId,
    String iconUrl,
    String color,
    TransactionsType transactionsType}) =>
      Category(
          id: id ?? this.id,
          name: name ?? this.name,
          parentId: parentId ?? this.parentId,
          iconUrl: iconUrl ?? this.iconUrl,
          color: color ?? this.color,
          transactionsType: transactionsType ?? this.transactionsType);

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    id: json["id"] == null ? null : json["id"],
    name: json["name"] == null ? null : json["name"],
    parentId: json["parentId"] == null ? null : json["parentId"],
    iconUrl: json["iconUrl"] == null ? null : json["iconUrl"],
    color: json["color"] == null ? null : json["color"],
    transactionsType: json["transactionsType"] == null
        ? null
        : transactionsTypeValues.map[json["transactionsType"]],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "name": name == null ? null : name,
    "parentId": parentId == null ? null : parentId,
    "iconUrl": iconUrl == null ? null : iconUrl,
    "color": color == null ? null : color,
    "transactionsType": transactionsType == null
        ? null
        : transactionsTypeValues.reverse[transactionsType],
  };

  @override
  List<Object> get props =>
      [id, name, parentId, iconUrl, color, transactionsType];
}
