import 'dart:convert';

import 'package:atoma_cash/models/api/api/categories/category.dart';

Categories categoriesFromJson(String str) =>
    Categories.fromJson(json.decode(str));

String categoriesToJson(Categories data) => json.encode(data.toJson());

class Categories {
  List<Category> categories;

  Categories({
    this.categories,
  });

  Categories copyWith({
    List<Category> categories,
  }) =>
      Categories(
        categories: categories ?? this.categories,
      );

  factory Categories.fromJson(Map<String, dynamic> json) => Categories(
        categories: json["categories"] == null
            ? null
            : List<Category>.from(
                json["categories"].map((x) => Category.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "categories": categories == null
            ? null
            : List<dynamic>.from(categories.map((x) => x.toJson())),
      };
}
