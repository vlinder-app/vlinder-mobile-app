import 'dart:convert';

import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/balances/balances.dart';

Accounts accountsFromJson(String str) => Accounts.fromJson(json.decode(str));

String accountsToJson(Accounts data) => json.encode(data.toJson());

class Accounts {
  final List<Account> accounts;
  final Amount totalBaseCurrencyBalance;

  Accounts({
    this.accounts,
    this.totalBaseCurrencyBalance,
  });

  Accounts copyWith({
    List<Account> accounts,
    Amount totalBaseCurrencyBalance,
  }) =>
      Accounts(
        accounts: accounts ?? this.accounts,
        totalBaseCurrencyBalance:
            totalBaseCurrencyBalance ?? this.totalBaseCurrencyBalance,
      );

  factory Accounts.fromJson(Map<String, dynamic> json) => Accounts(
        accounts: json['accounts'] == null
            ? null
            : List<Account>.from(
                json['accounts'].map((x) => Account.fromJson(x))),
        totalBaseCurrencyBalance: json['totalBaseCurrencyBalance'] == null
            ? null
            : Amount.fromJson(json['totalBaseCurrencyBalance']),
      );

  Map<String, dynamic> toJson() => {
        'accounts': accounts == null
            ? null
            : List<dynamic>.from(accounts.map((x) => x.toJson())),
        'totalBaseCurrencyBalance': totalBaseCurrencyBalance == null
            ? null
            : totalBaseCurrencyBalance.toJson(),
      };
}

class Account {
  final String id;
  final String publicId;
  final String name;
  final String type;
  final String issuerName;
  final String issuerIconUrl;
  final Amount balance;
  final Amount baseCurrencyBalance;
  final int sortOrder;
  final String financeProviderType;
  final DateTime refreshedAt;
  final String userId;
  final List<String> cards;
  final String cardType;
  final DateTime cardExpiresOn;

  String get formattedBalanceInBase {
    if (baseCurrencyBalance != null) {
      return baseCurrencyBalance.currencyFormatted();
    } else {
      return '';
    }
  }

  Account(
      {this.id,
      this.publicId,
      this.name,
      this.type,
      this.issuerName,
      this.issuerIconUrl,
      this.balance,
      this.baseCurrencyBalance,
      this.sortOrder,
      this.financeProviderType,
      this.refreshedAt,
      this.userId,
      this.cards,
      this.cardType,
      this.cardExpiresOn});

  Account copyWith({
    String id,
    String publicId,
    String name,
    String type,
    String issuerName,
    String issuerIconUrl,
    Amount balance,
    Amount baseCurrencyBalance,
    int sortOrder,
    String financeProviderType,
    DateTime refreshedAt,
    String userId,
    List<String> cards,
    String cardType,
    String cardExpiresOn,
  }) =>
      Account(
          id: id ?? this.id,
          publicId: publicId ?? this.publicId,
          name: name ?? this.name,
          type: type ?? this.type,
          issuerName: issuerName ?? this.issuerName,
          balance: balance ?? this.balance,
          baseCurrencyBalance: baseCurrencyBalance ?? this.baseCurrencyBalance,
          sortOrder: sortOrder ?? this.sortOrder,
          financeProviderType: financeProviderType ?? this.financeProviderType,
          refreshedAt: refreshedAt ?? this.refreshedAt,
          userId: userId ?? this.userId,
          cards: cards ?? this.cards,
          cardType: cardType ?? this.cardType,
          cardExpiresOn: cardExpiresOn ?? this.cardExpiresOn);

  factory Account.fromJson(Map<String, dynamic> json) => Account(
        id: json['id'] == null ? null : json['id'],
        publicId: json['publicId'] == null ? null : json['publicId'],
        name: json['name'] == null ? null : json['name'],
        type: json['type'] == null ? null : json['type'],
        issuerName: json['issuerName'] == null ? null : json['issuerName'],
        issuerIconUrl:
            json['issuerIconUrl'] == null ? null : json['issuerIconUrl'],
        balance:
            json['balance'] == null ? null : Amount.fromJson(json['balance']),
        baseCurrencyBalance: json['baseCurrencyBalance'] == null
            ? null
            : Amount.fromJson(json['baseCurrencyBalance']),
        sortOrder: json['sortOrder'] == null ? null : json['balance'].toInt(),
        financeProviderType: json['financeProviderType'] == null
            ? null
            : json['financeProviderType'],
        refreshedAt: json['refreshedAt'] == null
            ? null
            : DateTime.parse(json['refreshedAt']),
        userId: json['userId'] == null ? null : json['userId'],
        cards: json['cards'] == null ? null : List<String>.from(json['cards']),
        cardType: json['cardType'] == null ? null : json['cardType'],
        cardExpiresOn: json['cardExpiresOn'] == null
            ? null
            : DateTime.parse(json['cardExpiresOn']),
      );

  Map<String, dynamic> toJson() => {
        'id': id == null ? null : id,
        'publicId': publicId == null ? null : publicId,
        'name': name == null ? null : name,
        'type': type == null ? null : type,
        'issuerName': issuerName == null ? null : issuerName,
        'issuerIconUrl': issuerIconUrl == null ? null : issuerIconUrl,
        'balance': balance == null ? null : balance.toJson(),
        'baseCurrencyBalance':
            baseCurrencyBalance == null ? null : baseCurrencyBalance.toJson(),
        'sortOrder': sortOrder == null ? null : sortOrder,
        'financeProviderType':
            financeProviderType == null ? null : financeProviderType,
        'refreshedAt':
            refreshedAt == null ? null : refreshedAt.toIso8601String(),
        'userId': userId == null ? null : userId,
        'cards': cards == null ? null : cards,
        'cardType': cardType == null ? null : cardType,
        'cardExpiresOn':
            cardExpiresOn == null ? null : cardExpiresOn.toIso8601String(),
      };
}
