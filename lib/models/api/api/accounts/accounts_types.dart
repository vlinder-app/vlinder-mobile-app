import 'dart:convert';

AccountsTypes accountsTypesFromJson(String str) =>
    AccountsTypes.fromJson(json.decode(str));

String accountsTypesToJson(AccountsTypes data) => json.encode(data.toJson());

class AccountsTypes {
  AccountsTypes({
    this.types,
  });

  final List<AccountType> types;

  factory AccountsTypes.fromJson(Map<String, dynamic> json) => AccountsTypes(
        types: json["types"] == null
            ? null
            : List<AccountType>.from(
                json["types"].map((x) => AccountType.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "types": types == null
            ? null
            : List<dynamic>.from(types.map((x) => x.toJson())),
      };
}

class AccountType {
  AccountType({
    this.type,
    this.name,
    this.iconUrl,
  });

  final String type;
  final String name;
  final String iconUrl;

  factory AccountType.fromJson(Map<String, dynamic> json) => AccountType(
        type: json["type"] == null ? null : json["type"],
        name: json["name"] == null ? null : json["name"],
        iconUrl: json["iconUrl"] == null ? null : json["iconUrl"],
      );

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "name": name == null ? null : name,
        "iconUrl": iconUrl == null ? null : iconUrl,
      };
}
