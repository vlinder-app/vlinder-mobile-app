import 'package:atoma_cash/extensions/enum.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/cupertino.dart';

enum TransactionSourceType { BANK_ACCOUNT, MANUAL, BITCOIN_WALLET }

final transactionSourceTypeValues = EnumValues({
  "BankAccount": TransactionSourceType.BANK_ACCOUNT,
  "Manual": TransactionSourceType.MANUAL,
  "BitcoinWallet": TransactionSourceType.BITCOIN_WALLET,
});

extension TransactionTypeLocalization on TransactionSourceType {
  String toLocalizedString(BuildContext context) {
    switch (this) {
      case TransactionSourceType.BANK_ACCOUNT:
        return Localization.of(context).transactionTypeBank;
      case TransactionSourceType.MANUAL:
        return Localization
            .of(context)
            .transactionTypeBank;
      case TransactionSourceType.BITCOIN_WALLET:
        return Localization.of(context).legacyTransactionTypeBitcoinWallet;
      default:
        return "undefined transaction type";
    }
  }
}
