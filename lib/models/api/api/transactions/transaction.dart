// To parse this JSON data, do
//
//     final transaction = transactionFromJson(jsonString);

import 'dart:convert';

import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/categories/exchange_rate.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction_source_type.dart';
import 'package:atoma_cash/resources/config/palettes.dart';
import 'package:equatable/equatable.dart';

List<Transaction> transactionsFromJson(List<dynamic> transactions) =>
    List<Transaction>.from(transactions.map((x) => Transaction.fromJson(x)));

List<Transaction> transactionsFromJsonString(String str) =>
    List<Transaction>.from(
        json.decode(str).map((x) => Transaction.fromJson(x)));

String transactionToJson(List<Transaction> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

//[ BankAccount, Manual, BitcoinWallet ]
class Transaction extends Equatable {
  final TransactionSourceType sourceType;
  final String id;
  final Amount amount;
  final Amount chargedAmount;
  final ExchangeRate exchangeRate;
  final String accountId;
  final Counterparty counterparty;
  final List<Category> categories;
  final DateTime bookedAt;
  final DateTime settledAt;
  final String comment;
  final double carbonFootprint;
  final String receiptImageId;
  final Amount baseCurrencyAmount;
  final bool failedImageRequest;

  Transaction(
      {this.sourceType,
      this.id,
      this.amount,
      this.chargedAmount,
      this.exchangeRate,
      this.accountId,
      this.counterparty,
      this.categories,
      this.bookedAt,
      this.settledAt,
      this.comment,
      this.carbonFootprint,
      this.receiptImageId,
      this.baseCurrencyAmount,
      this.failedImageRequest});

  Transaction resetFootprint() => Transaction(
      sourceType: this.sourceType,
      id: this.id,
      amount: this.amount,
      chargedAmount: this.chargedAmount,
      exchangeRate: this.exchangeRate,
      accountId: this.accountId,
      counterparty: this.counterparty,
      categories: this.categories,
      bookedAt: this.bookedAt,
      settledAt: this.settledAt,
      comment: this.comment,
      carbonFootprint: null,
      failedImageRequest: this.failedImageRequest,
      receiptImageId: this.receiptImageId,
      baseCurrencyAmount: this.baseCurrencyAmount);

  Transaction copyWith({
    TransactionSourceType sourceType,
    String id,
    Amount amount,
    Amount chargedAmount,
    ExchangeRate exchangeRate,
    String accountId,
    Counterparty counterparty,
    List<Category> categories,
    DateTime bookedAt,
    DateTime settledAt,
    String comment,
    double carbonFootprint,
    String receiptImageId,
    bool failedImageRequest,
    Amount baseCurrencyAmount,
  }) =>
      Transaction(
        receiptImageId: receiptImageId ?? this.receiptImageId,
        sourceType: sourceType ?? this.sourceType,
        id: id ?? this.id,
        amount: amount ?? this.amount,
        chargedAmount: chargedAmount ?? this.chargedAmount,
        exchangeRate: exchangeRate ?? this.exchangeRate,
        accountId: accountId ?? this.accountId,
        counterparty: counterparty ?? this.counterparty,
        categories: categories ?? this.categories,
        bookedAt: bookedAt ?? this.bookedAt,
        settledAt: settledAt ?? this.settledAt,
        comment: comment ?? this.comment,
        failedImageRequest: failedImageRequest ?? this.failedImageRequest,
        carbonFootprint: carbonFootprint ?? this.carbonFootprint,
        baseCurrencyAmount: baseCurrencyAmount ?? this.baseCurrencyAmount,
      );

  Transaction clearReceiptImageId() => Transaction(
      receiptImageId: null,
      sourceType: sourceType ?? this.sourceType,
      id: id ?? this.id,
      baseCurrencyAmount: baseCurrencyAmount ?? this.baseCurrencyAmount,
      amount: amount ?? this.amount,
      chargedAmount: chargedAmount ?? this.chargedAmount,
      exchangeRate: exchangeRate ?? this.exchangeRate,
      accountId: accountId ?? this.accountId,
      counterparty: counterparty ?? this.counterparty,
      categories: categories ?? this.categories,
      bookedAt: bookedAt ?? this.bookedAt,
      settledAt: settledAt ?? this.settledAt,
      comment: comment ?? this.comment,
      carbonFootprint: carbonFootprint ?? this.carbonFootprint,
      failedImageRequest: false);

  factory Transaction.fromJson(Map<String, dynamic> json) => Transaction(
      receiptImageId:
          json["receiptImageId"] == null ? null : json["receiptImageId"],
      sourceType: json["sourceType"] == null
          ? null
          : transactionSourceTypeValues.map[json["sourceType"]],
      id: json["id"] == null ? null : json["id"],
      amount: json["amount"] == null ? null : Amount.fromJson(json["amount"]),
      chargedAmount: json["chargedAmount"] == null
          ? null
          : Amount.fromJson(json["chargedAmount"]),
      exchangeRate: json["exchangeRate"] == null
          ? null
          : ExchangeRate.fromJson(json["exchangeRate"]),
      accountId: json["accountId"] == null ? null : json["accountId"],
      counterparty: json["counterparty"] == null
          ? null
          : Counterparty.fromJson(json["counterparty"]),
      categories: json["categories"] == null
          ? null
          : List<Category>.from(
              json["categories"].map((x) => Category.fromJson(x))),
      bookedAt:
          json["bookedAt"] == null ? null : DateTime.parse(json["bookedAt"]),
      settledAt:
          json["settledAt"] == null ? null : DateTime.parse(json["settledAt"]),
      comment: json["comment"] == null ? null : json["comment"],
      carbonFootprint:
          json["carbonFootprint"] == null ? null : json["carbonFootprint"],
      baseCurrencyAmount: json["baseCurrencyAmount"] == null
          ? null
          : Amount.fromJson(json["baseCurrencyAmount"]));

  factory Transaction.empty() => Transaction(
        sourceType: TransactionSourceType.MANUAL,
        amount: Amount(),
        settledAt: DateTime.now(),
        chargedAmount: Amount(),
        failedImageRequest: false,
        counterparty: Counterparty(),
        categories: [],
      );

  Map<String, dynamic> toJson() => {
        "receiptImageId": receiptImageId == null ? null : receiptImageId,
        "sourceType": sourceType == null
            ? null
            : transactionSourceTypeValues.reverse[sourceType],
        "id": id == null ? null : id,
        "amount": amount == null ? null : amount.toJson(),
        "chargedAmount": chargedAmount == null ? null : chargedAmount.toJson(),
        "exchangeRate": exchangeRate == null ? null : exchangeRate.toJson(),
        "accountId": accountId == null ? null : accountId,
        "counterparty": counterparty == null ? null : counterparty.toJson(),
        "categories": categories == null
            ? null
            : List<dynamic>.from(categories.map((x) => x.toJson())),
        "bookedAt": bookedAt == null ? null : bookedAt.toIso8601String(),
        "settledAt": settledAt == null ? null : settledAt.toIso8601String(),
        "comment": comment == null ? null : comment,
        "carbonFootprint": carbonFootprint == null ? null : carbonFootprint,
        "baseCurrencyAmount":
            baseCurrencyAmount == null ? null : baseCurrencyAmount,
      };

  @override
  String toString() {
    return 'Transaction{sourceType: $sourceType, id: $id, amount: $amount, chargedAmount: $chargedAmount, exchangeRate: $exchangeRate, accountId: $accountId, counterparty: $counterparty, categories: $categories, bookedAt: $bookedAt, settledAt: $settledAt, comment: $comment, carbonFootprint: $carbonFootprint, baseCurrencyAmount: $baseCurrencyAmount}';
  }

  @override
  List<Object> get props => [
        sourceType,
        id,
        amount,
        chargedAmount,
        exchangeRate,
        accountId,
        counterparty,
        categories,
        bookedAt,
        settledAt,
        comment,
        carbonFootprint,
        receiptImageId,
        baseCurrencyAmount,
        failedImageRequest
      ];

  String get baseOrMainAmountFormatted {
    if (baseCurrencyAmount != null) {
      return baseCurrencyAmount.currencyFormatted();
    } else {
      return amount.currencyFormatted();
    }
  }

  String get formattedTransactionAmountInBase {
    if (baseCurrencyAmount != null) {
      return baseCurrencyAmount.currencyFormatted();
    } else {
      return '...';
    }
  }
}

class Counterparty extends Equatable {
  final String name;
  final String iconUrl;
  final String location;

  Counterparty({
    this.name,
    iconUrl,
    this.location,
  }) : this.iconUrl = iconUrl ??
            (name == null
                ? null
                : "https://eu.ui-avatars.com/api/?name=$name&background=${Palettes.runtimeBackgroundPalette[name.hashCode % Palettes.runtimeBackgroundPalette.length].value.toRadixString(16).substring(2)}&color=fff&rounded=true&bold=true&font-size=0.39&format=png&size=128");

  Counterparty copyWith({
    String name,
    String iconUrl,
    String location,
  }) =>
      Counterparty(
        name: name ?? this.name,
        iconUrl: iconUrl ?? this.iconUrl,
        location: location ?? this.location,
      );

  Counterparty resetName() =>
      Counterparty(iconUrl: this.iconUrl, location: this.location);

  factory Counterparty.fromJson(Map<String, dynamic> json) => Counterparty(
        name: json["name"] == null ? null : json["name"],
        iconUrl: json["iconUrl"] == null ? null : json["iconUrl"],
        location: json["location"] == null ? null : json["location"],
      );

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "iconUrl": iconUrl == null ? null : iconUrl,
        "location": location == null ? null : location,
      };

  @override
  String toString() {
    return 'Counterparty{name: $name, iconUrl: $iconUrl, location: $location}';
  }

  @override
  List<Object> get props => [name, iconUrl, location];
}
