import 'dart:convert';

import 'package:atoma_cash/models/api/api/transactions/transaction.dart';

Transactions transactionsFromJson(String str) =>
    Transactions.fromJson(json.decode(str));

String transactionsToJson(Transactions data) => json.encode(data.toJson());

class Transactions {
  List<Transaction> transactions;

  Transactions({
    this.transactions,
  });

  Transactions copyWith({
    List<Transaction> transactions,
  }) =>
      Transactions(
        transactions: transactions ?? this.transactions,
      );

  factory Transactions.fromJson(Map<String, dynamic> json) => Transactions(
        transactions: json["transactions"] == null
            ? null
            : List<Transaction>.from(
                json["transactions"].map((x) => Transaction.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "transactions": transactions == null
            ? null
            : List<dynamic>.from(transactions.map((x) => x.toJson())),
      };
}
