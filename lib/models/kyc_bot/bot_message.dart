import 'dart:convert';

BotMessage botMessageFromJson(String str) =>
    BotMessage.fromJson(json.decode(str));

String botMessageToJson(BotMessage data) => json.encode(data.toJson());

class BotMessage {
  bool close;

  BotMessage({
    this.close,
  });

  factory BotMessage.fromJson(Map<String, dynamic> json) => BotMessage(
    close: json["close"] == null ? null : json["close"],
  );

  Map<String, dynamic> toJson() => {
    "close": close == null ? null : close,
  };
}
