import 'package:atoma_cash/extensions/enum.dart';

class Scope<String> extends Enum<String> {
  static final Scope atomaCashApi = Scope._internal("atoma.cash.api");
  static final Scope openid = Scope._internal("openid");
  static final Scope profile = Scope._internal("profile");
  static final Scope offlineAccess = Scope._internal("offline_access");

  Scope._internal(String value) : super(value);
}
