class JwtTokenData {
  static const hasPasscodeKey = 'has_passcode';
  static const isEmailVerifiedKey = 'email_verified';
  static const emailKey = 'email';
  static const externalIdKey = "external_id";

  final bool hasPasscode;
  final bool isEmailVerified;
  final String email;
  final String externalId;

  JwtTokenData(
      {this.hasPasscode, this.isEmailVerified, this.email, this.externalId});

  factory JwtTokenData.fromJson(Map<String, dynamic> json) {
    return JwtTokenData(
        hasPasscode: json[hasPasscodeKey],
        isEmailVerified: json[isEmailVerifiedKey],
        externalId: json[externalIdKey],
        email: json[emailKey]);
  }

  @override
  String toString() {
    return 'JwtTokenData{hasPasscode: $hasPasscode, isEmailVerified: $isEmailVerified, email: $email, externalId: $externalId}';
  }

  Map<String, dynamic> toJson() =>
      {
        hasPasscodeKey: hasPasscode,
        isEmailVerifiedKey: isEmailVerified,
        emailKey: email,
        externalIdKey: externalId
      };
}
