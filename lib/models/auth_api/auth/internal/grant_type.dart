import 'package:atoma_cash/extensions/enum.dart';

class GrantType<String> extends Enum<String> {
  static final GrantType refreshToken = GrantType._internal("refresh_token");
  static final GrantType authentication = GrantType._internal("authentication");

  GrantType._internal(String value) : super(value);
}
