
class EmailVerificationResponse {
  final String verifyToken;
  final String authority;
  final List<String> parameters;

  EmailVerificationResponse({
    this.verifyToken,
    this.authority,
    this.parameters,
  });

  factory EmailVerificationResponse.fromJson(Map<String, dynamic> json) =>
      new EmailVerificationResponse(
        verifyToken: json["verify_token"],
        authority: json["authority"],
        parameters: List<String>.from(json["parameters"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
    "verify_token": verifyToken,
    "authority": authority,
    "parameters": List<dynamic>.from(parameters.map((x) => x)),
  };

  @override
  String toString() {
    return 'EmailVerificationResponse{verifyToken: $verifyToken, authority: $authority, parameters: $parameters}';
  }
}