import 'dart:convert';

TokenRefreshError tokenRefreshErrorFromJson(String str) =>
    TokenRefreshError.fromJson(json.decode(str));

String tokenRefreshErrorToJson(TokenRefreshError data) =>
    json.encode(data.toJson());

class TokenRefreshError {
  final String error;
  final String errorDescription;
  final String errorUri;

  TokenRefreshError({
    this.error,
    this.errorDescription,
    this.errorUri,
  });

  TokenRefreshError copyWith({
    String error,
    String errorDescription,
    String errorUri,
  }) =>
      TokenRefreshError(
        error: error ?? this.error,
        errorDescription: errorDescription ?? this.errorDescription,
        errorUri: errorUri ?? this.errorUri,
      );

  factory TokenRefreshError.fromJson(Map<String, dynamic> json) =>
      TokenRefreshError(
        error: json["error"] == null ? null : json["error"],
        errorDescription: json["error_description"] == null
            ? null
            : json["error_description"],
        errorUri: json["error_uri"] == null ? null : json["error_uri"],
      );

  Map<String, dynamic> toJson() => {
        "error": error == null ? null : error,
        "error_description": errorDescription == null ? null : errorDescription,
        "error_uri": errorUri == null ? null : errorUri,
      };
}
