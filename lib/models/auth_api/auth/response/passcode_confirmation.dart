class PasscodeConfirmation {
  String authToken;

  PasscodeConfirmation({
    this.authToken,
  });

  factory PasscodeConfirmation.fromJson(Map<String, dynamic> json) =>
      new PasscodeConfirmation(
        authToken: json["auth_token"],
      );

  Map<String, dynamic> toJson() => {
        "auth_token": authToken,
      };
}
