class PhoneConfirmation {
  final String verifyToken;
  final String authority;
  final List<String> parameters;

  PhoneConfirmation({
    this.verifyToken,
    this.authority,
    this.parameters,
  });

  factory PhoneConfirmation.fromJson(Map<String, dynamic> json) =>
      new PhoneConfirmation(
        verifyToken: json["verify_token"],
        authority: json["authority"],
        parameters: new List<String>.from(json["parameters"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "verify_token": verifyToken,
        "authority": authority,
        "parameters": new List<dynamic>.from(parameters.map((x) => x)),
      };

  @override
  String toString() {
    return 'PhoneConfirmationResult{verifyToken: $verifyToken, authority: $authority, parameters: $parameters}';
  }
}
