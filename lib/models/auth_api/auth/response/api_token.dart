class ApiToken {
  final String accessToken;
  final int expiresIn;
  final String tokenType;
  final String refreshToken;

  ApiToken({
    this.accessToken,
    this.expiresIn,
    this.tokenType,
    this.refreshToken,
  });

  factory ApiToken.fromJson(Map<String, dynamic> json) => new ApiToken(
        accessToken: json["access_token"],
        expiresIn: json["expires_in"],
        tokenType: json["token_type"],
        refreshToken: json["refresh_token"],
      );

  Map<String, dynamic> toJson() => {
        "access_token": accessToken,
        "expires_in": expiresIn,
        "token_type": tokenType,
        "refresh_token": refreshToken,
      };
}
