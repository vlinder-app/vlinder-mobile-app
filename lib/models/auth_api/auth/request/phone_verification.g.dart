// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_verification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AccountVerification _$AccountVerificationFromJson(Map<String, dynamic> json) {
  return AccountVerification(
    payload: json['payload'] == null
        ? null
        : Payload.fromJson(json['payload'] as Map<String, dynamic>),
    token: json['token'] as String,
  );
}

Map<String, dynamic> _$AccountVerificationToJson(
        AccountVerification instance) =>
    <String, dynamic>{
      'payload': instance.payload,
      'token': instance.token,
    };

Payload _$PayloadFromJson(Map<String, dynamic> json) {
  return Payload(
    phoneNumber: json['phoneNumber'] as String,
    countryIso2: json['countryIso2'] as String,
    confirmationCode: json['confirmationCode'] as String,
    passcode: json['passcode'] as String,
    replaceExisting: json['replaceExisting'] as bool,
  );
}

Map<String, dynamic> _$PayloadToJson(Payload instance) => <String, dynamic>{
      'phoneNumber': instance.phoneNumber,
      'countryIso2': instance.countryIso2,
      'confirmationCode': instance.confirmationCode,
      'passcode': instance.passcode,
      'replaceExisting': instance.replaceExisting,
    };
