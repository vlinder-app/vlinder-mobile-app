import 'package:atoma_cash/models/auth_api/auth/request/phone_verification.dart';

class PasscodeVerification {
  final Payload payload;
  final String token;

  PasscodeVerification({
    this.payload,
    this.token,
  });

  factory PasscodeVerification.verification(
      String token, String passcode, bool replaceExisting) {
    return PasscodeVerification(
        payload: Payload(passcode: passcode, replaceExisting: replaceExisting),
        token: token);
  }

  factory PasscodeVerification.fromJson(Map<String, dynamic> json) =>
      new PasscodeVerification(
        payload: Payload.fromJson(json["payload"]),
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "payload": payload.toJson(),
        "token": token,
      };
}
