// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'api_token_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TokenRequest _$TokenRequestFromJson(Map<String, dynamic> json) {
  return TokenRequest(
    clientId: json['client_id'] as String,
    grantType: json['grant_type'] as String,
    scope: json['scope'] as String,
    authToken: json['auth_token'] as String,
    refreshToken: json['refresh_token'] as String,
    clientSecret: json['client_secret'] as String,
  );
}

Map<String, dynamic> _$TokenRequestToJson(TokenRequest instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('client_id', instance.clientId);
  writeNotNull('grant_type', instance.grantType);
  writeNotNull('scope', instance.scope);
  writeNotNull('auth_token', instance.authToken);
  writeNotNull('refresh_token', instance.refreshToken);
  writeNotNull('client_secret', instance.clientSecret);
  return val;
}
