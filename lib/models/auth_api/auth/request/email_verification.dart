import 'package:atoma_cash/models/auth_api/auth/request/phone_verification.dart';

class EmailVerification {
  final Payload payload;
  final String token;

  EmailVerification({
    this.payload,
    this.token,
  });

  factory EmailVerification.verification(String token) {
    return EmailVerification(
        payload: Payload(),
        token: token);
  }

  factory EmailVerification.fromJson(Map<String, dynamic> json) =>
      new EmailVerification(
        payload: Payload.fromJson(json["payload"]),
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "payload": payload.toJson(),
        "token": token,
      };
}
