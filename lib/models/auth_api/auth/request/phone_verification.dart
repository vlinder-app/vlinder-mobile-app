import 'package:json_annotation/json_annotation.dart';

part 'phone_verification.g.dart';

@JsonSerializable()
class AccountVerification {
  Payload payload;
  String token;

  AccountVerification({
    this.payload,
    this.token,
  });

  AccountVerification.withConfirmationCode(
      {String confirmationCode, String verifyToken}) {
    this.payload = Payload(confirmationCode: confirmationCode);
    this.token = verifyToken;
  }

  AccountVerification.withPhone({String phoneNumber, String countryIso2}) {
    this.payload = Payload(phoneNumber: phoneNumber, countryIso2: countryIso2);
    this.token = null;
  }

  factory AccountVerification.fromJson(Map<String, dynamic> json) =>
      _$AccountVerificationFromJson(json);

  Map<String, dynamic> toJson() => _$AccountVerificationToJson(this);

  @override
  String toString() {
    return 'PhoneConfirmation{payload: $payload, token: $token}';
  }
}

@JsonSerializable()
class Payload {
  final String phoneNumber;
  final String countryIso2;
  final String confirmationCode;
  final String passcode;
  final bool replaceExisting;

  Payload(
      {this.phoneNumber,
        this.countryIso2,
      this.confirmationCode,
      this.passcode,
      this.replaceExisting});

  factory Payload.fromJson(Map<String, dynamic> json) =>
      _$PayloadFromJson(json);

  Map<String, dynamic> toJson() => _$PayloadToJson(this);

  @override
  String toString() {
    return 'Payload{phoneNumber: $phoneNumber, confirmationCode: $confirmationCode}';
  }
}
