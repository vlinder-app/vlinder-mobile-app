import 'package:json_annotation/json_annotation.dart';
import 'package:atoma_cash/models/auth_api/auth/internal/auth_scope.dart';
import 'package:atoma_cash/models/auth_api/auth/internal/client_id.dart';
import 'package:atoma_cash/models/auth_api/auth/internal/grant_type.dart';
import 'package:atoma_cash/utils/types_helper.dart';

part 'api_token_request.g.dart';

@JsonSerializable(includeIfNull: false)
class TokenRequest {
  @JsonKey(name: "client_id")
  String clientId;
  @JsonKey(name: "grant_type")
  String grantType;
  @JsonKey(name: "scope")
  String scope;
  @JsonKey(name: "auth_token")
  String authToken;
  @JsonKey(name: "refresh_token")
  String refreshToken;
  @JsonKey(name: "client_secret")
  String clientSecret;

  TokenRequest({this.clientId,
    this.grantType,
    this.scope,
    this.authToken,
    this.refreshToken,
    this.clientSecret});

  factory TokenRequest.fromJson(Map<String, dynamic> json) =>
      _$TokenRequestFromJson(json);

  Map<String, dynamic> toJson() => _$TokenRequestToJson(this);

  factory TokenRequest.bearer(String authToken) => new TokenRequest(
      clientId: ClientId.mc2clientId,
      grantType: TypesHelper.stringFromList([GrantType.authentication]),
      scope: TypesHelper.stringFromList(
          [Scope.atomaCashApi, Scope.offlineAccess, Scope.openid, Scope.profile]),
      authToken: authToken);

  factory TokenRequest.refresh(String refreshToken) => new TokenRequest(
      clientId: ClientId.mc2clientId,
      grantType: TypesHelper.stringFromList([GrantType.refreshToken]),
      clientSecret: "password",
      refreshToken: refreshToken);

  @override
  String toString() {
    return 'TokenRequest{clientId: $clientId, grantType: $grantType, scope: $scope, authToken: $authToken}';
  }
}
