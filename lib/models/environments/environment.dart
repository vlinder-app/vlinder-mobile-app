import 'package:flutter/cupertino.dart';

class Environment {
  Environment({
    @required this.environmentName,
    @required this.authUri,
    @required this.apiUri,
  });

  String environmentName;
  String authUri;
  String apiUri;

  Environment copyWith({
    String environmentName,
    String authUri,
    String apiUri,
  }) =>
      Environment(
        environmentName: environmentName ?? this.environmentName,
        authUri: authUri ?? this.authUri,
        apiUri: apiUri ?? this.apiUri,
      );

  factory Environment.prod() => Environment(
      environmentName: "Production",
      apiUri: "https://api.vlinder.app",
      authUri: "https://auth.vlinder.app");

  factory Environment.fromJson(Map<String, dynamic> json) => Environment(
        environmentName:
            json["environmentName"] == null ? null : json["environmentName"],
        authUri: json["authUri"] == null ? null : json["authUri"],
        apiUri: json["apiUri"] == null ? null : json["apiUri"],
      );

  Map<String, dynamic> toJson() => {
        "environmentName": environmentName == null ? null : environmentName,
        "authUri": authUri == null ? null : authUri,
        "apiUri": apiUri == null ? null : apiUri,
      };

  @override
  String toString() {
    return 'Environment{environmentName: $environmentName, authUri: $authUri, apiUri: $apiUri}';
  }
}
