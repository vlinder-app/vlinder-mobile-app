import 'dart:convert';

import 'package:atoma_cash/models/environments/environment.dart';

Environments environmentsFromJson(String str) =>
    Environments.fromJson(json.decode(str));

String environmentsToJson(Environments data) => json.encode(data.toJson());

class Environments {
  Environments({
    this.environments,
  });

  List<Environment> environments;

  Environments copyWith({
    List<Environment> environments,
  }) =>
      Environments(
        environments: environments ?? this.environments,
      );

  factory Environments.fromJson(Map<String, dynamic> json) => Environments(
        environments: json["environments"] == null
            ? null
            : List<Environment>.from(
                json["environments"].map((x) => Environment.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "environments": environments == null
            ? null
            : List<dynamic>.from(environments.map((x) => x.toJson())),
      };
}
