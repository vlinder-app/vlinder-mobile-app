import 'dart:core';

import 'package:intl/intl.dart';

extension NumberRounding on num {
  num toPrecision(int precision) {
    return num.parse((this).toStringAsFixed(precision));
  }
}

extension CurrencyFormatter on double {
  String get formatted {
    final nf = new NumberFormat("#,##0.00", "en_US");
    return nf.format(this);
  }

  String currencyFormatted(String ticker) {
    String prefix = this.isNegative ? "- " : "";
    return "$prefix${this.abs().formatted} ${ticker ?? "?"}";
  }
}

extension CurrencyAbridgedFormatter on double {
  String _abridgedAmountString() {
    if (this < 100) {
      return formatted;
    } else if (this < 1000) {
      return this.toInt().toString();
    } else {
      return NumberFormat.compactSimpleCurrency(
              locale: 'en_US', name: '', decimalDigits: 1)
          .format(this);
    }
  }

  String currencyAbridgedString(String ticker) {
    String prefix = this.isNegative ? "- " : "";
    return "$prefix${this.abs()._abridgedAmountString()} ${ticker ?? "?"}";
  }
}
