abstract class Enum<T> {
  final T _value;

  const Enum(this._value);

  T get value => _value;

  @override
  String toString() {
    return value.toString();
  }
}

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
