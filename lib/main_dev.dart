import 'dart:async';

import 'package:atoma_cash/src/app.dart';
import 'package:atoma_cash/utils/base_bloc_delegate.dart';
import 'package:atoma_cash/utils/crash_report_client/sentry_client.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'src/registration/authentication/bloc/bloc.dart';

void main() {
  Flavor flavor = Flavor.DEV;
  BlocSupervisor.delegate = BaseBlocDelegate();
  FlutterError.onError = SentryReportClient(flavor: flavor).captureFlutterError;
  runZonedGuarded<Future<void>>(() async {
    runApp(BlocProvider(
        create: (context) => AuthenticationBloc(flavor)..add(AppStarted()),
        child: App(
          flavor: flavor,
        )));
  }, SentryReportClient().captureZonedError);
}
