import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/cupertino.dart';

class WeightFormatter {
  static const String _lessThenOneHundredGram = '<0.1';
  static const double _oneHundredGram = 0.1;
  static const double _oneHundredKilogram = 100;
  static const double oneTon = 1e3;

  static String abridgedWeightString(double value, {String unitSymbol}) {
    final formattedValue = _abridgedFormattedValue(value);
    if (unitSymbol == null) {
      return formattedValue;
    } else {
      return '$formattedValue $unitSymbol';
    }
  }

  static String _abridgedFormattedValue(double value) {
    if (value < _oneHundredGram) {
      return '$_lessThenOneHundredGram';
    } else if (value < _oneHundredKilogram) {
      return value.toStringAsFixed(1);
    } else if (value < oneTon) {
      return value.toInt().toString();
    } else {
      return (value / oneTon).toStringAsFixed(1);
    }
  }

  static String unitSymbol(double value, BuildContext context) {
    if (context == null) {
      return '';
    }

    if (value < oneTon) {
      return Localization.of(context).kilogram;
    } else {
      return Localization.of(context).ton;
    }
  }
}
