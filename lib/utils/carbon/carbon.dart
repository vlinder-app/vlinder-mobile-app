import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/utils/weight_formatter.dart';
import 'package:flutter/cupertino.dart';

class CarbonHelper {
  static const carbonTrailingZerosCount = 3;

  static String carbonFootprintLocalizableValue(
      BuildContext context, double carbonFootprint,
      {String emptyPlaceholder = ""}) {
    if (carbonFootprint != null && carbonFootprint != 0.0) {
      return "${carbonFootprintValue(carbonFootprint)} ${Localization.of(context).kilogram}";
    } else {
      return emptyPlaceholder;
    }
  }

  static String carbonFootprintValue(double carbonFootprint,
      {int trailingZerosCount = carbonTrailingZerosCount}) {
    return "${(carbonFootprint >= 0.001) ? carbonFootprint.toStringAsFixed(trailingZerosCount) : 0}";
  }

  static String abridgedCarbonString(double carbonFootprint,
      {BuildContext context}) {
    final symbol = WeightFormatter.unitSymbol(carbonFootprint, context);
    return WeightFormatter.abridgedWeightString(carbonFootprint,
        unitSymbol: symbol);
  }
}
