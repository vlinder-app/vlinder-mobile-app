import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class LocalizationHelper {
  static const _defaultLocale = Locale('en', '');

  static List<Locale> get supportedLocales =>
      Localization.delegate.supportedLocales;

  static LocalizationsDelegate get localizationsDelegate =>
      Localization.delegate;

  static Locale localeListResolutionCallback(
      List<Locale> locales, Iterable<Locale> supportedLocales) {
    final locale = Localization.delegate.listResolution(
        fallback: _defaultLocale,
        withCountry: false)(locales, supportedLocales);
    Intl.defaultLocale = locale.languageCode;
    return locale;
  }
}
