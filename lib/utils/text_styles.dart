import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

const double kDefaultRadius = 12.0;

class TextStyles {
  static const primaryActionBarStyle = const TextStyle(
      fontSize: 20.0,
      color: Colors.white,
      letterSpacing: 0.28,
      fontWeight: FontWeight.w500);
  static const alertDialogTitleStyle = const TextStyle(
      fontSize: 18.0, fontWeight: FontWeight.w600, color: Colors.black);
  static const alertDialogSubTitleStyle =
  const TextStyle(fontSize: 14.0, color: Colors.black54);
}

class UiHelper {
  static TextStyle inkWellButtonTextStyle(double fontSize) {
    return TextStyle(
        color: Palette.blue, fontWeight: FontWeight.w500, fontSize: fontSize);
  }

  static double topSpacerHeight(BuildContext context) {
    var safePadding = MediaQuery
        .of(context)
        .padding
        .top;
    return safePadding == 44.0 ? 39.0 : 0.0;
  }

  static InputDecoration
  styledInputDecoration(String labelText) =>
          InputDecoration(
              labelText: labelText,
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Palette.paleGrey),
                  borderRadius: BorderRadius.circular(kDefaultRadius)),
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Palette.blue),
                  borderRadius: BorderRadius.circular(kDefaultRadius)),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(kDefaultRadius)));
}
