import 'dart:async';
import 'dart:developer';

import 'package:atoma_cash/src/registration/authentication/bloc/authentication_bloc.dart';
import 'package:atoma_cash/utils/crash_report_client/crash_reports_client.dart';
import 'package:flutter/material.dart';
import 'package:sentry/sentry.dart';

class SentryReportClient extends ICrashReportClient {
  static const _prodDsn =
      'https://2d2026754c494ab0ba27394bfcfd1097@o429174.ingest.sentry.io/5375296';
  static const _devDsn =
      'https://71e6a72efac24769accd423f5e00ad18@o429174.ingest.sentry.io/5375383';

  static final SentryReportClient _instance = SentryReportClient._internal();

  SentryClient _sentry;

  SentryReportClient._internal();

  bool get isInDebugMode {
    bool inDebugMode = false;
    assert(inDebugMode = true);

    return inDebugMode;
  }

  factory SentryReportClient({Flavor flavor}) {
    if (flavor != null) {
      _instance._sentry?.close();
      _instance._sentry = _clientForFlavor(flavor);
    }
    return _instance;
  }

  @override
  void captureZonedError(Object error, StackTrace stackTrace) {
    try {
      captureException(
        exception: error,
        stackTrace: stackTrace,
      );
    } catch (e) {
      log('Sending report to sentry.io failed: $e');
      log('Original error: $error');
    }
  }

  @override
  void captureFlutterError(FlutterErrorDetails details) {
    if (isInDebugMode) {
      FlutterError.dumpErrorToConsole(details);
    } else {
      Zone.current.handleUncaughtError(details.exception, details.stack);
    }
  }

  @override
  void captureException({
    @required dynamic exception,
    dynamic stackTrace,
  }) {
    if (isInDebugMode) {
      log(stackTrace);
    } else {
      _sentry.captureException(
        exception: exception,
        stackTrace: stackTrace,
      );
    }
  }

  static SentryClient _clientForFlavor(Flavor flavor) {
    switch (flavor) {
      case Flavor.PROD:
        return SentryClient(dsn: _prodDsn);
      case Flavor.DEV:
        return SentryClient(dsn: _devDsn);
      default:
        return null;
    }
  }
}
