import 'package:flutter/material.dart';

abstract class ICrashReportClient {
  void captureZonedError(Object error, StackTrace stackTrace);

  void captureFlutterError(FlutterErrorDetails details);

  void captureException({
    @required dynamic exception,
    dynamic stackTrace,
  });
}
