class Validators {
  static final RegExp _emailRegExp = RegExp(
    r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?",
  );

  static final RegExp _phoneE164RegExp = RegExp(r'^\+[1-9]\d{10,14}$');

  static final RegExp _nameRegExp = RegExp(r'[ a-zA-Z0-9\u0400-\u04FF]{2,}$');

  static isValidEmail(String email) {
    return _emailRegExp.hasMatch(email);
  }

  static isValidPhone(String phone) {
    return _phoneE164RegExp.hasMatch(phone);
  }

  static isValidName(String name) {
    return _nameRegExp.hasMatch(name);
  }
}
