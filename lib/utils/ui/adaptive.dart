import 'package:flutter/cupertino.dart';

class Adaptive {
  static bool isFourInchScreen(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return screenSize.width == 320.0 && screenSize.height == 568;
  }
}
