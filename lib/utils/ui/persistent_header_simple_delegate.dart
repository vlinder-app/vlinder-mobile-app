import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PersistentHeaderSimpleDelegate extends SliverPersistentHeaderDelegate {
  final Widget container;
  final double minHeight;
  final double height;

  const PersistentHeaderSimpleDelegate(
      {@required this.container,
      @required this.height,
      @required this.minHeight});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return container;
  }

  @override
  double get maxExtent => height;

  @override
  double get minExtent => minHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
