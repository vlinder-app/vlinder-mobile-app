import 'package:atoma_cash/models/api/api/geolocation/countries.dart';
import 'package:flutter/material.dart';
import 'package:atoma_cash/resources/config/palette.dart';

class RegionSearchDelegate extends SearchDelegate<Region> {
  final List<Region> values;

  List<Region> filterName = new List();

  RegionSearchDelegate(this.values);

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      if (query.isNotEmpty)
        IconButton(
          tooltip: 'Clear',
          icon: const Icon(
            (Icons.clear),
            color: Palette.mediumGrey,
          ),
          onPressed: () {
            query = '';
            showSuggestions(context);
          },
        ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return _queryFilterWidget();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return _queryFilterWidget();
  }

  Widget _queryFilterWidget() {
    final suggestions = values
        .where((c) => c.name.toLowerCase().contains(query.toLowerCase()))
        .toList();

    return ListView.builder(
        itemCount: suggestions.length,
        itemBuilder: (BuildContext context, int index) {
          return new ListTile(
            title: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                suggestions[index].name,
                style: TextStyle(fontSize: 16.0),
              ),
            ),
            onTap: () {
              close(context, suggestions[index]);
            },
          );
        });
  }
}
