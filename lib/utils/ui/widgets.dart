import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

import '../text_styles.dart';

const int snackBarDurationSec = 3;

class Widgets {
  static Widget styledFlatButton(
      {@required String text,
      @required Function() onPressed,
      Color textColor = Palette.blue}) {
    return FlatButton(
      child: Text(
        text,
        style: TextStyle(color: textColor, fontWeight: FontWeight.w600),
      ),
      onPressed: onPressed,
    );
  }

  static Widget raisedButtonWithLoadingIndicator(
      {@required String text,
      @required bool isLoading,
      @required VoidCallback onPressed}) {
    if (isLoading) {
      return Center(child: CircularProgressIndicator());
    } else {
      return RaisedButton(child: Text(text), onPressed: onPressed);
    }
  }

  static Widget flatButtonWithLoadingIndicator(
      {@required String text,
      @required bool isLoading,
      @required VoidCallback onPressed}) {
    if (isLoading) {
      return Center(child: CircularProgressIndicator());
    } else {
      return FlatButton(
          child: Text(
            text,
            style: TextStyle(color: Palette.blue),
          ),
          onPressed: onPressed);
    }
  }

  static void failedSnackBar(BuildContext context, String text,
      {ScaffoldState scaffoldState}) {
    (scaffoldState ?? Scaffold.of(context))
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          duration: Duration(seconds: snackBarDurationSec),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [Text(text), Icon(Icons.error)],
          ),
          backgroundColor: Palette.red,
        ),
      );
  }

  static void notificationSnackBar(BuildContext context, String text,
      {ScaffoldState scaffoldState}) {
    (scaffoldState ?? Scaffold.of(context))
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          behavior: SnackBarBehavior.floating,
          duration: Duration(seconds: snackBarDurationSec),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [Text(text)],
          ),
        ),
      );
  }

  static void hideKeyboardOnContext(BuildContext context) {
    FocusScope.of(context).requestFocus(new FocusNode());
  }

  static Widget bottomSheetActionPanel({@required String negativeButtonText,
    @required String positiveButtonText,
    @required VoidCallback onPositiveClick,
    @required VoidCallback onNegativeClick}) {
    return Row(children: <Widget>[
      Flexible(
        flex: 2,
        fit: FlexFit.tight,
        child: FlatButton(
          child: Text(
            negativeButtonText,
            style:
            TextStyle(color: Palette.darkGrey, fontWeight: FontWeight.w500),
          ),
          onPressed: onNegativeClick,
        ),
      ),
      SizedBox(
        width: 1.0,
        height: 40.0,
        child: const DecoratedBox(
          decoration: const BoxDecoration(color: Palette.paleGrey),
        ),
      ),
      Flexible(
        fit: FlexFit.tight,
        flex: 2,
        child: FlatButton(
            child: Text(positiveButtonText,
                style: TextStyle(color: Palette.blue)),
            onPressed: onPositiveClick),
      )
    ]);
  }

  static Widget plainButton(
      {@required String buttonTitle, @required VoidCallback onClick}) {
    return InkWell(
      onTap: onClick,
      child: Text(
        buttonTitle,
        style: UiHelper.inkWellButtonTextStyle(16.0),
      ),
    );
  }

  static Widget operationResultWidget({@required String title,
    @required String asset,
    @required Widget actionWidget,
    String subtitle = ""}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Image.asset(asset, width: 120.0, height: 120.0),
        Padding(
          padding: const EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(fontWeight: FontWeight.w600, fontSize: 20.0),
          ),
        ),
        _subtitleForString(subtitle),
        Padding(
          padding: const EdgeInsets.only(top: 40.0),
          child: actionWidget,
        ),
      ],
    );
  }

  static Widget _subtitleForString(String subtitle) {
    if (subtitle.isNotEmpty) {
      return Padding(
        padding: const EdgeInsets.only(top: 30.0, left: 30.0, right: 30.0),
        child: Text(
          subtitle,
          textAlign: TextAlign.center,
          style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16.0,
              color: Palette.mediumGrey),
        ),
      );
    } else {
      return Text(subtitle);
    }
  }
}
