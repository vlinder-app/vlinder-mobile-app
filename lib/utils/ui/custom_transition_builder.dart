import 'package:flutter/material.dart';

class CustomTransitionBuilder {
  static const RouteTransitionsBuilder leftToRightWithoutPop =
      _leftToRightWithoutPop;

  static Widget _leftToRightWithoutPop(
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    Animation<double> onlyForwardAnimation;
    switch (animation.status) {
      case AnimationStatus.reverse:
      case AnimationStatus.dismissed:
        onlyForwardAnimation = kAlwaysCompleteAnimation;
        break;
      case AnimationStatus.forward:
      case AnimationStatus.completed:
        onlyForwardAnimation = animation;
        break;
    }
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(1.0, 0.0),
        end: Offset.zero,
      ).animate(onlyForwardAnimation),
      child: child,
    );
  }
}
