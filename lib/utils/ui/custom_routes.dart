import 'package:flutter/material.dart';

class MaterialRouteWithoutAnimation<T> extends MaterialPageRoute<T> {
  MaterialRouteWithoutAnimation(
      {WidgetBuilder builder,
      RouteSettings settings,
      bool fullscreenDialog = false})
      : super(
      builder: builder,
      settings: settings,
      fullscreenDialog: fullscreenDialog);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return child;
  }
}

class NoAnimationRoute<T> extends PageRouteBuilder<T> {
  final Widget child;
  final bool fullscreenDialog;

  NoAnimationRoute({@required this.child, this.fullscreenDialog})
      : super(
      fullscreenDialog: fullscreenDialog,
      transitionDuration: Duration(microseconds: 0),
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) =>
      child);
}
