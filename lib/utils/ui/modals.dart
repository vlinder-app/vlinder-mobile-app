import 'package:flutter/material.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';

class Modals {
  static void showPositiveNegativeBottomSheet(
      {@required BuildContext context,
      @required String imageAsset,
      @required String message,
      @required String positiveButtonText,
      @required String negativeButtonText,
      @required VoidCallback onPositiveClick,
      @required VoidCallback onNegativeClick}) {
    showModalBottomSheet<void>(
        context: context,
        builder: (BuildContext context) {
          return Container(
            child: Padding(
              padding: const EdgeInsets.all(24.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(
                      height: 100.0,
                      width: 100.0,
                      child: Image.asset(
                        imageAsset,
                      )),
                  Padding(
                    padding: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                    child: Text(
                      message,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Palette.darkGrey,
                        fontWeight: FontWeight.w600,
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                  Widgets.bottomSheetActionPanel(
                      negativeButtonText: negativeButtonText,
                      positiveButtonText: positiveButtonText,
                      onPositiveClick: onPositiveClick,
                      onNegativeClick: onNegativeClick),
                ],
              ),
            ),
          );
        });
  }
}
