import 'package:dio/dio.dart';

const int maxCharactersPerLine = 65000;

class LoggingInterceptor {
  static void printRequest(RequestOptions options) {
    print("--> ${options.method} ${options.path} ${options.queryParameters}");
    print(options.data.toString());
    print("Content type: ${options.contentType}");
    print("<-- END HTTP");
  }

  static void printResponse(Response response) {
    print(
        "<-- ${response.statusCode} ${response.request.method} ${response.request.path}");
    String responseAsString = response.data.toString();
    if (responseAsString.length > maxCharactersPerLine) {
      int iterations = (responseAsString.length / maxCharactersPerLine).floor();
      for (int i = 0; i <= iterations; i++) {
        int endingIndex = i * maxCharactersPerLine + maxCharactersPerLine;
        if (endingIndex > responseAsString.length) {
          endingIndex = responseAsString.length;
        }
        print(
            responseAsString.substring(i * maxCharactersPerLine, endingIndex));
      }
    } else {
      print(response.data);
    }
    print("<-- END HTTP");
  }

  static void printError(DioError error) {
    print(error);
    print("error response: ${error.response?.data}");
  }
}
