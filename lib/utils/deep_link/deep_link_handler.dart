import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/models/auth_api/auth/response/email_verification_response.dart';
import 'package:atoma_cash/models/auth_api/base_models/base_response.dart'
    as auth;
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/resources/repository/remote/auth_remote_repository.dart';
import 'package:atoma_cash/src/registration/flow_controllers/authentication_flow_controller.dart';

class DeepLinkHandler {
  static const String _kEmailConfirmationPath = "/email/confirmation";
  static const String _kRecoveryPasscodeEmailStepPath = "/recovery/email";

  Future<bool> _confirmEmailWithToken(Uri emailConfirmationUri) async {
    var confirmationToken =
        emailConfirmationUri.queryParameters["confirmationToken"];
    if (confirmationToken != null) {
      BaseResponse<Object> response =
          await ApiRemoteRepository().confirmEmailWithToken(confirmationToken);
      return response.isSuccess();
    } else {
      return false;
    }
  }

  void _handleRecoveryEmailDeepLink(Uri uri) async {
    var emailConfirmationSuccess = await _confirmEmailWithToken(uri);
    if (emailConfirmationSuccess) {
      auth.BaseResponse<EmailVerificationResponse> response =
          await AuthRemoteRepository().checkIsEmailVerificationPassed();
      if (response.isSuccess()) {
        AuthenticationFlowController(AuthenticationFlow.PASSCODE_RECOVERY)
            .moveToRecoveryPinSetup();
      }
    }
  }

  void handleInitialUri(Uri uri) {
    if (uri != null && !uri.hasEmptyPath) {
      switch (uri.path) {
        case _kEmailConfirmationPath:
          _confirmEmailWithToken(uri);
          break;
      }
    }
  }

  void handleUriFromStream(Uri uri) {
    if (uri != null && !uri.hasEmptyPath) {
      switch (uri.path) {
        case _kEmailConfirmationPath:
          _confirmEmailWithToken(uri);
          break;
        case _kRecoveryPasscodeEmailStepPath:
          _handleRecoveryEmailDeepLink(uri);
          break;
      }
    }
  }
}
