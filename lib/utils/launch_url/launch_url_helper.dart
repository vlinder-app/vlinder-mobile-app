import 'package:atoma_cash/utils/launch_url/email_client_content_builder.dart';
import 'package:flutter/cupertino.dart';
import 'package:url_launcher/url_launcher.dart';

class LaunchUrlHelper {
  static void openLink(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static void openEmailClient(BuildContext context,
      {@required IMailtoContentBuilder contentBuilder}) {
    final content = contentBuilder?.build(context);
    if (content != null && content.isNotEmpty) {
      openLink('mailto:$content');
    } else {
      throw 'Could not open [mailto:] with empty or null content';
    }
  }
}
