import 'package:atoma_cash/utils/platform/platform_helper.dart';
import 'package:flutter/cupertino.dart';

abstract class IMailtoContentBuilder {
  String build(BuildContext context);
}

class DefaultMailtoContentBuilder extends IMailtoContentBuilder {
  final String email;
  final String subject;
  final String userId;
  final String deviceInfo;

  DefaultMailtoContentBuilder(
      {@required this.email, this.subject, this.userId, this.deviceInfo})
      : assert(email != null);

  @override
  String build(BuildContext context) {
    final emailData = _emailData(context, subject, userId, deviceInfo);
    if (emailData != null && emailData.isNotEmpty) {
      return '$email?$emailData';
    } else {
      return '$email';
    }
  }

  static String _emailData(
      BuildContext context, String subject, String userId, String deviceInfo) {
    String body = '';
    if (userId != null) {
      body += 'Vlinder account: $userId<br><br>';
    }
    if (deviceInfo != null) {
      body += 'System information: $deviceInfo<br><br>';
    }

    if (PlatformHelper.isIOS(context)) {
      final encodedSubject = Uri.encodeComponent(subject ?? '');
      final encodedBody = Uri.encodeComponent(body);
      return _configureEmailData(encodedSubject, encodedBody);
    } else {
      return _configureEmailData(subject, body);
    }
  }

  static String _configureEmailData(String subject, String body) {
    String data = '';
    if (subject != null && subject.isNotEmpty) {
      data += 'subject=$subject';
    }
    if (body != null && body.isNotEmpty) {
      data += data.isNotEmpty ? '&' : '';
      data += 'body=$body';
    }
    return data;
  }
}
