import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:intl/intl.dart';
import 'package:package_info/package_info.dart';

class DeviceInfo {
  static Future<String> getDeviceInfo() async {
    final packageInfo = await PackageInfo.fromPlatform();

    if (Platform.isAndroid) {
      var androidInfo = await DeviceInfoPlugin().androidInfo;

      return _configureDeviceInfo(
        device: androidInfo.model,
        osVersion:
            "${androidInfo.version.release} (SDK ${androidInfo.version.sdkInt})",
        localeId: Intl.defaultLocale,
        appVersion: packageInfo.version,
      );
    } else if (Platform.isIOS) {
      var iosInfo = await DeviceInfoPlugin().iosInfo;

      return _configureDeviceInfo(
        device: iosInfo.model,
        osVersion: "${iosInfo.systemName} ${iosInfo.systemVersion}",
        localeId: Intl.defaultLocale,
        appVersion: packageInfo.version,
      );
    } else {
      return "";
    }
  }

  static String _configureDeviceInfo({
    String device,
    String osVersion,
    String localeId,
    String appVersion,
  }) {
    return "$device $osVersion - $localeId($appVersion)";
  }
}
