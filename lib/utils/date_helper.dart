import 'package:atoma_cash/resources/config/date.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

const String humanDateFormat = "dd${dateSeparator}MM${dateSeparator}yyyy";
const String kGroupedByDateListFormat = "MMMM d, yyyy";
const String kTimeFormat = "hh:mm";
const String kShortMonthFormat = "MMM";
const String kMonthFormat = "MMMM";

class DateHelper {
  static TextInputFormatter dateInputFormatter() {
    return new MaskTextInputFormatter(
        mask: '##.##.####', filter: {"#": RegExp(r'[0-9]')});
  }

  static String dateForValidation(String date) {
    if (date.isNotEmpty && date != null) {
      List<String> dateParts = date.split(dateSeparator);
      if (dateParts.length == 3) {
        if (_isDayValid(int.tryParse(dateParts.first)) &&
            _isMonthValid(int.tryParse(dateParts[1])) &&
            _isYearValid(int.tryParse(dateParts[2]))) {
          return date.split(dateSeparator).reversed.join();
        }
      }
    }

    return "";
  }

  static bool _isDayValid(int day) {
    return day >= 1 && day <= 31;
  }

  static bool _isMonthValid(int month) {
    return month >= 1 && month <= 12;
  }

  static bool _isYearValid(int year) {
    return year >= 1930;
  }

  static bool isDateEarlierThan(String date, DateTime dateTime) {
    DateTime parsedDate = DateTime.tryParse(dateForValidation(date));
    return (parsedDate != null) && parsedDate.isBefore(dateTime);
  }

  static bool isDateValid(String date) {
    return isDateEarlierThan(date, DateTime.now());
  }

  static bool isSameMonthAndYear(DateTime date, DateTime currentMonth) {
    final localDate = date.toLocal();
    final dateWithoutDays = DateTime(localDate.year, localDate.month);

    return dateWithoutDays.isAtSameMomentAs(currentMonth);
  }

  static String _formattedDate(DateTime date, String format) {
    return DateFormat(format).format(date);
  }

  static String humanDate(DateTime date) {
    return _formattedDate(date, humanDateFormat);
  }

  static String formattedDate(DateTime date, String format) {
    return _formattedDate(date, format);
  }

  static DateTime endOfADay(DateTime dateTime) => beginOfADay(dateTime)
      .add(Duration(days: 1))
      .subtract(Duration(microseconds: 1));

  static DateTime beginOfADay(DateTime date) =>
      DateTime(date.year, date.month, date.day);

  static DateTime weekStart(DateTime date) {
    // This is ugly, but to avoid problems with daylight saving
    DateTime monday = DateTime.utc(date.year, date.month, date.day);
    monday = monday.subtract(Duration(days: monday.weekday - 1));

    return monday;
  }

  static String separatorStringForDateTime(
      BuildContext context, DateTime dateTime) {
    DateTime now = new DateTime.now();
    DateTime nowDateWithoutTime = new DateTime(now.year, now.month, now.day);
    if (nowDateWithoutTime.isAtSameMomentAs(dateTime)) {
      return Localization.of(context).today;
    } else if (dateTime
        .isAtSameMomentAs(nowDateWithoutTime.subtract(Duration(days: 1)))) {
      return Localization.of(context).yesterdayDatePeriod;
    } else {
      if (dateTime.year == nowDateWithoutTime.year) {
        return DateFormat.MMMMd().format(dateTime);
      } else {
        return DateFormat(kGroupedByDateListFormat).format(dateTime);
      }
    }
  }

  static DateTime weekEnd(DateTime date) {
    // This is ugly, but to avoid problems with daylight saving
    // Set the last microsecond to really be the end of the week
    DateTime sunday = DateTime.utc(
      date.year,
      date.month,
      date.day,
      23,
      59,
      59,
      999,
      999999,
    );
    sunday = sunday.add(Duration(days: 7 - sunday.weekday));

    return sunday;
  }

  static DateTime defaultGroupTrigger(DateTime date) {
    final localDate = date?.toLocal() ?? DateTime.now();
    return DateTime(localDate.year, localDate.month, localDate.day);
  }

  static String updatedAtFormattedString(
      BuildContext context, DateTime updatedAt) {
    final now = DateTime.now();
    final nowDateWithoutSeconds =
        DateTime(now.year, now.month, now.day, now.hour, now.minute);
    final localUpdatedAt = updatedAt.toLocal();
    final localUpdatedAtWithoutSeconds = DateTime(
        localUpdatedAt.year,
        localUpdatedAt.month,
        localUpdatedAt.day,
        localUpdatedAt.hour,
        localUpdatedAt.minute);

    if (localUpdatedAtWithoutSeconds.isAtSameMomentAs(nowDateWithoutSeconds)) {
      return Localization.of(context).updatedJustNow;
    } else if (localUpdatedAtWithoutSeconds
            .compareTo(nowDateWithoutSeconds.subtract(Duration(hours: 1))) >
        0) {
      final difference =
          nowDateWithoutSeconds.difference(localUpdatedAtWithoutSeconds);
      return Localization.of(context)
          .updatedRecentlyAtLabel(difference.inMinutes.toString());
    } else {
      //if (updatedAt.compareTo(beginOfADay(now)) > 0) {
      return Localization.of(context).updatedAtLabel(
          _formattedDate(localUpdatedAtWithoutSeconds, kTimeFormat));
    }
  }
}
