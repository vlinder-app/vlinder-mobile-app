import 'package:atoma_cash/utils/text_styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Dimensions {
  static const alertDialogPadding =
  const EdgeInsets.fromLTRB(24.0, 20.0, 24.0, 8.0);

  static const EdgeInsets leftRight24Padding =
  EdgeInsets.only(left: 24.0, right: 24.0);

  static double bottomSpacerHeight(BuildContext context, double headerHeight,
      contentHeight) {
    var safePadding = MediaQuery
        .of(context)
        .padding
        .top;

    final screenHeight = MediaQuery
        .of(context)
        .size
        .height -
        safePadding -
        kBottomNavigationBarHeight;

    double visibleContentPlaceholderHeight = screenHeight -
        headerHeight -
        kToolbarHeight -
        UiHelper.topSpacerHeight(context);
    double diff = visibleContentPlaceholderHeight - contentHeight;
    if (diff < 0.0) {
      if (contentHeight < headerHeight) {
        return screenHeight - headerHeight - contentHeight;
      } else {
        double notVisibleContentHeight =
            contentHeight - visibleContentPlaceholderHeight;
        if (notVisibleContentHeight < 0) {
          return 0.0;
        } else if (notVisibleContentHeight < headerHeight) {
          return headerHeight - notVisibleContentHeight;
        }
      }
    }
    return 0.0;
  }
}
