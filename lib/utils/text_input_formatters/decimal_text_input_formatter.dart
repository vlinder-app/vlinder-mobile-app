import 'dart:math' as math;

import 'package:flutter/services.dart';

class DecimalTextInputFormatter extends TextInputFormatter {
  DecimalTextInputFormatter({this.decimalRange, this.activatedNegativeValues})
      : assert(decimalRange == null || decimalRange >= 0,
            'DecimalTextInputFormatter declaretion error');

  final int decimalRange;
  final bool activatedNegativeValues;

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue, // unused.
    TextEditingValue newValue,
  ) {
    TextEditingValue dotSeparatedValue =
        newValue.copyWith(text: newValue.text.replaceAll(",", "."));

    TextSelection newSelection = dotSeparatedValue.selection;
    String truncated = dotSeparatedValue.text;

    if (dotSeparatedValue.text.contains(' ')) {
      return oldValue;
    }

    if (dotSeparatedValue.text.isEmpty) {
      return dotSeparatedValue;
    } else if (double.tryParse(dotSeparatedValue.text) == null &&
        !(dotSeparatedValue.text.length == 1 &&
            (activatedNegativeValues == true ||
                activatedNegativeValues == null) &&
            dotSeparatedValue.text == '-')) {
      return oldValue;
    }

    if (activatedNegativeValues == false &&
        double.tryParse(dotSeparatedValue.text) < 0) {
      return oldValue;
    }

    if (decimalRange != null) {
      String value = dotSeparatedValue.text;

      if (decimalRange == 0 && value.contains(".")) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      }

      if (value.contains(".") &&
          value
              .substring(value.indexOf(".") + 1)
              .length > decimalRange) {
        truncated = oldValue.text;
        newSelection = oldValue.selection;
      } else if (value == ".") {
        truncated = "0.";

        newSelection = dotSeparatedValue.selection.copyWith(
          baseOffset: math.min(truncated.length, truncated.length + 1),
          extentOffset: math.min(truncated.length, truncated.length + 1),
        );
      }

      return TextEditingValue(
        text: truncated,
        selection: newSelection,
        composing: TextRange.empty,
      );
    }
    return dotSeparatedValue;
  }
}
