class TypesHelper {
  static String stringFromList(List<dynamic> list) {
    String result = "";
    list.asMap().forEach((index, item) =>
    {
      result += '${item.toString()}${(index == list.length - 1) ? '' : ' '}'
    });
    return result;
  }
}
