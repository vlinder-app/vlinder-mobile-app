import 'dart:math';

import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/src/reports/reports_list/bloc/bloc.dart';

class ReportsHelper {
  static double extractHighestPercentageValue(List<Report> reports, Mode mode) {
    switch (mode) {
      case Mode.EXPENSES:
        return extractHighestAmountPercentage(reports);
      case Mode.IMPACT:
        return extractHighestCarbonFootprintPercentage(reports);
      default:
        return 0.0;
    }
  }

  static double extractHighestAmountPercentage(List<Report> reports) =>
      reports.fold(
          0.0, (result, report) => max(result, report.amountPercentage.abs()));

  static double extractHighestCarbonFootprintPercentage(List<Report> reports) =>
      reports.fold(
          0.0,
          (result, report) =>
              max(result, report.carbonFootprintPercentage ?? 0));

  static double extractHighestTransactionValue(
      List<Report> reports, Mode mode) {
    switch (mode) {
      case Mode.EXPENSES:
        return extractHighestAmount(reports);
      case Mode.IMPACT:
        return extractHighestCarbonFootprint(reports);
      default:
        return 0.0;
    }
  }

  static double extractHighestAmount(List<Report> reports) => reports.fold(
      0.0, (result, report) => max(result, report.amount.value.abs()));

  static double extractHighestCarbonFootprint(List<Report> reports) => reports
      .fold(0.0, (result, report) => max(result, report.carbonFootprint ?? 0));
}
