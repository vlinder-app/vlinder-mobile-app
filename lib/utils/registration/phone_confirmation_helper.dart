import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';
import 'package:atoma_cash/resources/repository/local/shared_storage_manager.dart';

class PhoneConfirmationHelper {
  static final int smsRequestTimeOutMillis = 180000;

  ApplicationLocalRepository _applicationLocalRepository =
      ApplicationLocalRepository();

  SharedStorageManager _sharedStorageManager = SharedStorageManager();

  int _currentTimeStamp() => new DateTime.now().millisecondsSinceEpoch;

  void savePhoneNumber(String phoneNumber) async {
    _applicationLocalRepository.savePhoneNumber(phoneNumber);
    _applicationLocalRepository.saveSmsRequestTimestamp(_currentTimeStamp());
  }

  Future<bool> isContainExistingRequest(String phoneNumber) async {
    String previousPhoneNumber =
        await _applicationLocalRepository.getPhoneNumber();
    int savedTimeStamp =
        await _applicationLocalRepository.getSmsRequestTimestamp();
    int timeDeltaMillis = _currentTimeStamp() - savedTimeStamp;
    print("time delta $timeDeltaMillis");
    return (previousPhoneNumber == phoneNumber) &&
        (timeDeltaMillis < smsRequestTimeOutMillis);
  }

  Future<int> remainingSmsRequestMillis() async {
    int savedTimeStamp =
        await _applicationLocalRepository.getSmsRequestTimestamp();
    int remainingMillis =
        smsRequestTimeOutMillis - (_currentTimeStamp() - savedTimeStamp);
    return remainingMillis;
  }

  Future<int> lastRequestTimeStamp() async {
    return _applicationLocalRepository.getSmsRequestTimestamp();
  }

  Future<int> getFailedSmsConfirmationAttempts(String phoneNumber) async {
    String previousPhoneNumber =
        await _applicationLocalRepository.getPhoneNumber();
    if (previousPhoneNumber == phoneNumber) {
      return await _sharedStorageManager.getFailedPhoneConfirmationAttempts();
    } else {
      return 0;
    }
  }

  void saveFailedSmsConfirmationAttempts(int failedAttemptsCount) async {
    return _sharedStorageManager.saveFailedPhoneConfirmationAttempts(
        failedAttemptsCount);
  }

  void reset() async {
    _sharedStorageManager.removeSmsRequestTimestamp();
    _sharedStorageManager.removeFailedPhoneConfirmationAttempts();
  }
}
