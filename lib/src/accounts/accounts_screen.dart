import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/banners/banner_model.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/accounts/bank_security_disclaimer/bank_security_disclaimer_result.dart';
import 'package:atoma_cash/src/accounts/bank_security_disclaimer/bank_security_disclamer_config.dart';
import 'package:atoma_cash/src/accounts/bloc/accounts_banners_data_provider.dart';
import 'package:atoma_cash/src/accounts/bloc/accounts_bloc.dart';
import 'package:atoma_cash/src/accounts/bloc/accounts_group_model.dart';
import 'package:atoma_cash/src/accounts/bloc/bloc.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/carbon_footprint_chart.dart';
import 'package:atoma_cash/src/accounts/sync/result/connections_sync_result.dart';
import 'package:atoma_cash/src/accounts/widgets/accounts_list_top_bar.dart';
import 'package:atoma_cash/src/accounts/widgets/empty_accounts_action_placeholder.dart';
import 'package:atoma_cash/src/accounts/widgets/list/cells/accounts_error_placeholder.dart';
import 'package:atoma_cash/src/accounts/widgets/list/cells/accounts_typed_group_item.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/bank_connected_event.dart';
import 'package:atoma_cash/src/analytics/models/events/connect_first_bank_event.dart';
import 'package:atoma_cash/src/analytics/models/events/manage_accounts_event.dart';
import 'package:atoma_cash/src/analytics/models/events/open_account_details_event.dart';
import 'package:atoma_cash/src/analytics/models/events/open_accounts_tab_event.dart';
import 'package:atoma_cash/src/analytics/models/events/open_bank_sync_select_event.dart';
import 'package:atoma_cash/src/analytics/models/events/view_security_disclaimer_event.dart';
import 'package:atoma_cash/src/bank/webview/bank_webview.dart';
import 'package:atoma_cash/src/bank/webview/webview_result/webview_result.dart';
import 'package:atoma_cash/src/banners/banners_navigator.dart';
import 'package:atoma_cash/src/banners/mixin/banners_support_state_mixin.dart';
import 'package:atoma_cash/src/home_placeholder/itab_listener.dart';
import 'package:atoma_cash/src/push_notifications/push_notification_permission_status_handler.dart';
import 'package:atoma_cash/src/registration/email_confirmation_request/email_confirmation_screen_configuration.dart'
    as EmailConfiguration;
import 'package:atoma_cash/src/registration/flow_controllers/email_confirmation_flow_controller.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/utils/ui/adaptive.dart';
import 'package:atoma_cash/utils/ui/persistent_header_simple_delegate.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AccountsScreen extends StatefulWidget implements ITabListener {
  final _AccountsScreenState _state = _AccountsScreenState();

  final IBannerNavigator bannerNavigator;
  final IPushNotificationPermissionStatusHandler
      pushNotificationPermissionStatusHandler;

  AccountsScreen(
      this.bannerNavigator, this.pushNotificationPermissionStatusHandler);

  @override
  _AccountsScreenState createState() => _state;

  @override
  void onAppear() {
    _state.onAppear();
  }
}

class _AccountsScreenState extends State<AccountsScreen>
    with
        AutomaticKeepAliveClientMixin,
        TickerProviderStateMixin,
        BannersSupportStateMixin
    implements ITabListener {
  static const double _maxChartHeight = 236.0;
  static const EdgeInsets _accountGroupsPadding =
      EdgeInsets.only(left: 12, top: 8, right: 12);

  AccountsBloc _bloc;

  ScrollController _scrollController = ScrollController(keepScrollOffset: true);

  @override
  IBannerNavigator get bannerNavigator => widget.bannerNavigator;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    _bloc = AccountsBloc(bannersRepository: AccountsBannersDataProvider())
      ..add(Started());
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: BlocListener(
        bloc: _bloc,
        listener: _blocListener,
        child: BlocProvider<AccountsBloc>(
          create: (context) => _bloc,
          child: BlocBuilder(
            bloc: _bloc,
            condition: _contentBuilderCondition,
            builder: _rootContentBuilder,
          ),
        ),
      ),
    );
  }

  void _blocListener(BuildContext context, AccountsState state) {
    if (state is ConnectionSafetyInfo) {
      _showSecurityDisclaimer(state.userId);
    }

    if (state is ManualAccountAuthentication) {
      _openBankWebViewForUri(state.uriModel);
    }

    if (state is NeedToFillEmail) {
      _moveToEmailConfirmation(state);
    }

    if (state is Loaded) {
      if (state.errorMessage != null) {
        _errorMessageSnackBar(context);
      }

      if (state.pushNotificationStatus != null) {
        widget.pushNotificationPermissionStatusHandler
            .handlePushNotificationPermissionStatus(
                context, state.pushNotificationStatus);
      }
    }
  }

  Widget _rootContentBuilder(BuildContext context, AccountsState state) {
    var minChartHeight = MediaQuery.of(context).padding.top;
    var chartHeight = _maxChartHeight + minChartHeight;
    return CustomScrollView(
      shrinkWrap: false,
      controller: _scrollController,
      slivers: <Widget>[
        SliverPersistentHeader(
          pinned: true,
          delegate: PersistentHeaderSimpleDelegate(
            height: chartHeight,
            minHeight: minChartHeight,
            container: CarbonFootprintChart(
              scrollController: _scrollController,
              height: chartHeight,
            ),
          ),
        ),
        _decorationHeader(context),
        _accountsListToolbar(context, state),
        _contentBuilder(context, state),
      ],
    );
  }

  Widget _decorationHeader(BuildContext context) {
    return SliverPersistentHeader(
      pinned: true,
      delegate: PersistentHeaderSimpleDelegate(
        height: 20.0,
        minHeight: 0.0,
        container: Container(
          height: 20.0,
          decoration: BoxDecoration(
              color: Palette.blue,
              borderRadius:
                  BorderRadius.only(bottomLeft: Radius.circular(20.0))),
        ),
      ),
    );
  }

  Widget _accountsListToolbar(BuildContext context, AccountsState state) {
    if (state is Loaded) {
      return _listToolbarForLoadedState(context, state);
    } else {
      return SliverToBoxAdapter();
    }
  }

  Widget _listToolbarForLoadedState(BuildContext context, Loaded state) {
    if (state.accountsGroups?.isNotEmpty ?? false) {
      return SliverToBoxAdapter(
        child: AccountsListTopBar(
          _totalBalanceTitle(state),
          isSyncAvailable: _isSyncAvailable(state),
          onSyncTapped: _onSyncAccountsTapped,
        ),
      );
    } else {
      return SliverToBoxAdapter();
    }
  }

  Widget _contentBuilder(BuildContext context, AccountsState state) {
    if (state is Loading) {
      return _loadingStateBuilder(context);
    } else if (state is Loaded) {
      return _loadedWidgetState(context, state);
    } else {
      return SliverFillRemaining(
        child: Center(
          child: AccountsErrorPlaceholder(
            onRetryClicked: () => _bloc.add(Refresh()),
          ),
        ),
      );
    }
  }

  Widget _loadingStateBuilder(BuildContext context) {
    return SliverFillRemaining(
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _loadedWidgetState(BuildContext context, Loaded state) {
    return _widgetsList(context, state.accountsGroups, state.banners);
  }

  Widget _emptyAccountStateBuilder(BuildContext context, Loaded state) {
    return SliverToBoxAdapter(
      child: Padding(
        padding: _placeholderContentPadding(context),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            EmptyAccountsActionPlaceholder(
              isLoading: state.isConnectingAccount,
              onActionTapped: _onConnectAccountTapped,
              onSubActionTapped: _viewSecurityDisclaimer,
            ),
          ],
        ),
      ),
    );
  }

  Widget _totalBalanceTitle(Loaded state) {
    if ((state.accountsGroups?.isNotEmpty ?? false) &&
        state.totalBaseCurrencyBalance != null) {
      return Text(
        state.totalBaseCurrencyBalance.currencyFormatted(),
        style: FontBook.headerH3.copyWith(color: Palette.darkGrey),
      );
    } else {
      return null;
    }
  }

  Widget _widgetsList(BuildContext context, List<AccountsGroupModel> groups,
      List<IBannerModel> banners) {
    return SliverList(
      delegate: SliverChildListDelegate([
        if (banners?.isNotEmpty ?? false) ...buildBanners(context, banners),
        if (groups?.isNotEmpty ?? false) ..._accountItems(groups),
      ]),
    );
  }

  List<Widget> _accountItems(List<AccountsGroupModel> groups) {
    return groups.map((oneGroup) {
      return Padding(
        padding: _accountGroupsPadding,
        child: AccountsTypedGroupItem(
          oneGroup,
          onAccountClick: _onAccountTapped,
          onGroupTapped: (type) => _bloc.add(AccountGroupTapped(type)),
          initiallyExpanded: groups.length == 1,
        ),
      );
    }).toList();
  }

  Widget _manageAccountsContainerWidget(BuildContext context, Loaded state) {
    if (state.accountsGroups?.isNotEmpty ?? false) {
      return SliverToBoxAdapter(
        child: Padding(
          padding: const EdgeInsets.only(
              top: 32.0, left: 24.0, right: 24.0, bottom: 24.0),
          child: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(12.0)),
            child: Container(
              height: 52.0,
              color: Palette.blue,
              child: _manageAccountsContent(context, state),
            ),
          ),
        ),
      );
    } else {
      return SliverToBoxAdapter();
    }
  }

  Widget _manageAccountsContent(BuildContext context, Loaded state) {
    if (state.isConnectingAccount) {
      return Center(
        child: SizedBox(
          width: 24,
          height: 24,
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Palette.white),
            strokeWidth: 2,
          ),
        ),
      );
    } else {
      return FlatButton(
        color: Palette.blue,
        child: Text(
          Localization.of(context).manageAccountsButton,
          style: TextStyle(color: Palette.white),
        ),
        onPressed: _onManageAccountTapped,
      );
    }
  }

  EdgeInsets _placeholderContentPadding(BuildContext context) {
    if (Adaptive.isFourInchScreen(context)) {
      return const EdgeInsets.only(
          left: 15.0, top: 8.0, right: 15.0, bottom: 8.0);
    } else {
      return const EdgeInsets.only(
          left: 24.0, top: 8.0, right: 24.0, bottom: 8.0);
    }
  }

  bool _contentBuilderCondition(
          AccountsState previous, AccountsState current) =>
      current is! ManualAccountAuthentication &&
      current is! NeedToFillEmail &&
      current is! ConnectionSafetyInfo &&
      current is! Refreshing;

  bool _isSyncAvailable(AccountsState state) {
    if (state is Loaded) {
      return (state.accountsGroups?.length ?? 0) > 0;
    } else {
      return false;
    }
  }

  void _onSyncAccountsTapped() async {
    Analytics().logEvent(OpenBankSyncSelectEvent());
    final result = await ExtendedNavigator.ofRouter<Router>()
        .pushNamed(Routes.connectionsSyncScreen);

    if (result != null) {
      if (result is ConnectionsSynchronized) {
        _bloc.add(Refresh());
      }
    }
  }

  void _onManageAccountTapped() {
    Analytics().logEvent(ManageAccountsEvent());
    _bloc.add(ConnectAccount());
  }

  void _onConnectAccountTapped() {
    Analytics().logEvent(ConnectFirstBankEvent());
    _bloc.add(ConnectAccount());
  }

  void _viewSecurityDisclaimer() {
    _bloc.add(ConnectionSafetyInfoAsked());
  }

  @override
  void markBannerAsRead(IBannerModel bannerModel) {
    _bloc.add(BannerRead(bannerModel));
  }

  void _onAccountTapped(Account account) {
    Analytics().logEvent(OpenAccountDetailsEvent());
    final result = ExtendedNavigator.ofRouter<Router>().pushNamed(
      Routes.accountDetailsScreen,
      arguments: AccountDetailsScreenArguments(account: account),
    );

    if (result != null) {
      if (result is ConnectionsSynchronized) {
        _bloc.add(Refresh());
      }
    }
  }

  void _showSecurityDisclaimer(String userId) async {
    Analytics().logEvent(ViewSecurityDisclaimerEvent());
    final result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
      Routes.bankSecurityDisclaimer,
      arguments: BankSecurityDisclaimerArguments(
          config: BankSecurityDisclaimerConfiguration.tokenIoProvider(userId)),
    );

    if (result != null) {
      if (result is BankSecurityDisclaimerAcceptedResult) {
        _bloc.add(ConnectAccount());
      }
    }
  }

  void _openBankWebViewForUri(UriModel uriModel) async {
    var result = await Navigator.push(
        context,
        PageRouteBuilder(
            fullscreenDialog: true,
            pageBuilder: (ctx, animation, secondaryAnimation) =>
                BankWebViewScreen(uriModel),
            transitionsBuilder: TransitionsBuilders.fadeIn));

    if (result is SuccessResult) {
      Analytics().logEvent(BankConnectedEvent());
      _bloc.add(
          AccountConnectionResultReceived(AccountConnectionResult.SUCCESS));
    } else if (result is FailedResult) {
      _bloc.add(AccountConnectionResultReceived(AccountConnectionResult.FAILED,
          errorMessage: result.error));
    } else {
      _bloc.add(
          AccountConnectionResultReceived(AccountConnectionResult.CANCELED));
    }
  }

  void _moveToEmailConfirmation(NeedToFillEmail state) {
    EmailConfirmationFlowController flowController =
        EmailConfirmationFlowController(EmailConfirmationFlow.SETTINGS,
            onEmailConfirmedCallback: _onEmailConfirmed,
            onEmailConfirmationCanceledCallback: _onSkipEmailConfirmation);

    ExtendedNavigator.ofRouter<Router>().pushNamed(
      Routes.emailConfirmationRequestScreen,
      arguments: EmailConfirmationRequestScreenArguments(
        flowController: flowController,
        configuration: EmailConfiguration.Accounts(email: state.email),
      ),
    );
  }

  void _onEmailConfirmed() {
    _bloc.add(ConnectAccount());
  }

  void _onSkipEmailConfirmation() {
    ExtendedNavigator.ofRouter<Router>().pop();
    _bloc.add(Refresh());
  }

  void _errorMessageSnackBar(BuildContext context) {
    Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          content: Text(Localization.of(context).somethingWentWrongPlaceholder,
              style: FontBook.textCaption.copyWith(color: Colors.white)),
          backgroundColor: Palette.red,
        ),
      );
  }

  @override
  void onAppear() {
    Analytics().logEvent(OpenAccountsTabEvent());
    _bloc?.add(SilentUpdate());
  }
}
