import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/models/api/api/banking/request/connection_sync_model.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/api/transactions/transactions.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/models/api/error/error.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:bloc/bloc.dart';
import 'bloc.dart';

class AccountDetailsBloc
    extends Bloc<AccountDetailsEvent, AccountDetailsState> {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();

  Account _account;
  List<Transaction> _cachedTransactions;

  Loaded get _cachedState =>
      Loaded(_account, transactions: _cachedTransactions);

  AccountDetailsBloc(this._account);

  @override
  AccountDetailsState get initialState => Loaded(_account, isLoading: true);

  @override
  Stream<AccountDetailsState> mapEventToState(
      AccountDetailsEvent event) async* {
    if (event is Started) {
      yield* _mapStartedToState();
    }
    if (event is SyncAccount) {
      yield* _mapSyncAccountToState();
    }
    if (event is Reload) {
      yield* _mapReloadToState(event.isTransactionsOnly);
    }
    if (event is ConnectionSyncResultReceived) {
      yield* _mapConnectionSyncResultReceivedToState(event);
    }
  }

  Stream<AccountDetailsState> _mapStartedToState() async* {
    yield Loaded(_account, isLoading: true);
    yield* _getTransactions();
  }

  Stream<AccountDetailsState> _mapReloadToState(
      bool isTransactionsOnly) async* {
    if (isTransactionsOnly) {
      yield* _getTransactions();
    } else {
      yield* _refresh();
    }
  }

  Stream<AccountDetailsState> _mapSyncAccountToState() async* {
    yield _cachedState.copyWith(isSyncing: true);
    BaseResponse<UriModel> syncConnectionResponse = await _apiRemoteRepository
        .syncConnection(ConnectionSyncModel(accountId: _account.id));
    if (syncConnectionResponse.isSuccess()) {
      yield ManualSyncConnection(syncConnectionResponse.result);
    } else {
      yield ErrorState(syncConnectionResponse.error);
      yield _cachedState;
    }
  }

  Stream<AccountDetailsState> _refresh() async* {
    if (_account == null) {
      yield Loading();
    } else {
      yield _cachedState.copyWith(isLoading: true);
    }

    final state = await _getAccount();

    if (state is Loaded) {
      yield state.copyWith(isLoading: true);
      yield* _getTransactions();
    } else {
      yield state;
    }
  }

  Stream<AccountDetailsState> _mapConnectionSyncResultReceivedToState(
      ConnectionSyncResultReceived event) async* {
    switch (event.result) {
      case ConnectionSyncResult.SUCCESS:
        _cachedTransactions = null;
        yield* _refresh();
        break;
      case ConnectionSyncResult.FAILED:
      case ConnectionSyncResult.CANCELED:
        yield _cachedState;
    }
  }

  Stream<AccountDetailsState> _getTransactions() async* {
    BaseResponse<Transactions> transactionsResponse =
        await _apiRemoteRepository.getTransactions(accountIds: [_account.id]);
    if (transactionsResponse.isSuccess()) {
      _cachedTransactions = transactionsResponse.result.transactions ?? [];
      yield _cachedState;
    } else {
      yield _cachedState;
    }
  }

  Future<AccountDetailsState> _getAccount() async {
    BaseResponse<Accounts> accountsResponse =
        await _apiRemoteRepository.getBankAccounts();
    if (accountsResponse.isSuccess()) {
      _account = accountsResponse.result.accounts
          .firstWhere((anAccount) => _account.id == anAccount.id);
      return _cachedState;
    } else {
      return ErrorState(accountsResponse.error);
    }
  }
}
