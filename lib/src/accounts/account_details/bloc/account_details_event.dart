
import 'package:equatable/equatable.dart';

abstract class AccountDetailsEvent extends Equatable {
  AccountDetailsEvent();
}

class Started extends AccountDetailsEvent {
  Started();

  @override
  List<Object> get props => null;
}

class AccountSyncStarted extends AccountDetailsEvent {
  AccountSyncStarted();

  @override
  List<Object> get props => null;
}

class SyncAccount extends AccountDetailsEvent {
  SyncAccount();

  @override
  List<Object> get props => null;
}

class Reload extends AccountDetailsEvent {
  final bool isTransactionsOnly;
  Reload({this.isTransactionsOnly = false});

  @override
  List<Object> get props => [isTransactionsOnly];
}

enum ConnectionSyncResult { SUCCESS, FAILED, CANCELED }

class ConnectionSyncResultReceived extends AccountDetailsEvent {
  final ConnectionSyncResult result;
  final String errorMessage;

  ConnectionSyncResultReceived(this.result, {this.errorMessage});

  @override
  List<Object> get props => [result, errorMessage];
}