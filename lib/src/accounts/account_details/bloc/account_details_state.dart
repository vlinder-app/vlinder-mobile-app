import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/models/api/api/banking/response/connections.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/error/error.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class AccountDetailsState extends Equatable {
  AccountDetailsState();
}

@immutable
class Loading extends AccountDetailsState {
  Loading();

  @override
  List<Object> get props => null;
}

@immutable
class Loaded extends AccountDetailsState {
  final Account account;
  final bool isLoading;
  final bool isSyncing;
  final List<Transaction> transactions;

  Loaded(
    this.account, {
    this.transactions,
    this.isLoading = false,
    this.isSyncing = false,
  });

  Loaded copyWith(
          {Account account,
          List<Transaction> transactions,
          bool isLoading,
          bool isSyncing}) =>
      Loaded(
        account ?? this.account,
        transactions: transactions ?? this.transactions,
        isLoading: isLoading ?? this.isLoading,
        isSyncing: isSyncing ?? this.isSyncing,
      );

  @override
  List<Object> get props => [account, transactions, isLoading, isSyncing];
}

@immutable
class ManualSyncConnection extends AccountDetailsState {
  final UriModel uriModel;

  ManualSyncConnection(this.uriModel);

  @override
  List<Object> get props => [uriModel];
}

@immutable
class ErrorState extends AccountDetailsState {
  final Error error;

  ErrorState(this.error);

  @override
  List<Object> get props => [error];
}
