import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/accounts/account_details/widgets/account_details_cell.dart';
import 'package:atoma_cash/src/accounts/account_details/widgets/account_details_transaction_cell.dart';
import 'package:atoma_cash/src/accounts/sync/result/connections_sync_result.dart';
import 'package:atoma_cash/src/accounts/widgets/list/cells/accounts_error_placeholder.dart';
import 'package:atoma_cash/src/activity/widgets/listview/group_separator.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/close_account_details_event.dart';
import 'package:atoma_cash/src/analytics/models/events/select_bank_event.dart';
import 'package:atoma_cash/src/bank/webview/bank_webview.dart';
import 'package:atoma_cash/src/bank/webview/custom_app_bar.dart';
import 'package:atoma_cash/src/bank/webview/webview_result/webview_result.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/src/transaction/transaction_details/transaction_details_screen.dart';
import 'package:atoma_cash/src/transaction/transaction_operations/transaction_operations_screen.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grouped_list/grouped_list.dart';

import 'bloc/bloc.dart';

class AccountDetailsScreen extends StatefulWidget {
  final Account account;

  AccountDetailsScreen(this.account, {Key key}) : super(key: key);

  @override
  _AccountDetailsScreenState createState() => _AccountDetailsScreenState();
}

class _AccountDetailsScreenState extends State<AccountDetailsScreen> {
  AccountDetailsBloc _bloc;
  ConnectionsSyncResult _connectionsSyncResult;

  @override
  void initState() {
    _bloc = AccountDetailsBloc(widget.account)..add(Started());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: ModalStyleAppBar(
        PreferredSize(
            preferredSize: const Size.fromHeight(0),
            child: BlocBuilder(
              bloc: _bloc,
              condition: _appBarUpdateCondition,
              builder: _appBar,
            )),
        statusBarBackgroundColor: Colors.transparent,
      ),
      body: Container(
        color: Colors.white,
        child: BlocListener(
          bloc: _bloc,
          listener: _blocListener,
          child: BlocBuilder(
            bloc: _bloc,
            condition: _contentBuilderCondition,
            builder: _contentBuilder,
          ),
        ),
      ),
    );
  }

  Widget _appBar(BuildContext context, AccountDetailsState state) {
    return AppBar(
      backgroundColor: Colors.transparent,
      leading: IconButton(
        icon: Icon(Icons.close),
        onPressed: _close,
      ),
      actions: [_appBarSyncWidget(context, state)],
    );
  }

  Widget _appBarSyncWidget(BuildContext context, AccountDetailsState state) {
    if (state is Loaded && state.isSyncing) {
      return Padding(
        padding: const EdgeInsets.only(right: 22.0),
        child: Container(
          child: Center(
            child: Container(
              height: 20,
              width: 20,
              child: Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                ),
              ),
            ),
          ),
        ),
      );
    } else {
      return Padding(
        padding: const EdgeInsets.only(right: 8.0),
        child: IconButton(
          icon: Image.asset(
            Assets.updateIcon,
            color: Palette.darkGrey,
          ),
          onPressed: _onSyncClicked,
        ),
      );
    }
  }

  void _blocListener(BuildContext context, AccountDetailsState state) {
    if (state is ManualSyncConnection) {
      _openBankWebViewForUri(state.uriModel);
    }
    if (state is ErrorState) {
      if (state.error.code == 429) {
        _errorMessageSnackBar(context);
      }
    }
  }

  Widget _contentBuilder(BuildContext context, AccountDetailsState state) {
    if (state is Loading) {
      return _loadingStateBuilder(context);
    } else if (state is Loaded) {
      return _loadedWidgetState(context, state);
    } else if (state is ErrorState) {
      return _errorStatePlaceholder();
    } else {
      return Container();
    }
  }

  Widget _loadingStateBuilder(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _errorStatePlaceholder() {
    return Center(
      child: AccountsErrorPlaceholder(
        onRetryClicked: () => _bloc.add(Reload()),
      ),
    );
  }

  Widget _loadedWidgetState(BuildContext context, Loaded state) {
    return CustomScrollView(slivers: [
      SliverToBoxAdapter(
        child: Padding(
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          child: AccountDetailsCell(state.account),
        ),
      ),
      _transactionsBuilder(context, state)
    ]);
  }

  Widget _transactionsBuilder(BuildContext context, Loaded state) {
    if (state.isLoading) {
      return SliverFillRemaining(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );
    } else if (state.transactions?.isNotEmpty ?? false) {
      return SliverToBoxAdapter(
        child: _transactionListViewBuilder(context, state.transactions),
      );
    } else if (state.transactions == null) {
      return SliverFillRemaining(
        child: Center(
          child: AccountsErrorPlaceholder(
            onRetryClicked: _reloadTransactions,
          ),
        ),
      );
    } else {
      return SliverFillRemaining(
        child: Container(),
      );
    }
  }

  Widget _transactionListViewBuilder(
      BuildContext context, List<Transaction> transactions) {
    return GroupedListView<Transaction, DateTime>(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      separator: Divider(
        height: 0.0,
        color: Colors.transparent,
        thickness: 0.0,
      ),
      padding: EdgeInsets.only(top: 16.0),
      groupBy: _groupTrigger,
      elements: transactions,
      sort: false,
      groupSeparatorBuilder: (DateTime dateTime) => Padding(
        padding: const EdgeInsets.only(left: 24.0, right: 24.0),
        child: GroupSeparator(
            DateHelper.separatorStringForDateTime(context, dateTime)
                .toUpperCase()),
      ),
      itemBuilder: (context, transaction) {
        return AccountDetailsTransactionCell(
          transaction,
          onClick: () => _onTransactionClick(transaction),
        );
      },
    );
  }

  void _reloadTransactions() {
    _bloc.add(Reload(isTransactionsOnly: true));
  }

  void _onSyncClicked() {
    Analytics().logEvent(SelectBankEvent(widget.account.issuerName,
        widget.account.refreshedAt, Source.ACCOUNT_DETAILS));
    _bloc.add(SyncAccount());
  }

  void _openBankWebViewForUri(UriModel uriModel) async {
    var result = await Navigator.push(
      context,
      PageRouteBuilder(
          fullscreenDialog: true,
          pageBuilder: (ctx, animation, secondaryAnimation) =>
              BankWebViewScreen(uriModel),
          transitionsBuilder: TransitionsBuilders.fadeIn),
    );

    if (result is SuccessResult) {
      _connectionsSyncResult = ConnectionsSynchronized();
      _bloc.add(ConnectionSyncResultReceived(ConnectionSyncResult.SUCCESS));
    } else if (result is FailedResult) {
      _bloc.add(ConnectionSyncResultReceived(ConnectionSyncResult.FAILED));
    } else {
      _bloc.add(ConnectionSyncResultReceived(ConnectionSyncResult.CANCELED));
    }
  }

  void _onTransactionClick(Transaction transaction) {
    _showTransactionDetails(transaction);
  }

  void _showTransactionDetails(Transaction transaction) async {
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.transactionDetailsScreen,
        arguments: TransactionDetailsScreenArguments(transaction: transaction));
    _handleResultActions(result);
  }

  void _errorMessageSnackBar(BuildContext context) {
    Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          duration: Duration(seconds: 10),
          content: Text(Localization.of(context).accountSyncTimeoutMessage,
              style: FontBook.textBody2Menu.copyWith(color: Colors.white)),
          backgroundColor: Palette.darkGrey,
        ),
      );
  }

  void _handleResultActions(dynamic result) {
    if (result != null) {
      if (result is RemoveAction) {
        _reloadTransactions();
      }
      if (result is UpdateOperation) {
        _reloadTransactions();
      }
      if (result is EditOperation) {
        _reloadTransactions();
      }
    }
  }

  void _close() {
    Analytics().logEvent(CloseAccountDetailsEvent());
    ExtendedNavigator.ofRouter<Router>().pop(_connectionsSyncResult);
  }

  DateTime _groupTrigger(Transaction transaction) {
    return DateHelper.defaultGroupTrigger(transaction.bookedAt);
  }

  bool _contentBuilderCondition(
          AccountDetailsState previous, AccountDetailsState current) =>
      current is! ManualSyncConnection &&
      !(current is ErrorState && (previous is Loaded && previous.isSyncing));

  bool _appBarUpdateCondition(
          AccountDetailsState previous, AccountDetailsState current) =>
      current is! Loaded ||
      (previous is Loaded &&
          current is Loaded &&
          current.isSyncing != previous.isSyncing);
}
