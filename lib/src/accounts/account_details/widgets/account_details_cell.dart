import 'dart:math';

import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/accounts/widgets/list/cells/remote_icon.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:flutter/cupertino.dart';

class AccountDetailsCell extends StatelessWidget {
  static const int additionalAccountNumberLength = 4;
  static const double _iconSize = 72;
  final Account account;

  AccountDetailsCell(this.account);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _accountIcon(),
        Padding(
          padding: EdgeInsets.only(left: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 2.0),
                child: Text(
                  account.name,
                  style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 3.0),
                child: Text(
                  _subtitleString(),
                  style:
                      FontBook.textBody2Menu.copyWith(color: Palette.darkGrey),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 7.0),
                child: Container(
                  decoration: BoxDecoration(
                    color: Palette.paleGrey,
                    borderRadius: BorderRadius.all(Radius.circular(2.0)),
                  ),
                  child: Padding(
                    padding:
                        EdgeInsets.only(left: 4, top: 3, right: 4, bottom: 2),
                    child: Text(
                      DateHelper.updatedAtFormattedString(
                          context, account.refreshedAt),
                      style: FontBook.textCharts
                          .copyWith(color: Palette.mediumGrey),
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 1.0),
                child: Text(
                  account.balance.currencyFormatted(),
                  style: FontBook.headerH1.copyWith(color: Palette.darkGrey),
                ),
              ),
              _amountInBase(),
            ],
          ),
        )
      ],
    );
  }

  Widget _amountInBase() {
    if (account.balance.currency != account.baseCurrencyBalance?.currency) {
      return Container(
        margin: EdgeInsets.only(top: 4.0, bottom: 8.0),
        child: Center(
          child: Text(
            account.formattedBalanceInBase,
            style: FontBook.textBody2Menu.copyWith(color: Palette.mediumGrey),
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  Widget _accountIcon() {
    return RemoteIcon(
      account.issuerIconUrl,
      size: _iconSize,
      placeholderBuilder: (context) {
        return Container(
          color: Palette.blue,
          child: Center(
            child: Image.asset(Assets.bankIcon),
          ),
        );
      },
    );
  }

  String _subtitleString() {
    final number = _firstCardNumber() ?? account.publicId;
    if (number != null) {
      final trimmedNumber = number.substring(
          number.length - min(additionalAccountNumberLength, number.length));
      return "${account.issuerName} •• $trimmedNumber";
    } else {
      return account.issuerName;
    }
  }

  String _firstCardNumber() {
    if (account.cards?.isNotEmpty ?? false) {
      return account.cards.first;
    } else {
      return null;
    }
  }
}
