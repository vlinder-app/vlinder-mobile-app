import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/transaction/widgets/transaction_icon.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:flutter/material.dart';

class AccountDetailsTransactionCell extends StatelessWidget {
  final Transaction transaction;
  final VoidCallback onClick;

  AccountDetailsTransactionCell(this.transaction, {Key key, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: FourRowListTile(
        onClick: onClick,
        leading: TransactionIcon(transaction),
        title: Text(
          _title(context),
          style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
          overflow: TextOverflow.ellipsis,
          softWrap: false,
        ),
        subtitle: _subtitleWidget(),
        trailing: Text(
          transaction.baseOrMainAmountFormatted,
          style: FourRowListTile.titleTextStyle,
        ),
        subTrailing: Text(
          transaction.carbonFootprint != null
              ? CarbonHelper.carbonFootprintLocalizableValue(
                  context, transaction.carbonFootprint)
              : "",
          style: FourRowListTile.subtitleTextStyle,
          overflow: TextOverflow.ellipsis,
          softWrap: false,
        ),
      ),
    );
  }

  Widget _subtitleWidget() {
    if (transaction.categories?.isNotEmpty ?? false) {
      String categoriesNames =
          transaction.categories.map((category) => category.name).join(" / ");
      return Container(
        decoration: BoxDecoration(
            color: Palette.paleGrey,
            borderRadius: BorderRadius.all(Radius.circular(2.0))),
        child: Padding(
          padding: EdgeInsets.only(left: 6.0, top: 1.0, right: 6.0, bottom: 3),
          child: Text(
            categoriesNames,
            style: FontBook.textBody2Menu.copyWith(color: Palette.mediumGrey),
            softWrap: false,
            overflow: TextOverflow.ellipsis,
          ),
        ),
      );
    } else {
      return null;
    }
  }

  String _title(BuildContext context) {
    if (transaction.counterparty?.name != null) {
      return transaction.counterparty.name;
    } else {
      return Localization.of(context)
          .unknownMerchantTransactionTitlePlaceholder;
    }
  }
}
