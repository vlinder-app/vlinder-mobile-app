import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/models/api/api/accounts/accounts_types.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class AccountsGroupModel extends Equatable {
  final AccountType accountsType;
  final List<Account> accounts;
  final bool isExpanded;

  AccountsGroupModel(
      {@required this.accountsType,
      @required this.accounts,
      this.isExpanded = false});

  AccountsGroupModel copyWith(
      {AccountType accountsType, List<Account> accounts, bool isExpanded}) {
    return AccountsGroupModel(
      accountsType: accountsType ?? this.accountsType,
      accounts: accounts ?? this.accounts,
      isExpanded: isExpanded ?? this.isExpanded,
    );
  }

  AccountsGroupModel toggledExpand() => copyWith(isExpanded: !isExpanded);

  List<Object> get props => [isExpanded, accountsType, accounts];
}
