import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/banners/banner_model.dart';
import 'package:atoma_cash/models/api/error/error.dart';
import 'package:atoma_cash/src/accounts/bloc/accounts_group_model.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:notification_permissions/notification_permissions.dart';

@immutable
abstract class AccountsState extends Equatable {
  AccountsState();
}

@immutable
class Loading extends AccountsState {
  @override
  List<Object> get props => null;
}

@immutable
class Loaded extends AccountsState {
  final List<AccountsGroupModel> accountsGroups;
  final List<IBannerModel> banners;
  final bool isConnectingAccount;
  final String errorMessage;
  final PermissionStatus pushNotificationStatus;
  final Amount totalBaseCurrencyBalance;

  Loaded({
    this.accountsGroups,
    this.banners,
    this.totalBaseCurrencyBalance,
    this.errorMessage,
    this.pushNotificationStatus,
    this.isConnectingAccount = false,
  });

  Loaded copyWith({
    List<AccountsGroupModel> accountsGroups,
    List<IBannerModel> banners,
    Amount totalBaseCurrencyBalance,
    String errorMessage,
    PermissionStatus pushNotificationStatus,
    bool isConnectingAccount,
  }) {
    return Loaded(
      accountsGroups: accountsGroups ?? this.accountsGroups,
      banners: banners ?? this.banners,
      totalBaseCurrencyBalance:
      totalBaseCurrencyBalance ?? this.totalBaseCurrencyBalance,
      errorMessage: errorMessage ?? this.errorMessage,
      pushNotificationStatus: pushNotificationStatus ?? this.pushNotificationStatus,
      isConnectingAccount: isConnectingAccount ?? this.isConnectingAccount,
    );
  }

  @override
  List<Object> get props =>
      [
        accountsGroups,
        banners,
        isConnectingAccount,
        errorMessage,
        totalBaseCurrencyBalance,
        pushNotificationStatus,
      ];
}

@immutable
class ErrorState extends AccountsState {
  final Error error;

  ErrorState({this.error});

  @override
  List<Object> get props => [error];
}

@immutable
class NeedToFillEmail extends AccountsState {
  final String email;

  NeedToFillEmail(this.email);

  @override
  List<Object> get props => [email];
}

@immutable
class ManualAccountAuthentication extends AccountsState {
  final UriModel uriModel;

  ManualAccountAuthentication({this.uriModel});

  @override
  List<Object> get props => [uriModel];
}

@immutable
class ConnectionSafetyInfo extends AccountsState {
  final String userId;

  ConnectionSafetyInfo(this.userId);

  @override
  List<Object> get props => null;
}

@immutable
class Refreshing extends AccountsState {
  @override
  List<Object> get props => null;
}
