import 'package:atoma_cash/models/api/api/accounts/accounts_types.dart';
import 'package:atoma_cash/models/api/api/banners/banner_model.dart';
import 'package:equatable/equatable.dart';

abstract class AccountsEvent extends Equatable {
  const AccountsEvent();
}

class Started extends AccountsEvent {
  @override
  List<Object> get props => null;
}

enum AccountConnectionResult { SUCCESS, FAILED, CANCELED }

class AccountConnectionResultReceived extends AccountsEvent {
  final AccountConnectionResult result;
  final String errorMessage;

  AccountConnectionResultReceived(this.result, {this.errorMessage});

  @override
  List<Object> get props => [result, errorMessage];
}

class Refresh extends AccountsEvent {
  @override
  List<Object> get props => null;
}

class ConnectAccount extends AccountsEvent {
  @override
  List<Object> get props => null;
}

class AccountGroupTapped extends AccountsEvent {
  final AccountType type;

  AccountGroupTapped(this.type);

  @override
  List<Object> get props => [type];
}

class ConnectionSafetyInfoAsked extends AccountsEvent {
  @override
  List<Object> get props => null;
}

class SilentUpdate extends AccountsEvent {
  @override
  List<Object> get props => null;
}

class BannerRead extends AccountsEvent {
  final IBannerModel bannerModel;

  BannerRead(this.bannerModel);

  @override
  List<Object> get props => [bannerModel];
}

class BannersUpdated extends AccountsEvent {
  final List<IBannerModel> banners;

  BannersUpdated(this.banners);

  @override
  List<Object> get props => [banners];
}
