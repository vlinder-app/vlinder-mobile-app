import 'dart:async';

import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/models/api/api/accounts/accounts_types.dart';
import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/banners/banner_model.dart';
import 'package:atoma_cash/models/api/api/profile/response/profile.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/src/accounts/bloc/accounts_group_model.dart';
import 'package:atoma_cash/src/banners/data_provider/banners_data_provider.dart';
import 'package:atoma_cash/src/push_notifications/push_notification_service.dart';
import 'package:bloc/bloc.dart';
import "package:collection/collection.dart";

import 'bloc.dart';

class AccountsBloc extends Bloc<AccountsEvent, AccountsState>
    implements IBannersDataProviderUpdateListener {
  static const _otherType = 'Other';

  ApplicationLocalRepository _localRepository = ApplicationLocalRepository();
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  final IBannersDataProvider _bannersRepository;

  AccountsTypes _cachedAccountsTypes;
  Amount _totalBaseCurrencyBalance;
  List<AccountsGroupModel> _cachedGroups;
  List<IBannerModel> _cachedBanners;
  String _email;

  Loaded get _cachedLoadedState => Loaded(
        accountsGroups: _cachedGroups,
        banners: _cachedBanners,
        totalBaseCurrencyBalance: _totalBaseCurrencyBalance,
      );

  @override
  AccountsState get initialState => Loading();

  AccountsBloc({IBannersDataProvider bannersRepository})
      : assert(bannersRepository != null),
        _bannersRepository = bannersRepository {
    _bannersRepository.addListener(this);
  }

  @override
  Stream<AccountsState> mapEventToState(AccountsEvent event) async* {
    if (event is Started) {
      yield* _mapStartedToState();
    }
    if (event is Refresh) {
      yield* _mapRefreshToState();
    }
    if (event is AccountConnectionResultReceived) {
      yield* _mapAccountConnectionResultReceivedToState(event);
    }
    if (event is ConnectAccount) {
      yield* _mapConnectAccountToState();
    }
    if (event is AccountGroupTapped) {
      yield* _mapAccountGroupTappedToState(event.type);
    }
    if (event is ConnectionSafetyInfoAsked) {
      yield* _connectionSafetyInfoAskedToState();
    }
    if (event is SilentUpdate) {
      yield* _mapSilentUpdateToState();
    }
    if (event is BannerRead) {
      yield* _mapBannerReadToState(event.bannerModel);
    }
    if (event is BannersUpdated) {
      yield _cachedLoadedState;
    }
  }

  Stream<AccountsState> _mapStartedToState() async* {
    yield Loading();
    _bannersRepository.begin();
    yield* _getPushNotificationStatus();
  }

  Stream<AccountsState> _mapRefreshToState() async* {
    yield Loading();
    yield* _refreshData();
  }

  Stream<AccountsState> _refreshData() async* {
    yield Refreshing();
    _bannersRepository.refresh();
  }

  Stream<AccountsState> _mapAccountConnectionResultReceivedToState(
      AccountConnectionResultReceived event) async* {
    switch (event.result) {
      case AccountConnectionResult.SUCCESS:
        yield* _mapRefreshToState();
        break;
      case AccountConnectionResult.FAILED:
      case AccountConnectionResult.CANCELED:
        yield _cachedLoadedState;
        break;
    }
  }

  Stream<AccountsState> _connectionSafetyInfoAskedToState() async* {
    final userId = await _localRepository.getUserId();
    yield ConnectionSafetyInfo(userId);
    yield _cachedLoadedState;
  }

  Stream<AccountsState> _mapConnectAccountToState() async* {
    yield _cachedLoadedState.copyWith(isConnectingAccount: true);
    BaseResponse<UriModel> uriModelResponse =
        await _apiRemoteRepository.authorizeBankAccount();
    if (uriModelResponse.isSuccess()) {
      yield ManualAccountAuthentication(uriModel: uriModelResponse.result);
    } else if (uriModelResponse.error.code == 412) {
      String email = await _fetchEmail();
      yield NeedToFillEmail(email);
    } else {
      yield _cachedLoadedState;
    }
  }

  Stream<AccountsState> _mapAccountGroupTappedToState(
      AccountType accountType) async* {
    // We need to create new instance of list due to equation issue
    // for current and next states in bloc
    List<AccountsGroupModel> updatedGroups = _cachedGroups.toList();

    final cachedGroupIndex = updatedGroups?.indexWhere(
        (element) => element.accountsType.type == accountType.type);

    if (cachedGroupIndex != -1) {
      updatedGroups[cachedGroupIndex] =
          updatedGroups[cachedGroupIndex].toggledExpand();
    }

    yield _cachedLoadedState.copyWith(accountsGroups: updatedGroups);
    _cachedGroups = updatedGroups;
  }

  Stream<AccountsState> _mapSilentUpdateToState() async* {
    if (state is Loaded) {
      if (!(state as Loaded).isConnectingAccount) {
        yield* _refreshData();
      }
    }
  }

  Stream<AccountsState> _fetchAccounts() async* {
    BaseResponse<Accounts> accountsResponse =
        await _apiRemoteRepository.getBankAccounts();
    if (accountsResponse.isSuccess()) {
      yield* _accountsFetched(accountsModel: accountsResponse.result);
      _bannersRepository.refresh();
    } else {
      yield ErrorState(error: accountsResponse.error);
    }
  }

  Stream<AccountsState> _accountsFetched(
      {Accounts accountsModel, bool isUpdate}) async* {
    _totalBaseCurrencyBalance = accountsModel.totalBaseCurrencyBalance;
    _cachedGroups = _groupAccountsByTypes(accountsModel.accounts);
    yield _cachedLoadedState;
  }

  Stream<AccountsState> _mapBannerReadToState(IBannerModel model) async* {
    _bannersRepository.markBannerAsRead(model);
    yield _cachedLoadedState;
  }

  Stream<AccountsState> _getPushNotificationStatus() async* {
    final status = await PushNotificationService().checkServiceStatus();
    yield _cachedLoadedState.copyWith(pushNotificationStatus: status);
  }

  Future<String> _fetchEmail() async {
    if (_email != null) {
      return _email;
    } else {
      BaseResponse<Profile> profileResponse =
          await _apiRemoteRepository.getProfile();
      if (profileResponse.isSuccess()) {
        _email = profileResponse.result.email;
        return _email;
      } else {
        return Future.error(profileResponse.error);
      }
    }
  }

  List<AccountsGroupModel> _groupAccountsByTypes(List<Account> accounts) {
    if (accounts != null) {
      final Map<String, List<Account>> groupedAccountsByType =
          groupBy(accounts, (anAccount) => anAccount.type);

      final List<AccountsGroupModel> groups = _cachedAccountsTypes.types
          .where((element) => groupedAccountsByType.containsKey(element.type))
          .map(
            (oneType) =>
                _accountsGroupModelForType(oneType, groupedAccountsByType),
          )
          .toList();

      return _reorderAccountsGroups(groups);
    } else {
      return null;
    }
  }

  AccountsGroupModel _accountsGroupModelForType(
      AccountType type, Map<String, List<Account>> mappedAccounts) {
    final cachedGroup = _cachedGroups?.firstWhere(
        (element) => element.accountsType.type == type.type,
        orElse: () => null);

    if (cachedGroup != null) {
      return cachedGroup.copyWith(accounts: mappedAccounts[type.type]);
    } else {
      return AccountsGroupModel(
        accountsType: type,
        accounts: mappedAccounts[type.type],
      );
    }
  }

  List<AccountsGroupModel> _reorderAccountsGroups(
      List<AccountsGroupModel> groups) {
    final groupWithOtherType = groups.firstWhere(
        (groups) => groups.accountsType.type == _otherType,
        orElse: () => null);

    if (groupWithOtherType != null) {
      // Put group with 'Other' type to the and of the list
      return groups
        ..removeWhere((element) => element.accountsType.type == _otherType)
        ..add(groupWithOtherType);
    } else {
      return groups;
    }
  }

  @override
  Future<void> close() {
    _bannersRepository.removeListener(this);
    return super.close();
  }

  @override
  void bannersUpdated(List<IBannerModel> banners) {
    _cachedBanners = banners;
    add(BannersUpdated(banners));
  }
}
