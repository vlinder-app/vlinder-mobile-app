import 'package:atoma_cash/models/api/api/banners/banner_model.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/src/banners/data_provider/banners_data_provider.dart';
import 'package:atoma_cash/src/banners/data_provider/local_banner_models/failed_receipt_banner_model.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:atoma_cash/src/pending_receipts/models/receipt_image_event_type.dart';
import 'package:atoma_cash/src/pending_receipts/pending_receipts_controller.dart';

class AccountsBannersDataProvider
    implements IBannersDataProvider, IReceiptUpdateListener {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  PendingReceiptController _pendingReceiptController =
      PendingReceiptController();

  List<IBannersDataProviderUpdateListener> _listeners = [];
  List<String> _ignoredFailedReceipts = [];
  FailedReceiptBannerModel _failedReceiptBanner;
  List<BannerModel> _remoteBanners;

  @override
  void begin() async {
    _pendingReceiptController.addListener(this);
    refresh();
  }

  @override
  void refresh() async {
    final bannersResponse = await _apiRemoteRepository.getBanners();
    if (bannersResponse.isSuccess()) {
      _notifyListeners(bannersResponse.result.banners, _failedReceiptBanner);
    }
  }

  @override
  void markBannerAsRead(IBannerModel bannerModel) {
    if (bannerModel is FailedReceiptBannerModel) {
      _markFailedReceiptBannerAsRead(bannerModel);
    } else {
      _markRemoteBannerAsRead(bannerModel);
    }
  }

  void _markFailedReceiptBannerAsRead(FailedReceiptBannerModel bannerModel) {
    _ignoredFailedReceipts
        .addAll(bannerModel.failedReceiptsIds.map((id) => id));
    _notifyListeners(_remoteBanners, null);
  }

  void _markRemoteBannerAsRead(BannerModel bannerModel) {
    final bannerIndex = _remoteBanners.indexOf(bannerModel);
    if (bannerIndex >= 0) {
      final banners = _remoteBanners.toList()
        ..[bannerIndex] = bannerModel.copyWith(readAt: DateTime.now().toUtc());
      _apiRemoteRepository.markBannerAsRead(bannerModel.id).then((response) {
        if (response.isSuccess()) {
          return true;
        } else {
          throw response.error;
        }
      });
      _notifyListeners(banners, _failedReceiptBanner);
    }
  }

  @override
  void addListener(IBannersDataProviderUpdateListener listener) =>
      _listeners.add(listener);

  @override
  void removeListener(IBannersDataProviderUpdateListener listener) =>
      _listeners.remove(listener);

  void _notifyListeners(List<BannerModel> remoteBanners,
      FailedReceiptBannerModel failedReceiptBannerModel) {
    List<IBannerModel> bannerList = [];

    if (failedReceiptBannerModel != null) {
      bannerList.add(failedReceiptBannerModel);
    }

    if (remoteBanners != null) {
      bannerList.addAll(remoteBanners);
    }

    _listeners.forEach((listener) => listener.bannersUpdated(bannerList));

    _failedReceiptBanner = failedReceiptBannerModel;
    _remoteBanners = remoteBanners;
  }

  void dispose() {
    _pendingReceiptController.removeListener(this);
  }

  @override
  void onReceiptsUpdate(List<PendingReceipt> receipts) {
    if (receipts?.isNotEmpty ?? false) {
      _handleFailedReceipts(receipts);
    } else {
      _failedReceiptBanner = null;
      _ignoredFailedReceipts = [];
    }
  }

  @override
  void onSuccessTransaction(Transaction transaction, String guid) {}

  void _handleFailedReceipts(List<PendingReceipt> receipts) {
    final failedReceipts = receipts
        .where((receipt) {
          return !_ignoredFailedReceipts.contains(receipt.id) &&
              (receipt.type == ReceiptImageEventType.RecognitionFailed ||
                  receipt.type == ReceiptImageEventType.UploadingFailed ||
                  receipt.type == ReceiptImageEventType.PartiallyRecognized);
        })
        .map((receipt) => receipt.id)
        .toSet();

    if (failedReceipts.isNotEmpty) {
      if (_failedReceiptBanner == null) {
        _notifyListeners(_remoteBanners,
            FailedReceiptBannerModel.fromFailedReceiptsIds(failedReceipts));
      } else {
        _notifyListeners(_remoteBanners,
            _failedReceiptBanner.copyWith(failedReceiptsIds: failedReceipts));
      }
    } else {
      _notifyListeners(_remoteBanners, null);
    }
  }
}
