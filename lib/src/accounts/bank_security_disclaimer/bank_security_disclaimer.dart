import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/accounts/bank_security_disclaimer/bank_security_disclaimer_result.dart';
import 'package:atoma_cash/src/accounts/bank_security_disclaimer/bank_security_disclamer_config.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/ask_security_question_event.dart';
import 'package:atoma_cash/src/analytics/models/events/connect_account_event.dart';
import 'package:atoma_cash/src/bank/webview/custom_app_bar.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/utils/launch_url/launch_url_helper.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class BankSecurityDisclaimer extends StatelessWidget {
  static const double _imageSize = 236.0;
  final BankSecurityDisclaimerConfiguration config;

  BankSecurityDisclaimer(this.config);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: _appBar(context),
      body: Container(
        color: Colors.white,
        child: _rootContentBuilder(context),
      ),
    );
  }

  Widget _appBar(BuildContext context) {
    return ModalStyleAppBar(
      AppBar(
        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: _close,
        ),
      ),
      statusBarBackgroundColor: Colors.transparent,
    );
  }

  Widget _rootContentBuilder(BuildContext context) {
    return SingleChildScrollView(
      child: SafeArea(
        child: Padding(
          padding: EdgeInsets.only(left: 24, right: 24),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _imageWidget(),
              _titleWidget(context),
              _descriptionWidget(context),
              for (String title in _pointTitles(context)) _infoItem(title),
              _connectAccountButton(context),
              _bottomWidget(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _imageWidget() {
    return Padding(
      padding: const EdgeInsets.only(top: 24),
      child: Container(
        width: _imageSize,
        height: _imageSize,
        child: Image.asset(Assets.bankSecurityDisclaimerImage),
      ),
    );
  }

  Widget _titleWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 14),
      child: Text(
        Localization.of(context).connectFirstBankSecurityTitle,
        textAlign: TextAlign.center,
        style: FontBook.headerH3.copyWith(color: Palette.darkGrey),
        softWrap: true,
      ),
    );
  }

  Widget _descriptionWidget(BuildContext context) {
    final descriptionTextStyle =
        FontBook.textBody1.copyWith(color: Palette.darkGrey);
    return Padding(
      padding: const EdgeInsets.only(top: 14.0),
      child: RichText(
        softWrap: true,
        text: TextSpan(children: <InlineSpan>[
          TextSpan(
              text: Localization.of(context)
                  .bankSecurityDisclaimerDescriptionFirstPart,
              style: descriptionTextStyle),
          TextSpan(
              text: " ${config.bankProviderName} ",
              style: FontBook.textButton.copyWith(color: Palette.blue),
              recognizer: TapGestureRecognizer()
                ..onTap = _onDescriptionLinkTapped),
          TextSpan(
              text: Localization.of(context)
                  .bankSecurityDisclaimerDescriptionSecondPart,
              style: descriptionTextStyle)
        ]),
      ),
    );
  }

  Widget _infoItem(String text) {
    return Padding(
      padding: const EdgeInsets.only(top: 36.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: Image.asset(Assets.bankSecurityDisclaimerCheckmark),
          ),
          Flexible(
            child: Text(
              text,
              softWrap: true,
              style: FontBook.textBody1.copyWith(color: Palette.darkGrey),
            ),
          )
        ],
      ),
    );
  }

  Widget _connectAccountButton(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 36),
      child: ClipRRect(
        borderRadius: const BorderRadius.all(Radius.circular(12)),
        child: Container(
          height: 52.0,
          child: SizedBox.expand(
            child: FlatButton(
                color: Palette.blue,
                child: Text(
                  Localization.of(context).connectFirstBankSecurityButton,
                  style: FontBook.textButton.copyWith(color: Colors.white),
                ),
                onPressed: _onConnectAccountTapped),
          ),
        ),
      ),
    );
  }

  Widget _bottomWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 24, bottom: 24),
      child: Container(
        height: 52.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Flexible(
              child: RichText(
                textAlign: TextAlign.center,
                softWrap: true,
                text: TextSpan(
                  children: <InlineSpan>[
                    TextSpan(
                        text: Localization.of(context)
                            .bankSecurityDisclaimerStillNotSure,
                        style: FontBook.textButton
                            .copyWith(color: Palette.darkGrey)),
                    TextSpan(
                        text: " ${Localization.of(context).askQuestion}",
                        style:
                            FontBook.textButton.copyWith(color: Palette.blue),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            _onAskQuestionTapped(context);
                          }),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  List<String> _pointTitles(BuildContext context) => [
        Localization.of(context).connectFirstBankSecurityListItem1,
        Localization.of(context).connectFirstBankSecurityListItem2,
        Localization.of(context).connectFirstBankSecurityListItem3
      ];

  void _onDescriptionLinkTapped() {
    LaunchUrlHelper.openLink(config.bankProviderUrl);
  }

  void _onAskQuestionTapped(BuildContext context) {
    Analytics().logEvent(AskSecurityQuestionEvent());
    LaunchUrlHelper.openEmailClient(context,
        contentBuilder: config.askQuestionMailtoContentBuilder);
  }

  void _close() {
    ExtendedNavigator.ofRouter<Router>().pop();
  }

  void _onConnectAccountTapped() {
    Analytics().logEvent(ConnectAccountEvent());
    ExtendedNavigator.ofRouter<Router>()
        .pop(BankSecurityDisclaimerAcceptedResult());
  }
}
