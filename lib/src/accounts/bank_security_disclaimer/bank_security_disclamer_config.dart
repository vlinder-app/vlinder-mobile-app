import 'package:atoma_cash/resources/config/links.dart';
import 'package:atoma_cash/utils/launch_url/email_client_content_builder.dart';
import 'package:flutter/material.dart';

class BankSecurityDisclaimerConfiguration {
  static const String _email = "info@vlinder.app";

  final String bankProviderName;
  final String bankProviderUrl;
  final IMailtoContentBuilder askQuestionMailtoContentBuilder;

  BankSecurityDisclaimerConfiguration({
    @required this.bankProviderName,
    @required this.bankProviderUrl,
    @required this.askQuestionMailtoContentBuilder,
  });

  factory BankSecurityDisclaimerConfiguration.tokenIoProvider(String userId) {
    return BankSecurityDisclaimerConfiguration(
      bankProviderName: 'Token.io',
      bankProviderUrl: Links.tokenIoWebsiteLink,
      askQuestionMailtoContentBuilder: DefaultMailtoContentBuilder(
        email: _email,
        subject: 'Question',
        userId: userId,
      ),
    );
  }
}
