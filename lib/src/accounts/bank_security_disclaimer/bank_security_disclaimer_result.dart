abstract class BankSecurityDisclaimerResult {
  const BankSecurityDisclaimerResult();
}

class BankSecurityDisclaimerAcceptedResult
    extends BankSecurityDisclaimerResult {
  const BankSecurityDisclaimerAcceptedResult();
}
