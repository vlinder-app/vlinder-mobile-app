import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/material.dart';

class AccountsErrorPlaceholder extends StatelessWidget {
  final VoidCallback onRetryClicked;
  final Color descriptionColor;

  AccountsErrorPlaceholder({this.onRetryClicked, this.descriptionColor});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          Localization.of(context).somethingWentWrongPlaceholder,
          style: FontBook.textBody2Menu
              .copyWith(color: descriptionColor ?? Palette.darkGrey),
          textAlign: TextAlign.center,
        ),
        if (onRetryClicked != null)
          Padding(
            padding: EdgeInsets.only(top: 30),
            child: RaisedButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(12.0)),
              child: Text(
                Localization.of(context).retryButton,
                style: FontBook.textButton.copyWith(color: Palette.blue),
              ),
              onPressed: onRetryClicked,
            ),
          ),
      ],
    );
  }
}
