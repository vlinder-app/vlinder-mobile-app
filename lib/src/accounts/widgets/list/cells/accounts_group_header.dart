import 'package:atoma_cash/models/api/api/accounts/accounts_types.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class AccountsGroupHeader extends StatelessWidget {
  static const double _iconSize = 24.0;
  static const double _iconContainerSize = 52.0;
  static const double _iconContainerVerticalPadding = 8.0;
  static const double height =
      _iconContainerSize + _iconContainerVerticalPadding * 2;

  final AccountType accountType;
  final int numberOfItems;
  final double chevronAngle;
  final VoidCallback onTap;

  const AccountsGroupHeader({
    Key key,
    this.accountType,
    this.numberOfItems,
    this.chevronAngle,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        child: InkWell(
          onTap: onTap,
          child: Row(
            children: [
              _iconWidget(),
              _bodyWidget(context),
              Padding(
                padding: const EdgeInsets.only(right: 14.0),
                child: Center(
                  child: Transform.rotate(
                    angle: chevronAngle,
                    child: Icon(Icons.keyboard_arrow_down),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _iconWidget() {
    return Padding(
      padding: const EdgeInsets.only(
        left: 12,
        top: _iconContainerVerticalPadding,
        right: 16,
        bottom: _iconContainerVerticalPadding,
      ),
      child: ClipOval(
        child: Container(
          width: _iconContainerSize,
          height: _iconContainerSize,
          color: Palette.paleGrey,
          child: Center(
            child: CachedNetworkImage(
              width: _iconSize,
              height: _iconSize,
              imageUrl: accountType.iconUrl,
            ),
          ),
        ),
      ),
    );
  }

  Widget _bodyWidget(BuildContext context) {
    return Expanded(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Flexible(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  accountType.name,
                  style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 3.0),
                  child: Text(
                    Localization.of(context)
                        .accountsGroupHeaderSubtitle('$numberOfItems'),
                    style: FontBook.textBody2Menu
                        .copyWith(color: Palette.mediumGrey),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
