import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/accounts/widgets/list/cells/remote_icon.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef OnAccountClick = Function(Account);

class AccountCell extends StatelessWidget {
  static const double kContentHeight = FourRowListTile.threeRowHeight;

  final Account account;
  final OnAccountClick onClick;

  AccountCell(this.account, {this.onClick});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: FourRowListTile(
        onClick: _onClick,
        title: Text(
          account.name,
          style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
          overflow: TextOverflow.ellipsis,
          softWrap: false,
        ),
        leading: _leading(),
        subtitle: Text(
          account.issuerName,
          style: FontBook.textBody2Menu.copyWith(color: Palette.mediumGrey),
          overflow: TextOverflow.ellipsis,
          softWrap: false,
        ),
        trailing: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text(
              account.balance.currencyFormatted(),
              style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
            ),
            if (account.balance.currency !=
                account.baseCurrencyBalance?.currency)
              Padding(
                padding: const EdgeInsets.only(top: 5.0),
                child: Text(
                  account.formattedBalanceInBase,
                  style: FontBook.textBody2Menu
                      .copyWith(color: Palette.mediumGrey),
                  overflow: TextOverflow.ellipsis,
                  softWrap: false,
                ),
              ),
          ],
        ),
        bottom: Text(
          DateHelper.updatedAtFormattedString(context, account.refreshedAt),
          style: FontBook.textCharts.copyWith(color: Palette.mediumGrey),
          overflow: TextOverflow.ellipsis,
          softWrap: false,
        ),
        contentPadding:
            const EdgeInsets.only(left: 12, top: 10, right: 12, bottom: 10),
      ),
    );
  }

  Widget _leading() {
    return RemoteIcon(
      account.issuerIconUrl,
      placeholderBuilder: (context) {
        return Container(
          color: Palette.blue,
          child: Center(
            child: Image.asset(Assets.bankIcon),
          ),
        );
      },
    );
  }

  void _onClick() {
    if (onClick != null) {
      onClick(account);
    }
  }
}
