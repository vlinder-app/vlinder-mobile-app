import 'dart:math';

import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/models/api/api/accounts/accounts_types.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/accounts/bloc/accounts_group_model.dart';
import 'package:atoma_cash/src/accounts/widgets/list/cells/account_cell.dart';
import 'package:atoma_cash/src/accounts/widgets/list/cells/accounts_group_header.dart';
import 'package:flutter/material.dart';

typedef AccountsTypedGroupTapped = Function(AccountType accountType);

class AccountsTypedGroupItem extends StatefulWidget {
  final AccountsGroupModel model;
  final OnAccountClick onAccountClick;
  final bool initiallyExpanded;

  final AccountsTypedGroupTapped onGroupTapped;

  AccountsTypedGroupItem(this.model,
      {Key key,
      this.onAccountClick,
      this.initiallyExpanded = false,
      this.onGroupTapped})
      : assert(initiallyExpanded != null),
        super(key: key);

  @override
  _AccountsTypedGroupItemState createState() => _AccountsTypedGroupItemState();
}

class _AccountsTypedGroupItemState extends State<AccountsTypedGroupItem>
    with SingleTickerProviderStateMixin {
  static final Animatable<double> _easeInTween =
      CurveTween(curve: Curves.easeIn);
  static const _expandDuration = Duration(milliseconds: 200);

  static const _borderRadius = BorderRadius.all(Radius.circular(12.0));

  AnimationController _controller;
  Animation<EdgeInsetsGeometry> _additionalPadding;
  Animation<double> _childrenHeightFactor;
  Animation<double> _headerChevronAngle;
  Animation<double> _firstShadowOpacity;
  Animation<double> _secondShadowOpacity;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(duration: _expandDuration, vsync: this);
    _childrenHeightFactor = _controller.drive(_easeInTween);
    _headerChevronAngle = Tween<double>(
      begin: 0.0,
      end: pi,
    ).animate(_controller);

    _firstShadowOpacity =
        Tween<double>(begin: 0.0, end: 0.08).animate(_controller);
    _secondShadowOpacity =
        Tween<double>(begin: 0.0, end: 0.12).animate(_controller);

    _additionalPadding = EdgeInsetsGeometryTween(
      begin: EdgeInsets.zero,
      end: EdgeInsets.only(bottom: 4),
    ).animate(_controller);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _handleTap() {
    widget.onGroupTapped(widget.model.accountsType);
  }

  @override
  Widget build(BuildContext context) {
    final closed = !widget.model.isExpanded && _controller.isDismissed;

    if (widget.model.isExpanded) {
      _controller.forward();
    } else {
      _controller.reverse();
    }

    return AnimatedBuilder(
      animation: _controller.view,
      builder: _buildHeaderWithChildren,
      child: closed
          ? null
          : _ExpandedTypedAccounts(
              accounts: widget.model.accounts,
              onAccountClick: widget.onAccountClick,
            ),
    );
  }

  Widget _buildHeaderWithChildren(BuildContext context, Widget child) {
    return Padding(
      padding: _additionalPadding.value,
      child: Container(
        decoration: BoxDecoration(
          color: Palette.white,
          borderRadius: _borderRadius,
          boxShadow: _shadows(),
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            ClipRRect(
              borderRadius: _borderRadius,
              child: AccountsGroupHeader(
                chevronAngle: _headerChevronAngle.value,
                accountType: widget.model.accountsType,
                numberOfItems: widget.model.accounts.length,
                onTap: _handleTap,
              ),
            ),
            ClipRect(
              child: Align(
                heightFactor: _childrenHeightFactor.value,
                child: child,
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<BoxShadow> _shadows() {
    return <BoxShadow>[
      BoxShadow(
        offset: const Offset(0, 6),
        blurRadius: 16,
        color: Palette.mediumGrey.withOpacity(_firstShadowOpacity.value),
      ),
      BoxShadow(
        offset: const Offset(0, 4),
        blurRadius: 8,
        color: Palette.mediumGrey.withOpacity(_secondShadowOpacity.value),
      ),
    ];
  }
}

class _ExpandedTypedAccounts extends StatelessWidget {
  final List<Account> accounts;
  final OnAccountClick onAccountClick;

  const _ExpandedTypedAccounts({
    Key key,
    this.accounts,
    this.onAccountClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _topSeparatorWidget(),
        for (final anAccount in accounts)
          AccountCell(
            anAccount,
            onClick: onAccountClick,
          ),

        const SizedBox(height: 8), // Extra space below.
      ],
    );
  }

  Widget _topSeparatorWidget() {
    return Padding(
      padding: EdgeInsets.only(left: 20, top: 7, right: 20, bottom: 8),
      child: Container(
        height: 1,
        color: Palette.paleGrey,
      ),
    );
  }
}
