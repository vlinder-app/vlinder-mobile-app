import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

typedef RemoteIconPlaceholderBuilder = Widget Function(BuildContext context);

class RemoteIcon extends StatelessWidget {
  static const double defaultSize = 54.0;
  static const String svgExtensionCheckPattern =
      r"([a-zA-Z0-9\s_\\.\-\(\):])+(.svg)";
  final String iconUrl;
  final RemoteIconPlaceholderBuilder placeholderBuilder;
  final double size;

  RemoteIcon(this.iconUrl, {this.placeholderBuilder, double size})
      : this.size = size ?? defaultSize;

  @override
  Widget build(BuildContext context) => ClipOval(
        child: Container(
          width: size,
          height: size,
          child: _iconWidget(context),
        ),
      );

  Widget _iconWidget(BuildContext context) {
    if (iconUrl == null) {
      return _placeholderWidget(context);
    } else if (_isSvgImage()) {
      return _svgNetworkIcon();
    } else {
      return _networkIcon();
    }
  }

  Widget _svgNetworkIcon() {
    return SvgPicture.network(
      iconUrl,
      fit: BoxFit.cover,
      placeholderBuilder: _placeholderWidget,
    );
  }

  Widget _networkIcon() {
    return CachedNetworkImage(
      imageUrl: iconUrl,
      fit: BoxFit.cover,
      fadeOutDuration: Duration(milliseconds: 200),
      placeholder: (context, _) => _placeholderWidget(context),
      errorWidget: (context, url, error) => _placeholderWidget(context),
    );
  }

  bool _isSvgImage() {
    if (iconUrl == null) {
      return false;
    }
    final regex = RegExp(
      svgExtensionCheckPattern,
      caseSensitive: false,
    );
    return regex.firstMatch(iconUrl) != null;
  }

  Widget _placeholderWidget(BuildContext context) {
    if (placeholderBuilder != null) {
      return placeholderBuilder(context);
    } else {
      return Container();
    }
  }
}
