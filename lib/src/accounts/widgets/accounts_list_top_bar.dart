import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class AccountsListTopBar extends StatelessWidget {
  static const double kHeight = kToolbarHeight;

  final Widget leading;
  final VoidCallback onSyncTapped;
  final bool isSyncAvailable;

  const AccountsListTopBar(this.leading,
      {this.isSyncAvailable, this.onSyncTapped});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: kHeight,
      color: Palette.white,
      child: Row(
        children: <Widget>[
          if (leading != null)
            Padding(
              padding: EdgeInsets.only(left: 24),
              child: leading,
            ),
          _trailingWidget(),
        ],
      ),
    );
  }

  Widget _trailingWidget() {
    if (isSyncAvailable ?? false) {
      return Expanded(
        child: Align(
          alignment: Alignment.centerRight,
          child: Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: IconButton(
              icon: Image.asset(
                Assets.updateIcon,
                color: Palette.darkGrey,
              ),
              onPressed: onSyncTapped,
            ),
          ),
        ),
      );
    } else {
      return Container();
    }
  }
}
