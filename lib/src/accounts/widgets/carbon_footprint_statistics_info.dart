import 'dart:math';

import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint_info.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:atoma_cash/utils/ui/adaptive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CarbonFootprintStatisticsPlaceholder extends StatelessWidget {
  static final double _containerBorderRadius = 16.0;
  static final double _defaultContentPadding = 9.0;
  static final double _titleBottomContentPadding = 12.0;
  static final double _bottomDescriptionTopPadding = 14.0;

  static final _mainTextStyle =
      FontBook.textBody2Menu.copyWith(color: Colors.white);

  static final Color _userFootprintBarColor =
      Palette.darkBlue.withOpacity(0.25);

  final EcoFootprintInfo ecoFootprintInfo;

  CarbonFootprintStatisticsPlaceholder(this.ecoFootprintInfo);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: _contentPadding(context),
      decoration: BoxDecoration(
        color: Palette.blue,
        borderRadius: BorderRadius.all(Radius.circular(_containerBorderRadius)),
      ),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Center(
                child: Text(
              Localization.of(context).footprintChartPlaceholderTitle,
              textAlign: TextAlign.center,
              style: _mainTextStyle,
            )),
            _carbonFootprintItem(
                context,
                _countryAverageCarbonFootprintTitle(context),
                _averagePercentRelativeValue(ecoFootprintInfo
                    .countryFootprintInfo.carbonFootprint.value),
                _titleBottomContentPadding),
            _carbonFootprintItem(
                context,
                _worldAverageCarbonFootprintTitle(context),
                _averagePercentRelativeValue(
                    ecoFootprintInfo.worldCarbonFootprint.value),
                _defaultContentPadding),
            _carbonFootprintItem(context, _userCarbonFootprintTitle(context),
                1.0, _defaultContentPadding,
                barColor: _userFootprintBarColor),
            _bottomDescription(context),
          ]),
    );
  }

  Widget _carbonFootprintItem(
      BuildContext context, Widget title, double value, double topPadding,
      {Color barColor = Palette.darkBlue}) {
    return Padding(
      padding: EdgeInsets.only(top: topPadding),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Expanded(
              child: Padding(
                  padding: EdgeInsets.only(
                      bottom: 3.0,
                      right: Adaptive.isFourInchScreen(context) ? 10.0 : 16.0),
                  child: title)),
          _progressBar(value, barColor),
        ],
      ),
    );
  }

  Widget _progressBar(double value, Color barColor) {
    return Expanded(
        child: Container(
      height: 4,
      child: FractionallySizedBox(
          alignment: Alignment.centerLeft,
          widthFactor: value,
          child: Container(
            decoration: BoxDecoration(
              color: barColor,
              borderRadius: BorderRadius.circular(2.0),
            ),
          )),
    ));
  }

  Widget _countryAverageCarbonFootprintTitle(BuildContext context) {
    String formattedFootprint = CarbonHelper.abridgedCarbonString(
        ecoFootprintInfo.countryFootprintInfo.carbonFootprint.value,
        context: context);

    return Text(
      '${ecoFootprintInfo.countryFootprintInfo.country.name}: $formattedFootprint',
      textAlign: TextAlign.end,
      style: _mainTextStyle,
    );
  }

  Widget _worldAverageCarbonFootprintTitle(BuildContext context) {
    String formattedFootprint = CarbonHelper.abridgedCarbonString(
        ecoFootprintInfo.worldCarbonFootprint.value,
        context: context);
    return Text(
      '${Localization.of(context).footprintChartPlaceholderWorld}: $formattedFootprint',
      textAlign: TextAlign.end,
      style: _mainTextStyle,
    );
  }

  Widget _userCarbonFootprintTitle(BuildContext context) {
    return RichText(
        textAlign: TextAlign.end,
        text: TextSpan(
          text: Localization.of(context).footprintChartPlaceholderYourFootprint,
          style: _mainTextStyle,
        ));
  }

  Widget _bottomDescription(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: _bottomDescriptionTopPadding),
      child: Center(
        child: Opacity(
          opacity: 0.6,
          child: RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              text: Localization.of(context).footprintChartPlaceholderLegend,
              style: FontBook.textCharts.copyWith(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }

  double _averagePercentRelativeValue(double value) {
    return value /
        max(ecoFootprintInfo.countryFootprintInfo.carbonFootprint.value,
            ecoFootprintInfo.worldCarbonFootprint.value);
  }

  EdgeInsets _contentPadding(BuildContext context) {
    if (Adaptive.isFourInchScreen(context)) {
      return EdgeInsets.only(left: 15.0, top: 25.0, right: 15.0, bottom: 25.0);
    } else {
      return EdgeInsets.only(left: 24.0, top: 25.0, right: 24.0, bottom: 25.0);
    }
  }
}
