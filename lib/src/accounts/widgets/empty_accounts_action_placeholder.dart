import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/utils/ui/adaptive.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EmptyAccountsActionPlaceholder extends StatelessWidget {
  static const actionButtonSize = Size(200.0, 52);
  static const double actionBorderRadius = 12.0;

  final VoidCallback onActionTapped;
  final VoidCallback onSubActionTapped;
  final bool isLoading;
  final Color backgroundColor;

  EmptyAccountsActionPlaceholder({
    @required this.isLoading,
    this.backgroundColor,
    this.onActionTapped,
    this.onSubActionTapped,
  });

  @override
  Widget build(BuildContext context) {
    bool isFourInchScreen = Adaptive.isFourInchScreen(context);
    return Container(
      padding: _connectPadding(isFourInchScreen),
      decoration: BoxDecoration(
        color: backgroundColor ?? Colors.white,
        borderRadius: const BorderRadius.all(Radius.circular(16.0)),
      ),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            if (!isFourInchScreen)
              Padding(
                padding: const EdgeInsets.only(bottom: 14),
                child: Image.asset(Assets.connectAccountIcon),
              ),
            Center(
                child: Text(
              Localization.of(context).bankPlaceholderTitle,
              textAlign: TextAlign.center,
              style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
            )),
            Padding(
              padding: const EdgeInsets.only(top: 3.0),
              child: Center(
                child: Text(
                  Localization.of(context).bankPlaceholderBody,
                  textAlign: TextAlign.center,
                  style: FontBook.textBody2Menu
                      .copyWith(color: Palette.mediumGrey),
                ),
              ),
            ),
            if (onActionTapped != null)
              Padding(
                padding: const EdgeInsets.only(top: 19.0),
                child: Container(
                  width: actionButtonSize.width,
                  height: actionButtonSize.height,
                  child: _actionContainer(context),
                ),
              ),
            if (onSubActionTapped != null)
              Padding(
                padding: EdgeInsets.only(top: 8.0),
                child: _subActionContainer(context),
              ),
          ]),
    );
  }

  Widget _actionContainer(BuildContext context) {
    if (isLoading) {
      return Center(child: CircularProgressIndicator());
    } else {
      return ClipRRect(
        borderRadius:
            const BorderRadius.all(Radius.circular(actionBorderRadius)),
        child: FlatButton(
            color: Palette.blue,
            child: Text(
              Localization.of(context).bankPlaceholderButton,
              style: FontBook.textButton.copyWith(color: Palette.white),
            ),
            onPressed: onActionTapped),
      );
    }
  }

  Widget _subActionContainer(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.all(Radius.circular(actionBorderRadius)),
      child: Container(
        width: actionButtonSize.width,
        height: actionButtonSize.height,
        child: FlatButton(
            child: Text(
              Localization.of(context).bankPlaceholderLink,
              style: FontBook.textButton.copyWith(color: Palette.blue),
            ),
            onPressed: !isLoading ? onSubActionTapped : null),
      ),
    );
  }

  EdgeInsets _connectPadding(bool isFourInchScreen) {
    if (isFourInchScreen) {
      return EdgeInsets.only(left: 16.0, top: 26.0, right: 15.0, bottom: 24);
    } else {
      return EdgeInsets.only(left: 24.0, top: 26.0, right: 24.0, bottom: 24);
    }
  }
}
