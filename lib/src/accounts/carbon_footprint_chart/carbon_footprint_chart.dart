import 'dart:math';

import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint_info.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/accounts/bloc/accounts_bloc.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/bloc/bloc.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/chart_builder/carbon_footprint_chart_builder.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/widgets/carbon_footprint_chart_legend.dart';
import 'package:atoma_cash/src/accounts/widgets/carbon_footprint_statistics_info.dart';
import 'package:atoma_cash/src/accounts/widgets/list/cells/accounts_error_placeholder.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:atoma_cash/utils/ui/adaptive.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CarbonFootprintChart extends StatefulWidget {
  final double height;
  final ScrollController scrollController;

  CarbonFootprintChart({Key key, @required this.height, this.scrollController})
      : assert(height > 0),
        super(key: key);

  @override
  _CarbonFootprintChartState createState() => _CarbonFootprintChartState();
}

class _CarbonFootprintChartState extends State<CarbonFootprintChart>
    with TickerProviderStateMixin {
  AnimationController _slidingAnimationController;
  Animation _contentOpacity;
  Animation _scaleTween;
  Animation _translationTween;

  @override
  void dispose() {
    widget.scrollController?.removeListener(_scrollListener);
    super.dispose();
  }

  @override
  void initState() {
    _configureAnimations(widget.scrollController);
    super.initState();
  }

  void _configureAnimations(ScrollController controller) {
    if (controller != null) {
      _slidingAnimationController =
          AnimationController(vsync: this, duration: Duration.zero);
      controller.addListener(_scrollListener);
      _scaleTween = Tween<double>(begin: 1.0, end: 0.9)
          .animate(_slidingAnimationController);
      _contentOpacity = Tween<double>(begin: 1.0, end: 0.0)
          .animate(_slidingAnimationController);
      _translationTween = Tween<double>(begin: 0.0, end: -widget.height)
          .animate(_slidingAnimationController);
    }
  }

  void _scrollListener() {
    final scrollPosition = widget.scrollController.position;
    if (scrollPosition.axis == Axis.vertical) {
      _slidingAnimationController
          .animateTo(max(0.0, min(1.0, scrollPosition.pixels / widget.height)));
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: BlocProvider(
        create: _blocCreator,
        child:
            BlocListener<CarbonFootprintChartBloc, CarbonFootprintChartState>(
          listener: _blocListener,
          child: Container(
            height: widget.height,
            color: Palette.blue,
            child: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: _widgetBuilder(context),
            ),
          ),
        ),
      ),
    );
  }

  CarbonFootprintChartBloc _blocCreator(BuildContext context) {
    final accountsBloc = context.bloc<AccountsBloc>();
    return CarbonFootprintChartBloc(accountsBloc: accountsBloc)..add(Started());
  }

  Widget _widgetBuilder(BuildContext context) {
    if (widget.scrollController == null) {
      return _rootContent(context);
    } else {
      return _animatedContentBuilder(context);
    }
  }

  Widget _animatedContentBuilder(BuildContext context) {
    return AnimatedBuilder(
        animation: _slidingAnimationController,
        builder: (BuildContext context, _) {
          return Transform.translate(
            offset: Offset(0, _translationTween.value),
            child: Transform.scale(
              scale: _scaleTween.value,
              child: Opacity(
                opacity: _contentOpacity.value,
                child: _rootContent(context),
              ),
            ),
          );
        });
  }

  Widget _rootContent(BuildContext context) {
    return SafeArea(
      child: Container(
        height: widget.height,
        child: BlocBuilder<CarbonFootprintChartBloc, CarbonFootprintChartState>(
          builder: _blocBuilder,
        ),
      ),
    );
  }

  void _blocListener(BuildContext context, CarbonFootprintChartState state) {}

  Widget _blocBuilder(BuildContext context, CarbonFootprintChartState state) {
    if (state is Loaded) {
      return _loadedStateBuilder(context, state);
    }
    if (state is Loading) {
      return _loadingStateBuilder();
    }
    if (state is ErrorState) {
      return _errorPlaceholder(context);
    }
    return Container();
  }

  Widget _loadingStateBuilder() {
    return Center(
      child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Palette.white)),
    );
  }

  Widget _loadedStateBuilder(BuildContext context, Loaded state) {
    if (state.chartData?.isNotEmpty ?? false) {
      return _loadedContent(context, state);
    } else if (state.ecoFootprintInfo != null) {
      return _emptyListPlaceholder(context, state.ecoFootprintInfo);
    } else {
      return _errorPlaceholder(context);
    }
  }

  Widget _errorPlaceholder(BuildContext context) {
    return Center(
      child: AccountsErrorPlaceholder(
        descriptionColor: Palette.white,
        onRetryClicked: () =>
            context.bloc<CarbonFootprintChartBloc>().add(Refresh()),
      ),
    );
  }

  Widget _loadedContent(BuildContext context, Loaded state) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _titleWidget(context, state),
        _subtitleWidget(context, state),
        _chartWidget(context, state),
      ],
    );
  }

  Widget _chartWidget(BuildContext context, Loaded state) {
    return Padding(
      padding: EdgeInsets.only(top: 16),
      child: CarbonFootprintChartBuilder(
        state.chartData,
        ecoFootprintInfo: state.ecoFootprintInfo,
        onItemTapped: (index) => _onChartItemTapped(context, index),
        selectedItem: state.selectedPeriod,
      ).build(context),
    );
  }

  Widget _titleWidget(BuildContext context, Loaded state) {
    return Padding(
      padding: EdgeInsets.only(top: 19, left: 24, right: 24),
      child: Row(
        textBaseline: TextBaseline.alphabetic,
        crossAxisAlignment: CrossAxisAlignment.baseline,
        children: <Widget>[
          Text(
            _carbonFootprintForSelectedPeriod(state),
            style: FontBook.headerH2.copyWith(color: Palette.white),
          ),
          Padding(
            padding: EdgeInsets.only(left: 4.0),
            child: Text(
              '${Localization.of(context).kilogram} ${Localization.of(context).carbonFormula}',
              style: FontBook.textBody2Menu.copyWith(color: Palette.white),
            ),
          )
        ],
      ),
    );
  }

  Widget _subtitleWidget(BuildContext context, Loaded state) {
    final selectedPeriod = state.chartData[state.selectedPeriod];
    final percentChange = selectedPeriod.percentChange;
    return Padding(
      padding: EdgeInsets.only(top: 5, left: 24, right: 24),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          _footprintChangeIndicator(percentChange),
          _percentChangeTextWidget(percentChange, selectedPeriod.date),
          _infoButton(state),
        ],
      ),
    );
  }

  Widget _footprintChangeIndicator(double percentChange) {
    if (percentChange == null) {
      return Container();
    } else {
      bool isRising = percentChange > 0;
      return Padding(
        padding: EdgeInsets.only(right: 6),
        child: Container(
          height: 16,
          width: 16,
          decoration: BoxDecoration(
            color: isRising ? Palette.red : Palette.green,
            borderRadius: BorderRadius.all(Radius.circular(2)),
          ),
          child: Center(
            child: Transform.rotate(
              angle: isRising ? 0 : pi,
              child: Image.asset(Assets.straightArrowIcon),
            ),
          ),
        ),
      );
    }
  }

  Widget _percentChangeTextWidget(double percentChange, DateTime date) {
    final formattedDate = DateHelper.formattedDate(date, kMonthFormat);
    final formattedPercentChange =
        percentChange != null ? "${percentChange.abs().round()}% " : "";
    return Padding(
      padding: EdgeInsets.only(top: 2),
      child: Text(
        Localization.of(context)
            .chartPercentChange(formattedPercentChange, formattedDate),
        style: FontBook.textCharts.copyWith(color: Palette.white),
      ),
    );
  }

  Widget _infoButton(Loaded state) {
    return Padding(
      padding: EdgeInsets.only(left: 5.0),
      child: Container(
        width: 16.0,
        height: 16.0,
        child: IconButton(
          padding: EdgeInsets.all(2.0),
          iconSize: 14.0,
          icon: Image.asset(Assets.infoIcon),
          onPressed: () => _onInfoButtonTapped(context, state.ecoFootprintInfo),
        ),
      ),
    );
  }

  Widget _emptyListPlaceholder(
      BuildContext context, EcoFootprintInfo footprintInfo) {
    if (footprintInfo != null) {
      return Center(
        child: Padding(
          padding: _placeholderContentPadding(context),
          child: Wrap(
            children: <Widget>[
              CarbonFootprintStatisticsPlaceholder(footprintInfo)
            ],
          ),
        ),
      );
    } else {
      return Container();
    }
  }

  EdgeInsets _placeholderContentPadding(BuildContext context) {
    if (Adaptive.isFourInchScreen(context)) {
      return const EdgeInsets.only(left: 15.0, right: 15.0);
    } else {
      return const EdgeInsets.only(left: 24.0, right: 24.0);
    }
  }

  String _carbonFootprintForSelectedPeriod(Loaded state) {
    final carbonFootprint = state.chartData[state.selectedPeriod].value;
    final carbonTrailingZerosCount =
        carbonFootprint < 1 ? CarbonHelper.carbonTrailingZerosCount : 0;
    return CarbonHelper.carbonFootprintValue(carbonFootprint,
        trailingZerosCount: carbonTrailingZerosCount);
  }

  void _onChartItemTapped(BuildContext context, int index) {
    context.bloc<CarbonFootprintChartBloc>().add(OnPeriodTapped(index));
  }

  void _onInfoButtonTapped(
      BuildContext context, EcoFootprintInfo ecoFootprintInfo) {
    showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            backgroundColor: Colors.transparent,
            insetPadding: EdgeInsets.all(16),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    color: Palette.white,
                    borderRadius: BorderRadius.all(Radius.circular(16)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(28, 26, 28, 28),
                    child: CarbonFootprintChartLegend(ecoFootprintInfo),
                  ),
                )
              ],
            ),
          );
        });
  }
}
