import 'package:equatable/equatable.dart';

abstract class CarbonFootprintChartEvent extends Equatable {
  CarbonFootprintChartEvent();
}

class Started extends CarbonFootprintChartEvent {
  Started();

  @override
  List<Object> get props => null;
}

class Refresh extends CarbonFootprintChartEvent {
  Refresh();

  @override
  List<Object> get props => null;
}

class OnPeriodTapped extends CarbonFootprintChartEvent {
  final int selectedIndex;

  OnPeriodTapped(this.selectedIndex);

  @override
  List<Object> get props => [selectedIndex];
}
