import 'dart:async';

import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint_info.dart';
import 'package:atoma_cash/models/api/api/reports/offset_history.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/src/accounts/bloc/accounts_bloc.dart';
import 'package:atoma_cash/src/accounts/bloc/accounts_state.dart' as Accounts;
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/bloc/bloc.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/bloc/carbon_footprint_data_configurator.dart';
import 'package:bloc/bloc.dart';

class CarbonFootprintChartBloc
    extends Bloc<CarbonFootprintChartEvent, CarbonFootprintChartState> {
  final ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  final AccountsBloc _accountsBloc;
  final _dataConfigurator = CarbonFootprintDataConfigurator();

  int _selectedPeriodIndex = 0;
  List<CarbonFootprintDataModel> _cachedOffsetItems;

  EcoFootprintInfo _cachedEcoFootprintInfo;

  StreamSubscription _accountsBlocSubscription;

  Loaded get _cachedLoadedState => Loaded(
        chartData: _cachedOffsetItems,
        ecoFootprintInfo: _cachedEcoFootprintInfo,
        selectedPeriod: _selectedPeriodIndex,
      );

  @override
  CarbonFootprintChartState get initialState => Loading();

  CarbonFootprintChartBloc({AccountsBloc accountsBloc})
      : _accountsBloc = accountsBloc {
    _accountsBlocSubscription = _accountsBloc?.listen(_accountsStateListener);
  }

  void _accountsStateListener(Accounts.AccountsState state) {
    if (state is Accounts.Refreshing) {
      add(Refresh());
    }
  }

  @override
  Future<void> close() {
    _accountsBlocSubscription.cancel();
    return super.close();
  }

  @override
  Stream<CarbonFootprintChartState> mapEventToState(
      CarbonFootprintChartEvent event) async* {
    if (event is Started) {
      yield* _mapStartedToState();
    }
    if (event is Refresh) {
      yield* _mapRefreshToState();
    }
    if (event is OnPeriodTapped) {
      yield* _mapOnPeriodTappedToState(event.selectedIndex);
    }
  }

  Stream<CarbonFootprintChartState> _mapStartedToState() async* {
    yield Loading();

    var offsetHistory = _apiRemoteRepository.getOffsetHistory();
    var footprintInfo = _apiRemoteRepository.getEcoFootprintInfo();
    var response = await Future.wait([offsetHistory, footprintInfo]);

    if (response.every((request) => request.isSuccess())) {
      _cachedOffsetItems = _dataConfigurator
          .configure((response[0].result as OffsetHistory).offsetHistory);
      _cachedEcoFootprintInfo = response[1].result as EcoFootprintInfo;
      yield _cachedLoadedState;
    } else {
      yield ErrorState(
          error:
              response.firstWhere((element) => element.error != null)?.error);
    }
  }

  Stream<CarbonFootprintChartState> _mapRefreshToState() async* {
    if (_cachedEcoFootprintInfo == null) {
      yield* _mapStartedToState();
    } else {
      yield* _getHistoryItems();
    }
  }

  Stream<CarbonFootprintChartState> _getHistoryItems() async* {
    var offsetHistoryResponse = await _apiRemoteRepository.getOffsetHistory();
    if (offsetHistoryResponse.isSuccess()) {
      _cachedOffsetItems = _dataConfigurator
          .configure(offsetHistoryResponse.result.offsetHistory);
      yield _cachedLoadedState;
    } else {
      yield ErrorState(error: offsetHistoryResponse.error);
    }
  }

  Stream<CarbonFootprintChartState> _mapOnPeriodTappedToState(
      int index) async* {
    _selectedPeriodIndex = index;
    yield _cachedLoadedState;
  }
}
