import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint_info.dart';
import 'package:atoma_cash/models/api/api/reports/offset_history.dart';
import 'package:atoma_cash/models/api/error/error.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/bloc/carbon_footprint_data_configurator.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class CarbonFootprintChartState extends Equatable {
  CarbonFootprintChartState();
}

class Loading extends CarbonFootprintChartState {
  Loading();

  @override
  List<Object> get props => null;
}

@immutable
class Loaded extends CarbonFootprintChartState {
  final List<CarbonFootprintDataModel> chartData;
  final EcoFootprintInfo ecoFootprintInfo;
  final int selectedPeriod;

  Loaded({this.chartData, this.ecoFootprintInfo, this.selectedPeriod = 1});

  Loaded copyWith({
    List<OffsetHistoryItem> chartData,
    EcoFootprintInfo ecoFootprintInfo,
    int selectedPeriod,
  }) {
    return Loaded(
      chartData: chartData ?? this.chartData,
      ecoFootprintInfo: ecoFootprintInfo ?? this.ecoFootprintInfo,
      selectedPeriod: selectedPeriod ?? this.selectedPeriod,
    );
  }

  @override
  List<Object> get props => [chartData, ecoFootprintInfo, selectedPeriod];
}

@immutable
class ErrorState extends CarbonFootprintChartState {
  final Error error;

  ErrorState({this.error});

  @override
  List<Object> get props => [error];
}
