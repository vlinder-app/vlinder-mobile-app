import 'package:atoma_cash/models/api/api/reports/offset_history.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:equatable/equatable.dart';

class CarbonFootprintDataModel extends Equatable {
  final double value;
  final double percentChange;
  final DateTime date;
  final bool isReal;

  CarbonFootprintDataModel(
      {this.value, this.percentChange, this.date, this.isReal});

  CarbonFootprintDataModel copyWith(
      {double value, double percentChange, DateTime date, bool isReal}) {
    return CarbonFootprintDataModel(
      value: value ?? this.value,
      percentChange: percentChange ?? this.percentChange,
      date: date ?? this.date,
      isReal: isReal ?? this.isReal,
    );
  }

  @override
  List<Object> get props => [value, percentChange, date, isReal];
}

class CarbonFootprintDataConfigurator {
  CarbonFootprintDataConfigurator();

  List<CarbonFootprintDataModel> configure(
      List<OffsetHistoryItem> historyItems) {
    if (historyItems?.isNotEmpty ?? false) {
      return _fillPercentChange(_mapOffsetHistoryItems(historyItems));
    } else {
      return null;
    }
  }

  List<CarbonFootprintDataModel> _mapOffsetHistoryItems(
      List<OffsetHistoryItem> historyItems) {
    final now = DateTime.now();
    final currentMonth = DateTime(now.year, now.month);
    if (historyItems.length <= 2) {
      return _shortInterval(historyItems, currentMonth);
    } else {
      return _halfYearInterval(historyItems, currentMonth);
    }
  }

  List<CarbonFootprintDataModel> _fillPercentChange(
      List<CarbonFootprintDataModel> data) {
    final filledItems = List<CarbonFootprintDataModel>();
    for (int i = 0; i < data.length - 1; i++) {
      final currentDataModel = data[i];
      final previousFootprint = data[i + 1].value;
      filledItems.add(currentDataModel.copyWith(
          percentChange:
              _percentChange(currentDataModel.value, previousFootprint)));
    }
    filledItems.add(data.last);
    return filledItems;
  }

  double _percentChange(double currentValue, double previousValue) {
    if (previousValue != 0.0) {
      return (currentValue - previousValue) / previousValue * 100;
    } else {
      if (currentValue != null && currentValue != 0.0) {
        return 100.0;
      } else {
        return null;
      }
    }
  }

  List<CarbonFootprintDataModel> _shortInterval(
      List<OffsetHistoryItem> historyItems, DateTime currentMonth) {
    final hasTransactionsThisMonth = DateHelper.isSameMonthAndYear(
        historyItems.first.startDate, currentMonth);
    if (historyItems.length == 1) {
      if (hasTransactionsThisMonth) {
        return [
          _footprintDataForOffsetHistoryItem(historyItems.first),
          _fakeFootprintData(
              DateTime(currentMonth.year, currentMonth.month - 1)),
        ];
      } else {
        return [
          _fakeFootprintData(currentMonth),
          _footprintDataForOffsetHistoryItem(historyItems.first),
        ];
      }
    } else if (hasTransactionsThisMonth) {
      return historyItems
          .map((item) => _footprintDataForOffsetHistoryItem(item))
          .toList();
    } else {
      return _halfYearInterval(historyItems, currentMonth);
    }
  }

  List<CarbonFootprintDataModel> _halfYearInterval(
      List<OffsetHistoryItem> historyItems, DateTime currentMonth) {
    var items = List<CarbonFootprintDataModel>();
    DateTime date = currentMonth;
    int index = 0;
    while (items.length < 6) {
      if (index < historyItems.length &&
          DateHelper.isSameMonthAndYear(date, historyItems[index].startDate)) {
        items.add(_footprintDataForOffsetHistoryItem(historyItems[index]));
        index += 1;
      } else {
        items.add(_fakeFootprintData(date));
      }
      date = DateTime(date.year, date.month - 1);
    }

    return items;
  }

  CarbonFootprintDataModel _footprintDataForOffsetHistoryItem(
      OffsetHistoryItem historyItem) {
    return CarbonFootprintDataModel(
      value: historyItem.carbonFootprint,
      date: historyItem.startDate,
      isReal: true,
    );
  }

  CarbonFootprintDataModel _fakeFootprintData(DateTime date) {
    return CarbonFootprintDataModel(
      value: 0.0,
      date: date,
      isReal: false,
    );
  }
}
