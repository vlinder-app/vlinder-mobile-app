import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint_info.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/widgets/circle_widget_container_with_border.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:flag/flag.dart';
import 'package:flutter/material.dart';

class CarbonFootprintChartLegend extends StatelessWidget {
  static const double _iconSize = 16.0;
  final EcoFootprintInfo ecoFootprintInfo;

  CarbonFootprintChartLegend(this.ecoFootprintInfo);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        _titleWidget(context),
        Padding(
          padding: const EdgeInsets.only(top: 14.0),
          child: _contentTextWidget(
              context, Localization.of(context).chartLegendSubtitle),
        ),
        _carbonFootprintInfo(context),
        _contentTextWidget(
            context, Localization.of(context).chartLegendEmissionInfo),
        Padding(
          padding: EdgeInsets.only(top: 12),
          child: _contentTextWidget(
              context, Localization.of(context).chartLegendSuggestion),
        ),
      ],
    );
  }

  Widget _titleWidget(BuildContext context) {
    return Text(
      Localization.of(context).chartLegendTitle,
      style: FontBook.headerH2.copyWith(color: Palette.darkGrey),
    );
  }

  Widget _contentTextWidget(BuildContext context, String text) {
    return Text(
      text,
      style: FontBook.textBody1.copyWith(color: Palette.darkGrey),
      softWrap: true,
    );
  }

  Widget _carbonFootprintInfo(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: 20, bottom: 16),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Expanded(
            child: _carbonFootprintItem(
              context,
              info: ecoFootprintInfo.countryFootprintInfo.carbonFootprint,
              icon: _countryFlagIcon(
                  ecoFootprintInfo.countryFootprintInfo.country.iso2Code),
              name: ecoFootprintInfo.countryFootprintInfo.country.name,
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Container(
              height: 32.0,
              width: 2.0,
              color: Palette.paleGrey,
            ),
          ),
          Expanded(
            child: _carbonFootprintItem(
              context,
              info: ecoFootprintInfo.worldCarbonFootprint,
              icon: _worldIcon(),
              name: Localization.of(context).footprintChartPlaceholderWorld,
            ),
          ),
        ],
      ),
    );
  }

  Widget _carbonFootprintItem(BuildContext context,
      {CarbonFootprint info, Widget icon, String name}) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: _carbonFootprintTitle(context, info.value),
        ),
        Padding(
          padding: EdgeInsets.only(top: 3.0, bottom: 11.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              icon,
              Padding(
                padding: const EdgeInsets.only(left: 6.0),
                child: Text(
                  name,
                  style: FontBook.textBody2Menu
                      .copyWith(color: Palette.mediumGrey),
                ),
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _countryFlagIcon(String isoCode) {
    return CircleWidgetContainerWithBorder(
      Flag(
        isoCode,
        fit: BoxFit.fill,
      ),
      borderColor: Palette.paleGrey,
    );
  }

  Widget _worldIcon() {
    return Container(
      width: _iconSize,
      height: _iconSize,
      child: Image.asset(
        Assets.worldIcon,
        color: Palette.darkGrey,
      ),
    );
  }

  Widget _carbonFootprintTitle(
      BuildContext context, double carbonFootprintValue) {
    String formattedFootprint = CarbonHelper.abridgedCarbonString(
        carbonFootprintValue,
        context: context);

    return Text(
      '$formattedFootprint ${Localization.of(context).carbonFormula}',
      textAlign: TextAlign.end,
      style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
    );
  }
}
