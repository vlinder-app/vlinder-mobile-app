import 'package:flag/flag.dart';
import 'package:flutter/material.dart';

class CircleWidgetContainerWithBorder extends StatelessWidget {
  static const defaultSize = 16.0;
  static const defaultBorderWidth = 1.0;

  final Flag flag;
  final Color borderColor;
  final double size;
  final double borderWidth;

  CircleWidgetContainerWithBorder(this.flag,
      {@required this.borderColor,
      this.size = defaultSize,
      this.borderWidth = defaultBorderWidth})
      : assert(flag != null),
        assert(borderColor != null),
        assert(size != null),
        assert(borderWidth != null);

  @override
  Widget build(BuildContext context) {
    final contentSize = size - borderWidth * 2;
    return ClipOval(
      child: Container(
        width: size,
        height: size,
        color: borderColor,
        child: Center(
          child: ClipOval(
            child: Container(
              width: contentSize,
              height: contentSize,
              child: flag,
            ),
          ),
        ),
      ),
    );
  }
}
