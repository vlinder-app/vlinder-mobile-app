import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/widgets/chart_interaction/chart_interaction_config.dart';
import 'package:flutter/material.dart';

typedef OnChartInteractingItemTapped = Function(int index);

class ChartInteractionWidget extends StatelessWidget {
  final IChartInteractionConfigurationBuilder configurationBuilder;
  final OnChartInteractingItemTapped onItemTapped;
  final int interval;

  ChartInteractionWidget(this.configurationBuilder,
      {this.onItemTapped, this.interval = 6});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Row(
        children: _items(context),
      ),
    );
  }

  List<Widget> _items(BuildContext context) {
    final items = configurationBuilder.build(context);
    if (items.length == 2) {
      return _shortPeriodInteractingItems(context, items);
    } else {
      return items.map((itemConfig) {
        return Expanded(
          child: _interactingWidget(itemConfig),
        );
      }).toList();
    }
  }

  List<Widget> _shortPeriodInteractingItems(
      BuildContext context, List<ChartInteractionItemConfig> items) {
    return [
      Expanded(
        flex: 1,
        child: _interactingWidget(items.first),
      ),
      Expanded(
        flex: interval - 2,
        child: Container(),
      ),
      Expanded(
        flex: 1,
        child: _interactingWidget(items.last),
      ),
    ];
  }

  Widget _interactingWidget(ChartInteractionItemConfig config) {
    return InkWell(
      onTap: () => onItemTapped(config.index),
      borderRadius: BorderRadius.all(Radius.circular(12.0)),
      splashColor: Color.fromRGBO(42, 49, 188, 0.3),
      highlightColor: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(12.0)),
          color: config.isSelected
              ? Palette.mediumBlue.withOpacity(0.3)
              : Colors.transparent,
        ),
        child: Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 32.0,
            decoration: BoxDecoration(
              color:
                  config.isSelected ? Palette.mediumBlue : Colors.transparent,
              borderRadius: BorderRadius.all(Radius.circular(12.0)),
            ),
            child: _annotation(config),
          ),
        ),
      ),
    );
  }

  Widget _annotation(ChartInteractionItemConfig config) {
    if (config.annotation != null) {
      return Center(
        child: config.annotation,
      );
    } else {
      return Container();
    }
  }
}
