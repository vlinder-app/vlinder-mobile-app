import 'package:flutter/cupertino.dart';

class ChartInteractionItemConfig {
  final Widget annotation;
  final bool isSelected;
  final int index;

  ChartInteractionItemConfig(
      {@required this.index, this.annotation, this.isSelected = false});
}

abstract class IChartInteractionConfigurationBuilder {
  List<ChartInteractionItemConfig> build(BuildContext context);
}
