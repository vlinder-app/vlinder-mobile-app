import 'dart:math';

import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint_info.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/bloc/carbon_footprint_data_configurator.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/chart_builder/carbon_footprint_chart_date_axis.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/chart_builder/carbon_footprint_chart_interaction_builder.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/widgets/chart_interaction/chart_interaction_widget.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/widgets/circle_widget_container_with_border.dart';
import 'package:atoma_cash/src/chart/chart.dart';
import 'package:atoma_cash/src/chart/chart_widget.dart';
import 'package:atoma_cash/src/chart/models/point_data_model.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:flag/flag.dart';
import 'package:flutter/material.dart';

class CarbonFootprintChartBuilder {
  final int interval = 6;
  static const double _iconSize = 16.0;
  final EcoFootprintInfo ecoFootprintInfo;
  final int selectedItem;
  final List<CarbonFootprintDataModel> data;
  final OnChartInteractingItemTapped onItemTapped;

  CarbonFootprintChartBuilder(this.data,
      {this.ecoFootprintInfo, this.selectedItem, this.onItemTapped});

  Widget build(BuildContext context) {
    final valuesList = data.reversed
        .map((item) => PointDataModel(item.date, item.value))
        .toList();

    final xAxis = CarbonFootprintChartDateAxis(
        dates: valuesList.map((item) => item.xValue).toList(),
        interval: interval);
    final yAxis = _valueAxis();
    final pointDataSeries =
        ChartPointDataSeries(valuesList, xAxis: xAxis, yAxis: yAxis);

    final paints = _paints(pointDataSeries);
    final annotations = _annotations(context, xAxis, yAxis);

    final selectionIndex = valuesList.length - 1 - selectedItem;
    final selectionPainter =
        _selectionPainter(xAxis, yAxis, valuesList[selectionIndex]);

    final interactionConfigurator = CarbonFootprintChartInteractionConfigurator(
      data,
      selectedItem: selectedItem,
    );

    return ChartWidget(
      interactionWidget: ChartInteractionWidget(
        interactionConfigurator,
        onItemTapped: onItemTapped,
      ),
      painters: paints..add(selectionPainter),
      annotations: annotations,
    );
  }

  List<IChartPainter> _paints(ChartPointDataSeries dataSeries) {
    final bezierPainter = ChartSimplePainter(dataSeries,
        paint: Paint()
          ..strokeWidth = 2.0
          ..color = Palette.white
          ..style = PaintingStyle.stroke,
        pathBuilder: ChartCubicBezierSymmetricPathBuilder(isClosed: false));

    final gradientPainter = ChartGradientPainter(dataSeries,
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [
              Palette.chartGradientColor,
              Palette.chartGradientColor.withOpacity(0.0),
            ]),
        pathBuilder: ChartCubicBezierSymmetricPathBuilder(isClosed: true));

    final dotPainter = ChartDotPainter(dataSeries,
        fillPaint: Paint()
          ..color = Palette.white
          ..style = PaintingStyle.fill,
        strokePaint: Paint()
          ..color = Palette.blue
          ..style = PaintingStyle.stroke
          ..strokeWidth = 2.0,
        pathBuilder: ChartDotPathBuilder(4.0));

    return <IChartPainter>[
      gradientPainter,
      bezierPainter,
      dotPainter,
    ];
  }

  List<IChartAnnotation> _annotations(BuildContext context,
      IChartAxis<DateTime> xAxis, IChartAxis<double> yAxis) {
    if (ecoFootprintInfo != null) {
      return [
        if (ecoFootprintInfo.worldCarbonFootprint != null)
          _annotation(context,
              color: Palette.darkBlue,
              leading: _worldIcon(),
              xAxis: xAxis,
              yAxis: yAxis,
              value: ecoFootprintInfo.worldCarbonFootprint.value),
        if (ecoFootprintInfo.countryFootprintInfo != null)
          _annotation(context,
              color: Palette.red,
              leading: _countryFlag(
                  ecoFootprintInfo.countryFootprintInfo.country.iso2Code),
              xAxis: xAxis,
              yAxis: yAxis,
              value:
                  ecoFootprintInfo.countryFootprintInfo.carbonFootprint.value),
      ];
    } else {
      return null;
    }
  }

  IChartAnnotation _annotation(BuildContext context,
      {Color color,
      Widget leading,
      IChartAxis<DateTime> xAxis,
      IChartAxis<double> yAxis,
      double value}) {
    String formattedFootprint =
        CarbonHelper.abridgedCarbonString(value, context: context);
    return ChartYAxisLineAnnotation(ChartLineDataSeries(xAxis, yAxis, value),
        color: color,
        leading: leading,
        trailing: Text(
          formattedFootprint,
          style: FontBook.textCharts.copyWith(color: Palette.white),
        ),
        tipWidth: 1.0);
  }

  ChartDotPainter _selectionPainter(
      IChartAxis<DateTime> xAxis,
      IChartAxis<double> yAxis,
      PointDataModel<DateTime, double> selectedPoint) {
    final dataSeries =
        ChartPointDataSeries([selectedPoint], xAxis: xAxis, yAxis: yAxis);
    final dotPainter = ChartDotPainter(dataSeries,
        fillPaint: Paint()
          ..color = Palette.blue
          ..style = PaintingStyle.fill,
        strokePaint: Paint()
          ..color = Palette.white
          ..style = PaintingStyle.stroke
          ..strokeWidth = 2.0,
        pathBuilder: ChartDotPathBuilder(4.0));
    return dotPainter;
  }

  Widget _worldIcon() {
    return Container(
      width: _iconSize,
      height: _iconSize,
      child: Image.asset(Assets.worldIcon),
    );
  }

  Widget _countryFlag(String isoCode) {
    return CircleWidgetContainerWithBorder(
      Flag(
        isoCode,
        fit: BoxFit.fill,
      ),
      borderColor: Palette.white,
    );
  }

  IChartAxis<double> _valueAxis() {
    final minValue = _extractValue(min);
    final maxValue = _extractValue(max);
    return ChartDoubleAxis(
        minValue: minValue, maxValue: maxValue, logarithm: ln10);
  }

  double _extractValue(double Function(double a, double b) operation) {
    return data.skip(1).fold(data.first.value,
        (previousValue, element) => operation(previousValue, element.value));
  }
}
