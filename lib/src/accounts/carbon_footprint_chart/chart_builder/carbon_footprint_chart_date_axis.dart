import 'package:atoma_cash/src/chart/axis/chart_axis.dart';
import 'package:flutter/material.dart';

class CarbonFootprintChartDateAxis extends IChartAxis<DateTime> {
  final List<DateTime> dates;
  final int interval;

  CarbonFootprintChartDateAxis({@required this.dates, this.interval});

  @override
  double coordinateForValue(DateTime value, double size) {
    int indexOfDate = dates.indexOf(value);
    if (indexOfDate >= 0) {
      return _mapValueToSize(indexOfDate, size);
    } else {
      return null;
    }
  }

  double _mapValueToSize(int dateIndex, double size) {
    final margins = size / (interval ?? dates.length);
    final adjustedSize = size - margins;
    return margins / 2.0 + dateIndex * (adjustedSize / (dates.length - 1));
  }
}
