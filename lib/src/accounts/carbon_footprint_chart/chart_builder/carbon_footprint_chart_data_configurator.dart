import 'package:atoma_cash/models/api/api/reports/offset_history.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/chart_builder/carbon_footprint_chart_item_data.dart';
import 'package:atoma_cash/utils/date_helper.dart';

abstract class ICarbonFootprintChartItemDataConfigurator {
  int get interval;

  List<CarbonFootprintChartItemData> configure();
}

class CarbonFootprintChartItemDataConfigurator
    extends ICarbonFootprintChartItemDataConfigurator {
  final List<OffsetHistoryItem> historyItems;

  @override
  int get interval => 6;

  CarbonFootprintChartItemDataConfigurator(this.historyItems);

  @override
  List<CarbonFootprintChartItemData> configure() {
    final now = DateTime.now();
    final currentMonth = DateTime(now.year, now.month);
    if (historyItems.isNotEmpty) {
      if (historyItems.length <= 2) {
        return _shortInterval(currentMonth);
      } else {
        return _halfYearInterval(currentMonth);
      }
    } else {
      return null;
    }
  }

  List<CarbonFootprintChartItemData> _shortInterval(DateTime currentMonth) {
    final hasTransactionsThisMonth = DateHelper.isSameMonthAndYear(
        historyItems.first.startDate, currentMonth);
    if (historyItems.length == 1) {
      if (hasTransactionsThisMonth) {
        return [
          _itemForOffsetHistoryItem(historyItems.first),
          _placeholderItemForDate(
              DateTime(currentMonth.year, currentMonth.month - 1)),
        ];
      } else {
        return [
          _placeholderItemForDate(currentMonth),
          _itemForOffsetHistoryItem(historyItems.first),
        ];
      }
    } else {
      final dataItems =
          historyItems.map((item) => _itemForOffsetHistoryItem(item)).toList();
      if (hasTransactionsThisMonth) {
        return dataItems;
      } else {
        return dataItems..insert(0, _placeholderItemForDate(currentMonth));
      }
    }
  }

  List<CarbonFootprintChartItemData> _halfYearInterval(DateTime currentMonth) {
    var items = List<CarbonFootprintChartItemData>();
    DateTime date = currentMonth;
    int index = 0;
    while (items.length < interval) {
      if (index < historyItems.length &&
          DateHelper.isSameMonthAndYear(date, historyItems[index].startDate)) {
        items.add(_itemForOffsetHistoryItem(historyItems[index]));
        index += 1;
      } else {
        items.add(_placeholderItemForDate(date));
      }
      date = DateTime(date.year, date.month - 1);
    }

    return items;
  }

  CarbonFootprintChartItemData _placeholderItemForDate(DateTime date) {
    return CarbonFootprintChartItemData(
        value: 0.0, date: date, isPlaceholder: true);
  }

  CarbonFootprintChartItemData _itemForOffsetHistoryItem(
      OffsetHistoryItem item) {
    return CarbonFootprintChartItemData(
        value: item.carbonFootprint ?? 0.0,
        date: item.startDate,
        isPlaceholder: false);
  }
}
