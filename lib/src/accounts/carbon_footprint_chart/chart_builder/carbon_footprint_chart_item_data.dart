import 'package:flutter/material.dart';

class CarbonFootprintChartItemData {
  final double value;
  final DateTime date;
  final bool isPlaceholder;

  CarbonFootprintChartItemData(
      {@required this.value, @required this.date, @required this.isPlaceholder})
      : assert(value != null),
        assert(date != null),
        assert(isPlaceholder != null);
}
