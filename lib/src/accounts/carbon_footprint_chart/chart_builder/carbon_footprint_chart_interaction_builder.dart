import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/bloc/carbon_footprint_data_configurator.dart';
import 'package:atoma_cash/src/accounts/carbon_footprint_chart/widgets/chart_interaction/chart_interaction_config.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:flutter/material.dart';

class CarbonFootprintChartInteractionConfigurator
    extends IChartInteractionConfigurationBuilder {
  final int selectedItem;
  final List<CarbonFootprintDataModel> data;

  CarbonFootprintChartInteractionConfigurator(this.data, {this.selectedItem});

  @override
  List<ChartInteractionItemConfig> build(BuildContext context) {
    final items = List<ChartInteractionItemConfig>();

    for (int index = this.data.length - 1; index >= 0; index--) {
      items.add(ChartInteractionItemConfig(
        annotation: _annotation(context, data[index].date),
        index: index,
        isSelected: index == selectedItem,
      ));
    }

    return items;
  }

  Widget _annotation(BuildContext context, DateTime date) {
    if (date != null) {
      return Text(
        _titleForDate(context, date),
        style: FontBook.textBody2Menu.copyWith(
          color: Palette.white.withOpacity(0.45),
        ),
      );
    } else {
      return Container();
    }
  }

  String _titleForDate(BuildContext context, DateTime date) {
    if (data.length <= 2) {
      return _shortPeriodFormattedDate(context, date);
    } else {
      return _formattedDate(date);
    }
  }

  String _shortPeriodFormattedDate(BuildContext context, date) {
    final now = DateTime.now();
    final currentMonth = DateTime(now.year, now.month);
    if (date.isAtSameMomentAs(currentMonth)) {
      return Localization.of(context).now;
    } else {
      return _formattedDate(date);
    }
  }

  String _formattedDate(DateTime date) {
    return DateHelper.formattedDate(date, kShortMonthFormat);
  }
}
