import 'package:atoma_cash/models/api/api/banking/response/connections.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/accounts/sync/result/connections_sync_result.dart';
import 'package:atoma_cash/src/accounts/sync/widgets/connections_sync_cell.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/close_bank_sync_select_event.dart';
import 'package:atoma_cash/src/analytics/models/events/select_bank_event.dart';
import 'package:atoma_cash/src/bank/webview/bank_webview.dart';
import 'package:atoma_cash/src/bank/webview/webview_result/webview_result.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bloc.dart';

class ConnectionsSyncScreen extends StatefulWidget {
  ConnectionsSyncScreen({Key key}) : super(key: key);

  @override
  _ConnectionsSyncScreenState createState() => _ConnectionsSyncScreenState();
}

class _ConnectionsSyncScreenState extends State<ConnectionsSyncScreen> {
  ConnectionsSyncBloc _bloc;
  ConnectionsSyncResult _connectionsSyncResult;

  @override
  void initState() {
    _bloc = ConnectionsSyncBloc()..add(Started());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: BlocListener(
        bloc: _bloc,
        listener: _blocListener,
        child: Builder(
          builder: (context) {
            return Stack(
              alignment: AlignmentDirectional.center,
              children: <Widget>[
                _background(context),
                _contentContainer(context),
              ],
            );
          },
        ),
      ),
    );
  }

  void _blocListener(BuildContext context, ConnectionsSyncState state) {
    if (state is ManualSyncConnection) {
      _openBankWebViewForUri(state.uriModel);
    }

    if (state is ErrorState) {
      _onErrorSnackBar(context);
    }
  }

  Widget _background(BuildContext context) {
    return Container(
      color: Colors.black.withOpacity(0.4),
      child: GestureDetector(
        onTap: () => _close(context),
      ),
    );
  }

  Widget _contentContainer(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.only(left: 16, right: 16),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 28, top: 30, right: 28),
                child: Text(
                  Localization.of(context).bankSyncModalTitle,
                  style: FontBook.headerH3.copyWith(color: Palette.darkGrey),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 28, top: 6, right: 28),
                child: Text(
                  Localization.of(context).bankSyncModalHint,
                  style: FontBook.textBody1.copyWith(
                    color: Palette.mediumGrey,
                  ),
                  maxLines: null,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 26, top: 18),
                child: _dynamicContent(context),
              )
            ],
          ),
        ),
      ),
    );
  }

  bool _dynamicContentBuilderCondition(
          ConnectionsSyncState previous, ConnectionsSyncState current) =>
      current is! ManualSyncConnection && current is! ErrorState;

  Widget _dynamicContent(BuildContext context) {
    return BlocBuilder(
      bloc: _bloc,
      condition: _dynamicContentBuilderCondition,
      builder: (context, state) {
        if (state is Loading) {
          return Container(
            height: FourRowListTile.twoRowHeight,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        }
        if (state is Loaded) {
          return Material(
            color: Colors.white,
            child: _connectionsList(context, state),
          );
        }
        return Container();
      },
    );
  }

  Widget _connectionsList(BuildContext context, Loaded state) {
    return Container(
      constraints: BoxConstraints(maxHeight: 296, minHeight: 0),
      child: ListView.builder(
        padding: EdgeInsets.zero,
        itemCount: state.connections.length,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          final connection = state.connections[index];
          return ConnectionsSyncCell(
            connection: connection,
            isLoading: state.syncingConnectionId == connection.connectionId,
            onSyncConnectionTapped: _onSynConnectionTapped,
          );
        },
      ),
    );
  }

  void _onErrorSnackBar(BuildContext context) {
    Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          action: SnackBarAction(
            textColor: Colors.white,
            label: Localization.of(context).retryButton,
            onPressed: () => _bloc.add(Reload()),
          ),
          content: Text(Localization.of(context).somethingWentWrongPlaceholder,
              style: FontBook.textCaption.copyWith(color: Colors.white)),
          backgroundColor: Palette.red,
        ),
      );
  }

  void _onSynConnectionTapped(Connection connection) {
    Analytics().logEvent(SelectBankEvent(
        connection.providerName, connection.refreshedAt, Source.HOME));
    _bloc.add(SyncStarted(connection.connectionId));
  }

  void _openBankWebViewForUri(UriModel uriModel) async {
    var result = await Navigator.push(
      context,
      PageRouteBuilder(
          fullscreenDialog: true,
          pageBuilder: (ctx, animation, secondaryAnimation) =>
              BankWebViewScreen(uriModel),
          transitionsBuilder: TransitionsBuilders.fadeIn),
    );

    if (result is SuccessResult) {
      _connectionsSyncResult = ConnectionsSynchronized();
      _bloc.add(ConnectionSyncResultReceived(ConnectionSyncResult.SUCCESS));
    } else if (result is FailedResult) {
      _bloc.add(ConnectionSyncResultReceived(ConnectionSyncResult.FAILED));
    } else {
      _bloc.add(ConnectionSyncResultReceived(ConnectionSyncResult.CANCELED));
    }
  }

  void _close(BuildContext context) {
    Analytics().logEvent(CloseBankSyncSelectEvent());
    Navigator.of(context).pop(_connectionsSyncResult);
    Scaffold.of(context)..hideCurrentSnackBar();
  }
}
