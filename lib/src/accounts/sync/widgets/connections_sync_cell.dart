import 'package:atoma_cash/models/api/api/banking/response/connections.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/accounts/widgets/list/cells/remote_icon.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:flutter/material.dart';

typedef OnSyncConnectionTapped = Function(Connection);

class ConnectionsSyncCell extends StatelessWidget {
  static const kProgressIndicatorSize = 20.0;

  final Connection connection;
  final OnSyncConnectionTapped onSyncConnectionTapped;
  final bool isLoading;

  ConnectionsSyncCell(
      {this.connection, this.onSyncConnectionTapped, this.isLoading = false});

  @override
  Widget build(BuildContext context) {
    return FourRowListTile(
      leading: RemoteIcon(connection.providerIconUrl),
      title: Text(
        connection.providerName,
        style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
        softWrap: false,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Text(
        DateHelper.updatedAtFormattedString(
            context, connection.refreshedAt ?? DateTime.now()),
        style: FontBook.textBody2Menu.copyWith(color: Palette.mediumGrey),
      ),
      trailing: _trailing(),
      onClick: _isRefreshPossible() ? _onSyncTapped : null,
    );
  }

  Widget _trailing() {
    if (isLoading) {
      return Container(
        child: Container(
          height: kProgressIndicatorSize,
          width: kProgressIndicatorSize,
          child: Center(
              child: CircularProgressIndicator(
            strokeWidth: 2,
          )),
        ),
      );
    } else {
      return _syncIcon(_isRefreshPossible() ? Palette.blue : Palette.mediumGrey);
    }
  }

  void _onSyncTapped() {
    if (onSyncConnectionTapped != null && _isRefreshPossible()) {
      onSyncConnectionTapped(connection);
    }
  }

  Widget _syncIcon(Color color) => Image.asset(
        Assets.updateIcon,
        color: color,
      );

  bool _isRefreshPossible() {
    final nextRefreshPossibleAt = connection.nextRefreshPossibleAt?.toLocal();
    if (nextRefreshPossibleAt != null) {
      return nextRefreshPossibleAt.compareTo(DateTime.now()) <= 0;
    }
    return true;
  }
}
