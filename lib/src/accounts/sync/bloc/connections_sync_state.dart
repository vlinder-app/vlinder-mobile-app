import 'package:atoma_cash/models/api/api/banking/response/connections.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:atoma_cash/models/api/error/error.dart';

@immutable
abstract class ConnectionsSyncState extends Equatable {
  ConnectionsSyncState();
}

@immutable
class Loading extends ConnectionsSyncState {
  Loading();

  @override
  List<Object> get props => null;
}

@immutable
class Loaded extends ConnectionsSyncState {
  final List<Connection> connections;
  final String syncingConnectionId;

  Loaded(this.connections, {this.syncingConnectionId});

  @override
  List<Object> get props => [connections, syncingConnectionId];
}

@immutable
class ManualSyncConnection extends ConnectionsSyncState {
  final UriModel uriModel;

  ManualSyncConnection(this.uriModel);

  @override
  List<Object> get props => [uriModel];
}

@immutable
class ErrorState extends ConnectionsSyncState {
  final Error error;

  ErrorState({this.error});

  @override
  List<Object> get props => [error];
}
