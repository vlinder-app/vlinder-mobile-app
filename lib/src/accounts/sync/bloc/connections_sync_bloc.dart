import 'package:atoma_cash/models/api/api/banking/request/connection_sync_model.dart';
import 'package:atoma_cash/models/api/api/banking/response/connections.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:bloc/bloc.dart';

import 'bloc.dart';

class ConnectionsSyncBloc
    extends Bloc<ConnectionsSyncEvent, ConnectionsSyncState> {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();

  List<Connection> _cachedConnections;

  @override
  ConnectionsSyncState get initialState => Loading();

  @override
  Stream<ConnectionsSyncState> mapEventToState(
      ConnectionsSyncEvent event) async* {
    if (event is Started) {
      yield* _mapStartedToState();
    }
    if (event is SyncStarted) {
      yield* _mapSyncToState(event.connectionId);
    }
    if (event is ConnectionSyncResultReceived) {
      yield* _mapConnectionSyncResultReceived(event);
    }
    if (event is Reload) {
      yield* _mapReloadToState();
    }
  }

  Stream<ConnectionsSyncState> _mapStartedToState() async* {
    yield Loading();
    yield* _getConnections();
  }

  Stream<ConnectionsSyncState> _mapReloadToState() async* {
    yield Loading();
    yield* _getConnections();
  }

  Stream<ConnectionsSyncState> _mapSyncToState(String connectionId) async* {
    yield Loaded(_cachedConnections, syncingConnectionId: connectionId);
    BaseResponse<UriModel> syncConnectionResponse = await _apiRemoteRepository
        .syncConnection(ConnectionSyncModel(connectionId: connectionId));
    if (syncConnectionResponse.isSuccess()) {
      yield ManualSyncConnection(syncConnectionResponse.result);
    } else {
      yield Loaded(_cachedConnections);
    }
  }

  Stream<ConnectionsSyncState> _mapConnectionSyncResultReceived(
      ConnectionSyncResultReceived event) async* {
    switch (event.result) {
      case ConnectionSyncResult.SUCCESS:
        yield* _getConnections();
        break;
      case ConnectionSyncResult.FAILED:
      case ConnectionSyncResult.CANCELED:
        yield Loaded(_cachedConnections);
    }
  }

  Stream<ConnectionsSyncState> _getConnections() async* {
    BaseResponse<Connections> connectionsResponse =
        await _apiRemoteRepository.getBankConnections();
    if (connectionsResponse.isSuccess()) {
      _cachedConnections = connectionsResponse.result.connections;
      yield Loaded(_cachedConnections);
    } else {
      yield ErrorState(error: connectionsResponse.error);
      yield Loaded([]);
    }
  }
}
