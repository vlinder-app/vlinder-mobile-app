import 'package:equatable/equatable.dart';

abstract class ConnectionsSyncEvent extends Equatable {
  ConnectionsSyncEvent();
}

class Started extends ConnectionsSyncEvent {
  Started();

  @override
  List<Object> get props => null;
}

class Reload extends ConnectionsSyncEvent {
  Reload();

  @override
  List<Object> get props => null;
}

class SyncStarted extends ConnectionsSyncEvent {
  final String connectionId;

  SyncStarted(this.connectionId);

  @override
  List<Object> get props => [connectionId];
}

class ConnectionUpdated extends ConnectionsSyncEvent {
  ConnectionUpdated();

  @override
  List<Object> get props => null;
}

enum ConnectionSyncResult { SUCCESS, FAILED, CANCELED }

class ConnectionSyncResultReceived extends ConnectionsSyncEvent {
  final ConnectionSyncResult result;
  final String errorMessage;

  ConnectionSyncResultReceived(this.result, {this.errorMessage});

  @override
  List<Object> get props => [result, errorMessage];
}
