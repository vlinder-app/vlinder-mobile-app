import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/accounts/accounts_screen.dart';
import 'package:atoma_cash/src/activity/activity_screen.dart';
import 'package:atoma_cash/src/banners/banners_navigator.dart';
import 'package:atoma_cash/src/home_placeholder/itab_listener.dart';
import 'package:atoma_cash/src/push_notifications/push_notification_permission_status_handler.dart';
import 'package:atoma_cash/src/reports/reports_list/reports_screen.dart';
import 'package:atoma_cash/src/settings/settings_screen.dart';
import 'package:flutter/material.dart';

enum TabId { ACCOUNTS, ACTIVITY, CATEGORIES, SETTINGS }

abstract class ITabNavigationProvider {
  void moveToTab(BuildContext context, TabId tabId);
}

class HomePlaceholder extends StatefulWidget {
  @override
  _HomePlaceholderState createState() => _HomePlaceholderState();
}

class _TabItem {
  final TabId tabId;
  final Widget widget;
  final String iconAsset;

  _TabItem(this.widget, this.iconAsset, this.tabId);
}

class _HomePlaceholderState extends State<HomePlaceholder>
    implements ITabNavigationProvider {
  PageController _pageController;
  int _tabControllerIndex = 0;

  List<_TabItem> _tabs;

  @override
  void initState() {
    _configureTabs();
    _pageController = PageController();
    super.initState();
  }

  void _configureTabs() {
    _tabs = [
      _TabItem(
          AccountsScreen(
              BannerNavigator(this), PushNotificationPermissionStatusHandler()),
          Assets.tabAccountsIcon,
          TabId.ACCOUNTS),
      _TabItem(ActivityScreen(), Assets.tabActivityIcon, TabId.ACTIVITY),
      _TabItem(ReportsScreen(), Assets.tabCategoriesIcon, TabId.CATEGORIES),
      _TabItem(MvpSettingsScreen(), Assets.tabSettingsIcon, TabId.SETTINGS),
    ];
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: _tabControllerIndex,
      length: _tabs.length,
      child: Builder(builder: (BuildContext context) {
        return Scaffold(
          body: _pageViewBuilder(context),
          bottomNavigationBar: SafeArea(
            child: new TabBar(
              onTap: (index) => _pageController.jumpToPage(index),
              tabs: _tabs
                  .map((t) => Tab(
                        icon: ImageIcon(AssetImage(t.iconAsset)),
                      ))
                  .toList(),
              labelColor: Palette.blue,
              unselectedLabelColor: Palette.mediumGrey,
            ),
          ),
        );
      }),
    );
  }

  void _onTabChanged(BuildContext context) {
    TabController tabController = DefaultTabController.of(context);
    int currentIndex = tabController.index;
    if (tabController.indexIsChanging && currentIndex != _tabControllerIndex) {
      _notifyTabWithIndex(currentIndex);
      _pageController.jumpToPage(currentIndex);
      _tabControllerIndex = currentIndex;
    }
  }

  void _notifyTabWithIndex(int index) {
    var widget = _tabs[index].widget;
    if (widget is ITabListener) {
      (widget as ITabListener).onAppear();
    }
  }

  Widget _pageViewBuilder(BuildContext context) {
    DefaultTabController.of(context).addListener(() => _onTabChanged(context));
    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: _tabs.map((t) => t.widget).toList(),
    );
  }

  @override
  void moveToTab(BuildContext context, TabId tabId) {
    DefaultTabController.of(context).animateTo(tabId.index,
        duration: Duration(milliseconds: 0), curve: Curves.ease);
  }
}
