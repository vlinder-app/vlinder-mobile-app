import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class TabPlaceholder extends StatelessWidget {
  final String text;

  const TabPlaceholder(this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
          child: Text(
        text,
        style: TextStyle(
            fontSize: 18.0,
            color: Palette.mediumGrey,
            fontWeight: FontWeight.w600),
      )),
    );
  }
}
