import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/navigation/navigation.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/click_ask_question_event.dart';
import 'package:atoma_cash/src/analytics/models/events/click_report_problem.dart';
import 'package:atoma_cash/src/analytics/models/events/click_suggest_feature_event.dart';
import 'package:atoma_cash/src/analytics/models/events/click_vote_on_features_event.dart';
import 'package:atoma_cash/src/analytics/models/events/logout_event.dart';
import 'package:atoma_cash/src/analytics/models/events/open_settings_tab_event.dart';
import 'package:atoma_cash/src/home_placeholder/itab_listener.dart';
import 'package:atoma_cash/src/registration/email_confirmation_request/email_confirmation_screen_configuration.dart';
import 'package:atoma_cash/src/registration/flow_controllers/email_confirmation_flow_controller.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/src/settings/bloc/bloc.dart';
import 'package:atoma_cash/src/settings/widgets/cells.dart';
import 'package:atoma_cash/src/settings/widgets/list/feedback_cell.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/src/widgets/rounded_icon.dart';
import 'package:atoma_cash/utils/launch_url/email_client_content_builder.dart';
import 'package:atoma_cash/utils/launch_url/launch_url_helper.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'base_asset/base_asset_search_delegate.dart';

class MvpSettingsScreen extends StatefulWidget implements ITabListener {
  @override
  _MvpSettingsScreenState createState() => _MvpSettingsScreenState();

  @override
  void onAppear() {
    Analytics().logEvent(OpenSettingsTabEvent());
  }
}

class _MvpSettingsScreenState extends State<MvpSettingsScreen>
    with AutomaticKeepAliveClientMixin {
  static const String valuesPlaceholder = "...";
  static const double feedbackItemBottomInset = 8.0;
  static const String bugReportEmailSubject = 'Bug Report';
  static const String questionEmailSubject = 'Question';

  SettingsBloc _settingsBloc;

  @override
  void initState() {
    super.initState();
    _settingsBloc = SettingsBloc()..add(Started());
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.dark,
        backgroundColor: Palette.blue,
        centerTitle: false,
        title: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(
            Localization.of(context).settingsTab,
            style: FontBook.headerH3.copyWith(color: Colors.white),
          ),
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(16.0),
          child: Container(),
        ),
        actions: <Widget>[],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20.0))),
      ),
      body: BlocListener(
        bloc: _settingsBloc,
        listener: _rootBlocListener,
        child: BlocBuilder(
          bloc: _settingsBloc,
          builder: _rootWidgetBuilder,
        ),
      ),
    );
  }

  void _rootBlocListener(BuildContext context, SettingsState state) {
    if (state is LoadedState) {
      if (!state.isFullyLoaded) {
        _showSettingsNotLoadedCorrectlySnackbar(context);
      }
    }
  }

  Widget _rootWidgetBuilder(BuildContext context, SettingsState state) {
    if (state is LoadedState) {
      return _loadedStateWidget(context, state);
    } else if (state is LoadingState) {
      return Center(child: CircularProgressIndicator());
    } else {
      return Container();
    }
  }

  Widget _loadedStateWidget(BuildContext context, LoadedState state) {
    return Container(
      color: Colors.white,
      child: ListView(
        children: <Widget>[
          Container(
              margin: EdgeInsets.only(top: 24.0),
              child: _securitySettingsSection(context, state)),
        ],
      ),
    );
  }

  Widget _securitySettingsSection(BuildContext context, LoadedState state) {
    return Container(
      child: Column(
        children: <Widget>[
          _accountSectionWidget(context, state),
          if (state.supportAndFeedbackLinks != null)
            _feedbackSection(context, state),
          Material(
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.only(top: 16),
                child: FourRowListTile(
                  leading: RoundedIcon(
                    iconColor: Colors.white,
                    asset: Assets.logoutIcon,
                  ),
                  title: Text(
                    Localization.of(context).logOut,
                    style: FontBook.textButton.copyWith(color: Palette.red),
                  ),
                  onClick: () {
                    Analytics().logEvent(LogoutEvent());
                    Navigation.signOut();
                  },
                ),
              ))
        ],
      ),
    );
  }

  Widget _accountSectionWidget(BuildContext context, LoadedState state) {
    return Column(
      children: <Widget>[
        Cells.header(Localization.of(context).accountSettingsHeader),
        FourRowListTile(
          leading: RoundedIcon(
            iconColor: Palette.paleGrey,
            asset: Assets.phoneIcon,
          ),
          title: Text(
            Localization.of(context).phoneNumber,
            style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
          ),
          subtitle: Text(
            state.phoneNumber != null && state.phoneNumber.isNotEmpty
                ? state.phoneNumber
                : Localization.of(context).tinNotProvided,
            style: FontBook.textBody2Menu.copyWith(color: Palette.mediumGrey),
          ),
        ),
        FourRowListTile(
          leading: RoundedIcon(
            iconColor: Palette.paleGrey,
            asset: Assets.emailIcon,
          ),
          title: Text(
            Localization.of(context).emailHint,
            style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
          ),
          subtitle: Text(
            state.email != null && state.email.isNotEmpty
                ? state.email
                : Localization.of(context).tinNotProvided,
            style: FontBook.textBody2Menu.copyWith(color: Palette.mediumGrey),
          ),
          bottom: _emailConfirmedWidget(context, state),
          onClick: state.isEmailConfirmed
              ? null
              : () => _moveToEmailConfirmation(state),
        ),
        FourRowListTile(
          leading: RoundedIcon(
            iconColor: Palette.paleGrey,
            asset: Assets.currencySettingsIndicator,
          ),
          title: Text(
            Localization.of(context).currencySettingsLabel,
            style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
          ),
          subtitle: Text(
            state.baseCurrency?.currency ?? "...",
            style: FontBook.textBody2Menu.copyWith(color: Palette.mediumGrey),
          ),
          trailing: Icon(
            Icons.arrow_forward_ios,
            size: 18.0,
            color: Palette.mediumGrey,
          ),
          onClick: () => _changeBaseAsset(
              context, _settingsBloc.currencies, state.baseCurrency),
        ),
      ],
    );
  }

  Widget _feedbackSection(BuildContext context, LoadedState state) {
    return Material(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Cells.header(Localization.of(context).helpAndFeedbackSettingsHeader),
          Padding(
            padding: EdgeInsets.only(bottom: feedbackItemBottomInset),
            child: FeedbackCell(
                leading: RoundedIcon(
                  iconColor: Palette.paleGrey,
                  asset: Assets.ideaIcon,
                ),
                title: Text(
                  Localization.of(context).suggestFeatureSettingsLink,
                  style: FontBook.textButton.copyWith(color: Palette.darkGrey),
                ),
                onClick: () {
                  Analytics().logEvent(ClickSuggestFeatureEvent());
                  LaunchUrlHelper.openLink(
                      state.supportAndFeedbackLinks.requestFeatureUrl);
                }),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: feedbackItemBottomInset),
            child: FeedbackCell(
                leading: RoundedIcon(
                  iconColor: Palette.paleGrey,
                  asset: Assets.thumbUpIcon,
                ),
                title: Text(
                  Localization.of(context).voteFeaturesSettingsLink,
                  style: FontBook.textButton.copyWith(color: Palette.darkGrey),
                ),
                onClick: () {
                  Analytics().logEvent(ClickVoteOnFeaturesEvent());
                  LaunchUrlHelper.openLink(
                      state.supportAndFeedbackLinks.voteFeaturesUrl);
                }),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: feedbackItemBottomInset),
            child: FeedbackCell(
                leading: RoundedIcon(
                  iconColor: Palette.paleGrey,
                  asset: Assets.reportIcon,
                ),
                title: Text(
                  Localization.of(context).reportProblemSettingsLink,
                  style: FontBook.textButton.copyWith(color: Palette.darkGrey),
                ),
                onClick: () {
                  Analytics().logEvent(ClickReportProblemEvent());
                  LaunchUrlHelper.openEmailClient(
                    context,
                    contentBuilder: DefaultMailtoContentBuilder(
                      email: state.supportAndFeedbackLinks.helpEmail,
                      subject: bugReportEmailSubject,
                      userId: state.externalUserId,
                      deviceInfo: state.deviceInfo,
                    ),
                  );
                }),
          ),
          FeedbackCell(
              leading: RoundedIcon(
                iconColor: Palette.paleGrey,
                asset: Assets.chatIcon,
              ),
              title: Text(
                Localization.of(context).askQuestion,
                style: FontBook.textButton.copyWith(color: Palette.darkGrey),
              ),
              onClick: () {
                Analytics().logEvent(ClickAskQuestionEvent());
                LaunchUrlHelper.openEmailClient(
                  context,
                  contentBuilder: DefaultMailtoContentBuilder(
                    email: state.supportAndFeedbackLinks.infoEmail,
                    subject: questionEmailSubject,
                    userId: state.externalUserId,
                    deviceInfo: state.deviceInfo,
                  ),
                );
              }),
        ],
      ),
    );
  }

  Widget _emailConfirmedWidget(BuildContext context, LoadedState state) {
    if (state.isEmailConfirmed || state.email == null) {
      return null;
    } else {
      return Text(
        Localization.of(context).notConfirmedLabel,
        style: FontBook.textBody2Menu.copyWith(color: Palette.mediumGrey),
      );
    }
  }

  void _showSettingsNotLoadedCorrectlySnackbar(BuildContext context) {
    Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          action: SnackBarAction(
            textColor: Colors.white,
            label: Localization.of(context).retryButton,
            onPressed: _updateSettings,
          ),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(Localization.of(context).settingsNotLoaded),
            ],
          ),
          backgroundColor: Palette.red,
        ),
      );
  }

  void _moveToEmailConfirmation(LoadedState state) {
    EmailConfirmationFlowController flowController =
        EmailConfirmationFlowController(EmailConfirmationFlow.SETTINGS,
            onEmailConfirmedCallback: _onSuccessEmailConfirmation);

    ExtendedNavigator.ofRouter<Router>().pushNamed(
      Routes.emailConfirmationRequestScreen,
      arguments: EmailConfirmationRequestScreenArguments(
        flowController: flowController,
        configuration: Default(email: state.email),
      ),
    );
  }

  void _changeBaseAsset(BuildContext context, List<Amount> currencies,
      Amount baseCurrency) async {
    final Amount selectedBaseCurrency = await showSearch(
        context: context,
        delegate: BaseAssetSearchDelegate(
            values: currencies, selectedAmount: baseCurrency));
    if (selectedBaseCurrency != null) {
      _settingsBloc.add(BaseCurrencyChanged(selectedBaseCurrency));
    }
  }

  void _onSuccessEmailConfirmation() {
    _updateSettings();
  }

  void _updateSettings() {
    _settingsBloc.add(Started());
  }

  @override
  bool get wantKeepAlive => true;
}
