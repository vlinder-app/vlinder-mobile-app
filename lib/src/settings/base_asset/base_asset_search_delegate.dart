import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class BaseAssetSearchDelegate extends SearchDelegate<Amount> {
  final List<Amount> values;
  final Amount selectedAmount;

  List<Amount> filterName = new List();

  BaseAssetSearchDelegate(
      {@required this.values, @required this.selectedAmount});

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      if (query.isNotEmpty)
        IconButton(
          icon: const Icon(
            (Icons.clear),
            color: Palette.mediumGrey,
          ),
          onPressed: () {
            query = '';
            showSuggestions(context);
          },
        ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return _queryFilterWidget();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return _queryFilterWidget();
  }

  bool _searchCondition(Amount currency, String query) =>
      currency.displayName.toLowerCase().contains(query.toLowerCase()) ||
      currency.currency.toLowerCase().contains(query.toLowerCase());

  Widget _queryFilterWidget() {
    final suggestions =
        values.where((c) => _searchCondition(c, query)).toList();

    return ListView.builder(
        itemCount: suggestions.length,
        itemBuilder: (BuildContext context, int index) {
          Amount currency = suggestions[index];
          return new ListTile(
            contentPadding:
                EdgeInsets.symmetric(horizontal: 16.0).copyWith(top: 8.0),
            trailing: Padding(
              padding: const EdgeInsets.all(8.0),
              child: _selectedIndicator(currency),
            ),
            title: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "${currency.displayName} (${currency.currency})",
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600),
              ),
            ),
            onTap: () {
              close(context, suggestions[index]);
            },
          );
        });
  }

  Widget _selectedIndicator(Amount currency) {
    if (currency.currency == selectedAmount.currency) {
      return Icon(
        Icons.check,
        size: 24.0,
        color: Palette.blue,
      );
    } else {
      return Container(
        width: 0.0,
        height: 0.0,
      );
    }
  }
}
