import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Cells {
  static Widget cell(
    String title, {
    String subtitle,
    Color titleColor = Palette.darkGrey,
    Color subtitleColor = Palette.mediumGrey,
    String imageAsset,
    VoidCallback onClick,
  }) {
    return InkWell(
      onTap: onClick,
      child: Container(
        margin: EdgeInsets.only(left: 24.0, right: 24.0),
        height: 64.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        title,
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: titleColor),
                      ),
                      margin: EdgeInsets.only(bottom: 3.0),
                    ),
                    if (subtitle != null)
                      Text(
                        subtitle,
                        style: TextStyle(
                            color: subtitleColor,
                            fontSize: 14.0,
                            fontWeight: FontWeight.w500),
                      ),
                  ],
                ),
              ),
            ),
            if (imageAsset != null)
              Container(
                width: 24.0,
                height: 24.0,
                child: Image.asset(imageAsset),
              ),
          ],
        ),
      ),
    );
  }

  static Widget header(String text, {VoidCallback onClick}) {
    return InkWell(
      onTap: onClick,
      child: Container(
        margin: EdgeInsets.only(left: 24.0, right: 24.0),
        height: 36.0,
        child: Container(
          alignment: Alignment(-1.0, 0.0),
          child: Text(
            text.toUpperCase(),
            style: FontBook.headerOverline.copyWith(color: Palette.mediumGrey),
          ),
        ),
      ),
    );
  }
}
