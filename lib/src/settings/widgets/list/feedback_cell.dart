import 'package:flutter/material.dart';

class FeedbackCell extends StatelessWidget {
  static const double defaultHeight = 52;

  final Widget leading;
  final Widget title;
  final VoidCallback onClick;

  FeedbackCell({Key key, this.leading, @required this.title, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: defaultHeight,
        child: InkWell(
          onTap: _onClick,
          child: Padding(
            padding: EdgeInsets.only(
              left: 24,
              top: 6,
              right: 24,
              bottom: 6,
            ),
            child: Row(
              children: <Widget>[
                if (leading != null)
                  Padding(
                    padding: const EdgeInsets.only(right: 16.0),
                    child: leading,
                  ),
                title
              ],
            ),
          ),
        ));
  }

  void _onClick() {
    if (onClick != null) {
      onClick();
    }
  }
}
