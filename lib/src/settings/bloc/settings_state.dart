import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/static_data/links.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SettingsState extends Equatable {
  SettingsState([List props = const <dynamic>[]]);
}

class InitialSettingsState extends SettingsState {
  @override
  String toString() {
    return 'InitialSettingsState{}';
  }

  @override
  List<Object> get props => null;
}

class LoadingState extends SettingsState {
  @override
  String toString() {
    return 'LoadingState{}';
  }

  @override
  List<Object> get props => null;
}

@immutable
class LoadedState extends SettingsState {
  final String userName;
  final String email;
  final String phoneNumber;
  final bool isFullyLoaded;
  final bool fatcaRelevant;
  final bool isEmailConfirmed;
  final Links supportAndFeedbackLinks;

  final String externalUserId;
  final String deviceInfo;
  final Amount baseCurrency;

  LoadedState(
      {this.userName,
      this.externalUserId,
      this.email,
      this.baseCurrency,
      this.phoneNumber,
      this.supportAndFeedbackLinks,
      this.deviceInfo,
      this.fatcaRelevant = false,
      this.isEmailConfirmed = false,
      this.isFullyLoaded = true});

  LoadedState copyWith(
      {String userName,
      Amount baseCurrency,
      String externalUserId,
      String email,
      String phoneNumber,
      String deviceInfo,
      Links supportAndFeedbackLinks,
      bool isFullyLoaded,
      bool isEmailConfirmed,
      bool fatcaRelevant}) {
    return LoadedState(
      baseCurrency: baseCurrency ?? this.baseCurrency,
      isEmailConfirmed: isEmailConfirmed ?? this.isEmailConfirmed,
      fatcaRelevant: fatcaRelevant ?? this.fatcaRelevant,
      userName: userName ?? this.userName,
      email: email ?? this.email,
      phoneNumber: phoneNumber ?? this.phoneNumber,
      isFullyLoaded: isFullyLoaded ?? this.isFullyLoaded,
      supportAndFeedbackLinks:
          supportAndFeedbackLinks ?? this.supportAndFeedbackLinks,
      externalUserId: externalUserId ?? this.externalUserId,
      deviceInfo: deviceInfo ?? this.deviceInfo,
    );
  }

  @override
  List<Object> get props => [
        userName,
        email,
        phoneNumber,
        isFullyLoaded,
        fatcaRelevant,
        isEmailConfirmed,
        baseCurrency,
        supportAndFeedbackLinks
      ];
}
