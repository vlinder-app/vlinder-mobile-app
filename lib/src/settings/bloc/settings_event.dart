import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SettingsEvent extends Equatable {
  SettingsEvent([List props = const <dynamic>[]]);
}

class Started extends SettingsEvent {
  @override
  String toString() {
    return 'Started{}';
  }

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class BaseCurrencyChanged extends SettingsEvent {
  final Amount baseCurrency;

  BaseCurrencyChanged(this.baseCurrency);

  @override
  List<Object> get props => [baseCurrency];
}
