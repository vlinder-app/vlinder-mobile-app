import 'dart:async';

import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/profile/response/profile.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/providers/currency_provider.dart';
import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/utils/device_info.dart';
import 'package:bloc/bloc.dart';

import './bloc.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  ApplicationLocalRepository _localRepository = ApplicationLocalRepository();

  List<Amount> _currencies = [];

  @override
  SettingsState get initialState => InitialSettingsState();

  List<Amount> get currencies => _currencies;

  @override
  Stream<SettingsState> mapEventToState(
    SettingsEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapStartedToState();
    }
    if (event is BaseCurrencyChanged) {
      yield* _mapBaseCurrencyChangedToState(event.baseCurrency);
    }
  }

  Stream<SettingsState> _mapStartedToState() async* {
    yield LoadingState();
    var baseCurrenciesFuture = BaseCurrencyProvider().baseCurrencies;
    var profileFuture = _apiRemoteRepository.getProfile();
    var linksFuture = _apiRemoteRepository.getLinks();
    var userIdFuture = _localRepository.getUserId();
    var deviceInfoFuture = DeviceInfo.getDeviceInfo();
    var baseCurrencyFuture = BaseCurrencyProvider().updateBaseCurrency();

    var futuresResult = await Future.wait([
      profileFuture,
      linksFuture,
      userIdFuture,
      deviceInfoFuture,
      baseCurrencyFuture,
      baseCurrenciesFuture
    ]);

    _currencies = futuresResult[5];
    LoadedState loadedState = LoadedState();

    if ((futuresResult[0] as BaseResponse).isSuccess()) {
      Profile profile = (futuresResult[0] as BaseResponse).result;
      loadedState = loadedState.copyWith(
          isEmailConfirmed: profile.emailConfirmed,
          userName: "${profile.lastName} ${profile.firstName}",
          externalUserId: futuresResult[2],
          deviceInfo: futuresResult[3],
          email: profile.email,
          phoneNumber: profile.phoneNumber,
          supportAndFeedbackLinks: (futuresResult[1] as BaseResponse).result,
          baseCurrency: (futuresResult[4] as Amount));
    }

    yield loadedState;
  }

  Stream<SettingsState> _mapBaseCurrencyChangedToState(
      Amount baseCurrency) async* {
    BaseCurrencyProvider().setBaseCurrency(baseCurrency);
    if (state is LoadedState) {
      yield (state as LoadedState).copyWith(baseCurrency: baseCurrency);
    }
  }
}
