import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:flutter/material.dart';

class ReceiptDeleteDialog extends StatelessWidget {
  final VoidCallback onDeleteClick;

  const ReceiptDeleteDialog({Key key, @required this.onDeleteClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: Dimensions.alertDialogPadding,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(12.0))),
      title: new Text(
        Localization.of(context).removeTransactionDialogTitle,
        style: TextStyles.alertDialogTitleStyle,
      ),
      content: new Text(Localization.of(context).removeTransactionDialogMessage,
          style: TextStyles.alertDialogSubTitleStyle),
      actions: <Widget>[
        new FlatButton(
          child: new Text(
            Localization.of(context).cancelButton,
            style: TextStyle(color: Palette.blue),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        new FlatButton(
          child: new Text(
            Localization.of(context).deleteButton,
            style: TextStyle(color: Palette.blue),
          ),
          onPressed: onDeleteClick,
        ),
        Container(
          width: 8.0,
        ),
      ],
    );
  }
}
