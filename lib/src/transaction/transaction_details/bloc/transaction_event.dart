import 'dart:io';

import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:equatable/equatable.dart';

abstract class TransactionEvent extends Equatable {
  const TransactionEvent();
}

class Started extends TransactionEvent {
  final Transaction transaction;

  Started(this.transaction);

  @override
  List<Object> get props => [transaction];
}

class RemoveEvent extends TransactionEvent {
  @override
  List<Object> get props => null;
}

class ClearTransactionImage extends TransactionEvent {
  @override
  List<Object> get props => null;
}

class RemoveTransactionImage extends TransactionEvent {
  RemoveTransactionImage();

  @override
  List<Object> get props => null;
}

class TransactionImageSelected extends TransactionEvent {
  final File transactionImage;

  TransactionImageSelected(this.transactionImage);

  @override
  List<Object> get props => [transactionImage];
}

class CategoryUpdated extends TransactionEvent {
  final Category category;

  CategoryUpdated(this.category);

  @override
  List<Object> get props => [category];
}

class UpdateCarbonFootprint extends TransactionEvent {
  @override
  List<Object> get props => null;
}

class UpdateAccountName extends TransactionEvent {
  @override
  List<Object> get props => null;
}

class PictureStateChanged extends TransactionEvent {
  final bool isLoading;

  PictureStateChanged(this.isLoading);

  @override
  List<Object> get props => [isLoading];
}
