import 'dart:typed_data';

import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/bloc/action.dart';
import 'package:equatable/equatable.dart';

abstract class BaseTransactionState extends Equatable {
  const BaseTransactionState();
}

class TransactionState extends BaseTransactionState {
  final Transaction transaction;
  final bool isLoading;
  final bool isPictureLoading;
  final List<Category> categories;
  final Action<Object> removeAction;
  final String pendingReceiptImageUri;
  final Uint8List transactionImage;
  final bool carbonFootprintLoading;
  final String accountName;

  TransactionState(
      {this.transaction,
      this.isLoading,
      this.isPictureLoading,
      this.accountName = "...",
      this.carbonFootprintLoading = false,
      this.categories = const [],
      this.pendingReceiptImageUri,
      this.transactionImage,
      this.removeAction});

  TransactionState copyWith(
          {Transaction transaction,
          bool isLoading,
            bool isPictureLoading,
            String accountName,
            bool carbonFootprintLoading,
          List<Category> categories,
            Uint8List transactionImage,
          String pendingReceiptImageUri,
          Uint8List receiptPhotoBytes,
          final Action<Object> removeAction}) =>
      TransactionState(
          accountName: accountName ?? this.accountName,
          transactionImage: transactionImage ?? this.transactionImage,
          pendingReceiptImageUri:
          pendingReceiptImageUri ?? this.pendingReceiptImageUri,
          transaction: transaction ?? this.transaction,
          isPictureLoading: isPictureLoading ?? this.isPictureLoading,
          carbonFootprintLoading:
          carbonFootprintLoading ?? this.carbonFootprintLoading,
          removeAction: removeAction ?? this.removeAction,
          isLoading: isLoading ?? this.isLoading,
          categories: categories ?? this.categories);

  TransactionState removeTransactionImage() =>
      TransactionState(
          transactionImage: null,
          isPictureLoading: this.isPictureLoading,
          accountName: this.accountName,
          pendingReceiptImageUri: null,
          categories: this.categories,
          carbonFootprintLoading: this.carbonFootprintLoading,
          transaction: this.transaction?.clearReceiptImageId(),
          isLoading: this.isLoading);

  @override
  List<Object> get props =>
      [
        isPictureLoading,
        accountName,
        transaction,
        carbonFootprintLoading,
        isLoading,
        removeAction,
        categories,
        pendingReceiptImageUri,
        transactionImage
      ];
}
