import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/categories/categories.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction_source_type.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/models/bloc/action.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/confirm_remove_receipt_event.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_container.dart';
import 'package:atoma_cash/src/transaction/transaction_receipt_image/transaction_image_controller.dart';
import 'package:bloc/bloc.dart';
import 'package:json_patch/json_patch.dart';

import './bloc.dart';

class TransactionBloc extends Bloc<TransactionEvent, TransactionState>
    implements ITransactionImageUpdateListener {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  TransactionImageController _imageController = TransactionImageController();
  String _transactionImageGuid;

  TransactionBloc() {
    _imageController.addListener(this);
  }

  @override
  Future<void> close() {
    _imageController.removeListener(this);
    return super.close();
  }

  @override
  TransactionState get initialState => TransactionState(isLoading: true);

  @override
  Stream<TransactionState> mapEventToState(
    TransactionEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapStartedToState(event.transaction);
    }
    if (event is RemoveEvent) {
      yield* _mapRemoveTransactionToState();
    }
    if (event is TransactionImageSelected) {
      yield* _mapTransactionImageSelectedToState(event.transactionImage);
    }
    if (event is RemoveTransactionImage) {
      yield* _mapRemoveTransactionImageToState();
    }
    if (event is CategoryUpdated) {
      yield* _mapCategoryUpdatedToState(event.category);
    }
    if (event is UpdateCarbonFootprint) {
      yield* _mapUpdateCarbonFootprintToState();
    }
    if (event is UpdateAccountName) {
      yield* _mapUpdateAccountNameToState();
    }
    if (event is PictureStateChanged) {
      yield* _mapPictureStateChangedToState(event.isLoading);
    }
    if (event is ClearTransactionImage) {
      yield* _mapClearTransactionImageToState();
    }
  }

  Stream<TransactionState> _mapClearTransactionImageToState() async* {
    yield state.removeTransactionImage();
  }

  Stream<TransactionState> _mapPictureStateChangedToState(
      bool isPictureLoading) async* {
    yield state.copyWith(isPictureLoading: isPictureLoading);
  }

  Stream<TransactionState> _mapUpdateCarbonFootprintToState() async* {
    yield state.copyWith(carbonFootprintLoading: true);
    BaseResponse<Amount> carbonResponse =
        await _apiRemoteRepository.getEcoFootprintForCategory(
            state.transaction.categories.first.id,
            state.transaction.baseCurrencyAmount);
    if (carbonResponse.isSuccess()) {
      double carbonValue = carbonResponse?.result?.value;
      if (carbonValue != null) {
        yield state.copyWith(
            transaction: state.transaction
                .copyWith(carbonFootprint: carbonResponse?.result?.value),
            carbonFootprintLoading: false);
        _silentlyUpdateTransaction(state.transaction,
            state.transaction.copyWith(carbonFootprint: carbonValue));
      } else {
        yield state.copyWith(
            transaction: state.transaction.resetFootprint(),
            carbonFootprintLoading: false);
      }
    }
  }

  Stream<TransactionState> _mapUpdateAccountNameToState() async* {
    BaseResponse<Accounts> accountsResponse =
        await _apiRemoteRepository.getBankAccounts();
    if (accountsResponse.isSuccess()) {
      List<Account> accounts = accountsResponse.result.accounts;
      Account account = accounts?.firstWhere(
          (element) => element.id == state.transaction.accountId,
          orElse: () => null);
      if (account != null) {
        yield state.copyWith(accountName: account.name);
      }
    }
  }

  Stream<TransactionState> _mapCategoryUpdatedToState(
      Category category) async* {
    Transaction transactionWithUpdatedCategory =
        state.transaction.copyWith(categories: [category]);
    _silentlyUpdateTransaction(
        state.transaction, transactionWithUpdatedCategory);
    yield state.copyWith(transaction: transactionWithUpdatedCategory);
    double amount = state.transaction.amount.value;
    if (amount != null && amount != 0.0) {
      add(UpdateCarbonFootprint());
    }
  }

  Stream<TransactionState> _mapStartedToState(Transaction transaction) async* {
    BaseResponse<Categories> categoriesResponse =
        await _apiRemoteRepository.getCategories();
    yield state.copyWith(
        transaction: transaction,
        isLoading: false,
        isPictureLoading: _imageController
                .imageRequestWithGuid(transaction?.receiptImageId) !=
            null,
        categories: categoriesResponse.result?.categories ?? []);
    if (transaction.sourceType == TransactionSourceType.BANK_ACCOUNT) {
      add(UpdateAccountName());
    }
    yield* _prepareReceiptPhoto();
  }

  Stream<TransactionState> _mapRemoveTransactionImageToState() async* {
    String guid = state.transaction.receiptImageId ?? _transactionImageGuid;
    _imageController.removeReceipt(guid);
    yield state.removeTransactionImage();
  }

  Stream<TransactionState> _mapTransactionImageSelectedToState(
      File transactionImage) async* {
    _transactionImageGuid = _imageController.addImageToTransaction(
        ReceiptImageContainer.fromFile(transactionImage),
        transactionId: state.transaction.id);
    _silentlyUpdateTransaction(state.transaction,
        state.transaction.copyWith(receiptImageId: _transactionImageGuid));
    var imageBytes = await transactionImage.readAsBytes();
    yield state.copyWith(
        transactionImage: imageBytes,
        transaction:
            state.transaction.copyWith(receiptImageId: _transactionImageGuid));
  }

  Stream<TransactionState> _prepareReceiptPhoto() async* {
    if (state.transaction.receiptImageId != null) {
      yield state.copyWith(
          pendingReceiptImageUri: _apiRemoteRepository
              .getPendingReceiptUriForGuid(state.transaction.receiptImageId));
    }
  }

  void _silentlyUpdateTransaction(
      Transaction _originTransaction, Transaction _editedTransaction) {
    if (_originTransaction != null) {
      final diffMap = JsonPatch.diff(
          _originTransaction.toJson(), _editedTransaction.toJson());
      _apiRemoteRepository.patchTransaction(
          state.transaction.id, json.encode(diffMap));
    }
  }

  Stream<TransactionState> _mapRemoveTransactionToState() async* {
    Analytics().logEvent(ConfirmRemoveReceiptEvent());
    BaseResponse<Null> removeTransactionResponse = await _apiRemoteRepository
        .removeTransactionWithId(state.transaction.id);
    if (removeTransactionResponse.isSuccess()) {
      yield state.copyWith(removeAction: Action(result: Object()));
    } else {
      yield state.copyWith(
          removeAction: Action(error: removeTransactionResponse.error));
    }
  }

  void _handlePictureEvent(String guid) {
    if (guid == state.transaction.receiptImageId) {
      add(PictureStateChanged(false));
    }
  }

  @override
  void onFailedUpload(String guid) => _handlePictureEvent(guid);

  @override
  void onSuccessUpload(String guid) => _handlePictureEvent(guid);
}
