import 'dart:io';

import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction_source_type.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/bank/webview/custom_app_bar.dart';
import 'package:atoma_cash/src/media/media_controller.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/src/transaction/transaction_details/bloc/bloc.dart';
import 'package:atoma_cash/src/transaction/transaction_details/dialogs/receipt_delete_dialog.dart';
import 'package:atoma_cash/src/transaction/transaction_operations/transaction_operations_screen.dart';
import 'package:atoma_cash/src/transaction/widgets/add_transaction_photo_button.dart';
import 'package:atoma_cash/src/transaction/widgets/dialogs/picture_options_dialog.dart';
import 'package:atoma_cash/src/transaction/widgets/merchant_icon.dart';
import 'package:atoma_cash/src/transaction/widgets/receipt_details.dart';
import 'package:atoma_cash/src/transaction/widgets/transaction_picture_loading_placeholder.dart';
import 'package:atoma_cash/src/transaction/widgets/transaction_universal_image.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/src/widgets/progress_dialog.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class RemoveAction {}

class TransactionDetailsScreen extends StatefulWidget {
  final Transaction transaction;

  const TransactionDetailsScreen({Key key, @required this.transaction})
      : super(key: key);

  @override
  _TransactionDetailsScreenState createState() =>
      _TransactionDetailsScreenState();
}

class _TransactionDetailsScreenState extends State<TransactionDetailsScreen> {
  TransactionBloc _bloc;
  MediaController _mediaController;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => _postFrameInit());
    _bloc = TransactionBloc()..add(Started(widget.transaction));
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _postFrameInit() {
    _mediaController = MediaController(context,
        onCameraFileSelect: _onTransactionImageSelected,
        onGalleryFileSelect: _onTransactionImageSelected);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _bloc,
      listener: _blocListener,
      child: BlocBuilder(
        bloc: _bloc,
        builder: (context, TransactionState state) {
          return Scaffold(
            appBar: ModalStyleAppBar(
              AppBar(
                leading: IconButton(
                  icon: Icon(Icons.close),
                  onPressed: () => Navigator.of(context)
                      .pop(UpdateOperation(state.transaction)),
                ),
                backgroundColor: Colors.transparent,
                actions: <Widget>[
                  Widgets.styledFlatButton(
                    text: Localization.of(context).editButton,
                    onPressed: () => _onEditClick(state),
                  )
                ],
              ),
              statusBarBackgroundColor: Colors.transparent,
            ),
            backgroundColor: Colors.transparent,
            body: Container(
              color: Colors.white,
              child: _rootContentBuilder(context, state),
            ),
          );
        },
      ),
    );
  }

  Widget _rootContentBuilder(BuildContext context, TransactionState state) {
    if (state.isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return _transactionFieldsWidget(context, state);
    }
  }

  Widget _transactionFieldsWidget(
      BuildContext context, TransactionState state) {
    return Container(
      color: Colors.white,
      child: ListView(
        children: <Widget>[
          _infoSection(context, state.transaction),
          _amountSection(context, state.transaction),
          _transactionPhotoSection(context, state),
          TransactionDetails(
            paidWithText: _paidWithTextForState(context, state),
            carbonFootprintLoading: state.carbonFootprintLoading,
            transaction: state.transaction,
            onCategoryClick: () => _onCategoryClick(context, state),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 16.0),
            child: _removeButton(context, state),
          ),
          Container(
            height: 50.0,
          )
        ],
      ),
    );
  }

  String _paidWithTextForState(BuildContext context, TransactionState state) {
    switch (state.transaction.sourceType) {
      case TransactionSourceType.BANK_ACCOUNT:
        return state.accountName;
      case TransactionSourceType.MANUAL:
        return Localization.of(context).transactionTypeManual;
      case TransactionSourceType.BITCOIN_WALLET:
        return "undefined";
    }
  }

  Widget _transactionPhotoSection(
      BuildContext context, TransactionState state) {
    if (state.isPictureLoading) {
      return TransactionPictureLoadingPlaceholder();
    } else if (state.pendingReceiptImageUri != null) {
      return TransactionUniversalImage(
        imageUrl: state.pendingReceiptImageUri,
        onRemoveImageClick: _onRemoveTransactionImageClick,
      );
    } else if (state.transactionImage != null) {
      return TransactionUniversalImage(
        imageBytes: state.transactionImage,
        onRemoveImageClick: _onRemoveTransactionImageClick,
      );
    } else {
      return AddTransactionPhotoButton(
        onClick: _onAddTransactionPictureClick,
      );
    }
  }

  void _onTransactionImageSelected(File transactionImage) {
    _bloc.add(TransactionImageSelected(transactionImage));
  }

  void _onCategoryClick(BuildContext context, TransactionState state) async {
    final result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.categorySelectScreen,
        arguments: CategorySelectScreenArguments(
            allCategories: state.categories,
            selectedCategory: state.transaction?.categories?.first));

    if (result is Category) {
      _bloc.add(CategoryUpdated(result));
    }
  }

  void _onCameraClick() {
    Navigator.of(context).pop();
    _mediaController.getPictureFromCamera(context);
  }

  void _onGalleryClick() {
    Navigator.of(context).pop();
    _mediaController.getPictureFromGallery(context);
  }

  void _onAddTransactionPictureClick() {
    showModalBottomSheet<void>(
        context: context,
        shape: PictureOptionsDialog.border,
        builder: (_) {
          return PictureOptionsDialog(
            onCameraClick: _onCameraClick,
            onGalleryClick: _onGalleryClick,
          );
        });
  }

  void _onRemoveTransactionImageClick() => _bloc.add(RemoveTransactionImage());

  Widget _infoSection(BuildContext context, Transaction transaction) {
    return Container(
      height: 120.0,
      padding: EdgeInsets.only(
        left: 24.0,
        right: 24.0,
      ),
      child: Row(
        children: <Widget>[
          ClipOval(
            child: Container(
              width: 72.0,
              height: 72.0,
              child: MerchantIcon(transaction?.counterparty?.iconUrl),
            ),
          ),
          Expanded(
            child: FourRowListTile(
              forceThreeRow: true,
              title: Text(
                DateFormat.yMMMMd()
                    .format(transaction.bookedAt?.toLocal() ?? DateTime.now()),
                style: FourRowListTile.subtitleTextStyle,
              ),
              subtitle: Text(
                _infoSectionSubtitle(context, transaction),
                maxLines: 2,
                overflow: TextOverflow.clip,
                style: FourRowListTile.titleTextStyle.copyWith(fontSize: 16.0),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _amountSection(BuildContext context, Transaction transaction) {
    return Column(
      children: <Widget>[
        Container(
          height: 88.0,
          child: Center(
            child: Text(
              transaction.amount.currencyFormatted(),
              style: TextStyle(fontSize: 40.0, fontWeight: FontWeight.w500),
            ),
          ),
        ),
        if (transaction.amount.currency !=
            transaction.baseCurrencyAmount?.currency)
          Container(
            margin: EdgeInsets.only(bottom: 16.0),
            child: Center(
              child: Text(
                transaction.formattedTransactionAmountInBase,
                style:
                    FontBook.textBody2Menu.copyWith(color: Palette.mediumGrey),
              ),
            ),
          ),
      ],
    );
  }

  Widget _removeButton(BuildContext context, TransactionState state) {
    if (state.transaction.sourceType == TransactionSourceType.MANUAL)
      return Container(
        height: 72.0,
        child: Material(
          color: Colors.white,
          child: FourRowListTile(
            onClick: _onRemoveReceiptClick,
            leading: Image.asset(
              Assets.trashIconRed,
              width: 40.0,
              height: 40.0,
              fit: BoxFit.fill,
            ),
            title: Text(
              Localization.of(context).removeReceipt,
              style:
                  FourRowListTile.titleTextStyle.copyWith(color: Palette.red),
            ),
          ),
        ),
      );
    else {
      return Container();
    }
  }

  String _infoSectionSubtitle(BuildContext context, Transaction transaction) {
    if (transaction.counterparty?.name != null) {
      return transaction.counterparty.name;
    } else {
      return Localization.of(context)
          .unknownMerchantTransactionTitlePlaceholder;
    }
  }

  void _showRemoveReceiptConfirmationDialog() {
    showDialog(
        context: context,
        builder: (_) => ReceiptDeleteDialog(
          onDeleteClick: _onConfirmReceiptRemoveClick,
        ));
  }

  void _onConfirmReceiptRemoveClick() {
    Navigator.of(context).pop();
    _showProcessingDialog();
    _bloc.add(RemoveEvent());
  }

  void _showProcessingDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) {
          return ProgressDialog(
            text: Localization.of(context).removingProcess,
          );
        });
  }

  void _onRemoveReceiptClick() {
    _showRemoveReceiptConfirmationDialog();
  }

  void _onEditClick(TransactionState state) {
    _editTransaction(state);
  }

  void _editTransaction(TransactionState state) async {
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.transactionOperationsScreen,
        arguments: TransactionOperationsScreenArguments(
            transaction: state.transaction,
            operation: TransactionOperation.EDIT));
    if (result is EditOperation) {
      Navigator.of(context).pop(result);
    }
    if (result is UpdateOperation) {
      _bloc.add(ClearTransactionImage());
      _bloc.add(Started(result.transaction));
    }
  }

  void _blocListener(BuildContext context, TransactionState state) {
    if (state.removeAction != null) {
      Navigator.of(context).pop();
      if (state.removeAction.result != null) {
        Navigator.pop(context, RemoveAction());
      }
      if (state.removeAction.error != null) {}
    }
  }
}
