import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_container.dart';
import 'package:dio/dio.dart';
import 'package:uuid/uuid.dart';

abstract class ITransactionImageUpdateListener {
  void onFailedUpload(String guid);

  void onSuccessUpload(String guid);
}

class ImageRequest {
  String transactionId;
  CancelToken cancelToken;
  bool failed;

  ImageRequest(this.cancelToken, {this.failed, this.transactionId});

  @override
  String toString() {
    return 'ImageRequest{transactionId: $transactionId, cancelToken: $cancelToken, failed: $failed}';
  }
}

class TransactionImageController {
  static final TransactionImageController _instance =
      TransactionImageController._internal();

  TransactionImageController._internal() {
    _apiRemoteRepository = ApiRemoteRepository();
  }

  Map<String, ImageRequest> _imageRequests = {};

  ImageRequest imageRequestWithGuid(String guid) => _imageRequests[guid];

  List<String> get failedImageTransactionsId => _imageRequests.entries
      .where((element) => element.value.failed)
      .map((e) => e.value.transactionId)
      .toList();

  factory TransactionImageController() {
    return _instance;
  }

  void dispose() {
    _imageRequests = null;
    _listeners = null;
  }

  List<ITransactionImageUpdateListener> _listeners = [];

  ApiRemoteRepository _apiRemoteRepository;

  void addListener(ITransactionImageUpdateListener listener) =>
      _listeners.add(listener);

  void removeListener(ITransactionImageUpdateListener listener) =>
      _listeners.remove(listener);

  void removeReceipt(String guid) {
    if (guid != null) {
      _stopProcessing(guid);
      _imageRequests.remove(guid);
      _apiRemoteRepository.removeTransactionReceiptImage(guid);
    }
  }

  void _stopProcessing(String guid) {
    _imageRequests[guid]?.cancelToken?.cancel("Stopped by user");
  }

  String _fillRequests(String transactionId) {
    String uuid = Uuid().v4();
    _imageRequests.update(
        uuid,
        (request) => ImageRequest(request.cancelToken,
            failed: false, transactionId: transactionId),
        ifAbsent: () => ImageRequest(CancelToken(),
            failed: false, transactionId: transactionId));
    return uuid;
  }

  String addImageToTransaction(ReceiptImageContainer imageContainer,
      {String transactionId}) {
    var guid = _fillRequests(transactionId);
    _performReceipt(transactionId, guid, _imageRequests[guid], imageContainer);
    return guid;
  }

  void _notifyWithSuccessUpload(String guid) {
    _listeners.forEach((listener) => listener.onSuccessUpload(guid));
  }

  void _notifyWithFailedUpload(String guid) {
    _listeners.forEach((listener) => listener.onFailedUpload(guid));
  }

  Future<void> _performReceipt(String transactionId, String guid,
      ImageRequest imageRequest, ReceiptImageContainer receiptContainer) async {
    var receiptFileBytes = await receiptContainer.receiptByteData();
    BaseResponse<Object> scanResponse = await _apiRemoteRepository
        .addTransactionReceiptImage(receiptFileBytes, transactionId, guid,
            cancelToken: imageRequest.cancelToken);
    if (scanResponse.isSuccess()) {
      _imageRequests.remove(guid);
      _notifyWithSuccessUpload(guid);
    } else {
      _imageRequests.update(guid, (value) => value..failed = true);
      _notifyWithFailedUpload(guid);
    }
  }
}
