import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/transaction/category_select/widgets/category_select_cell.dart';
import 'package:atoma_cash/src/transaction/widgets/transaction_title.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PrimaryCategorySelectList extends StatelessWidget {
  static const String primaryCategoryParentId = '-1';
  static const double disclosureIndicatorIconSize = 18.0;

  final List<Category> _allCategories;
  final Category _selectedCategory;
  final OnCategorySelected _onCategorySelected;
  int get _headerItemsCount => 1;

  PrimaryCategorySelectList(
      {Key key,
      List<Category> allCategories,
      Category selectedCategory,
      OnCategorySelected onCategorySelected})
      : _allCategories = allCategories,
        _selectedCategory = selectedCategory,
        _onCategorySelected = onCategorySelected,
        super(key: key);

  Widget build(BuildContext context) {
    final categories = _primaryCategories();
    return ListView.builder(
        itemCount: categories.length + _headerItemsCount,
        itemBuilder: (BuildContext context, int index) {
          if (index < _headerItemsCount) {
            return _headerItem(context);
          } else {
            Category category = categories[index - _headerItemsCount];

            final titleStyle = category.id == _selectedCategory?.id || category.id == _selectedCategory?.parentId
                ? CategorySelectCell.highlightedTitleStyle
                : CategorySelectCell.normalTitleStyle;

            return CategorySelectCell(
              category: category,
              trailing: Icon(
                Icons.arrow_forward_ios,
                size: disclosureIndicatorIconSize,
                color: Palette.mediumGrey,
              ),
              titleStyle: titleStyle,
              onTap: _onCategorySelected,
            );
          }
        });
  }

  Widget _headerItem(BuildContext context) => LargeTitle(Localization.of(context).categoriesHeader);

  List<Category> _primaryCategories() => _allCategories.where((category) {
        return category.parentId == primaryCategoryParentId;
      }).toList();
}
