import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/src/widgets/rounded_icon.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

typedef OnCategorySelected = Function(BuildContext context, Category category);

class CategorySelectCell extends StatelessWidget {
  static const double titlePadding = 8.0;

  static const TextStyle normalTitleStyle =
      TextStyle(fontSize: 14.0, fontWeight: FontWeight.w500);
  static const TextStyle highlightedTitleStyle =
      TextStyle(fontSize: 14.0, fontWeight: FontWeight.w700);

  final Category category;
  final Widget trailing;
  final TextStyle titleStyle;
  final OnCategorySelected onTap;

  CategorySelectCell(
      {Key key,
      this.category,
      this.trailing,
      this.titleStyle = CategorySelectCell.normalTitleStyle,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: RoundedIcon(
          iconStringColor: category.color, iconUrl: category.iconUrl),
      trailing: trailing,
      title: Padding(
        padding: const EdgeInsets.all(titlePadding),
        child: Text(
          category.name,
          style: titleStyle,
        ),
      ),
      onTap: () {
        onTap(context, category);
      },
    );
  }
}
