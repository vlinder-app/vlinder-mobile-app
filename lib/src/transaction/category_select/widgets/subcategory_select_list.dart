import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/settings/widgets/cells.dart';
import 'package:atoma_cash/src/transaction/category_select/widgets/category_select_cell.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SubcategoryCategorySelectList extends StatelessWidget {
  static const double headerTopPadding = 12.0;
  static const double checkMarkPadding = 8.0;

  final List<Category> _allCategories;
  final Category _selectedCategory;
  final Category _primaryCategory;
  final OnCategorySelected _onCategorySelected;

  int get _headerItemsCount => _primaryCategory == null ? 1 : 2;

  SubcategoryCategorySelectList(
      {Key key,
      List<Category> allCategories,
      Category selectedCategory,
      Category primaryCategory,
      OnCategorySelected onCategorySelected})
      : _allCategories = allCategories,
        _selectedCategory = selectedCategory,
        _primaryCategory = primaryCategory,
        _onCategorySelected = onCategorySelected,
        super(key: key);

  Widget build(BuildContext context) {
    final categories = _subcategoriesCategories();
    return ListView.builder(
        itemCount: categories.length + _headerItemsCount,
        itemBuilder: (BuildContext context, int index) {
          if (index < _headerItemsCount) {
            return _headerItem(context, index: index);
          } else {
            Category category = categories[index - _headerItemsCount];
            return _categoryCell(category);
          }
        });
  }

  Widget _headerItem(BuildContext context, {int index}) {
    if (_primaryCategory != null && index == 0) {
      return _categoryCell(_primaryCategory);
    } else {
      return Padding(
        padding: EdgeInsets.only(top: headerTopPadding),
        child: Cells.header(Localization.of(context).subcategoriesHeader),
      );
    }
  }

  Widget _categoryCell(Category category) {
    return CategorySelectCell(
      category: category,
      trailing: (category.id == _selectedCategory?.id) ? _checkMarkIndicator() : null,
      onTap: _onCategorySelected,
    );
  }

  Widget _checkMarkIndicator() {
    return Padding(
        padding: EdgeInsets.all(checkMarkPadding),
        child: Icon(
          Icons.check,
          color: Palette.blue,
        ));
  }

  List<Category> _subcategoriesCategories() => _allCategories.where((category) {
        return category.parentId == _primaryCategory?.id;
      }).toList();
}
