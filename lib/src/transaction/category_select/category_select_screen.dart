import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/src/bank/webview/custom_app_bar.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/src/transaction/category_select/widgets/primary_category_select_list.dart';
import 'package:atoma_cash/src/transaction/category_select/widgets/subcategory_select_list.dart';
import 'package:atoma_cash/src/transaction/widgets/categories_search_delegate.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategorySelectScreen extends StatelessWidget {
  final List<Category> allCategories;
  final Category selectedCategory;
  final Category primaryCategory;

  CategorySelectScreen(
      {Key key,
      this.allCategories,
      this.selectedCategory,
      this.primaryCategory})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: ModalStyleAppBar(
          AppBar(
            backgroundColor: Colors.transparent,
            actions: _appBarActions(context),
          ),
        ),
        body: _bodyWidget());
  }

  List<Widget> _appBarActions(BuildContext context) => [
        IconButton(
          icon: Image.asset(Assets.magnifierIcon, color: IconTheme.of(context).color),
          onPressed: () {
            _onSearchClicked(context);
          },
        ),
      ];

  Widget _bodyWidget() {
    if (primaryCategory == null) {
      return PrimaryCategorySelectList(
        allCategories: allCategories,
        selectedCategory: selectedCategory,
        onCategorySelected: _onCategoryTapped,
      );
    } else {
      return SubcategoryCategorySelectList(
          allCategories: allCategories,
          selectedCategory: selectedCategory,
          primaryCategory: primaryCategory,
          onCategorySelected: _onCategoryTapped);
    }
  }

  void _onSearchClicked(BuildContext context) async {
    final Category result = await showSearch(
        context: context,
        delegate: CategoriesSearchDelegate(allCategories, selectedCategory));

    if (result != null) {
      Navigator.of(context).pop(result);
    }
  }

  void _onCategoryTapped(BuildContext context, Category category) {
    if (primaryCategory != null) {
      Navigator.of(context).pop(category);
    } else {
      _showSubcategories(context, category);
    }
  }

  void _showSubcategories(BuildContext context, Category category) async {
    final result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.categorySelectScreen,
        arguments: CategorySelectScreenArguments(
            allCategories: allCategories,
            selectedCategory: selectedCategory,
            primaryCategory: category));

    if (result != null) {
      Navigator.of(context).pop(result);
    }
  }
}
