import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/activity/widgets/dialogs/option_cell.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:flutter/material.dart';

typedef CurrencySelectedCallback = void Function(Amount currency);

class CurrencySelectorDialog extends StatelessWidget {
  static const kBorderRadius = 20.0;
  static const RoundedRectangleBorder border = const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(kBorderRadius),
          topRight: Radius.circular(kBorderRadius)));

  final List<Amount> currencies;
  final CurrencySelectedCallback currencySelectedCallback;

  const CurrencySelectorDialog({
    Key key,
    @required this.currencies,
    @required this.currencySelectedCallback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: _receiptOptionsBuilder(context),
      )),
    );
  }

  List<Widget> _currenciesList(BuildContext context, List<Amount> currencies) =>
      List.generate(
          currencies.length,
          (index) => OptionsCell(
                _currencyText(currencies[index]),
                onClick: () => currencySelectedCallback(currencies[index]),
              ));

  String _currencyText(Amount currency) =>
      "${currency.currency} (${currency.displayName})";

  List<Widget> _receiptOptionsBuilder(BuildContext context) {
    return <Widget>[
      ..._currenciesList(context, currencies),
      OptionsCell(Localization.of(context).cancelButton,
          onClick: () => Navigator.of(context).pop(),
          style: FourRowListTile.titleTextStyle.copyWith(color: Palette.blue)),
    ];
  }
}
