import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:atoma_cash/models/api/api/categories/categories.dart';
import 'package:atoma_cash/models/api/api/categories/spendings.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction_source_type.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/providers/currency_provider.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/enter_amount_event.dart';
import 'package:atoma_cash/src/analytics/models/events/enter_merchant_name_event.dart';
import 'package:atoma_cash/src/analytics/models/events/save_date_event.dart';
import 'package:atoma_cash/src/analytics/models/events/save_manual_tx_event.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_container.dart';
import 'package:atoma_cash/src/transaction/transaction_receipt_image/transaction_image_controller.dart';
import 'package:bloc/bloc.dart';
import 'package:json_patch/json_patch.dart';

import './bloc.dart';
import '../transaction_operations_screen.dart';

class TransactionBloc extends Bloc<TransactionEvent, TransactionState>
    implements ITransactionImageUpdateListener {
  final TransactionOperation mode;

  static const kDefaultCategoryId = '0';
  static const _pathKey = 'path';
  static const _valueKey = 'value';

  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  TransactionImageController _imageController = TransactionImageController();

  Timer _amountChangedTimer;
  Timer _merchantNameTimer;
  String _transactionImageGuid;
  Transaction _originTransaction;

  TransactionBloc(this.mode) {
    _imageController.addListener(this);
  }

  @override
  Future<void> close() {
    _imageController.removeListener(this);
    return super.close();
  }

  @override
  TransactionState get initialState =>
      TransactionState(isLoading: true, ignoreValidation: true);

  @override
  Stream<TransactionState> mapEventToState(
    TransactionEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapStartedToState(event.transaction);
    }
    if (event is FieldUpdated) {
      yield* _mapFieldUpdatedState(event);
    }
    if (event is CreateEvent) {
      yield* _mapCreateToState();
    }
    if (event is EditEvent) {
      yield* _mapEditToState(event.originTransaction);
    }
    if (event is UpdateCarbonFootprint) {
      yield* _mapUpdateCarbonFootprintToState(
          withNetworkUpdate: event.withNetworkUpdate);
    }
    if (event is TransactionImageSelected) {
      yield* _mapTransactionImageSelectedToState(event.transactionImage);
    }
    if (event is RemoveTransactionImage) {
      yield* _mapRemoveTransactionImageToState();
    }
  }

  Stream<TransactionState> _mapRemoveTransactionImageToState() async* {
    String guid = state.transaction.receiptImageId ?? _transactionImageGuid;
    _imageController.removeReceipt(guid);
    yield state.removeTransactionImage();
  }

  Stream<TransactionState> _mapTransactionImageSelectedToState(
      File transactionImage) async* {
    _transactionImageGuid = _imageController.addImageToTransaction(
        ReceiptImageContainer.fromFile(transactionImage),
        transactionId: state.transaction.id);
    _silentlyUpdateTransaction(_originTransaction,
        _originTransaction.copyWith(receiptImageId: _transactionImageGuid));
    var imageBytes = await transactionImage.readAsBytes();
    yield state.copyWith(
        transactionImage: imageBytes,
        transaction:
            state.transaction.copyWith(receiptImageId: _transactionImageGuid));
  }

  Stream<TransactionState> _mapFieldUpdatedState(
      FieldUpdated fieldUpdated) async* {
    TransactionState nextState;
    if (fieldUpdated.isImageLoading != null) {
      nextState = state.copyWith(isPictureLoading: fieldUpdated.isImageLoading);
    }
    if (fieldUpdated.dateTime != null) {
      Analytics().logEvent(SaveDateEvent(fieldUpdated.dateTime));
      nextState = state.copyWith(
          transaction: state.transaction
              .copyWith(bookedAt: fieldUpdated.dateTime.toUtc()));
    }
    if (fieldUpdated.category != null) {
      Transaction transactionWithUpdatedCategory =
          state.transaction.copyWith(categories: [fieldUpdated.category]);
      if (mode == TransactionOperation.EDIT) {
        _silentlyUpdateTransaction(
            state.transaction, transactionWithUpdatedCategory);
      }
      nextState = state.copyWith(
          transaction:
              state.transaction.copyWith(categories: [fieldUpdated.category]));
      double amount = state.transaction.amount.value;
      if (amount != null && amount != 0.0) {
        add(UpdateCarbonFootprint(
            withNetworkUpdate: mode == TransactionOperation.EDIT));
      }
    }
    if (fieldUpdated.merchantName != null) {
      _merchantNameTimer?.cancel();
      Counterparty counterparty;
      if (fieldUpdated.merchantName.isNotEmpty) {
        counterparty = state.transaction.counterparty
            .copyWith(name: fieldUpdated.merchantName);
      } else {
        counterparty = state.transaction.counterparty.resetName();
      }
      nextState = state.copyWith(
          transaction: state.transaction.copyWith(counterparty: counterparty));
      _merchantNameTimer = Timer.periodic(
          Duration(milliseconds: 500), _onMerchantNameTimerAlarm);
    }
    if (fieldUpdated.currency != null) {
      BaseCurrencyProvider().setTransactionCurrency(fieldUpdated.currency);
      nextState = state.copyWith(
          transaction: state.transaction.copyWith(
              amount: fieldUpdated.currency
                  .copyWith(value: state.transaction.amount.value)));
      add(UpdateCarbonFootprint());
    }
    if (fieldUpdated.amount != null) {
      _amountChangedTimer?.cancel();
      Amount amount;
      double carbonFootprint = state.transaction.carbonFootprint;
      if (fieldUpdated.amount.isNotEmpty) {
        double originAmount = state.transaction.amount.value;
        double editedAmount = double.parse(fieldUpdated.amount);
        bool isAmountChanged = (originAmount ?? 0.0).abs() != editedAmount;
        if (isAmountChanged) {
          _amountChangedTimer =
              Timer.periodic(Duration(milliseconds: 300), _onAmountTimerAlarm);
        }
        amount = state.transaction.amount.copyWith(value: editedAmount);
      } else {
        amount = state.transaction.amount.resetAmount();
        carbonFootprint = 0.0;
      }
      nextState = state.copyWith(
          transaction: state.transaction
              .copyWith(amount: amount, carbonFootprint: carbonFootprint));
    }
    yield nextState.copyWith(ignoreValidation: false);
  }

  void _onMerchantNameTimerAlarm(Timer timer) {
    timer.cancel();
    Analytics().logEvent(
        EnterMerchantNameEvent(state.transaction.counterparty.name, mode));
  }

  void _onAmountTimerAlarm(Timer timer) {
    timer.cancel();
    Analytics()
        .logEvent(EnterAmountEvent(state.transaction.amount.value, mode));
    this.add(UpdateCarbonFootprint());
  }

  Stream<TransactionState> _mapUpdateCarbonFootprintToState(
      {bool withNetworkUpdate = false}) async* {
    yield state.copyWith(carbonFootprintLoading: true);
    Amount amount = state.transaction.amount;
    BaseResponse<Amount> carbonResponse =
    await _apiRemoteRepository.getEcoFootprintForCategory(
        state.transaction.categories.first.id, amount);
    if (carbonResponse.isSuccess()) {
      double carbonValue = carbonResponse?.result?.value;
      if (carbonValue != null) {
        yield state.copyWith(
            transaction: state.transaction
                .copyWith(carbonFootprint: carbonResponse?.result?.value),
            carbonFootprintLoading: false);
        if (withNetworkUpdate) {
          _silentlyUpdateTransaction(state.transaction,
              state.transaction.copyWith(carbonFootprint: carbonValue));
        }
      } else {
        yield state.copyWith(
            transaction: state.transaction.resetFootprint(),
            carbonFootprintLoading: false);
      }
    }
  }

  Stream<TransactionState> _mapStartedToState(Transaction transaction) async* {
    _originTransaction = transaction;
    Transaction summaryTransaction = transaction;
    BaseResponse<Categories> categoriesResponse =
    await _apiRemoteRepository.getCategories();
    BaseResponse<Amount> baseCurrencyResponse =
    await _apiRemoteRepository.getBaseCurrency();
    List<Amount> currencies = await BaseCurrencyProvider().baseCurrencies;
    if (categoriesResponse.isSuccess() && baseCurrencyResponse.isSuccess()) {
      var categories = categoriesResponse.result.categories;
      if (categories.isNotEmpty && summaryTransaction.categories.isEmpty) {
        summaryTransaction = transaction.copyWith(categories: [
          categories.firstWhere((c) => c.id == kDefaultCategoryId,
              orElse: () => categories.first),
        ]);
      }
      if (summaryTransaction.amount?.currency == null) {
        Amount baseAmount = BaseCurrencyProvider().transactionCurrency;
        summaryTransaction = summaryTransaction.copyWith(
            amount: baseAmount, baseCurrencyAmount: baseAmount);
      }

      if (summaryTransaction.counterparty == null) {
        summaryTransaction =
            summaryTransaction.copyWith(counterparty: Counterparty(name: ""));
      }
      bool isImageLoading =
          _imageController.imageRequestWithGuid(transaction?.receiptImageId) !=
              null;

      yield state.copyWith(
          baseCurrencies: currencies,
          isPictureLoading: isImageLoading,
          transaction: summaryTransaction,
          pendingReceiptImageUri: _apiRemoteRepository
              .getPendingReceiptUriForGuid(transaction.receiptImageId),
          isLoading: false,
          categories: categoriesResponse.result.categories);
    } else {
      //TODO: Add handling of failed categories response
    }
  }

  void _silentlyUpdateTransaction(Transaction _originTransaction,
      Transaction _editedTransaction) {
    if (_originTransaction != null) {
      final diffMap = JsonPatch.diff(
          _originTransaction.toJson(), _editedTransaction.toJson());
      _apiRemoteRepository.patchTransaction(
          state.transaction.id, json.encode(diffMap));
    }
  }

  Stream<TransactionState> _mapEditToState(
      Transaction originTransaction) async* {
    String diff = _diffForTransactionType(state.transaction, originTransaction);
    BaseResponse<Transaction> modifyResponse =
    await _apiRemoteRepository.patchTransaction(state.transaction.id, diff);
    if (modifyResponse.isSuccess()) {
      yield state.copyWith(transactionResult: modifyResponse.result);
    } else {
      yield state.copyWith(error: modifyResponse.error);
    }
  }

  String _diffForTransactionType(Transaction transaction,
      Transaction originTransaction) {
    switch (transaction.sourceType) {
      case TransactionSourceType.BANK_ACCOUNT:
        String merchantNameDiff =
            '[{"op":"replace","path":"/counterparty","value":{"name":"${transaction
            .counterparty.name}"}}]';
        return merchantNameDiff;
      case TransactionSourceType.BITCOIN_WALLET:
      case TransactionSourceType.MANUAL:
        final diff = JsonPatch.diff(
            originTransaction.toJson(), state.signedTransaction().toJson());
        return json.encode(_applyCounterpartyWorkaroundIfExistsList(diff));
    }
  }

  Stream<TransactionState> _mapCreateToState() async* {
    Analytics().logEvent(SaveNewTxEvent());
    BaseResponse<Transaction> addTransactionResponse =
    await _apiRemoteRepository.addTransaction(state.signedTransaction());
    if (addTransactionResponse.isSuccess()) {
      yield state.copyWith(transactionResult: addTransactionResponse.result);
    } else {
      yield state.copyWith(
          error: addTransactionResponse.error, isLoading: false);
    }
  }

  void _handlePictureEvent(String guid) {
    if (guid == state.transaction.receiptImageId) {
      add(FieldUpdated(isImageLoading: false));
    }
  }

  List<Map<String, dynamic>> _applyCounterpartyWorkaroundIfExistsList(
      List<Map<String, dynamic>> diff) {
    final indexOfRecord = diff.indexWhere(
            (element) => element[_pathKey] == '/counterparty/name');
    if (indexOfRecord >= 0) {
      final record = diff[indexOfRecord];
      record[_pathKey] = '/counterparty';
      record[_valueKey] = {'name': '${record[_valueKey]}'};
      diff[indexOfRecord] = record;
      return diff;
    } else {
      return diff;
    }
  }

  @override
  void onFailedUpload(String guid) => _handlePictureEvent(guid);

  @override
  void onSuccessUpload(String guid) => _handlePictureEvent(guid);
}
