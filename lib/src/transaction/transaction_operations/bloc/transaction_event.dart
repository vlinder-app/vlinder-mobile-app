import 'dart:io';

import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:equatable/equatable.dart';

abstract class TransactionEvent extends Equatable {
  const TransactionEvent();
}

class Started extends TransactionEvent {
  final Transaction transaction;

  Started(this.transaction);

  @override
  List<Object> get props => [transaction];
}

class EditEvent extends TransactionEvent {
  final Transaction originTransaction;

  EditEvent({this.originTransaction});

  @override
  List<Object> get props => [originTransaction];
}

class CreateEvent extends TransactionEvent {
  @override
  List<Object> get props => null;
}

class RemoveEvent extends TransactionEvent {
  @override
  List<Object> get props => null;
}

class FieldUpdated extends TransactionEvent {
  final String amount;
  final DateTime dateTime;
  final String merchantName;
  final Category category;
  final bool isImageLoading;
  final Amount currency;

  FieldUpdated(
      {this.amount,
      this.currency,
      this.dateTime,
      this.merchantName,
      this.category,
      this.isImageLoading});

  @override
  List<Object> get props =>
      [amount, dateTime, merchantName, category, isImageLoading, currency];

  @override
  String toString() {
    return 'FieldUpdated{amount: $amount, dateTime: $dateTime, merchantName: $merchantName, category: $category, isImageLoading: $isImageLoading}';
  }
}

class UpdateCarbonFootprint extends TransactionEvent {
  final bool withNetworkUpdate;

  UpdateCarbonFootprint({this.withNetworkUpdate = false});

  @override
  List<Object> get props => [withNetworkUpdate];
}

class TransactionImageSelected extends TransactionEvent {
  final File transactionImage;

  TransactionImageSelected(this.transactionImage);

  @override
  List<Object> get props => [transactionImage];
}

class RemoveTransactionImage extends TransactionEvent {
  RemoveTransactionImage();

  @override
  List<Object> get props => null;
}
