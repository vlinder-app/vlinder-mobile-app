import 'dart:typed_data';

import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/categories/transactions_type.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/error/error.dart';
import 'package:atoma_cash/src/transaction/transaction_operations/transaction_operations_screen.dart';
import 'package:equatable/equatable.dart';

abstract class BaseTransactionState extends Equatable {
  const BaseTransactionState();
}

class TransactionState extends BaseTransactionState {
  static const _kMinimumCounterpartyNameLength = 2;

  final Transaction transaction;
  final bool isLoading;
  final List<Category> categories;
  final bool ignoreValidation;
  final Error error;
  final Transaction transactionResult;
  final bool carbonFootprintLoading;
  final String pendingReceiptImageUri;
  final Uint8List transactionImage;
  final bool isPictureLoading;
  final List<Amount> baseCurrencies;

  bool get isDateValid => transaction?.bookedAt != null ?? false;

  bool get isMerchantNameValid =>
      (transaction?.counterparty?.name?.length ?? 0) >
      _kMinimumCounterpartyNameLength;

  bool get isAmountValid => (transaction?.amount?.value?.abs() ?? -1) > 0.0;

  bool get isActionEnabled =>
      isDateValid && isMerchantNameValid && isAmountValid;

  TransactionState(
      {this.transaction,
      this.carbonFootprintLoading = false,
      this.error,
      this.isPictureLoading,
      this.transactionResult,
      this.ignoreValidation,
      this.isLoading,
      this.categories,
      this.transactionImage,
      this.baseCurrencies,
      this.pendingReceiptImageUri});

  TransactionState copyWith(
          {Transaction transaction,
          Error error,
          List<Amount> baseCurrencies,
          Uint8List transactionImage,
          bool carbonFootprintLoading,
          Transaction transactionResult,
          TransactionOperation operation,
          bool isLoading,
          bool isPictureLoading,
          List<Category> categories,
          bool ignoreValidation,
          String pendingReceiptImageUri}) =>
      TransactionState(
          baseCurrencies: baseCurrencies ?? this.baseCurrencies,
          transactionImage: transactionImage ?? this.transactionImage,
          error: error ?? this.error,
          isPictureLoading: isPictureLoading ?? this.isPictureLoading,
          pendingReceiptImageUri:
              pendingReceiptImageUri ?? this.pendingReceiptImageUri,
          carbonFootprintLoading:
              carbonFootprintLoading ?? this.carbonFootprintLoading,
          transactionResult: transactionResult ?? this.transactionResult,
          transaction: transaction ?? this.transaction,
          isLoading: isLoading ?? this.isLoading,
          ignoreValidation: ignoreValidation ?? this.ignoreValidation,
          categories: categories ?? this.categories);

  @override
  List<Object> get props => [
        transaction,
        isLoading,
        categories,
        ignoreValidation,
        transactionResult,
        error,
        carbonFootprintLoading,
        pendingReceiptImageUri,
        transactionImage,
        baseCurrencies,
        isPictureLoading
      ];

  TransactionState removeTransactionImage() => TransactionState(
      transactionImage: null,
      error: this.error,
      pendingReceiptImageUri: null,
      baseCurrencies: this.baseCurrencies,
      carbonFootprintLoading: this.carbonFootprintLoading,
      transactionResult: this.transactionResult?.clearReceiptImageId(),
      transaction: this.transaction?.clearReceiptImageId(),
      isLoading: this.isLoading,
      ignoreValidation: this.ignoreValidation,
      isPictureLoading: this.isPictureLoading,
      categories: this.categories);

  @override
  String toString() {
    return 'TransactionState{transaction: $transaction, isLoading: $isLoading, ignoreValidation: $ignoreValidation}';
  }

  double _signedAmountValueForType(TransactionsType type, double amountValue) {
    switch (type) {
      case TransactionsType.INCOME:
        return amountValue.abs();
      case TransactionsType.EXPENSES:
        return amountValue.abs() * -1;
    }
  }

  Transaction signedTransaction() {
    double amountValue = _signedAmountValueForType(
        transaction.categories.first.transactionsType,
        transaction.amount.value);

    return transaction.copyWith(
        amount: transaction.amount.copyWith(value: amountValue));
  }
}
