import 'dart:io';

import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/categories/transactions_type.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction_source_type.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/bank/webview/custom_app_bar.dart';
import 'package:atoma_cash/src/media/media_controller.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/src/transaction/transaction_operations/bloc/bloc.dart';
import 'package:atoma_cash/src/transaction/widgets/add_transaction_photo_button.dart';
import 'package:atoma_cash/src/transaction/widgets/appbar_action_button.dart';
import 'package:atoma_cash/src/transaction/widgets/dialogs/picture_options_dialog.dart';
import 'package:atoma_cash/src/transaction/widgets/merchant_icon.dart';
import 'package:atoma_cash/src/transaction/widgets/receipt_details.dart';
import 'package:atoma_cash/src/transaction/widgets/transaction_picture_loading_placeholder.dart';
import 'package:atoma_cash/src/transaction/widgets/transaction_title.dart';
import 'package:atoma_cash/src/transaction/widgets/transaction_universal_image.dart';
import 'package:atoma_cash/src/widgets/progress_dialog.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:atoma_cash/utils/text_input_formatters/decimal_text_input_formatter.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:keyboard_actions/keyboard_actions.dart';
import 'package:keyboard_actions/keyboard_actions_config.dart';

import 'dialogs/currency_selector_dialog.dart';

enum TransactionOperation { EDIT, CREATE }

abstract class OperationResult {}

class UpdateOperation extends OperationResult {
  final Transaction transaction;

  UpdateOperation(this.transaction);
}

class EditOperation extends OperationResult {
  final Transaction transaction;

  EditOperation(this.transaction);
}

class TransactionOperationsScreen extends StatefulWidget {
  final Transaction transaction;
  final TransactionOperation operation;

  const TransactionOperationsScreen(
      {Key key, @required this.transaction, @required this.operation})
      : super(key: key);

  @override
  _TransactionOperationsScreenState createState() =>
      _TransactionOperationsScreenState();
}

class _TransactionOperationsScreenState
    extends State<TransactionOperationsScreen> {
  final TextEditingController _amountController = TextEditingController();
  final TextEditingController _merchantNameController = TextEditingController();
  final TextEditingController _dateTimeController = TextEditingController();

  final FocusNode _amountFocusNode = FocusNode();
  final DecimalTextInputFormatter _decimalInputFormatter =
      DecimalTextInputFormatter(
          activatedNegativeValues: false, decimalRange: 2);

  TransactionOperation _mode;

  TransactionBloc _bloc;
  MediaController _mediaController;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) => _postFrameInit());
    _mode = widget.operation;
    Transaction transaction = widget.transaction ?? Transaction.empty();
    _fillTransactionInputFields(transaction);

    _amountController.addListener(_onAmountUpdate);
    _merchantNameController.addListener(_onMerchantNameUpdate);

    _bloc = TransactionBloc(_mode)..add(Started(transaction));
    super.initState();
  }

  @override
  void dispose() {
    _amountController.dispose();
    _dateTimeController.dispose();
    _merchantNameController.dispose();
    super.dispose();
  }

  void _postFrameInit() {
    _mediaController = MediaController(context,
        onCameraFileSelect: _onTransactionImageSelected,
        onGalleryFileSelect: _onTransactionImageSelected);
  }

  void _onRemoveTransactionImageClick() => _bloc.add(RemoveTransactionImage());

  void _onTransactionImageSelected(File transactionImage) {
    _bloc.add(TransactionImageSelected(transactionImage));
  }

  void _onAmountUpdate() {
    _bloc.add(FieldUpdated(amount: _amountController.text));
  }

  void _onMerchantNameUpdate() {
    _bloc.add(FieldUpdated(merchantName: _merchantNameController.text));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _bloc,
      listener: _blocListener,
      child: BlocBuilder(
        bloc: _bloc,
        builder: (context, state) {
          return Scaffold(
            appBar: ModalStyleAppBar(
              AppBar(
                backgroundColor: Colors.transparent,
                automaticallyImplyLeading: false,
                title: _customAppBar(context, state),
              ),
            ),
            body: _rootContentBuilder(context, state),
          );
        },
      ),
    );
  }

  void _blocListener(BuildContext context, TransactionState state) {
    if (state.transactionResult != null) {
      // Dialog dismiss
      Navigator.of(context).pop();
      Navigator.pop(context, EditOperation(state.transactionResult));
    }
    if (state.error != null) {
      Navigator.of(context).pop();
    }
  }

  Widget _rootContentBuilder(BuildContext context, TransactionState state) {
    if (state.isLoading) {
      return _loadingWidget(context);
    } else {
      return _transactionFieldsWidget(context, state);
    }
  }

  Widget _transactionFieldsWidget(
      BuildContext context, TransactionState state) {
    return Container(
      color: Colors.white,
      child: KeyboardActions(
        config: _buildKeyboardActions(context),
        child: _transactionFieldsForState(context, state),
      ),
    );
  }

  Widget _transactionFieldsForState(
      BuildContext context, TransactionState state) {
    var isManualTransaction =
        state.transaction.sourceType == TransactionSourceType.MANUAL;
    return ListView(
      children: <Widget>[
        _title(),
        if (isManualTransaction) _amountSection(context, state),
        _dateMerchantSection(context, state),
        _transactionPhotoSection(context, state),
        TransactionDetails(
          carbonFootprintLoading: state.carbonFootprintLoading,
          transaction: state.transaction,
          onCategoryClick: () => _onCategoryClick(context, state),
        ),
        Container(
          height: 50.0,
        )
      ],
    );
  }

  Widget _transactionPhotoSection(
      BuildContext context, TransactionState state) {
    if (state.isPictureLoading) {
      return TransactionPictureLoadingPlaceholder();
    } else if (state.pendingReceiptImageUri != null) {
      return TransactionUniversalImage(
        imageUrl: state.pendingReceiptImageUri,
        onRemoveImageClick: _onRemoveTransactionImageClick,
      );
    } else if (state.transactionImage != null) {
      return TransactionUniversalImage(
        imageBytes: state.transactionImage,
        onRemoveImageClick: _onRemoveTransactionImageClick,
      );
    } else {
      return AddTransactionPhotoButton(
        onClick: _onAddTransactionPictureClick,
      );
    }
  }

  void _onCameraClick() {
    Navigator.of(context).pop();
    _mediaController.getPictureFromCamera(context);
  }

  void _onGalleryClick() {
    Navigator.of(context).pop();
    _mediaController.getPictureFromGallery(context);
  }

  void _onAddTransactionPictureClick() {
    FocusScope.of(context).unfocus();
    showModalBottomSheet<void>(
        context: context,
        shape: PictureOptionsDialog.border,
        builder: (_) {
          return PictureOptionsDialog(
            onCameraClick: _onCameraClick,
            onGalleryClick: _onGalleryClick,
          );
        });
  }

  void _onCategoryClick(BuildContext context, TransactionState state) async {
    final result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.categorySelectScreen,
        arguments: CategorySelectScreenArguments(
            allCategories: state.categories,
            selectedCategory: state.transaction?.categories?.first));

    if (result is Category) {
      _bloc.add(FieldUpdated(category: result));
    }
  }

  Widget _loadingWidget(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          _title(),
          Expanded(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          )
        ],
      ),
    );
  }

  KeyboardActionsConfig _buildKeyboardActions(BuildContext context) {
    return KeyboardActionsConfig(
        keyboardActionsPlatform: KeyboardActionsPlatform.IOS,
        actions: [
          KeyboardAction(
              focusNode: _amountFocusNode,
              displayArrows: false,
              enabled: false,
              displayDoneButton: false,
              toolbarButtons: [_toolbarButton])
        ]);
  }

  void _fillTransactionInputFields(Transaction transaction) {
    if (transaction != null) {
      double amount = transaction?.amount?.value;
      if (amount != null) {
        _amountController.text = amount.abs().toStringAsFixed(2);
      } else {
        _amountController.text = "";
      }
      DateTime bookedAt = transaction?.bookedAt?.toLocal();
      if (bookedAt != null) {
        _dateTimeController.text = DateHelper.humanDate(bookedAt);
      }
      _merchantNameController.text = transaction.counterparty?.name;
    }
  }

  Widget _customAppBar(BuildContext context, TransactionState state) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          AppBarActionButton(Localization.of(context).cancelButton,
              onClick: () {
            Navigator.of(context).pop(UpdateOperation(
                _resultingTransactionForPop(state.transaction)));
          }),
          AppBarActionButton(_actionTextByOperation(_mode),
              onClick: state.isActionEnabled ? _onActionClick : null),
        ],
      ),
    );
  }

  Transaction _resultingTransactionForPop(Transaction transaction) {
    Transaction sourceTransaction = transaction.receiptImageId == null
        ? widget.transaction?.clearReceiptImageId()
        : widget.transaction;
    return sourceTransaction?.copyWith(
            categories: transaction.categories,
            carbonFootprint: transaction.carbonFootprint,
            receiptImageId: transaction.receiptImageId) ??
        null;
  }

  String _actionTextByOperation(TransactionOperation operation) {
    switch (operation) {
      case TransactionOperation.EDIT:
        return Localization.of(context).doneButton;
      case TransactionOperation.CREATE:
        return Localization.of(context).addButton;
      default:
        return "undefined";
    }
  }

  Widget _toolbarButton(FocusNode focusNode) => Widgets.styledFlatButton(
      text: Localization.of(context).doneButton,
      onPressed: () => focusNode.unfocus());

  Widget _title() => LargeTitle(Localization.of(context).transactionHeader);

  Widget _amountSection(BuildContext context, TransactionState state) {
    const TextStyle amountTextStyle = TextStyle(
      fontSize: 24.0,
      color: Palette.blue,
      letterSpacing: 1.0,
      fontWeight: FontWeight.w600,
    );
    String amountSymbol = state.transaction?.amount?.symbol ?? "?";
    Category category = state.transaction?.categories?.first;
    return Container(
      height: 120.0,
      padding: EdgeInsets.only(
        left: 24.0,
        right: 24.0,
      ),
      child: Row(
        children: <Widget>[
          ClipOval(
            child: Container(
              width: 72.0,
              height: 72.0,
              child: MerchantIcon(state.transaction?.counterparty?.iconUrl),
            ),
          ),
          Container(
            width: 24.0,
          ),
          Expanded(
            child: TextFormField(
              focusNode: _amountFocusNode,
              keyboardType:
                  TextInputType.numberWithOptions(signed: false, decimal: true),
              style: FontBook.headerH2,
              controller: _amountController,
              inputFormatters: [_decimalInputFormatter],
              decoration: InputDecoration(
                errorText:
                    state.isAmountValid || state.ignoreValidation ? null : "",
                contentPadding: EdgeInsets.only(top: 0.0),
                labelText: Localization.of(context).amountHint,
                labelStyle: TextStyle(
                    fontSize: 16.0,
                    color: Palette.mediumGrey,
                    fontWeight: FontWeight.w500),
                helperStyle: TextStyle(
                    fontSize: 12.0,
                    color: Palette.mediumGrey,
                    fontWeight: FontWeight.w500),
                helperText: _amountHintText(context, category),
                alignLabelWithHint: true,
                prefixStyle: amountTextStyle.copyWith(height: 1.8),
              ),
            ),
          ),
          Container(
            width: 12.0,
          ),
          _currencySection(
              context, state.transaction.amount, state.baseCurrencies),
        ],
      ),
    );
  }

  Widget _currencySection(
          BuildContext context, Amount currency, List<Amount> currencies) =>
      InkWell(
        onTap: () => _showCurrencySelector(context, currencies),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              currency.currency,
              style: FontBook.textButton,
            ),
            Icon(
              Icons.keyboard_arrow_down,
              color: Palette.mediumGrey,
            )
          ],
        ),
      );

  String _amountHintText(BuildContext context, Category category) {
    switch (category.transactionsType) {
      case TransactionsType.INCOME:
        return Localization.of(context).amountDescriptionEarned;
      case TransactionsType.EXPENSES:
        return Localization.of(context).amountDescription;
    }
  }

  Widget _dateMerchantSection(BuildContext context, TransactionState state) {
    return Container(
      margin: EdgeInsets.only(left: 24.0, right: 24.0, top: 16.0, bottom: 16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          if (state.transaction.sourceType == TransactionSourceType.MANUAL)
            TextFormField(
              controller: _dateTimeController,
              readOnly: true,
              enableInteractiveSelection: false,
              onTap: () => {
                _showDatePicker(context,
                    state.transaction?.bookedAt?.toLocal() ?? DateTime.now())
              },
              autofocus: false,
              autovalidate: true,
              buildCounter: (BuildContext context,
                      {int currentLength, int maxLength, bool isFocused}) =>
                  null,
              keyboardType: null,
              maxLines: 1,
              decoration: UiHelper.styledInputDecoration(
                      Localization.of(context).dateHint)
                  .copyWith(
                      errorText: state.isDateValid || state.ignoreValidation
                          ? null
                          : "",
                      suffixIcon: Image.asset(
                        Assets.calendarIcon,
                        color: Colors.grey,
                      )),
            ),
          Padding(
            padding: const EdgeInsets.only(top: 24.0),
            child: TextFormField(
              scrollPadding: EdgeInsets.only(bottom: 120.0),
              textCapitalization: TextCapitalization.sentences,
              controller: _merchantNameController,
              decoration: UiHelper.styledInputDecoration(
                      Localization.of(context).merchantNameHint)
                  .copyWith(
                      errorText:
                          state.isMerchantNameValid || state.ignoreValidation
                              ? null
                              : ""),
            ),
          ),
        ],
      ),
    );
  }

  void _showCurrencySelector(
      BuildContext context, List<Amount> baseCurrencies) {
    showModalBottomSheet<void>(
        context: context,
        shape: CurrencySelectorDialog.border,
        isScrollControlled: true,
        builder: (_) {
          return CurrencySelectorDialog(
            currencies: baseCurrencies,
            currencySelectedCallback: (amount) {
              Navigator.of(context).pop();
              _bloc.add(FieldUpdated(currency: amount));
            },
          );
        });
  }

  void _showProgressDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (_) {
          return ProgressDialog(
            text: Localization.of(context).savingProcess,
          );
        });
  }

  void _showDatePicker(BuildContext context, DateTime currentDateTime) {
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(2016, 1, 1),
        maxTime: DateTime.now(),
        onChanged: (date) {}, onConfirm: (date) {
      _onDateConfirm(date);
    }, currentTime: currentDateTime);
  }

  void _onDateConfirm(DateTime dateTime) {
    _dateTimeController.text = DateHelper.humanDate(dateTime);
    _bloc.add(FieldUpdated(dateTime: dateTime));
  }

  void _onActionClick() {
    switch (_mode) {
      case TransactionOperation.EDIT:
        _showProgressDialog();
        _bloc.add(EditEvent(originTransaction: widget.transaction));
        break;
      case TransactionOperation.CREATE:
        _showProgressDialog();
        _bloc.add(CreateEvent());
        break;
    }
  }
}
