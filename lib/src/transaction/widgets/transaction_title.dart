import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/material.dart';

class LargeTitle extends StatelessWidget {

  final String text;

  LargeTitle(this.text);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          left: 24.0, right: 24.0, bottom: 10.0, top: 10.0),
      child: Text(
        text,
        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w600),
      ),
    );
  }
}
