import 'package:flutter/material.dart';

class TransactionPictureLoadingPlaceholder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 184.0,
      child: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
