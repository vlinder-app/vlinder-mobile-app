import 'package:atoma_cash/extensions/color_extensions.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/geolocation/countries.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/search_category_event.dart';
import 'package:flutter/material.dart';

class CategoriesSearchDelegate extends SearchDelegate<Category> {
  static const _kIconSize = 40.0;

  final List<Category> values;
  final Category selectedCategory;

  List<Country> filterName = new List();

  CategoriesSearchDelegate(this.values, this.selectedCategory);

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      if (query.isNotEmpty)
        IconButton(
          tooltip: 'Clear',
          icon: const Icon(
            (Icons.clear),
            color: Palette.mediumGrey,
          ),
          onPressed: () {
            query = '';
            showSuggestions(context);
          },
        ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return _queryFilterWidget(context);
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return _queryFilterWidget(context);
  }

  Widget _roundedIcon(
      BuildContext context, String iconStringColor, String iconUrl) {
    Widget icon = Padding(
      padding: const EdgeInsets.all(10.0),
      child: Image.network(iconUrl),
    );
    return ClipOval(
      child: Container(
        color: HexColor.fromHex(iconStringColor),
        width: _kIconSize,
        height: _kIconSize,
        child: icon,
      ),
    );
  }

  Widget _checkMarkIndicator() {
    return Padding(
        padding: EdgeInsets.all(8.0),
        child: Icon(
          Icons.check,
          color: Palette.blue,
        ));
  }

  Widget _emptySuggestionPlaceholder(BuildContext context) {
    return Container(
      child: Center(
        child: Text(
          Localization.of(context).categoriesPlaceholder,
          textAlign: TextAlign.center,
          style: TextStyle(color: Palette.mediumGrey),
        ),
      ),
    );
  }

  Widget _suggestionsListViewBuilder(
      BuildContext context, List<Category> suggestions) {
    return ListView.builder(
        itemCount: suggestions.length,
        itemBuilder: (BuildContext context, int index) {
          Category category = suggestions[index];

          return new ListTile(
            leading: _roundedIcon(context, category.color, category.iconUrl),
            trailing: (category.id == selectedCategory?.id)
                ? _checkMarkIndicator()
                : null,
            title: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                category.name,
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600),
              ),
            ),
            onTap: () {
              Analytics().logEvent(SearchCategoryEvent(query, category.id));
              close(context, category);
            },
          );
        });
  }

  Widget _queryFilterWidget(BuildContext context) {
    final suggestions = values
        .where((c) => c.name.toLowerCase().contains(query.toLowerCase()))
        .toList();

    if (suggestions.isEmpty) {
      return _emptySuggestionPlaceholder(context);
    } else {
      return _suggestionsListViewBuilder(context, suggestions);
    }
  }
}
