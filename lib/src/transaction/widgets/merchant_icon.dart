import 'package:atoma_cash/resources/config/assets.dart';
import 'package:flutter/material.dart';

class MerchantIcon extends StatelessWidget {
  final String iconUrl;

  const MerchantIcon(this.iconUrl, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (iconUrl == null) {
      return Image.asset(
        Assets.counterpartyPlaceholder,
        fit: BoxFit.fill,
      );
    } else {
      return Image.network(
        iconUrl,
        fit: BoxFit.fill,
      );
    }
  }
}
