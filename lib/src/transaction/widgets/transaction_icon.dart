import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class TransactionIcon extends StatelessWidget {
  static const double _kDefaultIconSize = 54.0;

  final bool withAttachmentIndicator;
  final double iconSize;
  final Transaction transaction;

  const TransactionIcon(this.transaction,
      {Key key,
      this.withAttachmentIndicator = true,
      this.iconSize = _kDefaultIconSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _transactionIcon();
  }

  Widget _transactionIcon() => Container(
        width: iconSize,
        height: iconSize,
        child: Stack(
          alignment: Alignment.bottomRight,
          children: <Widget>[
            ClipOval(child: _iconWidget()),
            if (withAttachmentIndicator) _attachmentIndicator()
          ],
        ),
      );

  Widget _attachmentIndicator() {
    if (transaction.receiptImageId != null) {
      return Image.asset(
        Assets.transactionAttachmentIndicator,
        width: 22.0,
        height: 22.0,
      );
    } else {
      return Container();
    }
  }

  Widget _iconWidget() {
    if (transaction.counterparty?.iconUrl != null) {
      return Image.network(
        transaction.counterparty.iconUrl,
        width: iconSize,
        height: iconSize,
      );
    } else {
      return Container(
        width: iconSize,
        height: iconSize,
        color: Palette.darkBlue,
        child: Image.asset(Assets.transactionPlaceholderIcon),
      );
    }
  }
}
