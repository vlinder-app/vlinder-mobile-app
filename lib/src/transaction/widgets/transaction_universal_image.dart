import 'dart:typed_data';

import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_details/widgets/hero_photo_view_wrapper.dart';
import 'package:atoma_cash/src/widgets/receipt_image_placeholder.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:atoma_cash/utils/ui/custom_routes.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class TransactionUniversalImage extends StatelessWidget {
  final VoidCallback onRemoveImageClick;

  final String imageUrl;
  final Uint8List imageBytes;

  const TransactionUniversalImage(
      {Key key, this.onRemoveImageClick, this.imageUrl, this.imageBytes})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _imageAccordingToSource(context);
  }

  Widget _imageFromMemoryWithPlaceholder(Uint8List bytes) {
    if (bytes != null) {
      return Image.memory(
        bytes,
        scale: 0.2,
        fit: BoxFit.cover,
      );
    } else {
      return Center(child: CircularProgressIndicator());
    }
  }

  Widget _imageAccordingToSource(BuildContext context) {
    if (imageUrl != null) {
      return _networkImage(context, imageUrl);
    } else if (imageBytes != null) {
      return _imageFromMemory(context, imageBytes);
    } else {
      return Center(child: Text("Image undefined!"));
    }
  }

  Widget _networkImage(BuildContext context, String imageUrl) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      imageBuilder: (context, imageProvider) => InkWell(
        onTap: () => _viewNetworkReceiptInFullScreen(context, imageUrl),
        child: Container(
          width: double.maxFinite,
          height: 184.0,
          margin:
              Dimensions.leftRight24Padding.copyWith(top: 16.0, bottom: 24.0),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.0),
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
          child: Align(
            child: Container(
              child: _removeImageButton(context),
              margin: EdgeInsets.only(bottom: 20.0, right: 20.0),
            ),
            alignment: Alignment.bottomRight,
          ),
        ),
      ),
      placeholder: (context, url) => ReceiptImagePlaceholder(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget _removeImageButton(BuildContext context) => Container(
        width: 40.0,
        height: 40.0,
        child: Material(
          borderRadius: BorderRadius.circular(12.0),
          child: Ink(
            child: Ink.image(
              image: AssetImage(Assets.trashCanRedIcon),
              child: InkWell(
                onTap: onRemoveImageClick,
              ),
            ),
          ),
        ),
      );

  Widget _imageFromMemory(BuildContext context, Uint8List bytes) =>
      Stack(
        alignment: Alignment.bottomRight,
        children: <Widget>[
          ReceiptImagePlaceholder(
            child: _imageFromMemoryWithPlaceholder(bytes),
            onImageTap: () => _viewReceiptInFullScreen(context, bytes),
          ),
          Container(
            child: _removeImageButton(context),
            margin: EdgeInsets.only(bottom: 40.0, right: 40.0),
          )
        ],
      );

  void _viewReceiptInFullScreen(BuildContext context, Uint8List bytes) {
    Navigator.push(
        context,
        MaterialRouteWithoutAnimation(
          builder: (context) => HeroPhotoViewWrapper(
            imageProvider: MemoryImage(bytes),
          ),
        ));
  }

  void _viewNetworkReceiptInFullScreen(BuildContext context, String imageUrl) {
    Navigator.push(
        context,
        MaterialRouteWithoutAnimation(
          builder: (context) =>
              HeroPhotoViewWrapper(
                loadingBuilder: (context, chunk) =>
                    Container(
                      color: Colors.black,
                      child: Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),
                imageProvider: NetworkImage(imageUrl),
              ),
        ));
  }
}
