import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class AppBarActionButton extends StatelessWidget {
  final String text;
  final VoidCallback onClick;

  const AppBarActionButton(this.text, {Key key, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      constraints: BoxConstraints(minHeight: 48.0),
      padding: EdgeInsets.all(10.0),
      child: Text(
        text,
        style: TextStyle(
            color: onClick == null ? Palette.mediumGrey : Palette.blue,
            fontWeight: FontWeight.w600),
      ),
      onPressed: onClick,
    );
  }
}
