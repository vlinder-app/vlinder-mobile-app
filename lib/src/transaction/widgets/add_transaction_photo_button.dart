import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:flutter/material.dart';

class AddTransactionPhotoButton extends StatelessWidget {
  final VoidCallback onClick;

  const AddTransactionPhotoButton({Key key, this.onClick}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FourRowListTile(
      onClick: onClick,
      leading: Image.asset(
        Assets.addTransactionPhotoIcon,
        width: 40.0,
        height: 40.0,
      ),
      title: Text(
        Localization.of(context).addPhotoOfReceiptButton,
        style: FourRowListTile.titleTextStyle,
      ),
    );
  }
}
