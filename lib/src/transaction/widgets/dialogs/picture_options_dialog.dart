import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/activity/widgets/dialogs/option_cell.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:flutter/material.dart';

class PictureOptionsDialog extends StatelessWidget {
  static const kBorderRadius = 20.0;
  static const RoundedRectangleBorder border = const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(kBorderRadius),
          topRight: Radius.circular(kBorderRadius)));

  final VoidCallback onCameraClick;
  final VoidCallback onGalleryClick;

  const PictureOptionsDialog(
      {Key key, @required this.onCameraClick, @required this.onGalleryClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
          height: 166.0,
          child: Column(
            children: _receiptOptionsBuilder(context),
          )),
    );
  }

  List<Widget> _receiptOptionsBuilder(BuildContext context) {
    return <Widget>[
      OptionsCell(Localization.of(context).cameraHint, onClick: onCameraClick),
      OptionsCell(Localization.of(context).photoLibraryHint,
          onClick: onGalleryClick),
      OptionsCell(Localization.of(context).cancelButton,
          onClick: () => Navigator.of(context).pop(),
          style: FourRowListTile.titleTextStyle.copyWith(color: Palette.blue)),
    ];
  }
}
