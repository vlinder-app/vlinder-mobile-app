import 'package:atoma_cash/extensions/color_extensions.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/settings/widgets/cells.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/src/widgets/rounded_icon.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:flutter/material.dart';

class TransactionDetails extends StatelessWidget {
  static const _cellImageSize = 40.0;

  final Transaction transaction;
  final VoidCallback onCategoryClick;
  final bool carbonFootprintLoading;
  final String paidWithText;

  const TransactionDetails(
      {Key key,
      @required this.transaction,
      this.paidWithText,
      this.onCategoryClick,
      this.carbonFootprintLoading = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: _columnBuilder(context),
    );
  }

  Widget _columnBuilder(BuildContext context) {
    return Column(
      children: <Widget>[
        Cells.header(Localization.of(context).detailsHeader),
        _carbonEmissionCell(context),
        _paidWithCell(context, paidWithText),
        _categoryCell(context),
      ],
    );
  }

  Widget _carbonEmissionCell(BuildContext context) {
    if (carbonFootprintLoading) {
      return _carbonFootPrintCell(context, _carbonFootprintLoadingIndicator());
    } else {
      if (transaction?.carbonFootprint != null &&
          transaction?.carbonFootprint != 0) {
        return _carbonFootPrintCell(context, _carbonValueWidget(context));
      } else {
        return Container();
      }
    }
  }

  Widget _carbonValueWidget(BuildContext context) {
    return Text(
      CarbonHelper.carbonFootprintLocalizableValue(
          context, transaction.carbonFootprint),
      style: FourRowListTile.titleTextStyle,
    );
  }

  Widget _carbonFootprintLoadingIndicator() =>
      Container(
        width: 25.0,
        height: 25.0,
        child: Center(
          child: CircularProgressIndicator(),
        ),
      );

  Widget _carbonFootPrintCell(BuildContext context, Widget leadingWidget) {
    return FourRowListTile(
        leading: RoundedIcon(
          asset: Assets.cloudIcon,
          iconColor: Palette.green,
          size: _cellImageSize,
        ),
        title: Text(
          Localization.of(context).impact,
          style: FourRowListTile.subtitleTextStyle,
        ),
        subtitle: Text(
          Localization.of(context).carbonEmission,
          style: FourRowListTile.titleTextStyle,
        ),
        trailing: leadingWidget);
  }

  Widget _paidWithCell(BuildContext context, String paidWith) {
    if (paidWith != null) {
      return FourRowListTile(
        leading: _roundedIcon(context,
            iconColor: Palette.blue, asset: Assets.cashWhiteIcon),
        title: Text(
          Localization
              .of(context)
              .paidWith,
          style: FourRowListTile.subtitleTextStyle,
        ),
        subtitle: Text(
          paidWith,
          style: FourRowListTile.titleTextStyle,
        ),
      );
    } else {
      return Container();
    }
  }

  Category _undefinedCategory(BuildContext context) => Category(
      id: "-1", name: Localization.of(context).uncategorized, color: "a1a8bc");

  Widget _categoryCell(BuildContext context) {
    Widget arrowIcon;
    if (onCategoryClick != null) {
      arrowIcon = Icon(
        Icons.arrow_forward_ios,
        size: 18.0,
        color: Palette.mediumGrey,
      );
    }

    Category category = (transaction?.categories?.isNotEmpty ?? false)
        ? (transaction?.categories?.first ?? _undefinedCategory(context))
        : (_undefinedCategory(context));

    return FourRowListTile(
      onClick: onCategoryClick,
      forceThreeRow: true,
      leading: _roundedIcon(context,
          iconStringColor: category.color, iconUrl: category.iconUrl),
      title: Text(
        Localization.of(context).categoryHint,
        style: FourRowListTile.subtitleTextStyle,
      ),
      subtitle: Text(
        category.name,
        style: FourRowListTile.titleTextStyle,
      ),
      trailing: arrowIcon,
//      bottom: Container(height: 0.0,),
    );
  }

  Widget _roundedIcon(BuildContext context,
      {String iconStringColor, Color iconColor, String asset, String iconUrl}) {
    Widget icon;
    if (asset != null) {
      icon = Image.asset(asset);
    } else if (iconUrl != null) {
      icon = Padding(
        padding: const EdgeInsets.all(10.0),
        child: Image.network(iconUrl),
      );
    } else {
      icon = Image.asset(Assets.unknownCategoryWhiteIcon);
    }

    Color backgroundColor = Palette.mediumGrey;
    if (iconColor != null) {
      backgroundColor = iconColor;
    } else if (iconStringColor != null) {
      backgroundColor = HexColor.fromHex(iconStringColor);
    }

    return ClipOval(
      child: Container(
        color: backgroundColor,
        width: _cellImageSize,
        height: _cellImageSize,
        child: icon,
      ),
    );
  }
}
