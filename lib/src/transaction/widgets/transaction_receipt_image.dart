import 'package:atoma_cash/src/pending_receipts/receipt_details/widgets/hero_photo_view_wrapper.dart';
import 'package:atoma_cash/src/widgets/receipt_image_placeholder.dart';
import 'package:atoma_cash/utils/ui/custom_routes.dart';
import 'package:flutter/material.dart';

class TransactionReceiptImage extends StatelessWidget {
  final String receiptImageUrl;
  final VoidCallback onImageClick;

  const TransactionReceiptImage(
      {Key key, @required this.receiptImageUrl, this.onImageClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _receiptImageSection(context);
  }

  Widget _receiptImageSection(BuildContext context) {
    if (receiptImageUrl != null) {
      return ReceiptImagePlaceholder(
        onImageTap: () => _viewReceiptInFullScreen(context, receiptImageUrl),
        child: Image.network(receiptImageUrl, scale: 0.2, fit: BoxFit.cover,
            loadingBuilder: (context, child, chunk) {
          if (chunk == null) return child;
          return Center(child: CircularProgressIndicator());
        }),
      );
    } else {
      return Container();
    }
  }

  void _viewReceiptInFullScreen(BuildContext context, String imageUrl) {
    Navigator.push(
        context,
        MaterialRouteWithoutAnimation(
          builder: (context) => HeroPhotoViewWrapper(
            loadingBuilder: (context, chunk) => Container(
              color: Colors.black,
              child: Center(
                child: CircularProgressIndicator(),
              ),
            ),
            imageProvider: NetworkImage(imageUrl),
          ),
        ));
  }
}
