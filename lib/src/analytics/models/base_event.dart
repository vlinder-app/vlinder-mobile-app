abstract class BaseEvent {
  String getName();

  Map<String, dynamic> getProperties();
}
