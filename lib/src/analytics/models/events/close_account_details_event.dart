import '../base_event.dart';

class CloseAccountDetailsEvent implements BaseEvent {
  CloseAccountDetailsEvent();

  @override
  String getName() {
    return 'Close account details';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}