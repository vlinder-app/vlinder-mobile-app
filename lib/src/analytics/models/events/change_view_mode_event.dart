import 'package:atoma_cash/src/reports/models.dart';

import '../base_event.dart';

enum Source { ALL_CATEGORIES, CATEGORY_DETAILS }

class ChangeViewModeEvent implements BaseEvent {
  final Mode mode;
  final Source source;
  final int categoriesNumber;

  ChangeViewModeEvent(this.mode, this.source, {this.categoriesNumber});

  @override
  String getName() {
    return 'Change view mode';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'categoriesNumber': categoriesNumber,
      'mode': mode.toString(),
      'source': source.toString(),
    };
  }
}
