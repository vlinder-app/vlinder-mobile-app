import '../base_event.dart';

enum Direction {
  left,
  right,
}

class SwipeChartsEvent implements BaseEvent {
  final Direction direction;

  SwipeChartsEvent(
    this.direction,
  );

  @override
  String getName() {
    return 'Swipe charts';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'direction': direction.toString(),
    };
  }
}
