import '../base_event.dart';

enum Confirmed {
  yes,
  no,
}

class EmailConfirmedEvent implements BaseEvent {
  final Confirmed confirmed;

  EmailConfirmedEvent(
    this.confirmed,
  );

  @override
  String getName() {
    return 'Email confirmed';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'confirmed': confirmed.toString(),
    };
  }
}
