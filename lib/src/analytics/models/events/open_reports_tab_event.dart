import '../base_event.dart';

class OpenReportsTabEvent implements BaseEvent {
  OpenReportsTabEvent();

  @override
  String getName() {
    return 'Open Reports tab';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
