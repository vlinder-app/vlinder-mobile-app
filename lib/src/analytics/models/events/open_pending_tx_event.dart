import 'package:atoma_cash/src/pending_receipts/models/receipt_image_event_type.dart';

import '../base_event.dart';

class OpenPendingTxEvent implements BaseEvent {
  final ReceiptImageEventType status;

  OpenPendingTxEvent(
    this.status,
  );

  @override
  String getName() {
    return 'Open pending tx';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'status': status.toString(),
    };
  }
}
