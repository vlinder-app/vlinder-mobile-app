import '../base_event.dart';

enum Direction {
  left,
  right,
}

class SwipeFilterPeriodEvent implements BaseEvent {
  final Direction direction;

  SwipeFilterPeriodEvent(
    this.direction,
  );

  @override
  String getName() {
    return 'Swipe filter period';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'direction': direction.toString(),
    };
  }
}
