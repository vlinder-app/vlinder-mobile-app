import '../base_event.dart';

enum Granted {
  yes,
  no,
}

class LibraryPermissionsEvent implements BaseEvent {
  final Granted granted;

  LibraryPermissionsEvent(
    this.granted,
  );

  @override
  String getName() {
    return 'Library permissions';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'granted': granted.toString(),
    };
  }
}
