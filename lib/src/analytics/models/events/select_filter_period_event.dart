import '../base_event.dart';

class SelectFilterPeriodEvent implements BaseEvent {
  final String periodValue;

  SelectFilterPeriodEvent(
    this.periodValue,
  );

  @override
  String getName() {
    return 'Select filter period';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'periodValue': periodValue.toString(),
    };
  }
}
