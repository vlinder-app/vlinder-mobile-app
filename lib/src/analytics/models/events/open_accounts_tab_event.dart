import '../base_event.dart';

class OpenAccountsTabEvent implements BaseEvent {
  OpenAccountsTabEvent();

  @override
  String getName() {
    return 'Open Accounts tab';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
