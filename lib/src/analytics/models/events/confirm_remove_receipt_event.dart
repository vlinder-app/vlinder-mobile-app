import '../base_event.dart';

class ConfirmRemoveReceiptEvent implements BaseEvent {
  ConfirmRemoveReceiptEvent();

  @override
  String getName() {
    return 'Confirm remove receipt';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
