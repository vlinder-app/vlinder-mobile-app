import 'package:atoma_cash/models/api/api/categories/period.dart';

import '../base_event.dart';

class SelectReportRangeEvent implements BaseEvent {
  final PeriodType period;

  SelectReportRangeEvent(
    this.period,
  );

  @override
  String getName() {
    return 'Select report range';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'period': period.toString(),
    };
  }
}
