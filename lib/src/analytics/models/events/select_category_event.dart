import '../base_event.dart';

class SelectCategoryEvent implements BaseEvent {
  final String categoryId;

  SelectCategoryEvent(
    this.categoryId,
  );

  @override
  String getName() {
    return 'Select category';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'categoryId': categoryId.toString(),
    };
  }
}
