import '../base_event.dart';

enum Action {
  add,
  edit,
}

class OpenCategoriesEvent implements BaseEvent {
  final Action action;

  OpenCategoriesEvent(
    this.action,
  );

  @override
  String getName() {
    return 'Open categories';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'action': action.toString(),
    };
  }
}
