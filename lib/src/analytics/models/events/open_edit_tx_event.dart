import '../base_event.dart';

class OpenEditTxEvent implements BaseEvent {
  OpenEditTxEvent();

  @override
  String getName() {
    return 'Open edit tx';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
