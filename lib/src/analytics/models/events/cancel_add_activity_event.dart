import '../base_event.dart';

class CancelAddActivityEvent implements BaseEvent {
  CancelAddActivityEvent();

  @override
  String getName() {
    return 'Cancel add activity';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
