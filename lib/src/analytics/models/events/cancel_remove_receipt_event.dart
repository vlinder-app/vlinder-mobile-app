import '../base_event.dart';

class CancelRemoveReceiptEvent implements BaseEvent {
  CancelRemoveReceiptEvent();

  @override
  String getName() {
    return 'Cancel remove receipt';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
