import '../base_event.dart';

class FilterByMerchantEvent implements BaseEvent {
  FilterByMerchantEvent();

  @override
  String getName() {
    return 'Filter by merchant';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
