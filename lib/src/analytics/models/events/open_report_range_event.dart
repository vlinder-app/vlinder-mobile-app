import '../base_event.dart';

class OpenReportRangeEvent implements BaseEvent {
  OpenReportRangeEvent();

  @override
  String getName() {
    return 'Open report range';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
