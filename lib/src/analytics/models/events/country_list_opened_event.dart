import '../base_event.dart';

class CountryListOpenedEvent implements BaseEvent {
  CountryListOpenedEvent();

  @override
  String getName() {
    return 'Country list opened';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
