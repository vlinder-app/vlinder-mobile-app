import '../base_event.dart';

class SearchCategoryEvent implements BaseEvent {
  final String query;
  final String categoryId;

  SearchCategoryEvent(this.query, this.categoryId);

  @override
  String getName() {
    return 'Search category';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'categoryId': categoryId.toString(),
      'query': query.toString(),
    };
  }
}
