import '../base_event.dart';

class QuitRecoveryEvent implements BaseEvent {
  QuitRecoveryEvent();

  @override
  String getName() {
    return 'Quit recovery';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
