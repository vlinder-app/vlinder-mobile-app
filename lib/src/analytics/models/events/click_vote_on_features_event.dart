import 'package:atoma_cash/src/analytics/models/base_event.dart';

class ClickVoteOnFeaturesEvent implements BaseEvent {
  ClickVoteOnFeaturesEvent();

  @override
  String getName() {
    return 'Click vote on features';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
