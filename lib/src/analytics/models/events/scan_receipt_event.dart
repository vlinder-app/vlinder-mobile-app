import '../base_event.dart';

class ScanReceiptEvent implements BaseEvent {
  ScanReceiptEvent();

  @override
  String getName() {
    return 'Scan receipt';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
