import '../base_event.dart';

class ViewSecurityDisclaimerEvent implements BaseEvent {
  ViewSecurityDisclaimerEvent();

  @override
  String getName() {
    return 'View security disclaimer';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
