import '../base_event.dart';

class ConnectAccountEvent implements BaseEvent {
  ConnectAccountEvent();

  @override
  String getName() {
    return 'Connect account';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
