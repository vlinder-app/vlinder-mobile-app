import '../base_event.dart';

class CancelManualTxEvent implements BaseEvent {
  CancelManualTxEvent();

  @override
  String getName() {
    return 'Cancel manual tx';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
