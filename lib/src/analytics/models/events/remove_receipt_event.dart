import 'package:atoma_cash/src/pending_receipts/models/receipt_image_event_type.dart';

import '../base_event.dart';

class RemoveReceiptEvent implements BaseEvent {
  final ReceiptImageEventType receiptStatus;

  RemoveReceiptEvent(
    this.receiptStatus,
  );

  @override
  String getName() {
    return 'Remove receipt';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'receiptStatus': receiptStatus.toString(),
    };
  }
}
