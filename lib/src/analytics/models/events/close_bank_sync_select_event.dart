import '../base_event.dart';

class CloseBankSyncSelectEvent implements BaseEvent {
  CloseBankSyncSelectEvent();

  @override
  String getName() {
    return 'Close bank sync select';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}