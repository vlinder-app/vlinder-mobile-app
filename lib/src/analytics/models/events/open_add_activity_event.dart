import '../base_event.dart';

class OpenAddActivityEvent implements BaseEvent {
  OpenAddActivityEvent();

  @override
  String getName() {
    return 'Open add activity';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
