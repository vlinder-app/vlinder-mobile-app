import '../base_event.dart';

class CloseViewTxEvent implements BaseEvent {
  CloseViewTxEvent();

  @override
  String getName() {
    return 'Close view tx';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
