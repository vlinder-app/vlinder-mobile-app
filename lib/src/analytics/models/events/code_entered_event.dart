import '../base_event.dart';

class CodeEnteredEvent implements BaseEvent {
  CodeEnteredEvent();

  @override
  String getName() {
    return 'Code entered';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
