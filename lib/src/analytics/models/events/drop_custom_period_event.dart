import '../base_event.dart';

class DropCustomPeriodEvent implements BaseEvent {
  DropCustomPeriodEvent();

  @override
  String getName() {
    return 'Drop custom period';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
