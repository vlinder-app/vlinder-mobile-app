import '../base_event.dart';

class PasscodeSetEvent implements BaseEvent {
  PasscodeSetEvent();

  @override
  String getName() {
    return 'Passcode set';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
