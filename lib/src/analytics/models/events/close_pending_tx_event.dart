import '../base_event.dart';

class ClosePendingTxEvent implements BaseEvent {
  ClosePendingTxEvent();

  @override
  String getName() {
    return 'Close pending tx';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
