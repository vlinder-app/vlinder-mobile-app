import '../base_event.dart';

class BackToParentCategoriesEvent implements BaseEvent {
  BackToParentCategoriesEvent();

  @override
  String getName() {
    return 'Back to parent categories';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
