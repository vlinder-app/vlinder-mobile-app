import '../base_event.dart';

class FilterByIncomeCatEvent implements BaseEvent {
  FilterByIncomeCatEvent();

  @override
  String getName() {
    return 'Filter by income cat';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
