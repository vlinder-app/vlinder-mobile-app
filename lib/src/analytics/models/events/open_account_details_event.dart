import '../base_event.dart';

class OpenAccountDetailsEvent implements BaseEvent {
  OpenAccountDetailsEvent();

  @override
  String getName() {
    return 'Open account details';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
