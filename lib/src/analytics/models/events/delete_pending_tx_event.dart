import 'package:atoma_cash/src/pending_receipts/models/receipt_image_event_type.dart';

import '../base_event.dart';

class DeletePendingTxEvent implements BaseEvent {
  final ReceiptImageEventType status;

  DeletePendingTxEvent(
    this.status,
  );

  @override
  String getName() {
    return 'Delete pending tx';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'status': status.toString(),
    };
  }
}
