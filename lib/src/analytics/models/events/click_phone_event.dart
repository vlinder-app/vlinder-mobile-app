import '../base_event.dart';

class ClickPhoneEvent implements BaseEvent {
  ClickPhoneEvent();

  @override
  String getName() {
    return 'Click phone';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
