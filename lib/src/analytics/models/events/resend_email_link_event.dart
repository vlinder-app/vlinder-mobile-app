import '../base_event.dart';

enum Process {
  onboarding,
  recovery,
}

class ResendEmailLinkEvent implements BaseEvent {
  final Process process;

  ResendEmailLinkEvent(
    this.process,
  );

  @override
  String getName() {
    return 'Resend email link';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'process': process.toString(),
    };
  }
}
