import '../base_event.dart';

class PasscodeEnteredEvent implements BaseEvent {
  PasscodeEnteredEvent();

  @override
  String getName() {
    return 'Passcode entered';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
