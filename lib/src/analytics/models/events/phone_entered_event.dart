import '../base_event.dart';

class PhoneEnteredEvent implements BaseEvent {
  final String phone;

  PhoneEnteredEvent(
    this.phone,
  );

  @override
  String getName() {
    return 'Phone entered';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'phone': phone.toString(),
    };
  }
}
