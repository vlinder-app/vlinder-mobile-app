import '../base_event.dart';

class LogoutEvent implements BaseEvent {
  LogoutEvent();

  @override
  String getName() {
    return 'Logout';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
