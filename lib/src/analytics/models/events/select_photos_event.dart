import '../base_event.dart';

class SelectPhotosEvent implements BaseEvent {
  final int photos;

  SelectPhotosEvent(
    this.photos,
  );

  @override
  String getName() {
    return 'Select photos';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'photos': photos.toString(),
    };
  }
}
