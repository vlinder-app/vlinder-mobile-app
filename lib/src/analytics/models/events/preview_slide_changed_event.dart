import '../base_event.dart';

class PreviewSlideChanged implements BaseEvent {
  PreviewSlideChanged();

  @override
  String getName() {
    return 'Preview slide changed';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
