import '../base_event.dart';

class ManageAccountsEvent implements BaseEvent {
  ManageAccountsEvent();

  @override
  String getName() {
    return 'Manage accounts';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}