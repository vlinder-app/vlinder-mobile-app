import '../base_event.dart';

class ZoomFullScreenReceiptEvent implements BaseEvent {
  ZoomFullScreenReceiptEvent();

  @override
  String getName() {
    return 'Zoom full screen receipt';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
