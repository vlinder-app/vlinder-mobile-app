import '../base_event.dart';

class DropIncomeCatFilterEvent implements BaseEvent {
  final String categoryId;

  DropIncomeCatFilterEvent(
    this.categoryId,
  );

  @override
  String getName() {
    return 'Drop income cat filter';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'categoryId': categoryId.toString(),
    };
  }
}
