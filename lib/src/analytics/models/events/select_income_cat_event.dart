import '../base_event.dart';

class SelectIncomeCatEvent implements BaseEvent {
  SelectIncomeCatEvent();

  @override
  String getName() {
    return 'Select income cat';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
