import '../base_event.dart';

class CloseCustomPeriodEvent implements BaseEvent {
  CloseCustomPeriodEvent();

  @override
  String getName() {
    return 'Close custom period';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
