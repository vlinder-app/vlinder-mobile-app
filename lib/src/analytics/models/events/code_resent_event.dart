import '../base_event.dart';

class CodeResentEvent implements BaseEvent {
  CodeResentEvent();

  @override
  String getName() {
    return 'Code resent';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
