import '../base_event.dart';

enum Source { ACTIVITY, RECEIPT_DETAILS }

class StopUploadingEvent implements BaseEvent {
  final Source source;

  StopUploadingEvent(
    this.source,
  );

  @override
  String getName() {
    return 'Stop uploading';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'source': source.toString(),
    };
  }
}
