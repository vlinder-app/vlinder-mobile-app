import '../base_event.dart';

class CloseEditTxEvent implements BaseEvent {
  CloseEditTxEvent();

  @override
  String getName() {
    return 'Close edit tx';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
