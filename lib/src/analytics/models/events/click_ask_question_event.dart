import 'package:atoma_cash/src/analytics/models/base_event.dart';

class ClickAskQuestionEvent implements BaseEvent {
  ClickAskQuestionEvent();

  @override
  String getName() {
    return 'Click ask question';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
