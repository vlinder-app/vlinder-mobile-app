import 'package:atoma_cash/src/transaction/transaction_operations/transaction_operations_screen.dart';

import '../base_event.dart';

class EnterMerchantNameEvent implements BaseEvent {
  final String value;
  final TransactionOperation action;

  EnterMerchantNameEvent(
    this.value,
    this.action,
  );

  @override
  String getName() {
    return 'Enter merchant name';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'value': value.toString(),
      'action': action.toString(),
    };
  }
}
