import '../base_event.dart';

class BackToPhoneEntryEvent implements BaseEvent {
  BackToPhoneEntryEvent();

  @override
  String getName() {
    return 'Back to phone entry';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
