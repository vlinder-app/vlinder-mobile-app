import '../base_event.dart';

class ViewFullScreenReceiptEvent implements BaseEvent {
  ViewFullScreenReceiptEvent();

  @override
  String getName() {
    return 'View full screen receipt';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
