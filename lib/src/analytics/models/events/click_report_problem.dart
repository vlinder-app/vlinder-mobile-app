import 'package:atoma_cash/src/analytics/models/base_event.dart';

class ClickReportProblemEvent implements BaseEvent {
  ClickReportProblemEvent();

  @override
  String getName() {
    return "Click report problem";
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
