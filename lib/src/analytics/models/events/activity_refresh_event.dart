import '../base_event.dart';

class ActivityRefreshEvent implements BaseEvent {
  ActivityRefreshEvent();

  @override
  String getName() {
    return 'Activity refresh';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
