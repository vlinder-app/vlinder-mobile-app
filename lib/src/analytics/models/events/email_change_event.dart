import '../base_event.dart';

class EmailChangeEvent implements BaseEvent {
  EmailChangeEvent();

  @override
  String getName() {
    return 'Email change';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
