import '../base_event.dart';

class OpenBankSyncSelectEvent implements BaseEvent {
  OpenBankSyncSelectEvent();

  @override
  String getName() {
    return 'Open bank sync select';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}