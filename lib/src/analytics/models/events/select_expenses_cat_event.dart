import '../base_event.dart';

enum Source { LIST, CHART }

class SelectExpensesCatEvent implements BaseEvent {
  final Source source;
  final String categoryId;

  SelectExpensesCatEvent(
    this.source,
    this.categoryId,
  );

  @override
  String getName() {
    return 'Select expenses cat';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'source': source.toString(),
      'categoryId': categoryId.toString(),
    };
  }
}
