import '../base_event.dart';

class SaveDateEvent implements BaseEvent {
  final DateTime date;

  SaveDateEvent(
    this.date,
  );

  @override
  String getName() {
    return 'Save date';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'date': date.toString(),
    };
  }
}
