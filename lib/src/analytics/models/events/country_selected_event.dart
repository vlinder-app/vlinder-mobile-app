import '../base_event.dart';

class CountrySelectedEvent implements BaseEvent {
  final String country;

  CountrySelectedEvent(
    this.country,
  );

  @override
  String getName() {
    return 'Country selected';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'country': country.toString(),
    };
  }
}
