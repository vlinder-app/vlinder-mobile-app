import '../base_event.dart';

class OnboardingLeadEvent implements BaseEvent {
  final String email;
  final String name;

  OnboardingLeadEvent(this.email, {this.name}) : assert(email != null);

  @override
  String getName() {
    return 'Onboarding lead';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'email': email,
      if (name != null) 'name': name,
    };
  }
}
