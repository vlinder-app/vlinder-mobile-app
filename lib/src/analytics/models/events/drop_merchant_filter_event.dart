import '../base_event.dart';

class DropMerchantFilterEvent implements BaseEvent {
  final String merchantName;

  DropMerchantFilterEvent(
    this.merchantName,
  );

  @override
  String getName() {
    return 'Drop merchant filter';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'merchantName': merchantName.toString(),
    };
  }
}
