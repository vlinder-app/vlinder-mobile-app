import '../base_event.dart';

class SaveNewTxEvent implements BaseEvent {
  SaveNewTxEvent();

  @override
  String getName() {
    return 'Save new tx';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
