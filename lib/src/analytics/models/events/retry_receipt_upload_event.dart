import 'package:atoma_cash/src/pending_receipts/models/receipt_image_event_type.dart';

import '../base_event.dart';

class RetryReceiptUploadEvent implements BaseEvent {
  final ReceiptImageEventType status;

  RetryReceiptUploadEvent(
    this.status,
  );

  @override
  String getName() {
    return 'Retry receipt upload';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'status': status.toString(),
    };
  }
}
