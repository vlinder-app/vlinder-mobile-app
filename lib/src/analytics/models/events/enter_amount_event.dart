import 'package:atoma_cash/src/transaction/transaction_operations/transaction_operations_screen.dart';

import '../base_event.dart';

class EnterAmountEvent implements BaseEvent {
  final double value;
  final TransactionOperation action;

  EnterAmountEvent(
    this.value,
    this.action,
  );

  @override
  String getName() {
    return 'Enter amount';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'value': value.toString(),
      'action': action.toString(),
    };
  }
}
