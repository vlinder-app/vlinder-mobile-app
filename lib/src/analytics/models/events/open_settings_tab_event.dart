import '../base_event.dart';

class OpenSettingsTabEvent implements BaseEvent {
  OpenSettingsTabEvent();

  @override
  String getName() {
    return 'Open Settings tab';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
