import '../base_event.dart';

class EmailEnteredEvent implements BaseEvent {
  EmailEnteredEvent();

  @override
  String getName() {
    return 'Email entered';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
