import '../base_event.dart';

class NewPendingTxEvent implements BaseEvent {
  NewPendingTxEvent();

  @override
  String getName() {
    return 'New pending tx';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
