import '../base_event.dart';

class BankConnectedEvent implements BaseEvent {
  BankConnectedEvent();

  @override
  String getName() {
    return 'Bank connected';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}