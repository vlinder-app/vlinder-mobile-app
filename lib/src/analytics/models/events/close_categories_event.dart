import '../base_event.dart';

class CloseCategoriesEvent implements BaseEvent {
  CloseCategoriesEvent();

  @override
  String getName() {
    return 'Close categories';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
