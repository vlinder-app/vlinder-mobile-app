import '../base_event.dart';

enum Action {
  add,
  edit,
}

class OpenDatePickerEvent implements BaseEvent {
  final Action action;

  OpenDatePickerEvent(
    this.action,
  );

  @override
  String getName() {
    return 'Open date picker';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'action': action.toString(),
    };
  }
}
