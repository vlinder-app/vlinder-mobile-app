import '../base_event.dart';

class BackToAllCatEvent implements BaseEvent {
  BackToAllCatEvent();

  @override
  String getName() {
    return 'Back to all cat';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
