import '../base_event.dart';

class ViewLastActivityEvent implements BaseEvent {
  ViewLastActivityEvent();

  @override
  String getName() {
    return 'View last activity';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
