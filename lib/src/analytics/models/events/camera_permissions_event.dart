import '../base_event.dart';

enum Granted {
  yes,
  no,
}

class CameraPermissionsEvent implements BaseEvent {
  final Granted granted;

  CameraPermissionsEvent(
    this.granted,
  );

  @override
  String getName() {
    return 'Camera permissions';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'granted': granted.toString(),
    };
  }
}
