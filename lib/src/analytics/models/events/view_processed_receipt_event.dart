import '../base_event.dart';

class ViewProcessedReceiptEvent implements BaseEvent {
  ViewProcessedReceiptEvent();

  @override
  String getName() {
    return 'View processed receipt';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
