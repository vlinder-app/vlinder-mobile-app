import '../base_event.dart';

enum Source {
  HOME,
  ACCOUNT_DETAILS,
}

class SelectBankEvent implements BaseEvent {
  final String bankName;
  final DateTime lastUpdated;
  final Source source;

  SelectBankEvent(
    this.bankName,
    this.lastUpdated,
    this.source,
  );

  @override
  String getName() {
    return 'Select bank';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'bankName': bankName,
      'lastUpdated': lastUpdated,
      'source': source.toString(),
    };
  }
}
