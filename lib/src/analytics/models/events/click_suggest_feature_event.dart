import 'package:atoma_cash/src/analytics/models/base_event.dart';

class ClickSuggestFeatureEvent implements BaseEvent {
  ClickSuggestFeatureEvent();

  @override
  String getName() {
    return 'Click suggest feature';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
