import '../base_event.dart';

class EmailSkipEvent implements BaseEvent {
  EmailSkipEvent();

  @override
  String getName() {
    return 'Email skip';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
