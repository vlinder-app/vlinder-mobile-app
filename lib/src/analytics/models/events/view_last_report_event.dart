import '../base_event.dart';

class ViewLastReportEvent implements BaseEvent {
  ViewLastReportEvent();

  @override
  String getName() {
    return 'View last report';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
