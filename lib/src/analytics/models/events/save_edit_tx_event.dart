import '../base_event.dart';

class SaveEditTxEvent implements BaseEvent {
  SaveEditTxEvent();

  @override
  String getName() {
    return 'Save edit tx';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
