import 'package:atoma_cash/src/pending_receipts/models/receipt_image_event_type.dart';

import '../base_event.dart';

class FillOutPendingTxDetailsEvent implements BaseEvent {
  final ReceiptImageEventType status;

  FillOutPendingTxDetailsEvent(
    this.status,
  );

  @override
  String getName() {
    return 'Fill out pending tx details';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'status': status.toString(),
    };
  }
}
