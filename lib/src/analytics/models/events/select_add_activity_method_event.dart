import '../base_event.dart';

enum Methods {
  loadReceiptPhoto,
  addManually,
}

class SelectAddActivityMethodEvent implements BaseEvent {
  final Methods methods;

  SelectAddActivityMethodEvent(
    this.methods,
  );

  @override
  String getName() {
    return 'Select add activity method';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'methods': methods.toString(),
    };
  }
}
