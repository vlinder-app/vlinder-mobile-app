import '../base_event.dart';

class CloseReportRangeEvent implements BaseEvent {
  CloseReportRangeEvent();

  @override
  String getName() {
    return 'Close report range';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
