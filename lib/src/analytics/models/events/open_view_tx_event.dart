import '../base_event.dart';

enum Source { ACTIVITY, REPORTS, ACCOUNTS }

class OpenViewTxEvent implements BaseEvent {
  final Source source;

  OpenViewTxEvent(
    this.source,
  );

  @override
  String getName() {
    return 'Open view tx';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'source': source.toString(),
    };
  }
}
