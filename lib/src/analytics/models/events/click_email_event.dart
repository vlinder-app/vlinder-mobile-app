import '../base_event.dart';

enum EmailStatus { NOT_PROVIDED, NOT_CONFIRMED, CONFIRMED }

class ClickEmailEvent implements BaseEvent {
  final EmailStatus emailStatus;

  ClickEmailEvent(
    this.emailStatus,
  );

  @override
  String getName() {
    return 'Click email';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'emailStatus': emailStatus.toString(),
    };
  }
}
