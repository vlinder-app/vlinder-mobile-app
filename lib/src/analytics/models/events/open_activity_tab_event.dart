import '../base_event.dart';

class OpenActivityTabEvent implements BaseEvent {
  OpenActivityTabEvent();

  @override
  String getName() {
    return 'Open Activity tab';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
