import '../base_event.dart';

class SetupCustomPeriodEvent implements BaseEvent {
  final DateTime from;
  final DateTime to;

  SetupCustomPeriodEvent(
    this.from,
    this.to,
  );

  @override
  String getName() {
    return 'Setup custom period';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {
      'from': from.toString(),
      'to': to.toString(),
    };
  }
}
