import '../base_event.dart';

class ForgotPasscodeEvent implements BaseEvent {
  ForgotPasscodeEvent();

  @override
  String getName() {
    return 'Forgot passcode';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
