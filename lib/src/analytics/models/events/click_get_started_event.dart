import '../base_event.dart';

class ClickGetStartedEvent implements BaseEvent {
  static const String _slide = "slide";
  final int slideIndex;

  ClickGetStartedEvent(this.slideIndex);

  @override
  String getName() {
    return 'Click Get started';
  }

  @override
  Map<String, dynamic> getProperties() {
    return { _slide : '$_slide$slideIndex'};
  }
}
