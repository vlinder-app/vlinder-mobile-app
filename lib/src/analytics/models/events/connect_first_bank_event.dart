import '../base_event.dart';

class ConnectFirstBankEvent implements BaseEvent {
  ConnectFirstBankEvent();

  @override
  String getName() {
    return 'Connect first bank';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
