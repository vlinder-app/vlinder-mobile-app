import '../base_event.dart';

class AskSecurityQuestionEvent implements BaseEvent {
  AskSecurityQuestionEvent();

  @override
  String getName() {
    return 'Ask security question';
  }

  @override
  Map<String, dynamic> getProperties() {
    return {};
  }
}
