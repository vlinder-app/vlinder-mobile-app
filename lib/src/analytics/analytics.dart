import 'package:amplitude_flutter/amplitude.dart';

import 'models/base_event.dart';

class Analytics {
  static final Analytics _instance = Analytics._internal();

  Amplitude _analytics;

  Analytics._internal() {
    _initAmplitude();
  }

  factory Analytics() {
    return _instance;
  }

  void _initAmplitude() {
    _analytics = Amplitude.getInstance(instanceName: "Vlinder");
    _analytics.enableCoppaControl();
    _analytics.trackingSessionEvents(true);
  }

  void clearUserData() => _analytics?.clearUserProperties();

  void setUserId(String userId, {bool startNewSession}) =>
      _analytics?.setUserId(userId, startNewSession: startNewSession);

  void setApiKey(String apiKey) => _analytics?.init(apiKey);

  void logEvent(BaseEvent event) {
    _analytics?.logEvent(event.getName(),
        eventProperties: event.getProperties());
  }
}
