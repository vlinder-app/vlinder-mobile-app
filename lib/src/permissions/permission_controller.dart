import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/utils/platform/platform_helper.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

class PermissionController {
  final String notGrantedDialogTitle;
  final String notGrantedDialogMessage;
  final String permissionUnknownMessage;
  final String permissionRestrictedMessage;
  final String iconAsset;
  final VoidCallback onPermissionGranted;
  final Permission permission;
  final bool hidePermissionWarningDialogByOptionClick;

  PermissionController(this.permission,
      this.onPermissionGranted,
      this.notGrantedDialogTitle,
      this.notGrantedDialogMessage,
      this.permissionUnknownMessage,
      this.permissionRestrictedMessage, {
        this.hidePermissionWarningDialogByOptionClick = false,
        this.iconAsset,
      });

  factory PermissionController.camera(
          BuildContext context, VoidCallback onPermissionGranted) =>
      PermissionController(
          Permission.camera,
          onPermissionGranted,
          Localization.of(context).cameraPermissionTitle,
          Localization.of(context).cameraPermissionMessage,
          Localization.of(context).cameraPermissionUnknownStateMessage,
          Localization.of(context).cameraPermissionRestrictedMessage,
          iconAsset: Assets.cameraPermissionIcon);

  factory PermissionController.gallery(
          BuildContext context, VoidCallback onPermissionGranted) =>
      PermissionController(
          PlatformHelper.isIOS(context)
              ? Permission.photos
              : Permission.storage,
          onPermissionGranted,
          Localization.of(context).photosPermissionTitle,
          Localization.of(context).photosPermissionMessage,
          Localization.of(context).photosPermissionUnknownStateMessage,
          Localization.of(context).photosPermissionRestrictedMessage,
          iconAsset: Assets.photoPermissionIcon,
          hidePermissionWarningDialogByOptionClick:
              !PlatformHelper.isIOS(context));

  Future<bool> openSettings(BuildContext context) async {
    if (hidePermissionWarningDialogByOptionClick) {
      Navigator.of(context).pop();
    }
    return await openAppSettings();
  }

  void requestPermissions(BuildContext context) async {
    Map<Permission, PermissionStatus> permissions =
    await [permission].request();
    _handleCameraPermissionState(context, permissions[permission]);
  }

  void _handleCameraPermissionState(BuildContext context, PermissionStatus status) {
    switch (status) {
      case PermissionStatus.granted:
        {
          onPermissionGranted();
          break;
        }
      case PermissionStatus.denied:
        {
          _showPermissionNotGrantedDialog(context);
          break;
        }
      case PermissionStatus.undetermined:
        {
          Widgets.failedSnackBar(context, permissionUnknownMessage);
          break;
        }
      case PermissionStatus.permanentlyDenied:
      case PermissionStatus.restricted:
        {
          Widgets.failedSnackBar(context, permissionRestrictedMessage);
          break;
        }
    }
  }

  void _showPermissionNotGrantedDialog(BuildContext context) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0))),
          title: new Text(
            notGrantedDialogTitle,
            style: TextStyles.alertDialogTitleStyle,
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              if (iconAsset != null)
                Padding(
                  padding: const EdgeInsets.only(top: 32.0, bottom: 32.0),
                  child: new Center(
                    child: Image.asset(
                      iconAsset,
                      width: 92.0,
                      height: 92.0,
                    ),
                  ),
                ),
              new Text(notGrantedDialogMessage,
                  style: TextStyles.alertDialogSubTitleStyle),
            ],
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text(
                Localization.of(context).cancelButton,
                style: TextStyle(color: Palette.blue),
              ),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
                child: new Text(
                  Localization.of(context).openSettingsButton,
                  style: TextStyle(color: Palette.blue),
                ),
                onPressed: () => openSettings(context)),
            Container(
              width: 8.0,
            ),
          ],
        );
      },
    );
  }
}
