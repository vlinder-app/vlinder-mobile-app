import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

import 'request_permission_screen_configuration.dart';

class RequestPermissionScreen extends StatelessWidget {
  final IRequestPermissionScreenConfiguration configuration;

  RequestPermissionScreen(this.configuration);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Palette.blue, // status bar color
        brightness: Brightness.dark, // status bar brightness
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          _topContentContainer(context),
          _actionsContentContainer(context),
        ],
      ),
    );
  }

  Widget _topContentContainer(BuildContext context) {
    return Expanded(
      child: Container(
          color: Palette.blue,
          child: Center(
            child: Column(
              children: <Widget>[
                Spacer(
                  flex: 15,
                ),
                Flexible(
                  flex: 85,
                  child: _centralContent(context),
                )
              ],
            ),
          )),
    );
  }

  Widget _centralContent(BuildContext context) {
    return Column(
      children: [
        _imageWidget(context),
        Padding(
          padding: const EdgeInsets.only(
            top: 40.0,
            left: 24.0,
            right: 24.0,
          ),
          child: _textDescription(context),
        ),
      ],
    );
  }

  Widget _actionsContentContainer(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 20),
      child: Container(
        child: Stack(
          children: [
            Container(
              height: 26.0,
              decoration: BoxDecoration(
                  color: Palette.blue,
                  borderRadius: const BorderRadius.vertical(
                    bottom: Radius.circular(12.0),
                  )),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 24.0, right: 24.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  _actionButton(context),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: _declineActionButton(context),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _imageWidget(BuildContext context) {
    return FractionallySizedBox(
      widthFactor: 0.62,
      child: AspectRatio(
        aspectRatio: 1,
        child: Stack(
          children: <Widget>[
            Image.asset(
              Assets.dotsPattern,
            ),
            Image.asset(
              configuration.assetId(),
            ),
          ],
          alignment: Alignment.center,
        ),
      ),
    );
  }

  Widget _textDescription(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          configuration.title(context),
          textAlign: TextAlign.center,
          style: FontBook.headerH4.copyWith(color: Palette.white),
        ),
        Padding(
          padding: EdgeInsets.only(top: 1),
          child: Text(
            configuration.subtitle(context),
            textAlign: TextAlign.center,
            style: FontBook.textBody2Menu.copyWith(color: Palette.white),
          ),
        )
      ],
    );
  }

  Widget _actionButton(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: RaisedButton(
        child: Text(
          configuration.actionButtonTitle(context),
          style: TextStyle(color: Palette.blue, fontWeight: FontWeight.w600),
        ),
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(10.0)),
        color: Colors.white,
        onPressed: () => _onActionButtonTapped(context),
        elevation: 5.0,
      ),
    );
  }

  Widget _declineActionButton(BuildContext context) {
    return SizedBox(
        width: double.infinity,
        child: FlatButton(
          onPressed: () => _hide(context),
          child: Text(
            configuration.declineButtonTitle(context),
            style: FontBook.textButton.copyWith(color: Palette.blue),
          ),
        ));
  }

  void _onActionButtonTapped(BuildContext context) async {
    await configuration.action();
    Navigator.of(context).pop();
  }

  void _hide(BuildContext context) {
    Navigator.of(context).pop();
  }
}
