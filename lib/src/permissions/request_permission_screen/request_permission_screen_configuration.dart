import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/material.dart';

abstract class IRequestPermissionScreenConfiguration {

  Future<void>Function() get action;

  String assetId();

  String title(BuildContext context);

  String subtitle(BuildContext context);

  String actionButtonTitle(BuildContext context);

  String declineButtonTitle(BuildContext context);
}

class RequestPushNotificationPermissionsConfiguration
    implements IRequestPermissionScreenConfiguration {

  @override
  final Future<void> Function() action;

  RequestPushNotificationPermissionsConfiguration(this.action);

  @override
  String assetId() => Assets.infoIcon;

  @override
  String title(BuildContext context) =>
      Localization.of(context).pushNotificationPermissionAskTitle;

  @override
  String subtitle(BuildContext context) =>
      Localization.of(context).pushNotificationPermissionAskDescription;

  @override
  String actionButtonTitle(BuildContext context) =>
      Localization.of(context).pushNotificationPermissionAllowButtonTitle;

  @override
  String declineButtonTitle(BuildContext context) =>
      Localization.of(context).pushNotificationPermissionDeclineButtonTitle;
}
