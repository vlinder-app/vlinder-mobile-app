import 'package:atoma_cash/models/api/api/banners/banner_model.dart';
import 'package:atoma_cash/src/banners/banners_support_state.dart';
import 'package:atoma_cash/src/banners/widgets/banner_widget.dart';
import 'package:flutter/material.dart';

mixin BannersSupportStateMixin<T extends StatefulWidget> on State<T>
    implements IBannersSupportState {
  @override
  List<Widget> buildBanners(
      BuildContext context, List<IBannerModel> bannersList) {
    List<Widget> widgets =
        _filterReadAndOutdatedBanners(bannersList).map((bannerModel) {
      final canPerformRout =
          bannerNavigator.canPerformNavigationForBanner(bannerModel);
      return Padding(
        padding: const EdgeInsets.only(left: 24, top: 8, right: 24, bottom: 8),
        child: Dismissible(
          key: Key(bannerModel.id),
          onDismissed: (_) => markBannerAsRead(bannerModel),
          child: BannerWidget(
            bannerModel,
            onContentClicked: canPerformRout
                ? (banner) => onBannerClicked(context, banner)
                : null,
            onCrossClicked: markBannerAsRead,
          ),
        ),
      );
    }).toList();

    return widgets;
  }

  @override
  void onBannerClicked(BuildContext context, IBannerModel bannerModel) {
    markBannerAsRead(bannerModel);
    bannerNavigator.performNavigationForBanner(context, bannerModel);
  }

  List<IBannerModel> _filterReadAndOutdatedBanners(List<IBannerModel> banners) {
    final currentDate = DateTime.now();
    final filteredBanners = banners.where((oneBannerModel) {
      bool isBannerNotRead = oneBannerModel.readAt == null;
      bool canBePresented =
          (oneBannerModel.appearsAt?.toLocal()?.compareTo(DateTime.now()) ??
                  1) <=
              0;
      bool isNotExpired =
          (oneBannerModel.expiresAt?.toLocal()?.compareTo(currentDate) ?? 1) >
              0;
      return isBannerNotRead && isNotExpired && canBePresented;
    }).toList();
    return filteredBanners;
  }
}
