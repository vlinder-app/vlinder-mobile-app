import 'package:atoma_cash/models/api/api/banners/banner_model.dart';
import 'package:atoma_cash/src/home_placeholder/home_placeholder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class BannerNavigationSupportedLinks {
  static const String accountsTab = 'accounts';
  static const String activityTab = 'activity';
  static const String reportsTab = 'reports';
  static const String settingsTab = 'settings';

  static const tabs = [accountsTab, activityTab, reportsTab, settingsTab];
  static const all = [...tabs];
}

abstract class IBannerNavigator {
  bool canPerformNavigationForBanner(IBannerModel banner);

  void performNavigationForBanner(BuildContext context, IBannerModel banner);
}

class BannerNavigator implements IBannerNavigator {
  final ITabNavigationProvider _tabNavigationProvider;

  BannerNavigator(ITabNavigationProvider tabNavigationProvider)
      : assert(tabNavigationProvider != null),
        _tabNavigationProvider = tabNavigationProvider;

  @override
  bool canPerformNavigationForBanner(IBannerModel banner) =>
      BannerNavigationSupportedLinks.all.contains(banner.action);

  @override
  void performNavigationForBanner(BuildContext context, IBannerModel banner) {
    final action = banner.action;
    if (BannerNavigationSupportedLinks.all.contains(action)) {
      if (BannerNavigationSupportedLinks.tabs.contains(action)) {
        _moveToTab(context, action);
      }
    } else {
      throw '$action - action is not supported';
    }
  }

  void _moveToTab(BuildContext context, String rout) {
    final tabId = _tabIdForRout(rout);
    if (tabId != null) {
      _tabNavigationProvider.moveToTab(context, tabId);
    }
  }

  TabId _tabIdForRout(String rout) {
    switch (rout) {
      case BannerNavigationSupportedLinks.accountsTab:
        return TabId.ACCOUNTS;
      case BannerNavigationSupportedLinks.activityTab:
        return TabId.ACTIVITY;
      case BannerNavigationSupportedLinks.reportsTab:
        return TabId.CATEGORIES;
      case BannerNavigationSupportedLinks.settingsTab:
        return TabId.SETTINGS;
      default:
        return null;
    }
  }
}
