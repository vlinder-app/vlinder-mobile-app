import 'package:atoma_cash/models/api/api/banners/banner_model.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

typedef BannerCallback = Function(IBannerModel);

class BannerWidget extends StatelessWidget {
  static const _borderRadius = Radius.circular(8.0);
  static const double _iconSize = 24.0;
  final IBannerModel bannerModel;
  final BannerCallback onContentClicked;
  final BannerCallback onCrossClicked;

  BannerWidget(this.bannerModel,
      {@required this.onCrossClicked, this.onContentClicked});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(_borderRadius),
            boxShadow: [
              BoxShadow(blurRadius: 12, color: Colors.black.withOpacity(0.11))
            ],
            color: Palette.white),
        child: Material(
          borderRadius: BorderRadius.all(_borderRadius),
          clipBehavior: Clip.antiAlias,
          child: Stack(
            children: <Widget>[
              InkWell(
                onTap: onContentClicked != null ? _onContentClicked : null,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _leadingWidget(),
                    _textContainer(context),
                  ],
                ),
              ),
              if (onCrossClicked != null)
                Align(
                  alignment: Alignment.topRight,
                  child: Container(
                    width: 32.0,
                    height: 32.0,
                    child: IconButton(
                      icon: Image.asset(
                        Assets.closeBannerIcon,
                        color: Palette.blue,
                      ),
                      onPressed: _onCrossClocked,
                    ),
                  ),
                )
            ],
          ),
        ));
  }

  Widget _leadingWidget() {
    return Padding(
      padding: const EdgeInsets.only(left: 12.0, top: 16.0),
      child: Container(
        width: 40.0,
        height: 40.0,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(8.0)),
            color: Palette.paleGrey),
        child: Center(
          child: _iconWidget(),
        ),
      ),
    );
  }

  Widget _iconWidget() {
    if (bannerModel.assetId != null) {
      return Image.asset(
        bannerModel.assetId,
        width: _iconSize,
        height: _iconSize,
        fit: BoxFit.contain,
      );
    } else if (bannerModel.imageUrl != null) {
      return Image.network(
        bannerModel.imageUrl,
        width: _iconSize,
        height: _iconSize,
        fit: BoxFit.contain,
      );
    } else {
      return null;
    }
  }

  Widget _textContainer(BuildContext context) {
    return Flexible(
        child: Padding(
          padding:
          const EdgeInsets.only(left: 16.0, top: 9.0, bottom: 16.0, right: 12),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 16.0, bottom: 3.0),
            child: Text(
              bannerModel.titleForContext(context),
              style: FontBook.headerH4.copyWith(color: Palette.darkGrey),
            ),
          ),
          Text(
            bannerModel.bodyForContext(context),
            softWrap: true,
            style: FontBook.textBody2Menu.copyWith(color: Palette.mediumGrey),
          )
        ],
      ),
    ));
  }

  void _onContentClicked() {
    onContentClicked(bannerModel);
  }

  void _onCrossClocked() {
    onCrossClicked(bannerModel);
  }
}
