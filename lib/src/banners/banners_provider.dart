import 'package:atoma_cash/models/api/api/banners/banner_model.dart';
import 'package:atoma_cash/src/banners/data_provider/banners_data_provider.dart';

abstract class IBannersProvider {
  IBannersDataProvider get bannersRepository;

  void markBannerAsRead(IBannerModel bannerModel);

  void subscribeToBannersUpdates();

  void unsubscribeFromBannersUpdates();

  void fetchBanners();

  void onBannersUpdated();
}
