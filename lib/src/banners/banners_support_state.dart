import 'package:atoma_cash/models/api/api/banners/banner_model.dart';
import 'package:atoma_cash/src/banners/banners_navigator.dart';
import 'package:flutter/material.dart';

abstract class IBannersSupportState {
  IBannerNavigator get bannerNavigator;

  List<Widget> buildBanners(
      BuildContext context, List<IBannerModel> bannersList);

  void onBannerClicked(BuildContext context, IBannerModel bannerModel);

  void markBannerAsRead(IBannerModel bannerModel);
}
