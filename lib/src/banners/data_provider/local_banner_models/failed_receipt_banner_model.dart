import 'package:atoma_cash/models/api/api/banners/banner_model.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/banners/banners_navigator.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class FailedReceiptBannerModel extends IBannerModel {
  @override
  String get action => BannerNavigationSupportedLinks.activityTab;

  @override
  final DateTime appearsAt;

  @override
  final DateTime readAt;

  @override
  final String id;

  @override
  String get assetId => Assets.failedReceiptBannerIcon;

  @override
  DateTime get expiresAt => null;

  @override
  String get imageUrl => null;

  final Set<String> failedReceiptsIds;

  FailedReceiptBannerModel(
      {this.id, this.failedReceiptsIds, this.appearsAt, this.readAt})
      : assert(id != null),
        assert(failedReceiptsIds != null);

  factory FailedReceiptBannerModel.fromFailedReceiptsIds(
          Set<String> failedReceiptsIds) =>
      FailedReceiptBannerModel(
        id: Uuid().v4(),
        failedReceiptsIds: failedReceiptsIds,
        appearsAt: DateTime.now(),
      );

  FailedReceiptBannerModel copyWith(
      {String id,
      Set<String> failedReceiptsIds,
      DateTime appearsAt,
      DateTime readAt}) {
    return FailedReceiptBannerModel(
      id: id ?? this.id,
      failedReceiptsIds: failedReceiptsIds ?? this.failedReceiptsIds,
      appearsAt: appearsAt ?? this.appearsAt,
      readAt: readAt ?? this.readAt,
    );
  }

  @override
  String titleForContext(BuildContext context) {
    return Localization.of(context).failedReceiptScanBannerTitle;
  }

  @override
  String bodyForContext(BuildContext context) {
    return Localization.of(context)
        .failedReceiptScanBannerBody(failedReceiptsIds?.length ?? 0);
  }

  @override
  List<Object> get props => [id, failedReceiptsIds, appearsAt, readAt];

  @override
  bool get stringify => true;
}
