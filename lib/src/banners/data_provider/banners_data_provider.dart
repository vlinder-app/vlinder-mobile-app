import 'dart:core';

import 'package:atoma_cash/models/api/api/banners/banner_model.dart';

abstract class IBannersDataProviderUpdateListener {
  void bannersUpdated(List<IBannerModel> banners);
}

abstract class IBannersDataProvider {
  void begin();

  void dispose();

  void addListener(IBannersDataProviderUpdateListener listener);

  void removeListener(IBannersDataProviderUpdateListener listener);

  void refresh();

  void markBannerAsRead(IBannerModel bannerModel);
}
