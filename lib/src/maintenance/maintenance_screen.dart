import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/maintenance/bloc/bloc.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:atoma_cash/utils/launch_url/email_client_content_builder.dart';
import 'package:atoma_cash/utils/launch_url/launch_url_helper.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MaintenanceScreen extends StatefulWidget {
  @override
  _MaintenanceScreenState createState() => _MaintenanceScreenState();
}

class _MaintenanceScreenState extends State<MaintenanceScreen>
    with WidgetsBindingObserver {
  static const String _KQuestionEmailSubject = 'Question';
  static const String _KSupportEmail = "info@vlinder.app";

  MaintenanceBloc _bloc;
  AppLifecycleState _previousState;

  @override
  void initState() {
    super.initState();
    _bloc = MaintenanceBloc()..add(Started());
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocListener(
        bloc: _bloc,
        listener: _blocListener,
        child: BlocBuilder(
          bloc: _bloc,
          builder: _blocBuilder,
        ),
      ),
    );
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state != _previousState && state == AppLifecycleState.resumed) {
      _bloc.add(UpdateStatus());
    }
  }

  void _blocListener(BuildContext context, MaintenanceState state) {
    if (state is MaintenanceModeDisabledState) {
      _moveToHomeScreen();
    }
  }

  Widget _blocBuilder(BuildContext context, MaintenanceState state) {
    if (state is LoadingState) {
      return _loadingWidgetState(context);
    }
    if (state is InitialMaintenanceState) {
      return _loadedWidgetState(context, state);
    }
    return Container();
  }

  Widget _loadingWidgetState(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _loadedWidgetState(
      BuildContext context, InitialMaintenanceState state) {
    const labelsPadding = EdgeInsets.only(top: 14.0);
    return Container(
      margin: Dimensions.leftRight24Padding,
      child: Column(
        children: <Widget>[
          Flexible(
            flex: 7,
            child: Container(
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Image.asset(Assets.maintenanceModeIcon),
                    Padding(
                      padding: labelsPadding,
                      child: Text(
                        Localization.of(context).maintenanceTitle,
                        style: FontBook.headerH3,
                      ),
                    ),
                    Padding(
                      padding: labelsPadding,
                      child: Text(
                        Localization.of(context).maintenanceDescription,
                        style: FontBook.textBody1,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Flexible(
            flex: 2,
            child: Container(
              child: Center(
                  child: FlatButton(
                child: Text(
                  Localization.of(context).contactUsButton,
                  style: FontBook.textButton.copyWith(color: Palette.blue),
                ),
                onPressed: () =>
                    {_onContactUsClick(state.userId, state.deviceInfo)},
              )),
            ),
          )
        ],
      ),
    );
  }

  void _onContactUsClick(String userId, String deviceInfo) {
    LaunchUrlHelper.openEmailClient(
      context,
      contentBuilder: DefaultMailtoContentBuilder(
        email: _KSupportEmail,
        subject: _KQuestionEmailSubject,
        userId: userId,
        deviceInfo: deviceInfo,
      ),
    );
  }

  void _moveToHomeScreen() {
    ExtendedNavigator.ofRouter<Router>()
        .pushNamedAndRemoveUntil(Routes.home, (Route<dynamic> route) => false);
  }
}
