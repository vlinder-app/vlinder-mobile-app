import 'package:equatable/equatable.dart';

abstract class MaintenanceState extends Equatable {
  const MaintenanceState();
}

class LoadingState extends MaintenanceState {
  @override
  List<Object> get props => null;
}

class InitialMaintenanceState extends MaintenanceState {
  final String deviceInfo;
  final String userId;

  InitialMaintenanceState({this.deviceInfo, this.userId});

  @override
  List<Object> get props => [deviceInfo, userId];
}

class MaintenanceModeDisabledState extends MaintenanceState {
  @override
  List<Object> get props => null;
}
