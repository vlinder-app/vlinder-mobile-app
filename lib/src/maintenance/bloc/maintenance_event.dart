import 'package:equatable/equatable.dart';

abstract class MaintenanceEvent extends Equatable {
  const MaintenanceEvent();
}

class Started extends MaintenanceEvent {
  @override
  List<Object> get props => null;
}

class UpdateStatus extends MaintenanceEvent {
  @override
  List<Object> get props => null;
}
