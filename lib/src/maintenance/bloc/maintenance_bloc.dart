import 'dart:async';

import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/utils/device_info.dart';
import 'package:bloc/bloc.dart';

import './bloc.dart';

class MaintenanceBloc extends Bloc<MaintenanceEvent, MaintenanceState> {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  ApplicationLocalRepository _localRepository = ApplicationLocalRepository();

  @override
  MaintenanceState get initialState => LoadingState();

  @override
  Stream<MaintenanceState> mapEventToState(
    MaintenanceEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapStartedToState();
    }
    if (event is UpdateStatus) {
      yield* _mapRefreshToState();
    }
  }

  Stream<MaintenanceState> _mapStartedToState() async* {
    var userIdFuture = _localRepository.getUserId();
    var deviceInfoFuture = DeviceInfo.getDeviceInfo();
    var futuresResult = await Future.wait([userIdFuture, deviceInfoFuture]);
    yield InitialMaintenanceState(
        deviceInfo: futuresResult[1], userId: futuresResult[0]);
  }

  Stream<MaintenanceState> _mapRefreshToState() async* {
    var linksResponse = await _apiRemoteRepository.getLinks();
    if (linksResponse.isSuccess()) {
      yield MaintenanceModeDisabledState();
    }
  }
}
