// To parse this JSON data, do
//
//     final pendingReceipt = pendingReceiptFromJson(jsonString);

import 'dart:convert';

import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/src/pending_receipts/models/receipt_image_event_type.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

PendingReceipt pendingReceiptFromJson(String str) =>
    PendingReceipt.fromJson(json.decode(str));

String pendingReceiptToJson(PendingReceipt data) => json.encode(data.toJson());

@immutable
class PendingReceipt extends Equatable {
  final String id;
  final int progressPercentage;
  final ReceiptImageEventType type;
  final DateTime createDateTime;
  final Transaction transaction;

  PendingReceipt(
      {this.id,
      this.progressPercentage = 0,
      this.type = ReceiptImageEventType.Uploading,
      this.createDateTime,
      this.transaction});

  factory PendingReceipt.empty() =>
      PendingReceipt(id: Uuid().v4(), createDateTime: DateTime.now());

  factory PendingReceipt.withReceipt(PendingReceipt receipt) => PendingReceipt(
      id: receipt.id,
      progressPercentage: receipt.progressPercentage,
      type: receipt.type,
      transaction: receipt.transaction,
      createDateTime: receipt.createDateTime);

  PendingReceipt copyWith(
          {String id,
          DateTime createDateTime,
          int progressPercentage,
          ReceiptImageEventType type,
          CancelToken cancelToken,
          Transaction transaction}) =>
      PendingReceipt(
        id: id ?? this.id,
        transaction: transaction ?? this.transaction,
        createDateTime: createDateTime ?? this.createDateTime,
        progressPercentage: progressPercentage ?? this.progressPercentage,
        type: type ?? this.type,
      );

  factory PendingReceipt.fromJson(Map<String, dynamic> json) => PendingReceipt(
        id: json["id"] == null ? null : json["id"],
        progressPercentage: json["progressPercentage"] == null
            ? null
            : json["progressPercentage"],
        type: json["type"] == null
            ? null
            : ReceiptImageEventType.values[json["type"]],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "progressPercentage":
            progressPercentage == null ? null : progressPercentage,
        "type": type == null ? null : receiptImageEventTypeValues.reverse[type],
      };

  @override
  List<Object> get props => [
        id,
        type,
        progressPercentage,
        createDateTime,
        transaction,
      ];

  @override
  String toString() {
    return 'PendingReceipt{id: $id, progressPercentage: $progressPercentage, type: $type, createDateTime: $createDateTime}';
  }
}
