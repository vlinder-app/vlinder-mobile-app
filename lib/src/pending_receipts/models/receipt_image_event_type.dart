import 'package:atoma_cash/extensions/enum.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/cupertino.dart';

enum ReceiptImageEventType {
  Uploading,
  UploadingFinished,
  Saving,
  SavingFinished,
  SearchingBarcodes,
  SearchingBarcodesFinished,
  Recognition,
  RecognitionFinished,
  Cancelled,
  UploadingFailed,
  PartiallyRecognized,
  RecognitionFailed
}

final receiptImageEventTypeValues = EnumValues({
  "Uploading": ReceiptImageEventType.Uploading,
  "UploadingFinished": ReceiptImageEventType.UploadingFinished,
  "Saving": ReceiptImageEventType.Saving,
  "SavingFinished": ReceiptImageEventType.SavingFinished,
  "SearchingBarcodes": ReceiptImageEventType.SearchingBarcodes,
  "SearchingBarcodesFinished": ReceiptImageEventType.SearchingBarcodesFinished,
  "Recognition": ReceiptImageEventType.Recognition,
  "RecognitionFinished": ReceiptImageEventType.RecognitionFinished,
  "Cancelled": ReceiptImageEventType.Cancelled,
  "UploadingFailed": ReceiptImageEventType.UploadingFailed,
  "PartiallyRecognized": ReceiptImageEventType.PartiallyRecognized,
  "RecognitionFailed": ReceiptImageEventType.RecognitionFailed
});

extension ReceiptImageEventTypeLocalization on ReceiptImageEventType {
  String toLocalizedString(BuildContext context) {
    switch (this) {
      case ReceiptImageEventType.Uploading:
      case ReceiptImageEventType.UploadingFinished:
      case ReceiptImageEventType.Saving:
      case ReceiptImageEventType.SavingFinished:
        return Localization.of(context).pendingReceiptProcessingUploading;
      case ReceiptImageEventType.SearchingBarcodes:
      case ReceiptImageEventType.SearchingBarcodesFinished:
        return Localization.of(context)
            .pendingReceiptProcessingBarcodeSearching;
      case ReceiptImageEventType.Recognition:
      case ReceiptImageEventType.RecognitionFinished:
        return Localization.of(context).pendingReceiptProcessingRecognition;
      case ReceiptImageEventType.Cancelled:
        return Localization.of(context).uploadingStoppedHint;
      case ReceiptImageEventType.UploadingFailed:
        return Localization.of(context).pendingReceiptProcessingFailedToUpload;
      case ReceiptImageEventType.PartiallyRecognized:
        return Localization.of(context)
            .pendingReceiptProcessingPartiallyRecognized;
      case ReceiptImageEventType.RecognitionFailed:
        return Localization.of(context)
            .pendingReceiptProcessingFailedToRecognize;
      default:
        return "Undefined state";
    }
  }
}
