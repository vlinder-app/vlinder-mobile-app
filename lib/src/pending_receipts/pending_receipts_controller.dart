import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/models/api/error/error.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/resources/repository/remote/signalr/pending_requests_event_listener.dart';
import 'package:atoma_cash/resources/repository/remote/signalr/signalr_client.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:atoma_cash/src/pending_receipts/models/receipt_image_event_type.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_container.dart';
import 'package:dio/dio.dart';

abstract class IReceiptUpdateListener {
  void onReceiptsUpdate(List<PendingReceipt> receipts);

  void onSuccessTransaction(Transaction transaction, String guid);
}

class ReceiptContainer {
  PendingReceipt receipt;
  CancelToken cancelToken;
  ReceiptImageContainer imageContainer;

  ReceiptContainer({this.receipt, this.cancelToken, this.imageContainer});

  @override
  String toString() {
    return 'ReceiptContainer{receipt: $receipt, cancelToken: $cancelToken}';
  }
}

class PendingReceiptController {
  static final PendingReceiptController _instance =
      PendingReceiptController._internal();

  PendingReceiptController._internal() {
    _signalrClient = SignalrClient();
    _apiRemoteRepository = ApiRemoteRepository();
    _pendingRequestsEventListener =
        PendingRequestsEventListener(_signalrClient, _onReceiveEvent);
  }

  Map<String, ReceiptContainer> _receipts = {};

  factory PendingReceiptController() {
    return _instance;
  }

  void dispose() {
    _receipts = null;
    _listeners = null;
    _pendingRequestsEventListener.dispose();
  }

  List<IReceiptUpdateListener> _listeners = [];

  List<PendingReceipt> get pendingReceipts => _pendingReceiptsList();

  ApiRemoteRepository _apiRemoteRepository;
  SignalrClient _signalrClient;
  PendingRequestsEventListener _pendingRequestsEventListener;

  List<PendingReceipt> _pendingReceiptsList() =>
      List<PendingReceipt>.generate(
          _receipts.values.length, (i) => _receipts.values.toList()[i].receipt)
        ..sort((b, a) => a.createDateTime.compareTo(b.createDateTime));

  ReceiptImageContainer getContainer(String guid) =>
      _receipts[guid].imageContainer;

  void addListener(IReceiptUpdateListener listener) => _listeners.add(listener);

  void removeListener(IReceiptUpdateListener listener) =>
      _listeners.remove(listener);

  void _notifyListeners() {
    _listeners.forEach(
        (listener) => listener.onReceiptsUpdate(_pendingReceiptsList()));
  }

  void _notifyAboutTransactionCreate(Transaction transaction, String guid) {
    _listeners.forEach((l) => l.onSuccessTransaction(transaction, guid));
  }

  PendingReceipt retryUpload(String guid) {
    PendingReceipt updatedReceipt = PendingReceipt.empty();
    _fillContainers(updatedReceipt.id, updatedReceipt, CancelToken(),
        _receipts[guid].imageContainer.copy());
    _scanReceipt([updatedReceipt.id]);
    removeReceipt(guid);
    return updatedReceipt;
  }

  void removeReceipt(String guid) {
    _receipts.remove(guid);
    _notifyListeners();
  }

  void stopProcessing(String guid) {
    _receipts[guid].cancelToken?.cancel("Stopped by user");
  }

  void scanReceipts(List<ReceiptImageContainer> receiptContainers) {
    _pendingRequestsEventListener.listenRequestsEvents();
    List<String> requests = List.generate(receiptContainers.length,
            (i) => _prepareReceiptForScan(receiptContainers[i]));
    _notifyListeners();
    _scanReceipt(requests);
  }

  String _prepareReceiptForScan(ReceiptImageContainer container) {
    PendingReceipt receipt = PendingReceipt.empty();
    String guid = receipt.id;
    _fillContainers(guid, receipt, CancelToken(), container);
    return guid;
  }

  void _fillContainers(String guid, PendingReceipt receipt,
      CancelToken cancelToken, ReceiptImageContainer receiptContainer) {
    _receipts.putIfAbsent(
        guid,
            () =>
            ReceiptContainer(
                receipt: receipt,
                cancelToken: cancelToken,
                imageContainer: receiptContainer));
  }

  Future<void> _performReceipt(ReceiptContainer receiptContainer) async {
    var receiptFileBytes =
    await receiptContainer.imageContainer.receiptByteData();
    BaseResponse<Transaction> scanResponse =
    await _apiRemoteRepository.scanReceipt(receiptFileBytes,
        cancelToken: receiptContainer.cancelToken,
        imageId: receiptContainer.receipt.id);
    if (scanResponse.isSuccess()) {
      _handleOcrResult(scanResponse.result, receiptContainer.receipt.id);
    } else {
      _handleFailedRequest(scanResponse, receiptContainer.receipt.id);
    }
  }

  void _handleOcrResult(Transaction transaction, String guid) {
    if (transaction.amount.value != null &&
        transaction.amount.value != 0.0 &&
        transaction.bookedAt != null &&
        transaction.counterparty?.name != null) {
      _addTransaction(transaction, guid);
    } else if ((transaction.amount.value != null &&
            transaction.amount.value != 0.0) ||
        transaction.bookedAt != null ||
        transaction.counterparty?.name != null) {
      _updateReceipt(guid, ReceiptImageEventType.PartiallyRecognized,
          transaction: transaction);
    } else {
      _updateReceipt(guid, ReceiptImageEventType.RecognitionFailed,
          transaction: Transaction.empty().copyWith(receiptImageId: guid));
    }
  }

  void _updateReceipt(String guid, ReceiptImageEventType type,
      {Transaction transaction, int progressPercentage}) {
    _receipts.update(
        guid,
            (receiptContainer) =>
        receiptContainer
          ..receipt = receiptContainer.receipt.copyWith(
              type: type,
              transaction: transaction,
              progressPercentage: progressPercentage));
    _notifyListeners();
  }

  Transaction _signedTransaction(Transaction transaction) {
    return transaction.copyWith(
        amount:
        transaction.amount.copyWith(value: transaction.amount.value * -1));
  }

  void _addTransaction(Transaction transaction, String guid) async {
    BaseResponse<Transaction> transactionResponse = await _apiRemoteRepository
        .addTransaction(_signedTransaction(transaction));
    if (transactionResponse.isSuccess()) {
      _receipts.remove(guid);
      _notifyListeners();
      _notifyAboutTransactionCreate(transactionResponse.result, guid);
    } else {
      _updateReceipt(guid, ReceiptImageEventType.RecognitionFailed);
    }
  }

  void _handleFailedRequest(
      BaseResponse<Transaction> response, String pendingReceiptGuid) {
    ReceiptImageEventType eventType;
    if (response.error.type == ErrorType.CANCEL) {
      eventType = ReceiptImageEventType.Cancelled;
    } else {
      eventType = ReceiptImageEventType.RecognitionFailed;
    }
    _updateReceipt(pendingReceiptGuid, eventType);
    _notifyListeners();
  }

  void _scanReceipt(List<String> receiptIds) {
    var futures = List<Future<void>>.generate(receiptIds.length, (i) {
      ReceiptContainer container = _receipts[receiptIds[i]];
      return _performReceipt(container);
    });
    Future.wait(futures);
  }

  bool _isShouldHandleEvent(PendingReceipt receipt) {
    var existingReceipt = _receipts[receipt.id].receipt;
    switch (existingReceipt.type) {
      case ReceiptImageEventType.Cancelled:
      case ReceiptImageEventType.UploadingFailed:
      case ReceiptImageEventType.PartiallyRecognized:
      case ReceiptImageEventType.RecognitionFailed:
        return false;
      default:
        return true;
    }
  }

  void _onReceiveEvent(PendingReceipt receipt) {
    if (_isShouldHandleEvent(receipt)) {
      _updateReceipt(receipt.id, receipt.type,
          progressPercentage: receipt.progressPercentage);
      _notifyListeners();
    }
  }
}
