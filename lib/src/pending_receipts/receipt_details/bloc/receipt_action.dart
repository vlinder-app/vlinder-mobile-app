import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:equatable/equatable.dart';

abstract class ReceiptAction extends Equatable {}

class RemoveAction extends ReceiptAction {
  @override
  List<Object> get props => null;
}

class ViewTransactionAction extends ReceiptAction {
  final Transaction transaction;

  ViewTransactionAction(this.transaction);

  @override
  List<Object> get props => [transaction];
}

class FillTransactionAction extends ReceiptAction {
  final Transaction transaction;

  FillTransactionAction(this.transaction);

  @override
  List<Object> get props => [transaction];
}
