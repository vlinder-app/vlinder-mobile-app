import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:equatable/equatable.dart';

abstract class ReceiptEvent extends Equatable {
  const ReceiptEvent();
}

class Started extends ReceiptEvent {
  final PendingReceipt receipt;

  Started(this.receipt);

  @override
  List<Object> get props => [receipt];
}

class ReceiptUpdated extends ReceiptEvent {
  final PendingReceipt receipt;

  ReceiptUpdated(this.receipt);

  @override
  List<Object> get props => [receipt];
}

class ReceiptScanned extends ReceiptEvent {
  final Transaction transaction;

  ReceiptScanned(this.transaction);

  @override
  List<Object> get props => [transaction];
}

class CancelProcessing extends ReceiptEvent {
  CancelProcessing();

  @override
  List<Object> get props => null;
}

class Retry extends ReceiptEvent {
  Retry();

  @override
  List<Object> get props => null;
}

class Remove extends ReceiptEvent {
  final bool withAction;

  Remove({this.withAction = false});

  @override
  List<Object> get props => [withAction];
}

class FillTransaction extends ReceiptEvent {
  @override
  List<Object> get props => null;
}
