import 'dart:typed_data';

import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_details/bloc/receipt_action.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class ReceiptState extends Equatable {
  final PendingReceipt receipt;
  final bool isLoading;
  final Uint8List receiptImageByteData;
  final Transaction transaction;
  final ReceiptAction action;

  ReceiptState(
      {this.receipt,
      this.isLoading,
      this.transaction,
      this.action,
      this.receiptImageByteData});

  ReceiptState copyWith(
          {PendingReceipt receipt,
          bool isLoading,
          List<Category> categories,
          Transaction transaction,
          ReceiptAction receiptAction,
          Uint8List receiptImageByteData}) =>
      ReceiptState(
          transaction: transaction ?? this.transaction,
          receiptImageByteData:
              receiptImageByteData ?? this.receiptImageByteData,
          receipt: receipt ?? this.receipt,
          action: receiptAction ?? this.action,
          isLoading: isLoading ?? this.isLoading);

  ReceiptState resetAction() => ReceiptState(
      receipt: this.receipt,
          isLoading: this.isLoading,
          transaction: this.transaction,
          action: null,
          receiptImageByteData: this.receiptImageByteData);

  @override
  List<Object> get props =>
      [receipt, isLoading, receiptImageByteData, transaction, action];
}
