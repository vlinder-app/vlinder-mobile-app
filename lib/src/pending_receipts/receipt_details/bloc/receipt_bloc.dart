import 'dart:async';

import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/fill_out_pending_tx_details_event.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_details/bloc/receipt_action.dart';
import 'package:bloc/bloc.dart';

import './bloc.dart';
import '../../pending_receipts_controller.dart';

class ReceiptBloc extends Bloc<ReceiptEvent, ReceiptState>
    implements IReceiptUpdateListener {
  PendingReceiptController _pendingReceiptController;

  ReceiptBloc() {
    _pendingReceiptController = PendingReceiptController()..addListener(this);
  }

  @override
  ReceiptState get initialState => ReceiptState(isLoading: true);

  @override
  Stream<ReceiptState> mapEventToState(
    ReceiptEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapStartedToState(event.receipt);
    }
    if (event is ReceiptUpdated) {
      yield* _mapReceiptUpdatedToState(event.receipt);
    }
    if (event is CancelProcessing) {
      yield* _mapCancelProcessingToState();
    }
    if (event is Retry) {
      yield* _mapRetryEventToState();
    }
    if (event is Remove) {
      yield* _mapRemoveToState(event.withAction);
    }
    if (event is ReceiptScanned) {
      yield* _mapTransactionScannedToState(event.transaction);
    }
    if (event is FillTransaction) {
      yield* _mapFillTransactionToState();
    }
  }

  Stream<ReceiptState> _mapStartedToState(PendingReceipt receipt) async* {
    yield state.copyWith(receipt: receipt, isLoading: false);
    var container = _pendingReceiptController.getContainer(receipt.id);
    var byteData = await container.receiptByteData();
    yield state.copyWith(receiptImageByteData: byteData);
  }

  Stream<ReceiptState> _mapReceiptUpdatedToState(
      PendingReceipt receipt) async* {
    yield state.copyWith(receipt: receipt);
  }

  Stream<ReceiptState> _mapRetryEventToState() async* {
    var updatedReceipt =
        _pendingReceiptController.retryUpload(state.receipt.id);
    yield state.copyWith(receipt: updatedReceipt);
  }

  Stream<ReceiptState> _mapCancelProcessingToState() async* {
    _pendingReceiptController.stopProcessing(state.receipt.id);
  }

  Stream<ReceiptState> _mapFillTransactionToState() async* {
    Analytics().logEvent(FillOutPendingTxDetailsEvent(state.receipt.type));
    yield state.copyWith(
        receiptAction: FillTransactionAction(state.receipt.transaction));
    yield state.resetAction();
  }

  Stream<ReceiptState> _mapRemoveToState(bool withAction) async* {
    _pendingReceiptController.removeReceipt(state.receipt.id);
    if (withAction) {
      yield state.copyWith(receiptAction: RemoveAction());
    }
  }

  Stream<ReceiptState> _mapTransactionScannedToState(
      Transaction transaction) async* {
    yield state.copyWith(transaction: transaction);
  }

  @override
  void onReceiptsUpdate(List<PendingReceipt> receipts) {
    PendingReceipt updatedReceipt = receipts.firstWhere(
            (receipt) => state.receipt.id == receipt.id,
        orElse: () => null);
    if (updatedReceipt != null) {
      add(ReceiptUpdated(updatedReceipt));
    }
  }

  @override
  void onSuccessTransaction(Transaction transaction, String guid) {
    add(ReceiptScanned(transaction));
  }
}
