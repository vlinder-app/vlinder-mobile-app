import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/src/widgets/rounded_icon.dart';
import 'package:flutter/material.dart';

class ReceiptScannedCell extends StatelessWidget {
  final VoidCallback onViewClick;

  const ReceiptScannedCell({Key key, this.onViewClick}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FourRowListTile(
      forceThreeRow: true,
      title: Text(
        Localization.of(context).receiptPhotoCellTitle,
        style: FourRowListTile.titleTextStyle,
      ),
      subtitle: Text(
        Localization.of(context).pendingReceiptProcessingSuccess,
        style: FourRowListTile.subtitleTextStyle,
      ),
      leading: RoundedIcon(
        asset: Assets.receiptWhiteIcon,
        size: 54.0,
        iconColor: Palette.mediumGrey,
      ),
      trailing: RawMaterialButton(
        constraints: BoxConstraints(minHeight: 44.0, minWidth: 55.0),
        child: Text(
          Localization
              .of(context)
              .viewTransactionButton,
          style: FourRowListTile.titleTextStyle.copyWith(color: Palette.blue),
        ),
        onPressed: onViewClick,
      ),
    );
  }
}
