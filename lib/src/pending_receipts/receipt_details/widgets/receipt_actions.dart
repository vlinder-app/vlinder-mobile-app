import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:atoma_cash/src/pending_receipts/models/receipt_image_event_type.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:flutter/material.dart';

class ReceiptActions extends StatelessWidget {
  final PendingReceipt pendingReceipt;
  final VoidCallback onFillDetailsClick;
  final VoidCallback onRetryClick;
  final VoidCallback onRemoveClick;

  const ReceiptActions(this.pendingReceipt, this.onFillDetailsClick,
      this.onRetryClick, this.onRemoveClick,
      {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: _receiptActionsForReceiptType(context, pendingReceipt),
    );
  }

  List<Widget> _receiptActionsForReceiptType(
      BuildContext context, PendingReceipt receipt) {
    var retryButton = _retryButton(context, onRetryClick);
    var removeButton = _removeButton(context, onRemoveClick);
    var fillDetailsButton = _fillDetailsButton(context, onFillDetailsClick);

    List<Widget> failedActionsGroup = [retryButton, removeButton];
    List<Widget> neutralActionsGroup = [fillDetailsButton, removeButton];

    switch (receipt.type) {
      case ReceiptImageEventType.Cancelled:
      case ReceiptImageEventType.UploadingFailed:
        return failedActionsGroup;
      case ReceiptImageEventType.PartiallyRecognized:
      case ReceiptImageEventType.RecognitionFailed:
        return neutralActionsGroup;
      default:
        return [];
    }
  }

  Widget _receiptActionButton(
          String text, Color textColor, String asset, VoidCallback onClick) =>
      InkWell(
        onTap: onClick,
        child: Container(
          margin: Dimensions.leftRight24Padding.copyWith(top: 8.0, bottom: 8.0),
          height: 44.0,
          child: Row(
            children: <Widget>[
              Image.asset(asset),
              Padding(
                padding: const EdgeInsets.only(left: 16.0),
                child: Text(
                  text,
                  style:
                      FourRowListTile.titleTextStyle.copyWith(color: textColor),
                ),
              ),
            ],
          ),
        ),
      );

  /*
      Container(
        padding: Dimensions.leftRight24Padding,
        child: ListTile(
          onTap: onClick,
          title: Text(
            text,
            style: FourRowListTile.titleTextStyle.copyWith(color: textColor),
          ),
          leading: Image.asset(asset),
        ),
      );
      */

  Widget _retryButton(BuildContext context, VoidCallback onClick) =>
      _receiptActionButton(Localization.of(context).retryUploadButton,
          Palette.blue, Assets.retryReceiptIcon, onClick);

  Widget _fillDetailsButton(BuildContext context, VoidCallback onClick) =>
      _receiptActionButton(Localization.of(context).fillOutDetailsMyselfButton,
          Palette.blue, Assets.fillDetailsIcon, onClick);

  Widget _removeButton(BuildContext context, VoidCallback onClick) =>
      _receiptActionButton(Localization
          .of(context)
          .removeReceipt, Palette.red,
          Assets.trashCanRedIcon, onClick);
}
