import 'dart:typed_data';

import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/src/activity/widgets/listview/pending_receipt_cell.dart';
import 'package:atoma_cash/src/bank/webview/custom_app_bar.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_details/bloc/bloc.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_details/widgets/hero_photo_view_wrapper.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_details/widgets/receipt_actions.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_details/widgets/receipt_scanned_cell.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/src/transaction/transaction_details/transaction_details_screen.dart'
    as transaction_details;
import 'package:atoma_cash/src/transaction/transaction_operations/transaction_operations_screen.dart';
import 'package:atoma_cash/src/widgets/receipt_image_placeholder.dart';
import 'package:atoma_cash/utils/ui/custom_routes.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/receipt_action.dart';

class PendingReceiptDetailsScreen extends StatefulWidget {
  final PendingReceipt receipt;

  const PendingReceiptDetailsScreen({Key key, @required this.receipt})
      : super(key: key);

  @override
  _PendingReceiptDetailsScreenState createState() =>
      _PendingReceiptDetailsScreenState();
}

class _PendingReceiptDetailsScreenState
    extends State<PendingReceiptDetailsScreen> {
  ReceiptBloc _bloc;

  @override
  void initState() {
    _bloc = ReceiptBloc()..add(Started(widget.receipt));
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _bloc,
      listener: _blocListener,
      child: BlocBuilder(
        bloc: _bloc,
        builder: (context, state) {
          return Scaffold(
            appBar: ModalStyleAppBar(
              AppBar(
                backgroundColor: Colors.transparent,
              ),
            ),
            body: _rootContentBuilder(context, state),
          );
        },
      ),
    );
  }

  void _blocListener(BuildContext context, ReceiptState state) {
    ReceiptAction action = state.action;
    if (action != null) {
      if (action is RemoveAction) {
        Navigator.of(context).pop();
      }
      if (action is FillTransactionAction) {
        _showTransactionData(action.transaction);
      }
    }
  }

  Widget _rootContentBuilder(BuildContext context, ReceiptState state) {
    if (state.isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return Column(
        children: <Widget>[
          _receiptCellForState(context, state),
          ReceiptImagePlaceholder(
            child: _imageFromMemoryWithPlaceholder(state.receiptImageByteData),
            onImageTap: () =>
                _viewReceiptInFullScreen(context, state.receiptImageByteData),
          ),
          _receiptActionsBuilder(context, state.receipt)
        ],
      );
    }
  }

  Widget _receiptActionsBuilder(BuildContext context, PendingReceipt receipt) =>
      ReceiptActions(
          receipt, _onFillTransactionClick, _onRetryClick, _onRemoveClick);

  Widget _receiptCellForState(BuildContext context, ReceiptState state) {
    if (state.transaction != null) {
      return ReceiptScannedCell(
        onViewClick: () => _onViewTransactionClick(state.transaction),
      );
    } else {
      return PendingReceiptCell(
        pendingReceipt: state.receipt,
        onProgressClick: _onPendingReceiptProgressClick,
      );
    }
  }

  Widget _imageFromMemoryWithPlaceholder(Uint8List bytes) {
    if (bytes != null) {
      return Image.memory(
        bytes,
        scale: 0.2,
        fit: BoxFit.cover,
      );
    } else {
      return Center(child: CircularProgressIndicator());
    }
  }

  void _viewReceiptInFullScreen(BuildContext context, Uint8List bytes) {
    Navigator.push(
        context,
        MaterialRouteWithoutAnimation(
          builder: (context) => HeroPhotoViewWrapper(
            imageProvider: MemoryImage(bytes),
          ),
        ));
  }

  void _onFillTransactionClick() => _bloc.add(FillTransaction());

  void _onRetryClick() => _bloc.add(Retry());

  void _onRemoveClick() => _bloc.add(Remove(withAction: true));

  void _onViewTransactionClick(Transaction transaction) async {
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.transactionDetailsScreen,
        arguments: TransactionDetailsScreenArguments(transaction: transaction));
    if (result != null) {
      if (result is transaction_details.RemoveAction) {
        Navigator.pop(context, transaction_details.RemoveAction());
      }
    }
  }

  void _onPendingReceiptProgressClick(String guid) =>
      _bloc.add(CancelProcessing());

  void _showTransactionData(Transaction transaction) async {
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.transactionOperationsScreen,
        arguments: TransactionOperationsScreenArguments(
            transaction: transaction, operation: TransactionOperation.CREATE));
    if (result is EditOperation) {
      _bloc.add(Remove());
      Navigator.of(context).pop(result);
    }
  }
}
