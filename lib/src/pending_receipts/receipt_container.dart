import 'dart:io';
import 'dart:typed_data';

import 'package:multi_image_picker/multi_image_picker.dart';

abstract class ReceiptImageContainer {
  Future<Uint8List> receiptByteData();

  ReceiptImageContainer();

  ReceiptImageContainer copy();

  factory ReceiptImageContainer.fromFile(File file) => ReceiptFile(file);

  factory ReceiptImageContainer.fromAsset(Asset asset) => ReceiptAsset(asset);
}

class ReceiptFile extends ReceiptImageContainer {
  final File file;

  ReceiptFile(this.file);

  @override
  Future<Uint8List> receiptByteData() {
    return file.readAsBytes();
  }

  @override
  ReceiptImageContainer copy() {
    return ReceiptImageContainer.fromFile(file);
  }
}

class ReceiptAsset extends ReceiptImageContainer {
  final Asset asset;

  ReceiptAsset(this.asset);

  @override
  Future<Uint8List> receiptByteData() async {
    var byteData = await asset.getByteData();
    return byteData.buffer.asUint8List();
  }

  @override
  ReceiptImageContainer copy() {
    return ReceiptImageContainer.fromAsset(asset);
  }
}
