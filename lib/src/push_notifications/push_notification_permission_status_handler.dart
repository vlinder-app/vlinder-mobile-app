import 'package:atoma_cash/src/permissions/request_permission_screen/request_permission_screen_configuration.dart';
import 'package:atoma_cash/src/push_notifications/push_notification_service.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/utils/platform/platform_helper.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:notification_permissions/notification_permissions.dart';

abstract class IPushNotificationPermissionStatusHandler {
  void handlePushNotificationPermissionStatus(
      BuildContext context, PermissionStatus status);
}

class PushNotificationPermissionStatusHandler
    implements IPushNotificationPermissionStatusHandler {
  void handlePushNotificationPermissionStatus(
      BuildContext context, PermissionStatus status) {
    if (PlatformHelper.isIOS(context)) {
      switch (status) {
        case PermissionStatus.unknown:
          _showPushNotificationRequestScreen(context);
          break;
        case PermissionStatus.granted:
          _subscribeToPushNotifications();
          break;
        default:
          break;
      }
    } else {
      _subscribeToPushNotifications();
    }
  }

  void _showPushNotificationRequestScreen(BuildContext context) {
    ExtendedNavigator.ofRouter<Router>().pushNamed(
      Routes.requestPermissionScreen,
      arguments: RequestPermissionScreenArguments(
        configuration: RequestPushNotificationPermissionsConfiguration(
            _subscribeToPushNotifications),
      ),
    );
  }

  Future<void> _subscribeToPushNotifications() async {
    return PushNotificationService().subscribe();
  }
}
