import 'package:atoma_cash/models/api/api/fcm_registration/request/fcm_token_model.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';

abstract class IPushNotificationsClient {
  Future<void> register(String token);

  Future<void> unregister();
}

class PushNotificationsClient implements IPushNotificationsClient {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  String _token;

  @override
  Future<void> register(String token) async {
    assert(token != null, 'FCM token must not be null');

    await unregister();

    _token = token;

    await _apiRemoteRepository
        .registerFcmToken(FcmTokenModel(registrationToken: token));
  }

  @override
  Future<void> unregister() async {
    if (_token != null) {
      await _apiRemoteRepository
          .unregisterFcmToken(FcmTokenModel(registrationToken: _token));
      _token = null;
    }
  }
}
