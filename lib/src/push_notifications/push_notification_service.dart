import 'dart:async';
import 'dart:developer';

import 'package:atoma_cash/src/push_notifications/push_notifications_client.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:notification_permissions/notification_permissions.dart';

abstract class IPushNotificationService {
  Future<PermissionStatus> checkServiceStatus();

  void configure();

  void subscribe();

  void unsubscribe();
}

class PushNotificationService implements IPushNotificationService {
  static final PushNotificationService _instance =
      PushNotificationService._internal();

  final _firebaseMessaging = FirebaseMessaging();
  IPushNotificationsClient _client;

  PushNotificationService._internal();

  factory PushNotificationService({IPushNotificationsClient client}) {
    _instance._client = client ?? _instance._client;
    return _instance;
  }

  void configure() {
    _firebaseMessaging.configure(
      onMessage: _onMessageHandler,
    );
  }

  Future<PermissionStatus> checkServiceStatus() {
    return NotificationPermissions.getNotificationPermissionStatus();//Permission.notification.status;
  }

  Future<void> subscribe() async {
    await _firebaseMessaging.requestNotificationPermissions();
    _firebaseMessaging
        .getToken()
        .then(_register)
        .catchError((error) => log('error $error'));
  }



  void unsubscribe() {
    _client.unregister();
  }

  void _register(String token) {
    if (token != null) {
      _client?.register(token);
    }
  }

  Future<dynamic> _onMessageHandler(Map<String, dynamic> message) async {}
}
