import 'package:atoma_cash/src/force_update/force_update_dialog.dart';
import 'package:flutter/material.dart';

class ForceUpdateScreen extends StatelessWidget {
  Future<bool> _willPopCallback() async {
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Container(
        color: Colors.black54,
        child: ForceUpdateDialog(),
      ),
    );
  }
}
