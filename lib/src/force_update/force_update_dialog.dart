import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/activity/widgets/dialogs/option_cell.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:atoma_cash/utils/platform/platform_helper.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';

class ForceUpdateDialog extends StatelessWidget {
  static const _kIOSAppId = "1489175523";
  static const _kAndroidPackage = "at.atoma.vlinder";

  static const _kIOSStoreName = "App Store";
  static const _kAndroidStoreName = "Google play";

  static const kBorderRadius = 20.0;
  static const RoundedRectangleBorder border = const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(kBorderRadius)));

  const ForceUpdateDialog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: Dimensions.alertDialogPadding.copyWith(bottom: 16.0),
      shape: border,
      title: _dialogTitleSection(context),
      content: Container(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: _receiptOptionsBuilder(context),
      )),
    );
  }

  Widget _dialogTitleSection(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          Localization.of(context).forceUpdateTitle,
          style: TextStyles.alertDialogTitleStyle,
        ),
        Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: Text(
            Localization.of(context)
                .forceUpdateDescription(_storePlatformName(context)),
            style: TextStyles.alertDialogSubTitleStyle,
          ),
        ),
      ],
    );
  }

  List<Widget> _receiptOptionsBuilder(BuildContext context) {
    return <Widget>[
      OptionsCell(Localization.of(context).updateNowButton,
          onClick: _openAppStore,
          style: FourRowListTile.titleTextStyle.copyWith(color: Palette.blue)),
    ];
  }

  String _storePlatformName(BuildContext context) {
    if (PlatformHelper.isIOS(context)) {
      return _kIOSStoreName;
    } else {
      return _kAndroidStoreName;
    }
  }

  void _openAppStore() {
    LaunchReview.launch(
        androidAppId: _kAndroidPackage,
        iOSAppId: _kIOSAppId,
        writeReview: false);
  }
}
