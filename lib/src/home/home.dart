import 'package:atoma_cash/resources/config/navigation/navigation.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/home/widgets/home_concept_screen.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class OldHome extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<OldHome> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Palette.paleGrey,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Palette.paleGrey,
        actions: <Widget>[],
      ),
      body: ListView(
        children: <Widget>[_homeThumb()],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget _homeThumb() {
    return Column(
      children: <Widget>[
        Text("It's home"),
        Widgets.styledFlatButton(
            text: "Load accounts", onPressed: _loadAccounts),
        Widgets.styledFlatButton(
            text: "Sign out", onPressed: Navigation.signOut),
      ],
    );
  }

  void _loadAccounts() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => HomeConceptScreen()));
  }
}
