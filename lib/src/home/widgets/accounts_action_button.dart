import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class AccountsActionButton extends StatelessWidget {
  static const buttonHeight = 65.0;

  final String asset;
  final String title;
  final VoidCallback onClick;

  const AccountsActionButton({Key key, @required this.asset, @required this.title, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Container(
        margin: EdgeInsets.only(left: 8.0, right: 8.0),
        height: buttonHeight,
        child: Center(
          child: InkWell(
            onTap: onClick,
            child: ListTile(
              leading: ClipOval(
                child: Material(
                  color: Palette.paleGrey, // button color
                  child: SizedBox(
                      width: 40, height: 40, child: Image.asset(asset)),
                ),
              ),
              title: Text(
                title,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Palette.blue,
                    fontSize: 14.0),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
