import 'package:atoma_cash/models/api/api/categories/goals.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/categories/list/categories_base_item.dart';
import 'package:flutter/material.dart';

class GoalsSummaryCell extends StatelessWidget {
  static const double height = 72.0;

  final String title;
  final String asset;
  final Goals goals;
  final VoidCallback onClick;

  const GoalsSummaryCell(
      {Key key,
      @required this.title,
      @required this.asset,
      @required this.goals,
      this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CategoriesBaseItem(
      leading: Image.asset(asset),
      title: title,
      trailing: _percentageString(context, goals.percentCompleted),
      subTrailing: _goalsCountLocalizedString(context, goals.goalsCount),
    );
  }

  String _percentageString(BuildContext context, double percentage) =>
      "$percentage%";

  String _goalsCountLocalizedString(BuildContext context, int count) =>
      Localization.of(context).legacyGoalsCount;
}
