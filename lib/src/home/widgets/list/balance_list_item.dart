import 'package:atoma_cash/extensions/double_extensions.dart';
import 'package:atoma_cash/models/api/api/balances/balances.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class BalanceListItem extends StatelessWidget {
  static const double _defaultHeight = 92;
  static const double _cryptoBalanceHeight = 72;

  final Balance balance;

  const BalanceListItem(this.balance, {Key key}) : super(key: key);

  String _formattedAccountPublicId(String publicId) {
    String placeholder = "••••";
    return publicId != null && publicId.length > 4
        ? "$placeholder ${balance.accountPublicId.substring(
        balance.accountPublicId.length - 4)}"
        : placeholder;
  }

  static double heightForBalance(bool isCryptoBalance) {
    return isCryptoBalance ? _cryptoBalanceHeight : _defaultHeight;
  }

  String _formattedRelevantAt(BuildContext context, DateTime dateTime) =>
      "${Localization
          .of(context)
          .legacyUpdatedOn} ${DateFormat("MMM d, HH:mm").format(dateTime)}";

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 6.0, right: 6.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 60.0,
            child: ListTile(
              leading: Image.network(
                balance.accountIconUrl,
                width: 40.0,
                height: 40.0,
              ),
              title: Text(
                balance.accountName,
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                    fontSize: 14.0),
              ),
              subtitle: Text(
                _formattedAccountPublicId(balance.accountPublicId),
                maxLines: 1,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Palette.mediumGrey,
                    fontSize: 13.0),
              ),
              trailing: Text(
                balance.value.currencyFormatted(balance.symbol),
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    color: Colors.black,
                    fontSize: 14.0),
              ),
            ),
          ),
          if (!balance.isCryptoBalance)
            Padding(
              padding: const EdgeInsets.only(left: 72.0),
              child: Text(
                _formattedRelevantAt(context, balance.relevantAt),
                maxLines: 1,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    color: Palette.mediumGrey,
                    fontSize: 13.0),
              ),
            ),
        ],
      ),
    );
  }
}
