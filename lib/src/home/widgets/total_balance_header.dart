import 'package:atoma_cash/extensions/double_extensions.dart';
import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/balances/balances.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/material.dart';

class TotalBalanceHeader extends StatelessWidget {
  final Amount totalBalance;

  const TotalBalanceHeader({Key key, @required this.totalBalance})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 145.0,
        child: Column(
          children: <Widget>[
            Text(
              Localization.of(context).legacyTotalBalance,
              style: TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Text(
                totalBalance.value.currencyFormatted(totalBalance.symbol),
                style: TextStyle(
                    fontSize: 36.0,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
            ),
          ],
        ));
  }
}
