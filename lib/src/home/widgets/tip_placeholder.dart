import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class TipPlaceholder extends StatelessWidget {
  final String title;
  final String subtitle;
  final String iconAsset;
  final VoidCallback onClick;

  const TipPlaceholder(
      {Key key,
      @required this.title,
      @required this.subtitle,
      @required this.iconAsset,
      this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 24.0, right: 24.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: Colors.white,
      ),
      child: InkWell(
        onTap: onClick,
        child: Padding(
          padding: const EdgeInsets.only(
              top: 12.0, bottom: 12.0, left: 24.0, right: 24.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                flex: 7,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      title,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Colors.black,
                          fontSize: 16.0),
                    ),
                    Text(
                      subtitle,
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          color: Palette.mediumGrey,
                          fontSize: 12.0),
                    ),
                  ],
                ),
              ),
              Flexible(
                  flex: 3,
                  child: Image.asset(
                    iconAsset,
                    height: 48.0,
                    width: 48.0,
                    fit: BoxFit.fill,
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
