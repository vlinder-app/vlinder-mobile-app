import 'dart:ui';

import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class DoubleTabIndicator extends StatelessWidget {
  final TabController tabController;
  final List<String> tabTitles;

  const DoubleTabIndicator(
    this.tabController, {
    Key key,
    this.tabTitles,
  })  : assert(tabTitles.length == tabController.length),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = CurvedAnimation(
      parent: tabController.animation,
      curve: Curves.fastOutSlowIn,
    );
    return AnimatedBuilder(
        animation: animation,
        builder: (BuildContext context, Widget child) {
          return Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                color: Palette.paleGrey.withAlpha(100)),
            height: 40.0,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: _indicatorList(context),
            ),
          );
        });
  }

  void _onTabSelected(int index) {
    if (tabController.index != index) {
      tabController.index = index;
    }
  }

  BoxShadow _tabBoxShadow() {
    return BoxShadow(
      offset: Offset(0.0, 3.0),
      color: Colors.grey.withAlpha(70),
      blurRadius: 5.0,
    );
  }

  Widget _tab(BuildContext context, int index) {
    return Flexible(
      flex: 3,
      child: Container(
        padding: EdgeInsets.only(left: 12.0, right: 12.0),
        decoration: BoxDecoration(
            boxShadow: index == tabController.index ? [_tabBoxShadow()] : [],
            borderRadius: BorderRadius.circular(10.0),
            color: index == tabController.index
                ? Colors.white
                : Colors.transparent),
        child: InkWell(
          onTap: () => _onTabSelected(index),
          child: Center(
            child: Text(
              tabTitles[index],
              style: TextStyle(
                  color: index == tabController.index
                      ? Palette.blue
                      : Palette.mediumGrey,
                  fontSize: 14.0,
                  letterSpacing: 0.3,
                  fontWeight: FontWeight.w600),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> _indicatorList(BuildContext context) {
    return List.generate(tabController.length, (index) {
      return _tab(context, index);
    });
  }
}
