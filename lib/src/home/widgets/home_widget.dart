import 'package:atoma_cash/models/api/api/balances/balances.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/categories/period_stats/savings_cell.dart';
import 'package:atoma_cash/src/home/bloc/home_state.dart';
import 'package:atoma_cash/src/home/widgets/header_widget.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:atoma_cash/utils/ui/custom_reorderable_list.dart';
import 'package:flutter/material.dart';

import 'accounts_action_button.dart';
import 'balances_sliver_bar.dart';
import 'goals_section.dart';
import 'list/balance_list_item.dart';
import 'list/snapping_scroll_physics.dart';

const Duration _kScrollDuration = Duration(milliseconds: 400);
const Curve _kScrollCurve = Curves.fastOutSlowIn;
const double _midScrollOffset = 223.0;

abstract class IHomeListener extends IHeaderListener {
  void onBalancesReorder(int start, int end);
}

class HomeWidget extends StatefulWidget {
  final Loaded state;
  final IHomeListener listener;

  const HomeWidget({Key key, @required this.state, @required this.listener})
      : super(key: key);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  static const _reorderableBottomOffset = 10.0;
  static const _headerSectionHeight = 170.0;
  static const _pinnedHeaderMaxExtent = 145.0;

  ScrollController _scrollController = ScrollController();
  AnimationController _balancesAnimationController;
  SnappingScrollPhysics _scrollPhysics =
      SnappingScrollPhysics(midScrollOffset: _midScrollOffset);

  bool _expanded = true;

  @override
  void initState() {
    _balancesAnimationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 150));
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _balancesAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Palette.blue,
      body: Container(
        child: SafeArea(
          bottom: false,
          child: CustomScrollView(
            physics: _scrollPhysics,
            controller: _scrollController,
            slivers: <Widget>[
              HeaderWidget(
                state: widget.state,
                headerListener: widget.listener,
              ),
              SliverPersistentHeader(
                delegate: BalancesSliverBar(
                  _balancesHeaderHeight(widget.state),
                  widget.state.balances.balances.isEmpty
                      ? null
                      : widget.state.balances.totalBaseCurrencyBalance,
                  _balancesAnimationController,
                  onToggleClick: _toggleHeader,
                ),
                pinned: true,
              ),
              _balancesBodyWidget(context, widget.state),
              SliverToBoxAdapter(
                child: Container(
                  color: Colors.white,
                  height: _bottomSpacer(context, widget.state),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _toggleHeader() {
    double scrollPosition =
        _scrollController.offset >= _midScrollOffset ? 0.0 : _midScrollOffset;
    _scrollController.animateTo(scrollPosition,
        curve: _kScrollCurve, duration: _kScrollDuration);
  }

  Widget _bottomButtons() {
    return Column(
      children: <Widget>[
        AccountsActionButton(
            title: Localization.of(context).legacyAddOrRemoveBankAccount,
            asset: Assets.manageBankAccounts,
            onClick: null),
        AccountsActionButton(
            title: Localization.of(context).legacyAddCryptoWallet,
            asset: Assets.addCryptoWallet,
            onClick: () => {
                  _scrollController.animateTo(0.0,
                      curve: _kScrollCurve, duration: _kScrollDuration)
                }),
      ],
    );
  }

  Widget _reorderableListView(Balances balances) {
    return PReorderableListView(
      header: null,
      onReorder: (start, end) {
        if (end > start) {
          end -= 1;
        }
        widget.listener.onBalancesReorder(start, end);
      },
      children: _balancesList(balances.balances),
    );
  }

  List<Widget> _balancesList(List<Balance> balances) {
    return List.generate(balances.length, (int index) {
      return Container(
        width: double.maxFinite,
        key: Key("key$index"),
        height:
            BalanceListItem.heightForBalance(balances[index].isCryptoBalance),
        child: BalanceListItem(balances[index]),
      );
    });
  }

  Widget _balancesBodyWidget(BuildContext context, Loaded loadedState) {
    Balances balances = loadedState.balances;
    if (!loadedState.balancesLoading) {
      return SliverList(
        delegate: SliverChildListDelegate(
          <Widget>[
            Container(
                color: Colors.white,
                height: _balancesListSummaryHeight(balances.balances),
                child: _reorderableListView(balances)),
            SavingsCell(
              savings: loadedState.savings,
            ),
            if (balances.balances.isNotEmpty)
              Container(
                color: Colors.white,
                child: GoalsSection(
                  goals: loadedState.goals,
                ),
              ),
            Container(
              child: _bottomButtons(),
            ),
            Container(
              color: Colors.white,
              height: 10.0,
            )
          ],
        ),
      );
    } else {
      return SliverToBoxAdapter(
        child: Container(
          height: 200.0,
          color: Colors.white,
          child: Center(
            child: CircularProgressIndicator(),
          ),
        ),
      );
    }
  }

  void _onNestedScroll() {
    bool changed = _scrollController.offset >= (_midScrollOffset - 50);
    if (changed != _expanded) {
      _expanded = !_expanded;
      _balancesAnimationController.animateTo(
        _balancesControllerPosition(_expanded),
      );
    }
  }

  double _balancesControllerPosition(bool expanded) {
    return expanded ? 0.0 : 1.0;
  }

  double _balancesListSummaryHeight(List<Balance> balances) {
    double summaryHeight = _reorderableBottomOffset;
    balances.forEach((b) {
      summaryHeight += BalanceListItem.heightForBalance(b.isCryptoBalance);
    });
    return summaryHeight;
  }

  double _goalsHeight(Loaded state) =>
      state.balances.balances.isNotEmpty ? GoalsSection.height : 0.0;

  double _bottomSpacer(BuildContext context, Loaded state) {
    var safePadding = MediaQuery.of(context).padding.top;

    final screenHeight = MediaQuery.of(context).size.height -
        safePadding -
        kBottomNavigationBarHeight;

    double headerHeight = _headerSectionHeight + kToolbarHeight;
    double contentHeight = _balancesListSummaryHeight(state.balances.balances) +
        _balancesHeaderHeight(state) +
        SavingsCell.height +
        AccountsActionButton.buttonHeight * 2 +
        UiHelper.topSpacerHeight(context) +
        _goalsHeight(state);

    double summaryHeight = headerHeight + contentHeight;

    if (contentHeight < headerHeight) {
      return screenHeight - headerHeight - contentHeight;
    } else {
      double visibleContentHeight = screenHeight - headerHeight;
      double notVisibleContentHeight = contentHeight - visibleContentHeight;
      if (notVisibleContentHeight < headerHeight) {
        return headerHeight - notVisibleContentHeight;
      }
    }
    return 0.0;
  }

  double _balancesHeaderHeight(Loaded loadedState) =>
      loadedState.balances.balances.isEmpty
          ? BalancesSliverBar.placeholderHeight
          : _pinnedHeaderMaxExtent;

  @override
  bool get wantKeepAlive => true;
}
