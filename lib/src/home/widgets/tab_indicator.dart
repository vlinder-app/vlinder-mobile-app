import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class TabIndicator extends StatelessWidget {
  final Color normalColor;
  final Color selectedColor;
  final TabController tabController;

  const TabIndicator(this.tabController,
      {Key key,
      this.normalColor = Palette.paleGrey,
      this.selectedColor = Palette.blue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = CurvedAnimation(
      parent: tabController.animation,
      curve: Curves.fastOutSlowIn,
    );
    return AnimatedBuilder(
        animation: animation,
        builder: (BuildContext context, Widget child) {
          return Container(
            height: 15.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: _indicatorList(context),
            ),
          );
        });
  }

  List<Widget> _indicatorList(BuildContext context) {
    return List.generate(tabController.length, (index) {
      return Padding(
          padding: EdgeInsets.all(5.0),
          child: Image.asset(
            index == tabController.index
                ? Assets.viewPagerSelected
                : Assets.viewPagerNormal,
            color: index == tabController.index ? selectedColor : normalColor,
          ));
    });
  }
}
