import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint.dart';
import 'package:atoma_cash/models/api/api/categories/period.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/navigation/navigation.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/categories/helpers/period_localization.dart';
import 'package:atoma_cash/src/home/bloc/home_state.dart';
import 'package:atoma_cash/src/home/widgets/tab_indicator.dart';
import 'package:atoma_cash/src/home/widgets/tip_model.dart';
import 'package:atoma_cash/src/home/widgets/tip_placeholder.dart';
import 'package:atoma_cash/src/home/widgets/tip_sample.dart';
import 'package:atoma_cash/src/home/widgets/tips_provider.dart';
import 'package:atoma_cash/src/widgets/rounded_icon.dart';
import 'package:flutter/material.dart';

abstract class IHeaderListener {
  void onSyncClick();

  void onManageAccountClick();

  void onReloadClick();
}

class HeaderWidget extends StatefulWidget {
  final Loaded state;
  final IHeaderListener headerListener;

  const HeaderWidget({
    Key key,
    @required this.state,
    @required this.headerListener,
  }) : super(key: key);

  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  static const _headerSectionHeight = 170.0;
  static const _carbonTipImageSize = 40.0;
  TabController _tipsTabController;

  @override
  void dispose() {
    _tipsTabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return SliverAppBar(
      leading: Container(
          child: InkWell(
            child: Image.asset(Assets.atomaLogoWhite),
            onTap: _signOut,
          ),
          transform: Matrix4.translationValues(5.0, 0.0, 0.0)),
      backgroundColor: Palette.blue,
      title: Container(
        transform: Matrix4.translationValues(-15.0, 0.0, 0.0),
        margin: EdgeInsets.only(
          left: 8.0,
          right: 8.0,
        ),
        child: Text(
          Localization.of(context).vlinderAppTitle,
          style: TextStyle(
              fontSize: 15.0, fontWeight: FontWeight.w500, color: Colors.white),
        ),
      ),
      centerTitle: false,
      bottom: PreferredSize(
        preferredSize: Size.fromHeight(_headerSectionHeight),
        child: _tipsWidget(context, widget.state),
      ),
      actions: _appBarActions(context),
    );
  }

  Widget _tipsWidget(BuildContext context, Loaded state) {
    List<Widget> tipWidgets;
    if (state.balances.balances.isNotEmpty) {
      /*
       * Not used because of carbon placeholder
       * 
      return TotalBalanceHeader(
        totalBalance: state.balances.totalBaseCurrencyBalance,
      );
       */
      tipWidgets = _carbonTips(context, state.ecoFootprint);
    } else {
      tipWidgets = _placeholderTips(context);
    }
    return _tipsPlaceholders(tipWidgets);
  }

  List<Widget> _placeholderTips(BuildContext context) {
    List<TipModel> tips = TipsProvider.placeHolderTips(
        context, widget.headerListener.onManageAccountClick, null);
    return List.generate(tips.length, (index) {
      TipModel tip = tips[index];
      return TipPlaceholder(
          title: tip.title,
          subtitle: tip.subtitle,
          iconAsset: tip.iconAsset,
          onClick: tip.onClick);
    });
  }

  List<Widget> _carbonTips(BuildContext context, EcoFootprint ecoFootprint) {
    return [
      Tip(
        title: Localization
            .of(context)
            .legacyGreenScoreTitle,
        subtitle:
        "${Localization
            .of(context)
            .legacyCarbonFootprint} ${Localization
            .of(context)
            .legacyLastPeriod
            .toLowerCase()} ${PeriodLocalization.localizationForPeriodType(
            context, PeriodType.MONTH)}",
        bottom: "CO2",
        value: "${ecoFootprint.carbonFootprint.toInt()} kg",
        image: RoundedIcon(
          asset: Assets.cloudIcon,
          iconColor: Palette.green,
          size: _carbonTipImageSize,
        ),
        onClick: () => {},
      )
    ];
  }

  Widget _tipsPlaceholders(List<Widget> tips) {
    _tipsTabController = TabController(length: tips.length, vsync: this);
    return Column(
      children: <Widget>[
        Container(
          height: 120.0,
          margin: EdgeInsets.only(bottom: 12.0, top: 16.0),
          child: TabBarView(controller: _tipsTabController, children: tips),
        ),
        Padding(
          padding: const EdgeInsets.only(bottom: 12.0),
          child: TabIndicator(
            _tipsTabController,
            selectedColor: Colors.white,
            normalColor: Colors.white.withAlpha(100),
          ),
        )
      ],
    );
  }

  List<Widget> _appBarActions(BuildContext context) {
    return [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: IconButton(
          icon: Image.asset(Assets.updateIcon),
          onPressed: widget.headerListener.onSyncClick,
        ),
      ),
    ];
  }

  void _signOut() {
    Navigation.signOut();
  }

  @override
  bool get wantKeepAlive => true;
}
