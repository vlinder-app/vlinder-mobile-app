import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/material.dart';

class TripleButton extends StatelessWidget {
  final void Function() onSendClick;
  final void Function() onRequestClick;
  final void Function() onQrClick;

  const TripleButton({Key key, @required this.onSendClick, @required this.onRequestClick, @required this.onQrClick}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              offset: Offset(0.0, 3.0),
              color: Colors.grey.withAlpha(70),
              blurRadius: 5.0,
            ),
          ]),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          FlatButton.icon(
              onPressed: onSendClick,
              icon: Image.asset(Assets.sendIcon),
              label: Text(
                Localization.of(context).legacySendButton,
                style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.w600,
                    color: Palette.blue),
              )),
          _divider(),
          FlatButton.icon(
              onPressed: onRequestClick,
              icon: Image.asset(Assets.requestIcon),
              label: Text(
                Localization.of(context).legacyRequestButton,
                style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.w600,
                    color: Palette.red),
              )),
          _divider(),
          IconButton(
            icon: Image.asset(Assets.qrIcon),
            onPressed: onQrClick,
          )
        ],
      ),
    );
  }

  Widget _divider() {
    return Padding(
      padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
      child: Container(width: 1.0, color: Palette.paleGrey,),
    );
  }
}
