import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/resources/config/navigation/navigation.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/bank/webview/bank_webview.dart';
import 'package:atoma_cash/src/bank/webview/webview_result/webview_result.dart';
import 'package:atoma_cash/src/home/bloc/bloc.dart';
import 'package:atoma_cash/src/widgets/data_load_failed.dart';
import 'package:atoma_cash/utils/ui/custom_routes.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'home_widget.dart';

class HomeConceptScreen extends StatefulWidget {
  @override
  _HomeConceptScreenState createState() => _HomeConceptScreenState();
}

class _HomeConceptScreenState extends State<HomeConceptScreen>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin
    implements IHomeListener {
  HomeBloc _homeBloc;
  ScrollController _scrollController = ScrollController();
  TabController _tipsTabController;
  AnimationController _balancesAnimationController;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    _homeBloc = HomeBloc()..add(Started());
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _tipsTabController.dispose();
    _balancesAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: BlocListener(
        bloc: _homeBloc,
        listener: _blocListener,
        child: BlocBuilder(
          bloc: _homeBloc,
          builder: _rootWidgetForState,
        ),
      ),
    );
  }

  void _blocListener(BuildContext context, HomeState state) {
    /*
    if (state is Loaded) {
      _balancesAnimationController.animateTo(
          _balancesControllerPosition(state.balances.balances.isEmpty),
          duration: Duration(microseconds: 0));
    }
     */
    if (state is ErrorState) {
      if (state.balancesSwapError) {
        Widgets.failedSnackBar(
            context, Localization.of(context).legacyCantSwapAccounts,
            scaffoldState: _scaffoldKey.currentState);
      }
      if (state.syncError) {
        Widgets.failedSnackBar(
            context, Localization
            .of(context)
            .legacyCantUpdateAccounts,
            scaffoldState: _scaffoldKey.currentState);
      }
      if (state.cantManageAccountsError) {
        Widgets.failedSnackBar(
            context, Localization
            .of(context)
            .legacyCantConnectToTheBanks,
            scaffoldState: _scaffoldKey.currentState);
      }
    }
    if (state is UserInteractionNeeded) {
      _openBankWebViewForUri(state.uriModel);
    }
  }

  Widget _loadingWidget() {
    return Scaffold(body: Center(child: CircularProgressIndicator()));
  }

  Widget _rootWidgetForState(BuildContext context, HomeState state) {
    if (state is Loading) {
      return _loadingWidget();
    }
    if (state is Loaded) {
      return HomeWidget(
        state: state,
        listener: this,
      );
    }
    if (state is DataLoadFailed) {
      return _dataLoadFailedWidget(context);
    }
    return _loadingWidget();
  }

  Widget _dataLoadFailedWidget(BuildContext context) {
    return DataLoadFailedWidget(
      onReloadClick: onReloadClick,
      onSignOutClick: _signOut,
    );
  }

  void _openBankWebViewForUri(UriModel uriModel) async {
    WebViewResult result = await Navigator.push(
        context,
        NoAnimationRoute<WebViewResult>(
            child: BankWebViewScreen(uriModel), fullscreenDialog: true));
    if (result is SuccessResult) {
      onReloadClick();
    }
    if (result is FailedResult) {}
  }

  void _signOut() {
    Navigation.signOut();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void onBalancesReorder(int start, int end) {
    _homeBloc.add(BalancesReordered(start, end));
  }

  @override
  void onManageAccountClick() {
    _homeBloc.add(ManageAccountsEvent());
  }

  @override
  void onReloadClick() {
    _homeBloc.add(Started());
  }

  @override
  void onSyncClick() {
    _homeBloc.add(SyncEvent());
  }
}
