import 'package:atoma_cash/models/api/api/categories/summary_goals.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/home/widgets/list/goals_summary_cell.dart';
import 'package:flutter/material.dart';

class GoalsSection extends StatelessWidget {
  static const double _topPadding = 16.0;
  static const double _bottomPadding = 24.0;

  final SummaryGoals goals;

  const GoalsSection({Key key, this.goals}) : super(key: key);

  static double get height =>
      GoalsSummaryCell.height * 2 + _topPadding + _bottomPadding;

  @override
  Widget build(BuildContext context) {
    if (goals != null) {
      return Padding(
        padding: EdgeInsets.only(top: _topPadding, bottom: _bottomPadding),
        child: Column(
          children: <Widget>[
            GoalsSummaryCell(
              title: Localization
                  .of(context)
                  .legacyCommunityGoals,
              asset: Assets.communityGoalsIcon,
              goals: goals.communityGoals,
            ),
            GoalsSummaryCell(
              title: Localization
                  .of(context)
                  .legacyPersonalGoals,
              asset: Assets.goalsIcon,
              goals: goals.personalGoals,
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
