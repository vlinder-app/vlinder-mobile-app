import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class Tip extends StatelessWidget {
  final String title;
  final String subtitle;
  final String value;
  final String bottom;
  final Widget image;
  final VoidCallback onClick;

  const Tip({Key key,
    @required this.title,
    @required this.subtitle,
    @required this.value,
    @required this.bottom,
    @required this.image,
    this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 24.0, right: 24.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: Colors.white,
      ),
      child: Material(color: Colors.transparent,
        child: InkWell(
          onTap: onClick,
          child: Padding(
            padding: const EdgeInsets.only(
                top: 8.0, bottom: 8.0, left: 24.0, right: 24.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text(
                          title,
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Colors.black,
                              fontSize: 16.0),
                        ),
                        Text(
                          value,
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              color: Palette.blue,
                              fontSize: 16.0),
                        ),
                      ],
                    ),
                    Text(
                      subtitle,
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          color: Palette.mediumGrey,
                          fontSize: 13.0),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    image,
                    Padding(
                      padding: const EdgeInsets.only(left: 10.0),
                      child: Text(
                        bottom,
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 14.0),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
