import 'package:atoma_cash/extensions/double_extensions.dart';
import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/home/widgets/triple_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BalancesSliverBar extends SliverPersistentHeaderDelegate {
  static const double placeholderHeight = 120;

  final double height;
  final Amount balance;
  final VoidCallback onToggleClick;
  final AnimationController animationController;

  BalancesSliverBar(this.height, this.balance, this.animationController,
      {this.onToggleClick});

  @override
  Widget build(BuildContext context, double shrinkOffset,
      bool overlapsContent) {
    return _headerWidget(context);
  }

  @override
  double get maxExtent {
    return height;
  }

  @override
  double get minExtent {
    return height;
  }

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  Widget _rootWidget(BuildContext context) {
    if (balance == null) {
      return _placeholder(context);
    } else {
      return _balancesWidget(context);
    }
  }

  Widget _toggleButton(BuildContext context) {
    return InkWell(
      onTap: onToggleClick,
      child: Container(
        padding: EdgeInsets.only(bottom: 20.0),
        height: 26.0,
        child: Center(
          child: Image.asset(Assets.expandIcon),
        ),
      ),
    );
  }

  Widget _placeholder(BuildContext context) {
    return Column(
      children: <Widget>[
        _toggleButton(context),
        Expanded(
          child: Center(
              child: Text(Localization
                  .of(context)
                  .legacyAccountsPlaceholderTitle,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Palette.mediumGrey,
                      fontWeight: FontWeight.w600,
                      fontSize: 14.0))),
        ),
      ],
    );
  }

  Widget _balancesWidget(BuildContext context) {
    var colorTween =
    ColorTween(begin: Palette.blue, end: Colors.transparent)
        .animate(animationController);
    return Column(
      children: <Widget>[
        _toggleButton(context),
        AnimatedBuilder(
          animation: animationController,
          builder: (context, child) =>
              Container(
                height: 30.0,
                child: Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                  Text(
                    Localization.of(context).legacyBalancesHeader,
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Colors.black,
                        fontSize: 20.0),
                  ),
                  Text(
                    balance.value.currencyFormatted(balance.symbol),
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
//                            color: colorTween.value,
                        color: Palette.blue,
                        fontSize: 15.0),
                  ),
                ],
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10.0),
          child: TripleButton(),
        ),
      ],
    );
  }

  Widget _headerWidget(BuildContext context) {
    return Container(
      color: Palette.blue,
      child: Container(
        padding: EdgeInsets.only(top: 16.0, left: 24.0, right: 24.0),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(15.0),
                topRight: Radius.circular(15.0))),
        height: height,
        child: _rootWidget(context),
      ),
    );
  }
}
