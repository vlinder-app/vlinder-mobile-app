import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/home/widgets/tip_model.dart';
import 'package:flutter/material.dart';

class TipsProvider {
  static List<TipModel> placeHolderTips(BuildContext context,
      VoidCallback addBankAccountClick, VoidCallback addCryptoWalletClick) {
    return [
      TipModel(
          Localization.of(context).legacyTipBankTitle,
          Localization.of(context).legacyTipBankSubtitle,
          Assets.bankAccountIcon,
          addBankAccountClick),
      TipModel(
          Localization
              .of(context)
              .legacyTipCryptoTitle,
          Localization
              .of(context)
              .legacyTipCryptoSubtitle,
          Assets.cryptoIcon,
          addCryptoWalletClick),
    ];
  }
}
