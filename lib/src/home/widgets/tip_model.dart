import 'package:flutter/material.dart';

class TipModel {
  String title;
  String subtitle;
  String iconAsset;
  VoidCallback onClick;

  TipModel(this.title, this.subtitle, this.iconAsset, this.onClick);

  @override
  String toString() {
    return 'TipModel{title: $title, subtitle: $subtitle, iconAsset: $iconAsset, onClick: $onClick}';
  }
}
