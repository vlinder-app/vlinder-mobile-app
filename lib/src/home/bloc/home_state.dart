import 'package:atoma_cash/models/api/api/balances/balances.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint.dart';
import 'package:atoma_cash/models/api/api/categories/savings.dart';
import 'package:atoma_cash/models/api/api/categories/summary_goals.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class HomeState extends Equatable {
  const HomeState();
}

class Loading extends HomeState {
  @override
  List<Object> get props => [];
}

class Loaded extends HomeState {
  final Balances balances;
  final bool balancesLoading;
  final SummaryGoals goals;
  final EcoFootprint ecoFootprint;
  final Savings savings;

  Loaded({this.balances,
    this.savings,
    this.balancesLoading = false,
    this.goals,
    this.ecoFootprint});

  @override
  List<Object> get props =>
      [balances, balancesLoading, goals, ecoFootprint, savings];

  Loaded copyWith({bool balancesLoading,
    SummaryGoals goals,
    Balances balances,
    Savings savings,
    EcoFootprint ecoFootprint}) =>
      Loaded(
          savings: savings ?? this.savings,
          goals: goals ?? this.goals,
          balances: this.balances,
          ecoFootprint: ecoFootprint ?? this.ecoFootprint,
          balancesLoading: balancesLoading ?? this.balancesLoading);
}

class UserInteractionNeeded extends HomeState {
  final UriModel uriModel;

  UserInteractionNeeded(this.uriModel);

  @override
  List<Object> get props => [uriModel];

  @override
  String toString() {
    return 'UserInteractionNeeded{uriModel: $uriModel}';
  }
}

class DataLoadFailed extends HomeState {
  @override
  List<Object> get props => null;
}

class ErrorState extends HomeState {
  final bool balancesSwapError;
  final bool syncError;
  final bool cantManageAccountsError;

  ErrorState({this.balancesSwapError = false,
    this.syncError = false,
    this.cantManageAccountsError = false});

  @override
  List<Object> get props =>
      [balancesSwapError, syncError, cantManageAccountsError];

  @override
  String toString() {
    return 'ErrorState{balancesSwapError: $balancesSwapError, syncError: $syncError, cantManageAccountsError: $cantManageAccountsError}';
  }
}
