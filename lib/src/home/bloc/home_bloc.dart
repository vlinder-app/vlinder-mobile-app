import 'dart:async';

import 'package:atoma_cash/models/api/api/balances/balances.dart';
import 'package:atoma_cash/models/api/api/balances/sync_status.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint.dart';
import 'package:atoma_cash/models/api/api/categories/goals.dart';
import 'package:atoma_cash/models/api/api/categories/period.dart';
import 'package:atoma_cash/models/api/api/categories/savings.dart';
import 'package:atoma_cash/models/api/api/categories/summary_goals.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:bloc/bloc.dart';

import './bloc.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();

  Balances _balances;
  SummaryGoals _goals;
  EcoFootprint _ecoFootprint;
  Savings _savings;

  @override
  HomeState get initialState => Loading();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapHomeStartedToState();
    }
    if (event is BalancesReordered) {
      yield* _mapBalancesReorderedToState(event);
    }
    if (event is SyncEvent) {
      yield* _mapSyncEventToState();
    }
    if (event is ManageAccountsEvent) {
      yield* _mapAccountsEditToState();
    }
  }

  Loaded _cachedLoadedState() {
    return Loaded(
        balances: _balances,
        goals: _goals,
        ecoFootprint: _ecoFootprint,
        savings: _savings);
  }

  Stream<HomeState> _mapAccountsEditToState() async* {
    yield Loading();
    BaseResponse<UriModel> uriModelResponse =
        await _apiRemoteRepository.authorizeBankAccount();
    if (uriModelResponse.isSuccess()) {
      yield UserInteractionNeeded(uriModelResponse.result);
    } else {
      yield ErrorState(cantManageAccountsError: true);
    }
    yield _cachedLoadedState();
  }

  Stream<HomeState> _mapSyncStatusToState(String status) async* {
    if (status == SyncStatus.statusOk) {
      yield* _mapHomeStartedToState();
    } else {
      BaseResponse<UriModel> uriModelResponse;
      if (status == SyncStatus.statusSyncRequired) {
        uriModelResponse = await _apiRemoteRepository.syncBankAccounts();
      }
      if (status == SyncStatus.statusAuthRequired) {
        uriModelResponse = await _apiRemoteRepository.authorizeBankAccount();
      }
      if (uriModelResponse.isSuccess()) {
        yield UserInteractionNeeded(uriModelResponse.result);
      } else {
        yield ErrorState(syncError: true);
      }
      yield _cachedLoadedState();
    }
  }

  Stream<HomeState> _mapSyncEventToState() async* {
    yield Loading();
    BaseResponse<SyncStatus> syncBalanceResponse =
        await _apiRemoteRepository.syncBalances();
    if (syncBalanceResponse.isSuccess()) {
      String syncStatus = syncBalanceResponse.result.status;
      yield* _mapSyncStatusToState(syncStatus);
    } else {
      yield ErrorState(syncError: true);
    }
    yield _cachedLoadedState();
  }

  Stream<HomeState> _mapBalancesReorderedToState(
      BalancesReordered event) async* {
    yield _cachedLoadedState().copyWith(balancesLoading: true);
    BaseResponse<Balances> balancesResponse =
        await _apiRemoteRepository.moveBalance(event.from, event.to);
    if (balancesResponse.isSuccess()) {
      _balances = balancesResponse.result;
    } else {
      yield ErrorState(balancesSwapError: true);
    }
    yield _cachedLoadedState().copyWith(balancesLoading: false);
  }

  Stream<HomeState> _mapHomeStartedToState() async* {
    yield Loading();
    BaseResponse<Balances> balancesResponse =
        await _apiRemoteRepository.getBalances();
    BaseResponse<Goals> personalGoalsResponse =
    await _apiRemoteRepository.getPersonalGoals();
    BaseResponse<Goals> communityGoalsResponse =
    await _apiRemoteRepository.getCommunityGoals();
    BaseResponse<EcoFootprint> ecoFootprintResponse =
    await _apiRemoteRepository.getEcoFootprint(null);
    BaseResponse<Savings> savingsResponse =
    await _apiRemoteRepository.getSavings(Period.currentMonth());

    if (balancesResponse.isSuccess() &&
        personalGoalsResponse.isSuccess() &&
        communityGoalsResponse.isSuccess() &&
        ecoFootprintResponse.isSuccess() &&
        savingsResponse.isSuccess()) {
      _goals = SummaryGoals(
          personalGoals: personalGoalsResponse.result,
          communityGoals: communityGoalsResponse.result);
      _balances = balancesResponse.result;
      _ecoFootprint = ecoFootprintResponse.result;
      _savings = savingsResponse.result;
      yield Loaded(
          balances: _balances,
          goals: _goals,
          ecoFootprint: _ecoFootprint,
          savings: _savings);
    } else {
      yield DataLoadFailed();
    }
  }
}
