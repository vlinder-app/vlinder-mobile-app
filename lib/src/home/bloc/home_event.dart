import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class Started extends HomeEvent {
  @override
  List<Object> get props => null;
}

class ManageAccountsEvent extends HomeEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'AccountsEditEvent{}';
  }
}

class SyncEvent extends HomeEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'SyncEvent{}';
  }
}

class BalancesReordered extends HomeEvent {
  final int from;
  final int to;

  BalancesReordered(this.from, this.to);

  @override
  List<Object> get props => [from, to];

  @override
  String toString() {
    return 'BalancesReordered{from: $from, to: $to}';
  }
}
