import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/src/bank/web_view_user/bloc.dart';
import 'package:atoma_cash/src/bank/webview/bank_webview.dart';
import 'package:atoma_cash/src/bank/webview/webview_result/webview_result.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WebViewUserScreen extends StatefulWidget {
  @override
  _WebViewUserScreenState createState() => _WebViewUserScreenState();
}

class _WebViewUserScreenState extends State<WebViewUserScreen> {
  WebViewUserBloc _bloc;

  @override
  void initState() {
    _bloc = WebViewUserBloc()..add(Started());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: BlocListener(
        bloc: _bloc,
        listener: (BuildContext context, WebViewUserState state) {
          if (state is UriLoadedState) {
            _openWebViewForUri(context, state.uriModel);
          }
        },
        child: BlocBuilder(
          bloc: _bloc,
          builder: _builder,
        ),
      ),
    );
  }

  Widget _builder(BuildContext context, WebViewUserState state) {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  void _openWebViewForUri(BuildContext context, UriModel uriModel) async {
    WebViewResult result = await Navigator.push(
        context,
        MaterialPageRoute<WebViewResult>(
            builder: (context) => BankWebViewScreen(uriModel),
            fullscreenDialog: true));
    if (result is SuccessResult) {}
    if (result is FailedResult) {}
  }
}
