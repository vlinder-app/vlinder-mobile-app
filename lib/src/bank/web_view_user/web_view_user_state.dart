import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:equatable/equatable.dart';

abstract class WebViewUserState extends Equatable {
  const WebViewUserState();
}

class InitialState extends WebViewUserState {
  @override
  List<Object> get props => [];
}

class UriLoadedState extends WebViewUserState {
  final UriModel uriModel;

  UriLoadedState(this.uriModel);

  @override
  List<Object> get props => [uriModel];
}

class FailedUriState extends WebViewUserState {
  @override
  List<Object> get props => null;
}
