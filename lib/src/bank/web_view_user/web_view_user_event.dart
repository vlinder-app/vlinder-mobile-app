import 'package:equatable/equatable.dart';

abstract class WebViewUserEvent extends Equatable {
  const WebViewUserEvent();
}

class Started extends WebViewUserEvent {
  @override
  List<Object> get props => null;
}