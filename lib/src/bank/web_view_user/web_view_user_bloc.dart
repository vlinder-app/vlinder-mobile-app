import 'dart:async';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:bloc/bloc.dart';
import 'bloc.dart';

class WebViewUserBloc extends Bloc<WebViewUserEvent, WebViewUserState> {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();

  @override
  WebViewUserState get initialState => InitialState();

  @override
  Stream<WebViewUserState> mapEventToState(
    WebViewUserEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapHomeStartedToState();
    }
  }

  Stream<WebViewUserState> _mapHomeStartedToState() async* {
    BaseResponse<UriModel> uriModelResponse =
        await _apiRemoteRepository.authorizeBankAccount();
    if (uriModelResponse.isSuccess()) {
      yield UriLoadedState(uriModelResponse.result);
    } else {
      yield FailedUriState();
    }
  }
}
