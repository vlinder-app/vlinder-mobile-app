abstract class WebViewResult {}

class SuccessResult extends WebViewResult {}

class FailedResult extends WebViewResult {
  final String error;

  FailedResult(this.error);

  @override
  String toString() {
    return 'Failed{error: $error}';
  }
}
