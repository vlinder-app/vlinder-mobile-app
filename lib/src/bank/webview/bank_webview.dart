import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/bank/webview/webview_result/webview_result.dart';
import 'package:atoma_cash/utils/platform/platform_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:url_launcher/url_launcher.dart';

import 'custom_app_bar.dart';

class BankWebViewScreen extends StatefulWidget {
  final UriModel uriModel;

  const BankWebViewScreen(this.uriModel, {Key key}) : super(key: key);

  @override
  _BankWebViewScreenState createState() => _BankWebViewScreenState();
}

class _BankWebViewScreenState extends State<BankWebViewScreen> {
  final _flutterWebViewPlugin = new FlutterWebviewPlugin();

  List<String> _history = [];
  int _position = -1;

  bool _canGoBack = false;
  bool _canGoForward = false;

  String successPath, failedPath;

  @override
  void initState() {
    _initPath();
    _initWebViewCallbacks();
    super.initState();
  }

  @override
  void dispose() {
    _flutterWebViewPlugin.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _webViewWidget(context);
  }

  void _initPath() {
    successPath = Uri.parse(widget.uriModel.successUri).path;
    failedPath = Uri.parse(widget.uriModel.failureUri).path;
  }

  void _initWebViewCallbacks() {
    _flutterWebViewPlugin.onHttpError.listen((event) {
      print(event.code);
    });
    _flutterWebViewPlugin.onUrlChanged.listen((url) async {});
    _flutterWebViewPlugin.onStateChanged.listen((WebViewStateChanged event) {
      if (event.type == WebViewState.shouldStart) {
        _handleShouldStartEvent(event.url);
      }
      if (event.type == WebViewState.finishLoad) {
        if (_history.isNotEmpty && _position != _history.length - 1) {
          _history.removeRange(_position, _history.length - 1);
        }
        if (_history.isEmpty ||
            (_history.isNotEmpty && _history.last != event.url)) {
          _history.add(event.url);
          _position++;
        }
        _updateState();
      }
    });
  }

  void _updateState() {
    _canGoBack = _history.isNotEmpty && _position != 0;
    _canGoForward = _history.isNotEmpty && _position != _history.length - 1;
    setState(() {});
    print(_history);
    print(_position);
  }

  void _handleShouldStartEvent(String url) async {
    if (url.contains(successPath)) {
      Navigator.pop(context, new SuccessResult());
    } else if (url.contains(failedPath)) {
      _onFailedUri(url);
    }
  }

  void _onFailedUri(String uri) {
    String errorMessage;
    try {
      errorMessage = Uri.parse(uri).queryParameters['Detail'];
    } catch (e) {
      print(e);
    }
    Navigator.pop(context, FailedResult(errorMessage));
  }

  Widget _webViewWidget(BuildContext context) {
    return WebviewScaffold(
        appBar: ModalStyleAppBar(AppBar(
          actions: _appBarActions(),
          backgroundColor: Colors.transparent,
        )),
        resizeToAvoidBottomInset: true,
        url: widget.uriModel.uri,
        appCacheEnabled: true,
        withJavascript: true,
        hidden: true);
  }

  Color _actionColorForState(bool active) {
    return active ? Colors.black : Colors.grey;
  }

  void _onBackPressed() async {
    if (_canGoBack) {
      await _flutterWebViewPlugin.goBack();
      _position--;
      _updateState();
    }
  }

  void _onForwardPressed() async {
    if (_canGoForward) {
      await _flutterWebViewPlugin.goForward();
      _position++;
      _updateState();
    }
  }

  void _onReloadPressed() async {
    await _flutterWebViewPlugin.reload();
  }

  List<Widget> _appBarActions() {
    return [
      IconButton(
          icon: Image.asset(
            Assets.webViewBack,
            color: _actionColorForState(_canGoBack),
          ),
          onPressed: _onBackPressed),
      IconButton(
          icon: Image.asset(Assets.webViewReload), onPressed: _onReloadPressed),
      IconButton(
          icon: Image.asset(
            Assets.webViewForward,
            color: _actionColorForState(_canGoForward),
          ),
          onPressed: _onForwardPressed)
    ];
  }
}
