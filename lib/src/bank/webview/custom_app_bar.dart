import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ModalStyleAppBar extends StatelessWidget implements PreferredSizeWidget {
  final PreferredSizeWidget appBar;
  final Color statusBarBackgroundColor;

  const ModalStyleAppBar(this.appBar,
      {Key key, this.statusBarBackgroundColor = Palette.blue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle.light,
      child: Container(
        color: statusBarBackgroundColor,
        child: SafeArea(
          child: Container(
//            height: 140.0,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15.0),
                    topRight: Radius.circular(15.0)),
                color: Colors.white),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: _toggleButton(context),
                ),
                appBar,
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _toggleButton(BuildContext context, {VoidCallback onClick}) {
    return InkWell(
      onTap: onClick,
      child: Container(
        height: 20.0,
        child: Center(
          child: Image.asset(Assets.expandIcon),
        ),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(86.0);
}
