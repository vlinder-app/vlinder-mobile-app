import 'dart:io';

import 'package:path_provider/path_provider.dart';

class PathProvider {
  static Future<String> couponPath() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    return "${appDocDir.path}/coupon.pdf";
  }
}
