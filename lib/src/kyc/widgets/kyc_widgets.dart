import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/material.dart';

class KycWidgets {
  static Widget appBarCancelButton(BuildContext context) {
    return FlatButton(
      child: Text(
        Localization.of(context).cancelButton,
        style: TextStyle(color: Palette.blue),
      ),
      onPressed: () => {},
    );
  }
}
