import 'package:flutter/material.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/utils/ui/modals.dart';

class KycModals {
  static void showVerifyIdentitySheet(
      {@required BuildContext context,
      @required VoidCallback onPositiveClick,
      @required VoidCallback onNegativeClick}) {
    Modals.showPositiveNegativeBottomSheet(
        context: context,
        imageAsset: Assets.verifyIdentity,
        message: Localization
            .of(context)
            .legacyVerifyIdentityDescription,
        positiveButtonText: Localization
            .of(context)
            .legacyVerifyButton,
        negativeButtonText: Localization
            .of(context)
            .notNowButton,
        onPositiveClick: onPositiveClick,
        onNegativeClick: onNegativeClick);
  }

  static void showTaxInfoSheet({@required BuildContext context,
    @required VoidCallback onPositiveClick,
    @required VoidCallback onNegativeClick}) {
    Modals.showPositiveNegativeBottomSheet(
        context: context,
        imageAsset: Assets.taxInfo,
        message: Localization
            .of(context)
            .legacyTaxInfoSheetMessage,
        positiveButtonText: Localization
            .of(context)
            .legacyProvideButton,
        negativeButtonText: Localization
            .of(context)
            .notNowButton,
        onPositiveClick: onPositiveClick,
        onNegativeClick: onNegativeClick);
  }
}
