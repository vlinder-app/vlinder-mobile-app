import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';

class NotificationsManager {
  static const int _timeInMillis = 300000; // 5 minutes for debug
  static const int _taxTimeInMillis = 600000; // 10 minutes for debug
//  static const int _dayInMillis = 86400000;

  static int _currentTimeStamp() => new DateTime.now().millisecondsSinceEpoch;

  static Future<bool> shouldShowVerifyIdentityRequest() async {
    int lastRequestTimeStamp =
    await ApplicationLocalRepository().getVerifyIdentityRequestTimestamp();
    return (_currentTimeStamp() - _timeInMillis) > lastRequestTimeStamp;
  }

  static Future<void> saveLastVerifyIdentityRequestTime() async {
    ApplicationLocalRepository()
        .saveVerifyIdentityRequestTimestamp(_currentTimeStamp());
  }

  static Future<bool> shouldShowTaxInfoRequest() async {
    int lastRequestTimeStamp =
    await ApplicationLocalRepository().getTaxInfoRequestTimestamp();
    return (_currentTimeStamp() - _taxTimeInMillis) > lastRequestTimeStamp;
  }

  static Future<void> saveLastTaxInfoRequestTime() async {
    ApplicationLocalRepository()
        .saveTaxInfoRequestTimestamp(_currentTimeStamp());
  }
}
