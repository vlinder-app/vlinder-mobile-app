import 'dart:math';
import 'dart:ui';

import 'package:atoma_cash/src/chart/axis/chart_axis.dart';
import 'package:atoma_cash/src/chart/models/point_data_model.dart';
import 'package:flutter/material.dart';

import 'chart_data_series.dart';

class ChartPointDataSeries<X, Y> extends IChartDataSeries<Point<double>> {
  final List<PointDataModel<X, Y>> valuesList;

  final IChartAxis<X> xAxis;
  final IChartAxis<Y> yAxis;

  Rect _previousZone;
  List<Point<double>> _cachedPoints;

  ChartPointDataSeries(this.valuesList,
      {@required this.xAxis, @required this.yAxis})
      : assert(valuesList != null),
        assert(xAxis != null),
        assert(yAxis != null);

  @override
  List<Point<double>> componentsForCanvasZone(Rect zone) {
    if (zone == _previousZone) {
      return _cachedPoints;
    } else {
      _cachedPoints = _configurePointsSeries(zone);
      return _cachedPoints;
    }
  }

  List<Point<double>> _configurePointsSeries(Rect zone) {
    _previousZone = zone;

    _cachedPoints =
        valuesList.fold(List<Point<double>>(), (previousValue, element) {
      final point = _pointForModel(element, zone);
      if (point != null) {
        return previousValue..add(point);
      } else {
        return previousValue;
      }
    });

    return _cachedPoints;
  }

  Point<double> _pointForModel(PointDataModel model, Rect zone) {
    final xPoint =
        xAxis.coordinateForValue(model.xValue, zone.left + zone.width);
    final yPoint =
        yAxis.coordinateForValue(model.yValue, zone.top + zone.height);

    if (xPoint != null && yPoint != null) {
      return Point(xPoint, yPoint);
    } else {
      return null;
    }
  }

  @override
  double amplitudeForValue<V>(V value, Rect zone) {
    if (value is Y) {
      return yAxis.coordinateForValue(value, zone.height);
    } else {
      return null;
    }
  }
}
