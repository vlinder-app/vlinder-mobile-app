import 'dart:math';
import 'dart:ui';

import 'package:atoma_cash/src/chart/axis/chart_axis.dart';

import 'chart_data_series.dart';

class ChartLineDataSeries extends IChartDataSeries<Point<double>> {
  final double value;
  final IChartAxis<double> yAxis;
  final IChartAxis<DateTime> xAxis;

  Rect _previousZone;
  List<Point<double>> _cachedPoints;

  ChartLineDataSeries(this.xAxis, this.yAxis, this.value);

  @override
  List<Point<double>> componentsForCanvasZone(Rect zone) {
    if (zone == _previousZone) {
      return _cachedPoints;
    } else {
      _cachedPoints = _configurePointsSeries(zone);
      return _cachedPoints;
    }
  }

  List<Point<double>> _configurePointsSeries(Rect zone) {
    _previousZone = zone;

    double amplitudeValue = amplitudeForValue(value, zone);

    if (amplitudeValue != null) {
      return [
        Point(zone.left, amplitudeValue),
        Point(zone.right, amplitudeValue)
      ];
    } else {
      return null;
    }
  }

  @override
  double amplitudeForValue<V>(V value, Rect zone) {
    if (value is double) {
      return yAxis.coordinateForValue(value, zone.height);
    } else {
      return null;
    }
  }
}
