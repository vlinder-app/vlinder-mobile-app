import 'package:flutter/cupertino.dart';

abstract class IChartDataSeries<T> {
  List<T> componentsForCanvasZone(Rect zone);

  double amplitudeForValue<V>(V value, Rect zone);
}
