class PointDataModel<X, Y> {
  final X xValue;
  final Y yValue;

  PointDataModel(this.xValue, this.yValue);
}
