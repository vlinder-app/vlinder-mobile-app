import 'package:atoma_cash/src/chart/chart.dart';
import 'package:flutter/cupertino.dart';

class ChartCanvas extends CustomPainter {
  final List<IChartPainter> painters;

  const ChartCanvas(this.painters);

  @override
  void paint(Canvas canvas, Size size) {
    painters
      ..forEach((painter) {
        painter.draw(canvas, Offset.zero & size);
      });
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
