import 'package:atoma_cash/src/chart/chart.dart';
import 'package:flutter/cupertino.dart';

import 'chart_annotation.dart';

class ChartYAxisLineAnnotation extends IChartAnnotation {
  final ChartLineDataSeries dataSeries;
  final Widget leading;
  final Widget trailing;
  final Color color;
  final double tipWidth;

  ChartYAxisLineAnnotation(this.dataSeries,
      {this.color, this.leading, this.trailing, this.tipWidth})
      : assert(leading != null || trailing != null),
        assert(tipWidth != null);

  @override
  Widget annotation(BuildContext context, Rect zone) {
    final amplitude = dataSeries.amplitudeForValue(dataSeries.value, zone);
    if (amplitude != null) {
      return Positioned(
          left: zone.left,
          top: amplitude,
          child: Container(
            height: 20,
            decoration: BoxDecoration(
              color: color,
              borderRadius: BorderRadius.all(
                Radius.circular(2),
              ),
            ),
            child: Padding(
              padding: EdgeInsets.only(
                left: 4.0,
                top: 2.0,
                right: 4.0,
                bottom: 2.0,
              ),
              child: Row(
                children: <Widget>[
                  if (leading != null) leading,
                  if (trailing != null) _trailingWidget(),
                ],
              ),
            ),
          ));
    } else {
      return Container();
    }
  }

  @override
  IChartPainter tipPainter() {
    return ChartSimplePainter(dataSeries,
        paint: Paint()
          ..color = color
          ..style = PaintingStyle.stroke
          ..strokeWidth = tipWidth,
        pathBuilder: ChartLinePathBuilder());
  }

  Widget _trailingWidget() {
    return Padding(
      padding: EdgeInsets.only(left: leading != null ? 2.0 : 0.0),
      child: trailing,
    );
  }
}
