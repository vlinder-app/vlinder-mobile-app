import 'package:atoma_cash/src/chart/chart.dart';
import 'package:flutter/cupertino.dart';

abstract class IChartAnnotation {
  const IChartAnnotation();

  IChartPainter tipPainter();

  Widget annotation(BuildContext context, Rect zone);
}
