import 'package:atoma_cash/src/chart/chart.dart';
import 'package:flutter/material.dart';

abstract class IChartPainter<T> {
  IChartDataSeries<T> get dataSeries;

  const IChartPainter();

  void draw(Canvas canvas, Rect paintZone);
}
