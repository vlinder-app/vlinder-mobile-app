import 'dart:math';

import 'package:atoma_cash/src/chart/chart.dart';
import 'package:flutter/cupertino.dart';

import 'chart_painter.dart';

class ChartSimplePainter extends IChartPainter<Point<double>> {
  @override
  final IChartDataSeries<Point<double>> dataSeries;
  final Paint paint;
  final IChartPathBuilder pathBuilder;

  const ChartSimplePainter(this.dataSeries,
      {@required this.paint, @required this.pathBuilder})
      : assert(dataSeries != null),
        assert(paint != null),
        assert(pathBuilder != null);

  @override
  void draw(Canvas canvas, Rect paintZone) {
    final components = dataSeries.componentsForCanvasZone(paintZone);

    if (components?.isNotEmpty ?? false) {
      final path = pathBuilder.build(components, paintZone);

      if (path != null) {
        canvas.drawPath(path, paint);
      }
    }
  }
}
