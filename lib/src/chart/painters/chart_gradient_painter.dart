import 'dart:math';

import 'package:atoma_cash/src/chart/chart.dart';
import 'package:flutter/material.dart';

import 'chart_painter.dart';

class ChartGradientPainter extends IChartPainter<Point<double>> {
  @override
  final IChartDataSeries<Point<double>> dataSeries;
  final LinearGradient gradient;
  final IChartPathBuilder pathBuilder;

  const ChartGradientPainter(this.dataSeries, {this.gradient, this.pathBuilder})
      : assert(pathBuilder != null),
        assert(dataSeries != null);

  @override
  void draw(Canvas canvas, Rect paintZone) {
    final components = dataSeries.componentsForCanvasZone(paintZone);
    if (components?.isNotEmpty ?? false) {
      final path = pathBuilder.build(components, paintZone);
      if (path != null) {
        final paint = Paint()..shader = gradient.createShader(paintZone);
        canvas.drawPath(path, paint);
      }
    }
  }
}
