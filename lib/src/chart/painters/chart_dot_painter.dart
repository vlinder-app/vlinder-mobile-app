import 'dart:math';

import 'package:atoma_cash/src/chart/chart.dart';
import 'package:flutter/material.dart';

import 'chart_painter.dart';

class ChartDotPainter extends IChartPainter<Point<double>> {
  @override
  final IChartDataSeries<Point<double>> dataSeries;
  final Paint fillPaint;
  final Paint strokePaint;
  final IChartPathBuilder pathBuilder;

  const ChartDotPainter(this.dataSeries,
      {this.fillPaint, this.strokePaint, this.pathBuilder})
      : assert(dataSeries != null),
        assert(pathBuilder != null);

  @override
  void draw(Canvas canvas, Rect paintZone) {
    final components = dataSeries.componentsForCanvasZone(paintZone);
    if (components?.isNotEmpty ?? false) {
      final path = pathBuilder.build(components, paintZone);
      if (path != null) {
        if (fillPaint != null) {
          canvas.drawPath(path, fillPaint);
        }
        if (strokePaint != null) {
          canvas.drawPath(path, strokePaint);
        }
      }
    }
  }
}
