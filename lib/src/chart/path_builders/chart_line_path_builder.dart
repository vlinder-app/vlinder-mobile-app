import 'dart:math';
import 'dart:ui';

import 'package:atoma_cash/src/chart/chart.dart';
import 'package:flutter/cupertino.dart';

class ChartLinePathBuilder extends IChartPathBuilder<Point<double>> {
  const ChartLinePathBuilder();

  @override
  Path build(List<Point<double>> points, Rect paintZone) {
    if (points.length > 1) {
      final path = Path()..moveTo(points.first.x, points.first.y);

      for (int i = 1; i < points.length; i++) {
        final point = points[i];
        path.lineTo(point.x, point.y);
      }

      return path;
    } else {
      return null;
    }
  }
}
