import 'dart:math';

extension PointUtilities on Point {
  Point midPoint(Point anotherPoint) {
    return Point(midX(anotherPoint), midY(anotherPoint));
  }

  double midX(Point anotherPoint) {
    return (x + anotherPoint.x) / 2;
  }

  double midY(Point anotherPoint) {
    return (y + anotherPoint.y) / 2;
  }
}
