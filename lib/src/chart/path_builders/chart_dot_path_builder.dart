import 'dart:math';
import 'dart:ui';

import 'package:atoma_cash/src/chart/chart.dart';

class ChartDotPathBuilder extends IChartPathBuilder<Point<double>> {
  final double radius;

  const ChartDotPathBuilder(this.radius);

  @override
  Path build(List<Point<double>> points, Rect paintZone) {
    return points.fold(
        Path(),
        (path, point) => path
          ..addOval(Rect.fromCircle(
              center: Offset(point.x, point.y), radius: radius)));
  }
}
