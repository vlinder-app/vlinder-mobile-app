import 'package:flutter/material.dart';

abstract class IChartPathBuilder<T> {
  const IChartPathBuilder();

  Path build(List<T> points, Rect paintZone);
}
