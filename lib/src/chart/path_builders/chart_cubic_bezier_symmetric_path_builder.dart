import 'dart:math';
import 'dart:ui';

import 'package:atoma_cash/src/chart/chart.dart';
import 'package:flutter/cupertino.dart';

import 'point_utilities.dart';

class ChartCubicBezierSymmetricPathBuilder
    extends IChartPathBuilder<Point<double>> {
  final bool isClosed;

  const ChartCubicBezierSymmetricPathBuilder({@required this.isClosed})
      : assert(isClosed != null);

  @override
  Path build(List<Point<double>> points, Rect paintZone) {
    if (points.length > 1) {
      final path = Path()..moveTo(points.first.x, points.first.y);

      for (int i = 0; i < points.length - 1; i++) {
        final startPoint = points[i];
        final endPoint = points[i + 1];
        final midStart = Point(startPoint.midX(endPoint), startPoint.y);
        final midEnd = Point(startPoint.midX(endPoint), endPoint.y);

        path.cubicTo(
          midStart.x,
          midStart.y,
          midEnd.x,
          midEnd.y,
          endPoint.x,
          endPoint.y,
        );
      }

      if (isClosed) {
        path.lineTo(points.last.x, paintZone.bottom);
        path.lineTo(points.first.x, paintZone.bottom);
        path.close();
      }

      return path;
    } else {
      return null;
    }
  }
}
