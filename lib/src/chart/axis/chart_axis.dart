abstract class IChartAxis<T> {
  double coordinateForValue(T value, double size);
}
