import 'dart:math';

import 'package:atoma_cash/src/chart/axis/chart_axis.dart';
import 'package:flutter/material.dart';

class ChartDoubleAxis extends IChartAxis<double> {
  final double maxValue;
  final double minValue;

  final double logarithm;

  double get _length => maxValue - minValue;

  ChartDoubleAxis(
      {@required this.minValue, @required this.maxValue, this.logarithm = ln2});

  @override
  double coordinateForValue(double value, double size) {
    if (_isInRange(value)) {
      return _mapValueToSize(value, size);
    } else {
      return null;
    }
  }

  bool _isInRange(double value) => value <= maxValue && value >= minValue;

  double _mapValueToSize(double value, double size) {
    final relativeValue = value - minValue;
    final length = _length;
    if (logarithm != 0 ?? false) {
      final relativeValue = _logarithmicValue(value - minValue, length);
      final highestValue = _logarithmicValue(length, length);
      final scale = size / highestValue;
      final result = (highestValue - relativeValue) * scale;
      return result;
    } else {
      return (size / length * relativeValue).roundToDouble();
    }
  }

  double _logarithmicValue(double value, double maxValue) {
    return log(1 + value / maxValue) / logarithm;
  }
}
