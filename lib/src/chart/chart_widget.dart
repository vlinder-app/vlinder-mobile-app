import 'package:atoma_cash/src/chart/chart.dart';
import 'package:flutter/cupertino.dart';

class ChartWidget extends StatelessWidget {
  static const double kHeight = 148.0;
  static const double kDefaultSideMargin = 24.0;
  static const double _chartContainerHeight = 99;
  static const double _topMargin = 10.0;
  static const double _bottomMargin = 38.0;

  final List<IChartPainter> painters;
  final List<IChartAnnotation> annotations;
  final Widget interactionWidget;
  final double sideMargin;

  const ChartWidget(
      {@required this.painters,
      @required this.interactionWidget,
      this.annotations,
      this.sideMargin = kDefaultSideMargin})
      : assert(sideMargin != null);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: kHeight,
      child: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: sideMargin, right: sideMargin),
            child: Container(
              height: kHeight,
              child: interactionWidget,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
              left: sideMargin,
              top: _topMargin,
              right: sideMargin,
              bottom: _bottomMargin,
            ),
            child: IgnorePointer(
              child: Container(
                width: size.width - sideMargin * 2,
                height: _chartContainerHeight,
                child: CustomPaint(painter: ChartCanvas(_chartPainters())),
              ),
            ),
          ),
          if (annotations != null) ..._annotations(context, size),
        ],
      ),
    );
  }

  List<Widget> _annotations(BuildContext context, Size screenSize) {
    return annotations
        .map((oneAnnotation) => oneAnnotation.annotation(
            context,
            Rect.fromLTWH(sideMargin, _topMargin,
                screenSize.width - sideMargin * 2, _chartContainerHeight)))
        .toList();
  }

  List<IChartPainter> _chartPainters() {
    return annotations
        .map((oneAnnotation) => oneAnnotation.tipPainter())
        .toList()
          ..addAll(painters);
  }
}
