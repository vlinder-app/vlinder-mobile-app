import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Palette.blue,
      body: Center(
          child: Image.asset(
        Assets.splashIcon,
        width: 120.0,
        height: 120.0,
      )),
    );
  }
}
