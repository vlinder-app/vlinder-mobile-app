import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:equatable/equatable.dart';

abstract class ReportsEvent extends Equatable {
  const ReportsEvent();
}

class Started extends ReportsEvent {
  @override
  List<Object> get props => null;
}

class ToggleMode extends ReportsEvent {
  @override
  List<Object> get props => null;
}

class LastTransactionDateTimeSelected extends ReportsEvent {
  final DateTime lastTransactionDateTime;

  LastTransactionDateTimeSelected(this.lastTransactionDateTime);

  @override
  List<Object> get props => [lastTransactionDateTime];
}

class DateRangeChanged extends ReportsEvent {
  final DateRange item;

  DateRangeChanged(this.item);

  @override
  List<Object> get props => [item];
}

class Update extends ReportsEvent {
  @override
  List<Object> get props => null;
}
