import 'dart:async';

import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/change_view_mode_event.dart';
import 'package:bloc/bloc.dart';

import 'bloc.dart';

class ReportsBloc extends Bloc<ReportsEvent, ReportsState> {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();

  @override
  ReportsState get initialState {
    return ReportsState(
        dateRangeItem: DateRange.month(), mode: Mode.EXPENSES, isLoading: true);
  }

  @override
  Stream<ReportsState> mapEventToState(
    ReportsEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapHomeStartedToState();
    }
    if (event is DateRangeChanged) {
      yield* _mapPeriodChangedToState(event.item);
    }
    if (event is ToggleMode) {
      yield* _mapToggleModeToState();
    }
    if (event is LastTransactionDateTimeSelected) {
      yield* _mapLastTransactionDateTimeSelectedToState(
          event.lastTransactionDateTime);
    }
    if (event is Update) {
      yield* _mapUpdatedToState();
    }
  }

  Stream<ReportsState> _mapLastTransactionDateTimeSelectedToState(
      DateTime lastTransactionDateTime) async* {
    add(DateRangeChanged(
        state.dateRangeItem.fromDateTime(lastTransactionDateTime)));
  }

  Stream<ReportsState> _mapToggleModeToState() async* {
    Mode nextMode = state.mode == Mode.IMPACT ? Mode.EXPENSES : Mode.IMPACT;
    yield state.copyWith(mode: nextMode);
    Analytics().logEvent(ChangeViewModeEvent(nextMode, Source.ALL_CATEGORIES,
        categoriesNumber: state.filteredReportsForMode.length));
  }

  Stream<ReportsState> _mapHomeStartedToState(
      {bool withIndicator = true}) async* {
    if (withIndicator) {
      yield state.copyWith(isLoading: true);
    }
    BaseResponse<Reports> reportsResponse =
    await _apiRemoteRepository.getReports(state.dateRangeItem.period);
    if (reportsResponse.isSuccess()) {
      yield state.copyWith(reports: reportsResponse.result, isLoading: false);
    } else {
      yield state.copyWith(error: reportsResponse.error, isLoading: false);
    }
  }

  Stream<ReportsState> _mapPeriodChangedToState(DateRange item) async* {
    yield state.copyWith(dateRangeItem: item);
    add(Started());
  }

  Stream<ReportsState> _mapUpdatedToState() async* {
    if (!state.isLoading) {
      yield* _mapHomeStartedToState(withIndicator: false);
    }
  }
}
