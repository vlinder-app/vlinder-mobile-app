import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/categories/transactions_type.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/models/api/error/error.dart';
import 'package:atoma_cash/src/reports/reports_list/list/filters.dart';
import 'package:atoma_cash/src/reports/reports_list/list/sorters.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../models.dart';

@immutable
abstract class BaseReportsState extends Equatable {
  const BaseReportsState();
}

class ReportsState extends BaseReportsState {
  final DateRange dateRangeItem;
  final Reports reports;
  final Mode mode;
  final bool isLoading;
  final Error error;

  ReportsState({@required this.dateRangeItem,
    this.reports,
    this.mode,
    this.isLoading,
    this.error});

  ReportsState copyWith({DateRange dateRangeItem,
    Reports reports,
    Mode mode,
    bool isLoading,
    Error error}) =>
      ReportsState(
          error: error ?? this.error,
          dateRangeItem: dateRangeItem ?? this.dateRangeItem,
          isLoading: isLoading ?? this.isLoading,
          mode: mode ?? this.mode,
          reports: reports ?? this.reports);

  @override
  List<Object> get props => [mode, dateRangeItem, reports, isLoading, error];

  List<Report> get filteredReportsForMode {
    var filteredReports = Filters.filteredReports(reports.reports, mode)
      ..sort(ReportsSorter.reportsSortForMode(mode));
    return filteredReports.reversed.toList();
  }

  List<Report> get expensesReports =>
      filteredReportsForMode
          .where((r) =>
      r.category.transactionsType == TransactionsType.EXPENSES)
          .toList();

}
