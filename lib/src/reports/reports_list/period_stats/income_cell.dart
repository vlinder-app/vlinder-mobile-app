import 'package:atoma_cash/models/api/api/categories/income.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/categories/helpers/period_localization.dart';
import 'package:atoma_cash/src/categories/list/categories_base_item.dart';
import 'package:flutter/material.dart';

class IncomeCell extends StatelessWidget {
  final Income income;

  const IncomeCell({Key key, @required this.income}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CategoriesBaseItem(
      leading: Image.asset(Assets.incomeIcon),
      title: Localization.of(context).income,
      trailing: "+ ${income.currentPeriodAmount.currencyFormatted()}",
      subTrailing: PeriodLocalization.periodLocalization(
          context, income.periodType, Localization.of(context).legacyThisPeriod),
    );
  }
}
