import 'package:atoma_cash/models/api/api/categories/savings.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/categories/helpers/period_localization.dart';
import 'package:atoma_cash/src/categories/list/categories_base_item.dart';
import 'package:flutter/material.dart';

class SavingsCell extends StatelessWidget {
  static const double height = 72.0;
  final Savings savings;

  const SavingsCell({Key key, @required this.savings}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: CategoriesBaseItem(
        leading: Image.asset(Assets.savingsBlueIcon),
        title: Localization.of(context).legacySavings,
        subtitle: _previousPeriodString(context),
        trailing: savings.currentPeriodAmount.currencyFormatted(),
        subTrailing: PeriodLocalization.periodLocalization(
            context, savings.periodType, Localization.of(context).legacyThisPeriod),
      ),
    );
  }

  String _previousPeriodString(BuildContext context) {
    return savings.previousPeriodAmount != null
        ? "${Localization.of(context).legacyPreviousPeriod} ${savings.previousPeriodAmount.currencyFormatted()}"
        : "";
  }

/*
   * Old implementation
   *  
  String _previousPeriodString(BuildContext context) {
    return savings.previousPeriodAmount != null
        ? "${PeriodLocalization.periodLocalization(
        context, savings.periodType, Localization
        .of(context)
        .lastPeriod)} "
        "${savings.previousPeriodAmount.currencyFormatted()}"
        : "";
  }  
   */
}
