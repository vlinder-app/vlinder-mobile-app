import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/src/reports/reports_list/bloc/bloc.dart';
import 'package:atoma_cash/src/widgets/fraction_view.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:flutter/material.dart';

typedef OnFractionClick = void Function(Report report);

class ReportsFractionView<Report> extends FractionView {
  static const double _kViewFractionMaxHeight = 136.0;

  final Mode mode;
  final List<Report> reports;
  final OnFractionClick onColumnClick;
  final double highestPercentageValue;

  const ReportsFractionView({
    Key key,
    @required this.mode,
    @required this.reports,
    @required this.highestPercentageValue,
    this.onColumnClick,
  }) : super(key: key);

  @override
  String fractionColor(fraction) {
    return fraction.category.color;
  }

  @override
  double fractionHeight(fraction) {
    if (highestPercentageValue != 0) {
      switch (mode) {
        case Mode.EXPENSES:
          return (_kViewFractionMaxHeight *
              fraction.amountPercentage.abs() /
              highestPercentageValue);
        case Mode.IMPACT:
          return _kViewFractionMaxHeight *
              fraction.carbonFootprintPercentage /
              highestPercentageValue;
        default:
          return 0.0;
      }
    } else {
      return 0.0;
    }
  }

  @override
  String fractionValue(BuildContext context, fraction) {
    switch (mode) {
      case Mode.EXPENSES:
        return fraction.amount
            .copyWith(value: fraction.amount.value.abs())
            .currencyShortFormatted();
      case Mode.IMPACT:
        return CarbonHelper.abridgedCarbonString(fraction.carbonFootprint,
            context: context);
      default:
        return "Undefined";
    }
  }

  @override
  List get fractions => reports;

  @override
  String subtitle(BuildContext context, fraction) => null;

  @override
  void onFractionClick(fraction) {
    onColumnClick(fraction);
  }
}
