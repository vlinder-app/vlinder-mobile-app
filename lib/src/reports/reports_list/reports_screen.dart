import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/open_reports_tab_event.dart';
import 'package:atoma_cash/src/analytics/models/events/select_expenses_cat_event.dart';
import 'package:atoma_cash/src/analytics/models/events/select_income_cat_event.dart';
import 'package:atoma_cash/src/home/widgets/list/snapping_scroll_physics.dart';
import 'package:atoma_cash/src/home_placeholder/home_placeholder.dart';
import 'package:atoma_cash/src/home_placeholder/itab_listener.dart';
import 'package:atoma_cash/src/reports/report_details/result/result.dart';
import 'package:atoma_cash/src/reports/reports_list/bloc/bloc.dart';
import 'package:atoma_cash/src/reports/reports_list/list/filters.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/categories_item.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/header.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/header_stats.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/income_cell.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/placeholder/empty_period_reports_placeholder.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/placeholder/empty_periods_placeholder.dart';
import 'package:atoma_cash/src/reports/widgets/actions_list.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/src/settings/widgets/cells.dart';
import 'package:atoma_cash/src/widgets/data_load_failed.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/src/widgets/fraction_view.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:atoma_cash/utils/reports_helper.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'categories_fraction/reports_fraction_list.dart';

class ReportsScreen extends StatefulWidget implements ITabListener {
  final _ReportsScreenState _state = _ReportsScreenState();

  @override
  _ReportsScreenState createState() => _state;

  @override
  void onAppear() {
    _state.onAppear();
  }
}

class _ReportsScreenState extends State<ReportsScreen>
    with AutomaticKeepAliveClientMixin
    implements IHeaderEventsListener, ITabListener {
  SnappingScrollPhysics _scrollPhysics =
  SnappingScrollPhysics(midScrollOffset: 240.0);

  TapGestureRecognizer _showActivityRecognizer;
  ReportsBloc _bloc;

  @override
  void initState() {
    _showActivityRecognizer = TapGestureRecognizer();
    _bloc = ReportsBloc()..add(Started());
    super.initState();
  }

  @override
  void dispose() {
    _showActivityRecognizer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder(
      bloc: _bloc,
      builder: _rootWidgetForState,
    );
  }

  Widget _rootWidgetForState(BuildContext context, ReportsState state) {
    if (state is ReportsState) {
      return _loadedWidget(context, state);
    } else {
      return Container();
    }
  }

  double _bottomSpacerHeight(BuildContext context, ReportsState state) {
    double contentMargin = 16.0;
    double categoriesListHeight =
        Filters
            .filteredReports(state.expensesReports, state.mode)
            .length *
            FourRowListTile.twoRowHeight;
    double incomeCellListHeight = state.reports.incomeReports.isNotEmpty
        ? FourRowListTile.twoRowHeight
        : 0.0;
    double fractionViewHeight =
    state.expensesReports.isNotEmpty ? FractionView.fullHeight : 0.0;
    double contentHeight = incomeCellListHeight +
        categoriesListHeight +
        contentMargin +
        fractionViewHeight;

    return Dimensions.bottomSpacerHeight(
        context, HeaderWidget.kHeaderHeight, contentHeight);
  }

  List<Widget> _fillRemainingSliver(BuildContext context, Widget child) {
    return [
      SliverFillRemaining(child: Center(child: child)),
    ];
  }

  List<Widget> _rootContent(BuildContext context, ReportsState state) {
    return [
      if (state.reports.incomeReports.isNotEmpty)
        ..._incomeSection(context, state),
      ..._expensesHeader(context, state),
      ..._fractionViewSection(context, state),
      SliverList(
        delegate: SliverChildListDelegate(_categoriesListBuilder(
            context, state.mode, state.expensesReports, state.dateRangeItem)),
      ),
      SliverToBoxAdapter(
        child: Container(
          height: _bottomSpacerHeight(context, state),
          color: Colors.white,
        ),
      ),
    ];
  }

  List<Widget> _fractionViewSection(BuildContext context, ReportsState state) {
    return [
      if (state.expensesReports.isNotEmpty)
        SliverToBoxAdapter(
          child: Container(
            margin: const EdgeInsets.only(top: 16.0),
            child: ReportsFractionView(
              mode: state.mode,
              reports: state.expensesReports,
              highestPercentageValue:
                  ReportsHelper.extractHighestPercentageValue(
                      state.expensesReports, state.mode),
              onColumnClick: (report) => _onColumnClick(
                  context, report.category, state.mode, state.dateRangeItem),
            ),
          ),
        )
    ];
  }

  List<Widget> _expensesHeader(BuildContext context, ReportsState state) {
    return [
      if (state.expensesReports.isNotEmpty &&
          state.reports.incomeReports.isNotEmpty)
        SliverToBoxAdapter(
          child: Container(
            child: Cells.header(Localization
                .of(context)
                .expenses),
          ),
        )
    ];
  }

  List<Widget> _incomeSection(BuildContext context, ReportsState state) {
    return [
      SliverToBoxAdapter(
        child: Container(
          margin: const EdgeInsets.only(top: 8.0, bottom: 16.0),
          child: IncomeCell(
            totalAmount: state.reports.totalIncome,
            onClick: () =>
                _onIncomeCellClick(state.reports, state.dateRangeItem),
          ),
        ),
      ),
    ];
  }

  List<Widget> _buildRootContent(BuildContext context, ReportsState state) {
    if (state.isLoading) {
      return _fillRemainingSliver(context, CircularProgressIndicator());
    } else {
      if (state.reports == null) {
        return _fillRemainingSliver(
          context,
          DataLoadFailedWidget(
            onReloadClick: _onReload,
          ),
        );
      } else {
        if (state.reports.reports.isEmpty) {
          _showActivityRecognizer.onTap = () {
            _onLastTransactionPeriodClick(context);
          };
          if (state.reports.lastTransaction != null) {
            return _emptyTransactionListPlaceholder(
                state.reports.lastTransaction);
          } else {
            return _fillRemainingSliver(context, EmptyReportsPlaceholder());
          }
        } else {
          return _rootContent(context, state);
        }
      }
    }
  }

  List<Widget> _emptyTransactionListPlaceholder(Transaction lastTransaction) {
    return _fillRemainingSliver(
        context,
        EmptyPeriodReportsPlaceholder(
          lastTransactionDateTime: lastTransaction.bookedAt?.toLocal(),
          dateTimeGestureRecognizer: _showActivityRecognizer,
          onShowMeClick: () =>
              _onShowLastTransactionPeriodClick(lastTransaction.bookedAt),
        ));
  }

  Widget _loadedWidget(BuildContext context, ReportsState state) {
    return CustomScrollView(
      shrinkWrap: false,
      physics: _scrollPhysics,
      slivers: <Widget>[
        HeaderWidget(
          dateRange: state.dateRangeItem,
          listener: this,
          title: Localization
              .of(context)
              .allCategories,
          appBarActions:
          ReportsAppBarActionList.actionsList(state.mode, _onToggleMode),
          child: Padding(
            padding: Dimensions.leftRight24Padding,
            child: Column(
              children: <Widget>[
                HeaderStats(
                  mode: state.mode,
                  amount: state.reports?.totalAmount,
                  carbonFootprint: state.reports?.totalCarbonFootprint,
                ),
              ],
            ),
          ),
        ),
        ..._buildRootContent(context, state),
      ],
    );
  }

  List<Widget> _categoriesListBuilder(BuildContext context, Mode mode,
      List<Report> reports, DateRange dateRange) {
    return reports
        .map<Widget>((c) =>
        CategoriesItem(
          category: c.category,
          amount: c.amount,
          mode: mode,
          carbonFootrpint: c.carbonFootprint,
          onClick: () =>
          {_onListItemClick(context, c.category, mode, dateRange)},
        ))
        .toList();
  }

  void _onColumnClick(BuildContext context, Category category, Mode mode,
      DateRange dateRange) {
    Analytics().logEvent(
        SelectExpensesCatEvent(Source.CHART, category.id));
    _onCategoryClick(context, category, mode, dateRange);
  }

  void _onListItemClick(BuildContext context, Category category, Mode mode,
      DateRange dateRange) {
    Analytics().logEvent(
        SelectExpensesCatEvent(Source.LIST, category.id));
    _onCategoryClick(context, category, mode, dateRange);
  }

  void _onCategoryClick(BuildContext context, Category category, Mode mode,
      DateRange dateRange) async {
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.reportDetailsScreen,
        arguments: ReportDetailsScreenArguments(
            category: category, mode: mode, dateRange: dateRange));
    if (result != null) {
      if (result is SwitchToTabResult) {
        _changeTabToActivity(result.tabId);
      }
    } else {
      _silentUpdate();
    }
  }

  void _onIncomeCellClick(Reports reports, DateRange dateRange) async {
    Analytics().logEvent(SelectIncomeCatEvent());
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.incomeReportScreen,
        arguments: IncomeReportScreenArguments(
            reports: reports, dateRange: dateRange));
    if (result != null) {
      if (result is SwitchToTabResult) {
        _changeTabToActivity(result.tabId);
      }
    } else {
      _silentUpdate();
    }
  }

  void _onShowLastTransactionPeriodClick(DateTime lastTransactionDateTime) {
    _bloc.add(LastTransactionDateTimeSelected(lastTransactionDateTime));
  }

  void _changeTabToActivity(TabId tabId) {
    DefaultTabController.of(context).animateTo(tabId.index,
        duration: Duration(milliseconds: 0), curve: Curves.ease);
  }

  void _onLastTransactionPeriodClick(BuildContext context) async {
    DefaultTabController.of(context).animateTo(0);
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void onDateRangeChange(DateRange dateRangeItem) {
    _bloc.add(DateRangeChanged(dateRangeItem));
  }

  void _silentUpdate() {
    _bloc?.add(Update());
  }

  void _onReload() {
    _bloc.add(Started());
  }

  void _onToggleMode() {
    _bloc.add(ToggleMode());
  }

  @override
  void onAppear() {
    Analytics().logEvent(OpenReportsTabEvent());
    _silentUpdate();
  }
}
