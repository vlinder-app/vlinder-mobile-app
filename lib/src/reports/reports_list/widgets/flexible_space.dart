import 'package:atoma_cash/src/reports/reports_list/bloc/bloc.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/header_stats.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:flutter/material.dart';

class FlexibleSpace extends StatelessWidget {
  final ReportsState state;

  const FlexibleSpace({Key key, this.state}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: Dimensions.leftRight24Padding,
          child: HeaderStats(
            mode: state.mode,
            amount: state.reports?.totalAmount,
            carbonFootprint: state.reports?.totalCarbonFootprint,
          ),
        ),
      ],
    );
  }
}
