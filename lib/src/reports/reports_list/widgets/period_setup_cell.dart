import 'package:atoma_cash/utils/date_helper.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

typedef DateTimeConfirmCallback = void Function(DateTime dateTime);

class PeriodSetupCell extends StatelessWidget {
  final DateTime value;
  final String hint;
  final DateTimeConfirmCallback onDateTimeConfirm;
  final String errorMessage;

  const PeriodSetupCell(
      {Key key,
      this.hint,
      this.onDateTimeConfirm,
      this.value,
      this.errorMessage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController controller = TextEditingController();
    if (value != null) {
      controller.text = DateHelper.humanDate(value);
    }
    return TextFormField(
      readOnly: true,
      enableInteractiveSelection: false,
      onTap: () => {_showDatePicker(context)},
      autofocus: false,
      controller: controller,
      decoration: UiHelper.styledInputDecoration(hint).copyWith(hintText: hint),
      maxLength: 15,
      autovalidate: true,
      buildCounter: (BuildContext context,
              {int currentLength, int maxLength, bool isFocused}) =>
          null,
      keyboardType: null,
      maxLines: 1,
      validator: (_) {
        return errorMessage;
      },
    );
  }

  void _showDatePicker(BuildContext context) {
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(2016, 1, 1),
        maxTime: DateTime.now(),
        onChanged: (date) {}, onConfirm: (date) {
      onDateTimeConfirm(date);
    }, currentTime: value);
  }
}
