import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/resources/providers/currency_provider.dart';
import 'package:atoma_cash/src/reports/reports_list/bloc/bloc.dart';
import 'package:atoma_cash/src/reports/widgets/header_stat_cell.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:flutter/material.dart';

class HeaderStats extends StatelessWidget {
  static const _height = 65.0;

  final Mode mode;
  final Amount amount;
  final double carbonFootprint;

  const HeaderStats({Key key, this.mode, this.amount, this.carbonFootprint})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return HeaderStatCell(
      title: _hintForMode(context),
      subtitle: _valueForMode(context),
      height: _height,
    );
  }

  String _hintForMode(BuildContext context) {
    if (mode == Mode.IMPACT) {
      return Localization.of(context).carbonEmissions;
    } else if (mode == Mode.EXPENSES) {
      return Localization.of(context).totalSpent;
    } else {
      return "...";
    }
  }

  String _valueForMode(BuildContext context) {
    if (mode == Mode.EXPENSES) {
      if (amount == null) {
        return BaseCurrencyProvider().baseCurrency?.currencyFormatted() ??
            "...";
      } else {
        return amount.currencyFormatted();
      }
    } else if (mode == Mode.IMPACT) {
      return CarbonHelper.carbonFootprintLocalizableValue(
          context, carbonFootprint ?? 0,
          emptyPlaceholder: "0 ${Localization
              .of(context)
              .kilogram}");
    } else {
      return "...";
    }
  }
}
