import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:flutter/material.dart';

import 'period_setup_cell.dart';

typedef OnPeriodChanged = void Function(DateTime startDate, DateTime endDate);

class PeriodSelectorDialog extends StatefulWidget {
  final DateTime startDate;
  final DateTime endDate;
  final OnPeriodChanged onPeriodChanged;

  const PeriodSelectorDialog(
      {Key key,
      @required this.startDate,
      @required this.endDate,
      @required this.onPeriodChanged})
      : super(key: key);

  @override
  _PeriodSelectorDialogState createState() => _PeriodSelectorDialogState();
}

class _PeriodSelectorDialogState extends State<PeriodSelectorDialog> {
  DateTime _startDate;
  DateTime _endDate;

  @override
  void initState() {
    _startDate = widget.startDate;
    _endDate = widget.endDate;
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      title: Text(
        Localization.of(context).customPeriodDialogTitle,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.w600,
        ),
      ),
      content: StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: PeriodSetupCell(
                    hint: Localization.of(context).startDate,
                    value: _startDate,
                    onDateTimeConfirm: _onStartDateConfirm),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 32.0),
                child: PeriodSetupCell(
                    hint: Localization.of(context).endDate,
                    value: _endDate,
                    onDateTimeConfirm: _onEndDateConfirm),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: _actions(),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  void _onStartDateConfirm(DateTime startDate) {
    if (startDate.isAfter(_endDate)) {
      _endDate = startDate.add(Duration(days: 1));
    }
    setState(() {
      _startDate = startDate;
    });
  }

  void _onEndDateConfirm(DateTime endDate) {
    setState(() {
      if (endDate.isBefore(_startDate)) {
        _startDate = endDate.subtract(Duration(days: 1));
      }
      _endDate = endDate;
    });
  }

  List<Widget> _actions() {
    return [
      Widgets.styledFlatButton(
          text: Localization.of(context).cancelButton,
          onPressed: () => {Navigator.of(context).pop()}),
      FlatButton(
        onPressed: _startDate.isBefore(_endDate) ? _onDatesConfirm : null,
        child: Text(Localization.of(context).confirmButton,
            textAlign: TextAlign.end,
            style: TextStyle(
                color: _startDate.isBefore(_endDate)
                    ? Palette.blue
                    : Palette.mediumGrey,
                fontWeight: FontWeight.w600)),
      ),
    ];
  }

  void _onDatesConfirm() {
    Navigator.of(context).pop();
    widget.onPeriodChanged(_startDate, _endDate);
  }
}
