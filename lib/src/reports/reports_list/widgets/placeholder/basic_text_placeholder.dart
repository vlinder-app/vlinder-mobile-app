import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class BasicTextPlaceholder extends StatelessWidget {
  final String text;

  const BasicTextPlaceholder({Key key, @required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24.0),
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(color: Palette.mediumGrey),
      ),
    );
  }
}
