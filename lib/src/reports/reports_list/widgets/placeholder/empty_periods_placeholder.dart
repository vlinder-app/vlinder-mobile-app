import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/material.dart';

class EmptyReportsPlaceholder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(24.0),
      child: Text(
        Localization.of(context).emptyReportsPlaceholderText,
        textAlign: TextAlign.center,
        style: TextStyle(color: Palette.mediumGrey),
      ),
    );
  }
}
