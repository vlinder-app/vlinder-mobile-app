import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class EmptyPeriodReportsPlaceholder extends StatelessWidget {
  final DateTime lastTransactionDateTime;
  final TapGestureRecognizer dateTimeGestureRecognizer;
  final VoidCallback onShowMeClick;

  const EmptyPeriodReportsPlaceholder(
      {Key key,
      @required this.lastTransactionDateTime,
      @required this.dateTimeGestureRecognizer,
      this.onShowMeClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    const TextStyle baseTextStyle = TextStyle(color: Colors.black54);
    return Container(
      margin: EdgeInsets.all(24.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
                text: Localization.of(context).dontHaveTransactionsInPeriod,
                style: baseTextStyle,
                children: [
                  TextSpan(
                      text: DateHelper.separatorStringForDateTime(
                          context, lastTransactionDateTime),
                      style: TextStyle(color: Palette.blue),
                      recognizer: dateTimeGestureRecognizer,
                      children: [
                        TextSpan(text: ".\n"),
                        TextSpan(
                            text: Localization.of(context)
                                .wouldYouLikeSeeTransactions,
                            style: baseTextStyle)
                      ])
                ]),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 24.0),
            child: RaisedButton(
              color: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(12.0)),
              child: Text(
                Localization.of(context).yesShowMeButton,
                style: TextStyle(
                    color: Palette.blue,
                    fontWeight: FontWeight.w600,
                    fontSize: 15.0),
              ),
              onPressed: onShowMeClick,
            ),
          )
        ],
      ),
    );
  }
}
