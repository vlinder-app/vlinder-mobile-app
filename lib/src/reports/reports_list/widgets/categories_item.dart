import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/src/widgets/rounded_icon.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:flutter/material.dart';

import '../../models.dart';

class CategoriesItem extends StatelessWidget {
  final Category category;
  final Amount amount;
  final double carbonFootrpint;
  final VoidCallback onClick;
  final Mode mode;

  const CategoriesItem(
      {Key key,
      this.category,
      this.amount,
      this.onClick,
      this.mode,
      this.carbonFootrpint})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _categoryCell(context);
  }

  Widget _categoryCell(BuildContext context) {
    return FourRowListTile(
        onClick: onClick,
        leading: RoundedIcon(
            iconStringColor: category.color, iconUrl: category.iconUrl),
        title: Text(
          category.name,
          style: FourRowListTile.titleTextStyle,
        ),
        trailing: Text(
          _trailingText(context),
          style: FourRowListTile.titleTextStyle,
        ),
        subTrailing: _subtrailingWidget(_subTrailingText(context)));
  }

  String _subTrailingText(BuildContext context) {
    if (mode == Mode.EXPENSES) {
      if (carbonFootrpint != null && carbonFootrpint != 0.0) {
        return CarbonHelper.carbonFootprintLocalizableValue(
            context, carbonFootrpint);
      } else {
        return null;
      }
    }
    if (mode == Mode.IMPACT) {
      return amount.currencyFormatted();
    }
  }

  String _trailingText(BuildContext context) {
    switch (mode) {
      case Mode.EXPENSES:
        return amount.currencyFormatted();
      case Mode.IMPACT:
        return CarbonHelper.carbonFootprintLocalizableValue(
            context, carbonFootrpint);
    }
  }

  Widget _subtrailingWidget(String text) {
    if (text != null) {
      return Text(
        text,
        style: FourRowListTile.subtitleTextStyle,
      );
    } else {
      return null;
    }
  }
}
