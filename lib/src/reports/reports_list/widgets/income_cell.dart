import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/src/widgets/rounded_icon.dart';
import 'package:flutter/material.dart';

class IncomeCell extends StatelessWidget {
  final Amount totalAmount;
  final VoidCallback onClick;

  const IncomeCell({Key key, @required this.totalAmount, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FourRowListTile(
        onClick: onClick,
        leading: RoundedIcon(
          iconColor: Palette.mediumGrey,
          asset: Assets.categoryIncomeIcon,
        ),
        title: Text(
          Localization.of(context).income,
          style: FourRowListTile.titleTextStyle,
        ),
        trailing: Text(
          totalAmount.currencyFormatted(),
          style: FourRowListTile.titleTextStyle,
        ));
  }
}
