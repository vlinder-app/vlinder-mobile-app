import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/categories/period.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/categories/widgets/period_selector_dialog.dart';
import 'package:atoma_cash/src/widgets/periods_widget/periods_widget.dart';
import 'package:atoma_cash/utils/platform/platform_helper.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

abstract class IHeaderEventsListener {
  void onDateRangeChange(DateRange dateRangeItem);
}

class HeaderWidget extends StatefulWidget {
  static const kHeaderHeight = 220.0;

  final double height;
  final IHeaderEventsListener listener;
  final DateRange dateRange;
  final Widget child;
  final String title;
  final List<Widget> appBarActions;

  const HeaderWidget({Key key,
    this.listener,
    this.height = kHeaderHeight,
    this.child,
    this.title,
    @required this.dateRange,
    this.appBarActions})
      : super(key: key);

  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget>
    implements IPeriodsListener {
  @override
  Widget build(BuildContext context) {
    return _headerAppBarWidget(context, widget.dateRange);
  }

  Widget _headerAppBarWidget(BuildContext context, DateRange dateRange) {
    Widget titleWidget;
    if (widget.title != null) {
      titleWidget = _titleWidget(widget.title);
    }
    return SliverAppBar(
      iconTheme: IconThemeData(
        color: Colors.white,
      ),
      brightness: Brightness.dark,
      pinned: true,
      backgroundColor: Palette.blue,
      title: titleWidget,
      actions: widget.appBarActions,
      centerTitle: false,
      expandedHeight: widget.height,
      flexibleSpace: _flexibleSpace(context, dateRange),
    );
  }

  Widget _flexibleSpace(BuildContext context, DateRange dateRange) {
    double statusBarHeight = MediaQuery.of(context).padding.top;
    double topMargin = kToolbarHeight +
        (PlatformHelper.isIOS(context) ? statusBarHeight / 2 : 0.0);
    return FlexibleSpaceBar(
      collapseMode: CollapseMode.parallax,
      background: Container(
        color: Colors.white,
        child: Container(
          decoration: BoxDecoration(
              color: Palette.blue,
              borderRadius:
                  BorderRadius.only(bottomLeft: Radius.circular(20.0))),
          margin: EdgeInsets.only(top: 0.0, bottom: 0.0),
          padding: EdgeInsets.only(top: topMargin, bottom: 0.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              if (widget.child != null) widget.child,
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: PeriodsWidget(
                  dateRange,
                  listener: this,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onPeriodChanged(PeriodType type, DateRange selectedDateRange) {
    if (type == selectedDateRange.periodType) {
      return;
    }
    if (type == PeriodType.CUSTOM) {
      _showCustomPeriodDialog(null, null);
    } else {
      DateRange item;
      switch (type) {
        case PeriodType.TODAY:
          item = DateRange.day();
          break;
        case PeriodType.WEEK:
          item = DateRange.week();
          break;
        case PeriodType.MONTH:
          item = DateRange.month();
          break;
        default:
          item = DateRange.month();
      }
      widget.listener.onDateRangeChange(item);
    }
  }

  void _showCustomPeriodDialog(DateTime startDate, DateTime endDate) {
    if (startDate == null) {
      startDate = DateTime.now().subtract(Duration(days: 1));
    }

    if (endDate == null) {
      endDate = DateTime.now();
    }

    showDialog(
        context: context,
        builder: (_) {
          return PeriodSelectorDialog(
            startDate: startDate,
            endDate: endDate,
            onPeriodChanged: _onCustomPeriodSelected,
          );
        });
  }

  void _onCustomPeriodSelected(DateTime startDate, DateTime endDate) {
    DateRange customDateRangeItem = DateRange(
        period: Period(startDate: startDate, endDate: endDate),
        periodType: PeriodType.CUSTOM);
    widget.listener.onDateRangeChange(customDateRangeItem);
  }

  Widget _titleWidget(String title) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Text(
        Localization
            .of(context)
            .allCategories,
        overflow: TextOverflow.fade,
        style: TextStyle(
            color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.w500),
      ),
    );
  }

  Widget _updatedLabel(DateTime relevantAt) {
    return Text(
      timeago.format(relevantAt),
      style: TextStyle(
          fontSize: 13.0,
          fontWeight: FontWeight.w500,
          color: Colors.white.withAlpha(120)),
    );
  }

  @override
  void onCustomPeriodClick(Period period) {
    _showCustomPeriodDialog(period.startDate, period.endDate);
  }

  @override
  void onRangeSelected(DateRange dateRangeItem) {
    widget.listener.onDateRangeChange(dateRangeItem);
  }

  @override
  void onResetCustomPeriod() {
    widget.listener.onDateRangeChange(DateRange.month());
  }

  @override
  void onPeriodChanged(PeriodType type, DateRange selectedDateRange) {
    _onPeriodChanged(type, selectedDateRange);
  }

  @override
  bool get wantKeepAlive => true;
}
