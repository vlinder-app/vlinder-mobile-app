import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/categories/list/categories_base_item.dart';
import 'package:atoma_cash/src/categories/list/goal_cell.dart';
import 'package:atoma_cash/src/home/widgets/double_tab_indicator.dart';
import 'package:atoma_cash/src/widgets/total_spending_cell.dart';
import 'package:flutter/material.dart';

class CategoriesScreen extends StatefulWidget {
  const CategoriesScreen({Key key}) : super(key: key);

  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen>
    with TickerProviderStateMixin, AutomaticKeepAliveClientMixin {
  static const List<String> _tabTitles = ["Categories", "Activity"];
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: _tabTitles.length, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        centerTitle: true,
        actions: _appBarActions(context),
        title: DoubleTabIndicator(
          _tabController,
          tabTitles: _tabTitles,
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        children: _tabTitles.map((i) => _placeholderWidget(i)).toList(),
      ),
    );
  }

  void _onSyncClicked() {}

  Widget _placeholderWidget(String text) {
    return Container(
      child: ListView(
        children: <Widget>[
          TotalSpendingCell(),
          GoalCell("Tesla Model S", "\$14400 of \$160000", "9%", 0.09),
          CategoriesBaseItem(
            leading: Image.asset(Assets.incomeIcon),
            title: "Income",
            trailing: "+ \$ 15",
            subTrailing: "Today",
          ),
          CategoriesBaseItem(
            leading: Image.asset(Assets.savingsIcon),
            title: "Savings",
            subtitle: "Last month \$ 0",
            trailing: "\$ 125.25",
            subTrailing: "This month",
          ),
          CategoriesBaseItem(
            leading: Image.asset(Assets.shoppingIcon),
            title: "Shopping",
            subtitle: "€500.19 over expense",
            trailing: "€1,200.19",
            subTrailing: "€700 budget",
            subtitleColor: Palette.red,
          ),
          CategoriesBaseItem(
            leading: Image.asset(Assets.categoryFood),
            title: "Food and drinks",
            trailing: "€23.43",
          ),
          Center(
              child: Text(
            text,
            style: TextStyle(
                fontSize: 18.0,
                color: Palette.mediumGrey,
                fontWeight: FontWeight.w600),
          )),
        ],
      ),
    );
  }

  List<Widget> _appBarActions(BuildContext context) {
    return [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: IconButton(
          icon: Image.asset(
            Assets.updateIcon,
            color: Colors.black,
          ),
          onPressed: _onSyncClicked,
        ),
      ),
    ];
  }
}
