import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:flutter/material.dart';

typedef PeriodClickCallback = void Function(DateRange dateRangeItem);

class PeriodListItem extends StatelessWidget {
  final bool isLast;
  final DateRange item;
  final bool isActive;
  final PeriodClickCallback onClick;

  const PeriodListItem(
      {Key key,
      @required this.isLast,
      @required this.item,
      @required this.isActive,
      @required this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: isLast ? 16.0 : 0.0, left: 16.0),
      child: InkWell(
        onTap: () => onClick(item),
        child: Container(
          padding: EdgeInsets.only(left: 16.0, right: 16.0),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: isActive ? Colors.black12 : Colors.transparent),
          child: Center(
            child: Text(
              item.rangeString(),
              style: TextStyle(
                  color: isActive ? Colors.white : Colors.white.withAlpha(100)),
            ),
          ),
        ),
      ),
    );
  }
}
