import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/src/reports/reports_list/bloc/bloc.dart';

class ReportsSorter {
  static int _impactSort(Report a, Report b) {
    if (a.carbonFootprintPercentage == null ||
        b.carbonFootprintPercentage == null) {
      print("Impact sort failed: One/both carbonFootprintPercentage are null");
      return 0;
    } else {
      return a.carbonFootprintPercentage.compareTo(b.carbonFootprintPercentage);
    }
  }

  static int _expensesSort(Report a, Report b) =>
      a.amountPercentage.compareTo(b.amountPercentage);

  static int Function(Report, Report) reportsSortForMode(Mode mode) {
    switch (mode) {
      case Mode.EXPENSES:
        return _expensesSort;
      case Mode.IMPACT:
        return _impactSort;
    }
  }
}
