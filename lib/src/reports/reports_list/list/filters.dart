import 'package:atoma_cash/models/api/api/categories/transactions_type.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/src/reports/reports_list/bloc/bloc.dart';

class Filters {
  static List<Report> filteredReports(List<Report> reports, Mode mode) {
    if (mode == Mode.IMPACT) {
      return List<Report>.from(reports
          .where((r) => r.carbonFootprint != null && r.carbonFootprint != 0.0)
          .toList());
    } else {
      return List<Report>.from(reports);
    }
  }

  static List<Report> reportsForCategoryType(List<Report> reports,
      TransactionsType type) {
    return List<Report>.from(
        reports.where((report) => report.category.transactionsType == type));
  }
}
