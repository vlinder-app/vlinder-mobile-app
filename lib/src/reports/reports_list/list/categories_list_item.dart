import 'package:atoma_cash/models/api/api/categories/spending_by_category.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/categories/list/categories_base_item.dart';
import 'package:atoma_cash/src/categories/list/category_icon.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:flutter/material.dart';

class CategoriesListItem extends StatelessWidget {
  final SpendingByCategory category;
  final VoidCallback onClick;

  const CategoriesListItem(this.category, {Key key, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CategoriesBaseItem(
      title: category.category.name,
      subtitleColor: Palette.red,
      subtitle: _amountOverExpenseText(context),
      subTrailing: _limitText(context),
      trailing: category.budget.spending.currencyFormatted(),
      leading: CategoryIcon(category),
      bottom: _carbonFootprintWidget(category.budget.carbonFootprint),
      onClick: onClick,
    );
  }

  String _amountOverExpenseText(BuildContext context) {
    return category.budget.amountOverExpense.value > 0
        ? Localization.of(context)
            .legacyOverExpense
        : null;
  }

  String _limitText(BuildContext context) {
    return category.budget.limit.value > 0
        ? Localization.of(context)
            .legacyBudget
        : null;
  }

  Widget _carbonFootprintWidget(double carbonFootprint) {
    Widget carbonWidget;
    if (carbonFootprint != null) {
      carbonWidget = Text(
        _carbonFootprintLocalizedValue(carbonFootprint),
        overflow: TextOverflow.ellipsis,
        softWrap: false,
        maxLines: 1,
        style: FourRowListTile.subtitleTextStyle.copyWith(
            color: carbonFootprint > 0.0 ? Colors.green : Palette.mediumGrey),
      );
    }
    return carbonWidget;
  }

  String _carbonFootprintLocalizedValue(double carbonFootprint) {
    String prefix = carbonFootprint > 0.0 ? "+ " : "";
    return "$prefix\u2618${carbonFootprint.toInt().abs()}g";
  }
}
