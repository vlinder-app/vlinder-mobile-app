import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/src/activity/widgets/listview/group_separator.dart';
import 'package:atoma_cash/src/activity/widgets/listview/transaction_cell.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';

class TransactionsList extends StatelessWidget {
  final List<Transaction> transactions;
  final void Function(Transaction transaction) onTransactionClick;

  const TransactionsList(
      {Key key, @required this.transactions, this.onTransactionClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GroupedListView<Transaction, DateTime>(
      separator: Divider(
        height: 0.0,
        color: Colors.transparent,
        thickness: 0.0,
      ),
      padding: EdgeInsets.only(top: 16.0),
      groupBy: _groupTrigger,
      shrinkWrap: true,
      elements: transactions,
      sort: false,
      physics: NeverScrollableScrollPhysics(),
      groupSeparatorBuilder: (DateTime dateTime) => Padding(
        padding: const EdgeInsets.only(left: 24.0, right: 24.0),
        child: GroupSeparator(
            DateHelper.separatorStringForDateTime(context, dateTime)
                .toUpperCase()),
      ),
      itemBuilder: (context, transaction) {
        return TransactionCell(
          transaction,
          shouldColorizeCategory: false,
          onClick: () => onTransactionClick(transaction),
          amountInBase: true,
        );
      },
    );
  }

  DateTime _groupTrigger(Transaction transaction) {
    return DateHelper.defaultGroupTrigger(transaction.bookedAt);
  }
}
