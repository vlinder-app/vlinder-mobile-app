import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/src/reports/income_report/bloc/bloc.dart';
import 'package:atoma_cash/src/reports/widgets/filter_cell.dart';
import 'package:atoma_cash/utils/reports_helper.dart';
import 'package:flutter/material.dart';

import 'income_fraction_view.dart';

class FractionFilterView extends StatelessWidget {
  final ReportsState state;
  final VoidCallback onResetFilter;
  final void Function(Report report) onColumnClick;

  const FractionFilterView(
      {Key key, @required this.state, this.onResetFilter, this.onColumnClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    const EdgeInsets sectionMargin = const EdgeInsets.only(top: 16.0);
    if (state.filterCategoryId != null) {
      return Container(
        margin: sectionMargin,
        child: FilterCell(
          text: state.reports.reports.first.category.name,
          onDelete: onResetFilter,
        ),
      );
    } else {
      return Container(
        margin: sectionMargin,
        child: IncomeFractionView(
          reports: state.incomeReports,
          maxIncome:
              ReportsHelper.extractHighestAmount(state.reports.incomeReports),
          onColumnClick: onColumnClick,
        ),
      );
    }
  }
}
