import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/src/widgets/fraction_view.dart';
import 'package:flutter/material.dart';

typedef OnFractionClick = void Function(Report report);

class IncomeFractionView extends FractionView<Report> {
  static const double _kViewFractionMaxHeight = 136.0;
  static const double kIncomeFractionViewHeight = 181.0;

  final List<Report> reports;
  final double maxIncome;
  final OnFractionClick onColumnClick;

  const IncomeFractionView({
    Key key,
    @required this.reports,
    @required this.maxIncome,
    this.onColumnClick,
  }) : super(key: key);

  @override
  String fractionColor(Report report) {
    return report.category.color;
  }

  @override
  double height() => kIncomeFractionViewHeight;

  @override
  double fractionHeight(Report report) {
    return (_kViewFractionMaxHeight * report.amount.value.abs() / maxIncome);
  }

  @override
  String fractionValue(BuildContext context, Report report) {
    return report.amount
        .copyWith(value: report.amount.value.abs())
        .currencyShortFormatted();
  }

  @override
  List<Report> get fractions => reports;

  @override
  String subtitle(BuildContext context, report) => report.category.name;

  @override
  void onFractionClick(Report report) {
    onColumnClick(report);
  }
}
