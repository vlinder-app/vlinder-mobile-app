import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/filter_by_income_cat_event.dart';
import 'package:atoma_cash/src/home/widgets/list/snapping_scroll_physics.dart';
import 'package:atoma_cash/src/reports/income_report/bloc/bloc.dart';
import 'package:atoma_cash/src/reports/income_report/widgets/fraction_filter_view.dart';
import 'package:atoma_cash/src/reports/income_report/widgets/income_fraction_view.dart';
import 'package:atoma_cash/src/reports/income_report/widgets/list/transactions_list.dart';
import 'package:atoma_cash/src/reports/mixins/reports_placeholder_mixin.dart';
import 'package:atoma_cash/src/reports/report_details/widgets/category_header_title.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/header.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/placeholder/empty_period_reports_placeholder.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/placeholder/empty_periods_placeholder.dart';
import 'package:atoma_cash/src/reports/widgets/header_stat_cell.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/src/widgets/data_load_failed.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class IncomeReportScreen extends StatefulWidget {
  final Reports reports;
  final DateRange dateRange;

  const IncomeReportScreen(
      {Key key, @required this.reports, @required this.dateRange})
      : super(key: key);

  @override
  _IncomeReportScreenState createState() => _IncomeReportScreenState();
}

class _IncomeReportScreenState extends State<IncomeReportScreen>
    with ReportsPlaceholderMixin
    implements IHeaderEventsListener {
  static const _headerHeight = 250.0;

  TapGestureRecognizer _showActivityRecognizer;
  ReportsBloc _bloc;

  SnappingScrollPhysics _scrollPhysics =
      SnappingScrollPhysics(midScrollOffset: 240.0);

  @override
  void initState() {
    _showActivityRecognizer = TapGestureRecognizer();
    _bloc = ReportsBloc()
      ..add(Started(reports: widget.reports, dateRange: widget.dateRange));
    super.initState();
  }

  @override
  void dispose() {
    _showActivityRecognizer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _bloc,
      listener: _blocListener,
      child: BlocBuilder(
        bloc: _bloc,
        builder: _rootWidgetBuild,
      ),
    );
  }

  void _blocListener(BuildContext context, ReportsState state) {}

  Widget _rootWidgetBuild(BuildContext context, ReportsState state) {
    return Scaffold(
      body: CustomScrollView(
        shrinkWrap: false,
        physics: _scrollPhysics,
        slivers: <Widget>[
          HeaderWidget(
            height: _headerHeight,
            dateRange: state.dateRangeItem,
            listener: this,
            child: Padding(
              padding: Dimensions.leftRight24Padding.copyWith(top: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  CategoryHeaderTitle(title: Localization.of(context).income),
                  HeaderStatCell(
                    title: Localization.of(context).totalIncome,
                    subtitle: state.reports.totalIncome.currencyFormatted(),
                  )
                ],
              ),
            ),
          ),
          ..._buildRootContent(context, state),
          SliverToBoxAdapter(
            child: Container(
              height: _bottomSpacerHeight(context, state),
              color: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _fillRemainingSliver(BuildContext context, Widget child) {
    return [
      SliverFillRemaining(child: Center(child: child)),
    ];
  }

  List<Widget> _buildRootContent(BuildContext context, ReportsState state) {
    if (state.isLoading) {
      return _fillRemainingSliver(context, CircularProgressIndicator());
    } else {
      if (state.error != null) {
        return _fillRemainingSliver(
          context,
          DataLoadFailedWidget(
            onReloadClick: _onReload,
          ),
        );
      } else {
        if (state.reports.incomeReports.isEmpty) {
          return _emptyReportsPlaceholder(context, state);
        } else {
          return _incomeStatsBuilder(context, state);
        }
      }
    }
  }

  List<Widget> _emptyReportsPlaceholder(BuildContext context,
      ReportsState state) {
    _showActivityRecognizer.onTap = () {
      onLastTransactionPeriodClick(context);
    };
    if (state.reports.lastTransaction != null) {
      return _emptyTransactionListPlaceholder(state.reports.lastTransaction);
    } else {
      return _fillRemainingSliver(context, EmptyReportsPlaceholder());
    }
  }

  List<Widget> _incomeStatsBuilder(BuildContext context, ReportsState state) {
    return [
      SliverToBoxAdapter(
        child: _fractionFilterIncomeView(context, state),
      ),
      SliverList(
          delegate: SliverChildListDelegate(
              [_transactionListViewBuilder(context, state.transactions)]))
    ];
  }

  List<Widget> _emptyTransactionListPlaceholder(Transaction lastTransaction) {
    return _fillRemainingSliver(
        context,
        EmptyPeriodReportsPlaceholder(
          lastTransactionDateTime: lastTransaction.bookedAt?.toLocal(),
          dateTimeGestureRecognizer: _showActivityRecognizer,
          onShowMeClick: () => onShowLastTransactionPeriodClick(
              context, lastTransaction.bookedAt),
        ));
  }

  Widget _transactionListViewBuilder(BuildContext context,
      List<Transaction> transactions) {
    return TransactionsList(
      transactions: transactions,
      onTransactionClick: _onTransactionClick,
    );
  }

  Widget _fractionFilterIncomeView(BuildContext context, ReportsState state) =>
      FractionFilterView(
        state: state,
        onResetFilter: _onResetFilter,
        onColumnClick: _onCategoryFractionClick,
      );

  double _bottomSpacerHeight(BuildContext context, ReportsState state) {
    double categoriesListHeight =
        state.reports.incomeReports.length * FourRowListTile.twoRowHeight;
    double contentHeight =
        categoriesListHeight + IncomeFractionView.kIncomeFractionViewHeight;
    return Dimensions.bottomSpacerHeight(context, _headerHeight, contentHeight);
  }

  @override
  void onDateRangeChange(DateRange dateRangeItem) {
    _bloc.add(DateRangeChanged(dateRangeItem));
  }

  void _onReload() {
    _bloc.add(Update());
  }

  void _onResetFilter() {
    _bloc.add(FilterSelected(null));
  }

  void _onCategoryFractionClick(Report report) {
    Analytics().logEvent(FilterByIncomeCatEvent());
    _bloc.add(FilterSelected(report.category.id));
  }

  void _onTransactionClick(Transaction transaction) {
    _showTransactionDetails(transaction);
  }

  void _showTransactionDetails(Transaction transaction) async {
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.transactionDetailsScreen,
        arguments: TransactionDetailsScreenArguments(transaction: transaction));
    /*
     * Possible actions and models - Remove action & Transaction
     */
    if (result != null) {
      _onResetFilter();
      _onReload();
    }
  }
}
