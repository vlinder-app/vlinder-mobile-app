import 'dart:async';

import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/categories/transactions_type.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:bloc/bloc.dart';

import 'bloc.dart';

class ReportsBloc extends Bloc<ReportsEvent, ReportsState> {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();

  @override
  ReportsState get initialState {
    return ReportsState(
        dateRangeItem: DateRange.month(),
        isLoading: true,
        reports: Reports(reports: []),
        transactions: []);
  }

  @override
  Stream<ReportsState> mapEventToState(
    ReportsEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapHomeStartedToState(event.reports, event.dateRange);
    }
    if (event is Update) {
      yield* _mapUpdateToState();
    }
    if (event is DateRangeChanged) {
      yield* _mapPeriodChangedToState(event.item);
    }
    if (event is FilterSelected) {
      yield* _mapFilterSelectedToState(event.filterCategoryId);
    }
  }

  Stream<ReportsState> _mapHomeStartedToState(
      Reports reports, DateRange dateRange) async* {
    yield state.copyWith(dateRangeItem: dateRange);
    add(Update());
  }

  Stream<ReportsState> _mapUpdateToState() async* {
    yield state.copyWith(isLoading: true);
    var reportsRequest = _apiRemoteRepository.getReports(
        state.dateRangeItem.period,
        categoryIds: [state.filterCategoryId]);
    var lastRequest = _apiRemoteRepository.getLastTransaction(
        transactionsType: TransactionsType.INCOME);
    var response = await Future.wait([reportsRequest, lastRequest]);
    if (response.every((request) => request.isSuccess())) {
      Reports reports = response[0].result as Reports;
      yield state.copyWith(
        transactions: _aggregatedTransactions(reports.incomeReports),
        reports: reports.copyWith(lastTransaction: response[1].result),
        isLoading: false,
      );
    } else {
      yield state.copyWith(
          error: response
              .firstWhere((response) => !response.isSuccess())
              .error,
          isLoading: false);
    }
  }

  Stream<ReportsState> _mapFilterSelectedToState(String categoryId) async* {
    if (categoryId != null) {
      yield state.copyWith(filterCategoryId: categoryId);
    } else {
      yield state.resetFilters();
    }
    add(Update());
  }

  Stream<ReportsState> _mapPeriodChangedToState(DateRange item) async* {
    yield state.copyWith(dateRangeItem: item);
    add(Update());
  }

  List<Transaction> _aggregatedTransactions(List<Report> reports) {
    List<Transaction> aggregatedTransactionsList = [];
    reports.forEach((r) => aggregatedTransactionsList.addAll(r.transactions));
    return aggregatedTransactionsList
      ..sort((b, a) => a.bookedAt.compareTo(b.bookedAt));
  }
}
