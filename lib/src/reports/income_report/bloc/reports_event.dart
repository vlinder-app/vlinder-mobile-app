import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

abstract class ReportsEvent extends Equatable {
  const ReportsEvent();
}

class Started extends ReportsEvent {
  final Reports reports;
  final DateRange dateRange;

  Started({@required this.reports, @required this.dateRange});

  @override
  List<Object> get props => [reports, dateRange];
}

class DateRangeChanged extends ReportsEvent {
  final DateRange item;

  DateRangeChanged(this.item);

  @override
  List<Object> get props => [item];
}

class Update extends ReportsEvent {
  @override
  List<Object> get props => null;
}

class FilterSelected extends ReportsEvent {
  final String filterCategoryId;

  FilterSelected(this.filterCategoryId);

  @override
  List<Object> get props => [filterCategoryId];
}
