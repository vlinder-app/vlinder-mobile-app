import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/error/error.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class BaseReportsState extends Equatable {
  const BaseReportsState();
}

class ReportsState extends BaseReportsState {
  final DateRange dateRangeItem;
  final List<Transaction> transactions;
  final String filterCategoryId;
  final Reports reports;
  final bool isLoading;
  final Error error;

  List<Report> get incomeReports =>
      List<Report>.from(reports.incomeReports)
        ..sort((b, a) => a.amount.value.compareTo(b.amount.value));

  ReportsState({@required this.dateRangeItem,
    this.reports,
    this.transactions,
    this.isLoading,
    this.error,
    this.filterCategoryId});

  ReportsState copyWith(
          {DateRange dateRangeItem,
            String filterCategoryId,
            List<Transaction> transactions,
          Reports reports,
          bool isLoading,
            Transaction lastTransaction,
          Error error}) =>
      ReportsState(
          filterCategoryId: filterCategoryId ?? this.filterCategoryId,
          transactions: transactions ?? this.transactions,
          error: error ?? this.error,
          dateRangeItem: dateRangeItem ?? this.dateRangeItem,
          isLoading: isLoading ?? this.isLoading,
          reports: reports ?? this.reports);

  ReportsState resetFilters() =>
      ReportsState(
          dateRangeItem: this.dateRangeItem,
          reports: this.reports,
          transactions: this.transactions,
          isLoading: this.isLoading,
          error: this.error,
          filterCategoryId: null);

  @override
  List<Object> get props =>
      [
        dateRangeItem,
        reports,
        isLoading,
        error,
        transactions,
        filterCategoryId
      ];

  @override
  String toString() {
    return 'ReportsState{dateRangeItem: $dateRangeItem, filterCategoryId: $filterCategoryId, isLoading: $isLoading, error: $error}';
  }
}
