import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:flutter/material.dart';

class FilterCell extends StatelessWidget {
  final String text;
  final VoidCallback onDelete;
  final Color backgroundColor;

  const FilterCell(
      {Key key,
      @required this.text,
      this.onDelete,
      this.backgroundColor = Palette.mediumGrey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: Dimensions.leftRight24Padding,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Chip(
            shape: RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(10.0)),
            backgroundColor: backgroundColor,
            labelPadding: EdgeInsets.all(5.0),
            deleteIcon: Icon(
              Icons.clear,
              size: 16.0,
            ),
            deleteIconColor: Colors.white,
            label: Text(
              text,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w500,
                  fontSize: 13.0),
            ),
            onDeleted: onDelete,
          ),
        ],
      ),
    );
  }
}
