import 'package:atoma_cash/src/reports/models.dart';
import 'package:atoma_cash/src/reports/widgets/mode_toggle_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ReportsAppBarActionList {
  static List<Widget> actionsList(Mode mode, VoidCallback onToggle) {
    return [
      ModeToggleButton(
        mode: mode,
        onClick: onToggle,
      ),
    ];
  }
}
