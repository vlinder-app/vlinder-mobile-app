import 'package:flutter/material.dart';

class HeaderStatCell extends StatelessWidget {
  static const _height = 65.0;

  final String title;
  final String subtitle;
  final double height;

  const HeaderStatCell(
      {Key key,
      @required this.title,
      @required this.subtitle,
      this.height = _height})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 8.0),
      height: _height,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            title,
            style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w500,
                fontSize: 14.0),
          ),
          Text(
            subtitle,
            style: const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w600,
                fontSize: 24.0),
          )
        ],
      ),
    );
  }
}
