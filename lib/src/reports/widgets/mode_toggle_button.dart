import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/reports/models.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:flutter/material.dart';

class ModeToggleButton extends StatelessWidget {
  final Mode mode;
  final VoidCallback onClick;

  const ModeToggleButton({Key key, @required this.mode, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    String buttonText;
    switch (mode) {
      case Mode.EXPENSES:
        buttonText = Localization.of(context).viewImpactButton;
        break;
      case Mode.IMPACT:
        buttonText = Localization.of(context).viewExpensesButton;
        break;
    }
    return Widgets.styledFlatButton(
        text: buttonText, onPressed: onClick, textColor: Colors.white);
  }
}
