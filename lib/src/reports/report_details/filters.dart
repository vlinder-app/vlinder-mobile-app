import 'package:atoma_cash/models/api/api/categories/transactions_type.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/src/reports/report_details/sorters.dart';
import 'package:atoma_cash/src/reports/reports_list/bloc/bloc.dart';

class Filters {
  static List<Transaction> filteredTransactions(List<Transaction> transactions,
      Mode mode) {
    if (mode == Mode.IMPACT) {
      return List<Transaction>.from(transactions)
          .where((r) => r.carbonFootprint != null && r.carbonFootprint != 0.0)
          .toList();
    } else {
      return List<Transaction>.from(transactions);
    }
  }

  static List<Transaction> expenses(List<Transaction> transactions,
      Mode mode) =>
      filteredTransactions(transactions, mode)
          .where((r) =>
      r.categories.first.transactionsType == TransactionsType.EXPENSES)
          .toList()
        ..sort(TransactionsSorter.reportsSortForMode(mode));

  static List<Transaction> filteredSortedReportsForMode(
      List<Transaction> transactions, Mode mode) {
    var filteredReports = Filters.filteredTransactions(transactions, mode)
      ..sort(TransactionsSorter.reportsSortForMode(mode));
    return filteredReports.reversed.toList();
  }
}
