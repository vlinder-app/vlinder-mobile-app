import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/activity/widgets/listview/group_separator.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/filter_by_merchant_event.dart';
import 'package:atoma_cash/src/home/widgets/list/snapping_scroll_physics.dart';
import 'package:atoma_cash/src/reports/mixins/reports_placeholder_mixin.dart';
import 'package:atoma_cash/src/reports/report_details/widgets/category_header_title.dart';
import 'package:atoma_cash/src/reports/report_details/widgets/fraction_filters_section.dart';
import 'package:atoma_cash/src/reports/report_details/widgets/transaction_item.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/header.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/header_stats.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/placeholder/basic_text_placeholder.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/placeholder/empty_period_reports_placeholder.dart';
import 'package:atoma_cash/src/reports/reports_list/widgets/placeholder/empty_periods_placeholder.dart';
import 'package:atoma_cash/src/reports/widgets/actions_list.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/src/transaction/transaction_operations/transaction_operations_screen.dart';
import 'package:atoma_cash/src/widgets/data_load_failed.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/src/widgets/fraction_view.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grouped_list/grouped_list.dart';

import '../models.dart';
import 'bloc/bloc.dart';

class ReportDetailsScreen extends StatefulWidget {
  final Category category;
  final DateRange dateRange;
  final Mode mode;

  const ReportDetailsScreen({
    Key key,
    this.category,
    this.dateRange,
    this.mode,
  }) : super(key: key);

  @override
  _ReportDetailsScreenState createState() =>
      _ReportDetailsScreenState(category, dateRange, mode);
}

class _ReportDetailsScreenState extends State<ReportDetailsScreen>
    with AutomaticKeepAliveClientMixin, ReportsPlaceholderMixin
    implements IHeaderEventsListener {
  static const _headerHeight = 250.0;

  final Category category;
  final DateRange dateRange;
  final Mode mode;

  TapGestureRecognizer _showActivityRecognizer;

  SnappingScrollPhysics _scrollPhysics =
      SnappingScrollPhysics(midScrollOffset: 240.0);

  ReportDetailsBloc _bloc;

  _ReportDetailsScreenState(this.category, this.dateRange, this.mode);

  @override
  void initState() {
    _showActivityRecognizer = TapGestureRecognizer();
    _bloc = ReportDetailsBloc()..add(Started(category, dateRange, mode));
    super.initState();
  }

  @override
  void dispose() {
    _showActivityRecognizer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder(
      bloc: _bloc,
      builder: _rootWidgetForState,
    );
  }

  Widget _rootWidgetForState(BuildContext context, ReportState state) {
    if (state is ReportState) {
      return _loadedWidget(context, state);
    } else {
      return Container();
    }
  }

  double _bottomSpacerHeight(BuildContext context, ReportState state) {
    double contentMargin = -50.0;

    double categoriesListHeight =
        _transactionsListHeight(state.filteredTransactionsByMode);
    double contentHeight =
        categoriesListHeight + contentMargin + FractionView.fullHeight;

    return Dimensions.bottomSpacerHeight(context, _headerHeight, contentHeight);
  }

  double _transactionsListHeight(List<Transaction> reports) {
    double summaryHeight = 0.0;
    reports.forEach((c) {
      summaryHeight += FourRowListTile.twoRowHeight;
    });
    return summaryHeight;
  }

  List<Widget> _fillRemainingSliver(BuildContext context, Widget child) {
    return [
      SliverFillRemaining(child: Center(child: child)),
    ];
  }

  List<Widget> _rootContent(BuildContext context, ReportState state) {
    return [
      SliverToBoxAdapter(
        child: Container(
            margin: EdgeInsets.only(top: 16.0, bottom: 16.0),
            child: FractionFiltersSection(
              state: state,
              onDeleteFilter: _onFilterDelete,
              onColumnClick: _onFractionClick,
            )),
      ),
      SliverList(
        delegate: SliverChildListDelegate([
          _transactionsListBuilder(state.mode, state.report.transactions,
              state.dateRange, state.merchantNameFilter)
        ]),
      ),
      SliverToBoxAdapter(
        child: Container(
          height: _bottomSpacerHeight(context, state),
          color: Colors.white,
        ),
      ),
    ];
  }

  List<Widget> _buildRootContent(BuildContext context, ReportState state) {
    if (state.isLoading) {
      return _fillRemainingSliver(context, CircularProgressIndicator());
    } else {
      if (state.report?.transactions == null) {
        return _fillRemainingSliver(
          context,
          DataLoadFailedWidget(
            onReloadClick: () => _onReload(),
          ),
        );
      } else {
        if (state.report.transactions.isEmpty) {
          _showActivityRecognizer.onTap = () {
            onLastTransactionPeriodClick(context);
          };
          if (state.lastTransaction != null) {
            return _emptyTransactionListPlaceholder(
                context, state.lastTransaction);
          } else {
            return _fillRemainingSliver(context, EmptyReportsPlaceholder());
          }
        } else {
          if (state.filteredTransactionsByMode.isEmpty) {
            if (state.mode == Mode.IMPACT) {
              return _fillRemainingSliver(
                  context,
                  BasicTextPlaceholder(
                    text: Localization
                        .of(context)
                        .noImpactForSelectedPeriod,
                  ));
            }
          }
          return _rootContent(context, state);
        }
      }
    }
  }

  List<Widget> _emptyTransactionListPlaceholder(BuildContext context,
      Transaction lastTransaction) {
    return _fillRemainingSliver(
        context,
        EmptyPeriodReportsPlaceholder(
          lastTransactionDateTime: lastTransaction.bookedAt?.toLocal(),
          dateTimeGestureRecognizer: _showActivityRecognizer,
          onShowMeClick: () => onShowLastTransactionPeriodClick(
              context, lastTransaction.bookedAt),
        ));
  }

  Widget _loadedWidget(BuildContext context, ReportState state) {
    return Scaffold(
      body: CustomScrollView(
        shrinkWrap: false,
        physics: _scrollPhysics,
        slivers: <Widget>[
          HeaderWidget(
            height: _headerHeight,
            dateRange: state.dateRange,
            listener: this,
            appBarActions:
                ReportsAppBarActionList.actionsList(state.mode, _onToggleMode),
            child: Padding(
              padding: Dimensions.leftRight24Padding.copyWith(top: 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  CategoryHeaderTitle(
                      title: state.report?.category?.name ?? "..."),
                  HeaderStats(
                    mode: state.mode,
                    amount: state.report?.amount,
                    carbonFootprint: state.report?.carbonFootprint,
                  ),
                ],
              ),
            ),
          ),
          ..._buildRootContent(context, state),
        ],
      ),
    );
  }

  Widget _transactionsListBuilder(Mode mode, List<Transaction> transactions,
      DateRange dateRange, String filter) {
    return GroupedListView<Transaction, DateTime>(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      separator: Divider(
        height: 0.0,
        color: Colors.transparent,
        thickness: 0.0,
      ),
      padding: EdgeInsets.only(top: 0.0),
      groupBy: _groupTrigger,
      elements: transactions,
      sort: false,
      groupSeparatorBuilder: (DateTime dateTime) =>
          Padding(
            padding: const EdgeInsets.only(left: 24.0, right: 24.0),
            child: GroupSeparator(
                DateHelper.separatorStringForDateTime(context, dateTime)
                    .toUpperCase()),
          ),
      itemBuilder: (context, transaction) {
        return TransactionItem(
          transaction: transaction,
          mode: mode,
          onClick: () => {_onTransactionClick(transaction, filter)},
        );
      },
    );
  }

  DateTime _groupTrigger(Transaction transaction) {
    return DateHelper.defaultGroupTrigger(transaction.bookedAt);
  }

  void _onFilterDelete() => _bloc.add(FilterSelected(null));

  void _onTransactionClick(Transaction transaction, String merchantNameFilter) {
    _openTransactionDetails(transaction);
  }

  void _onFractionClick(Transaction transaction) {
    Analytics().logEvent(FilterByMerchantEvent());
    _bloc.add(FilterSelected(transaction.counterparty.name));
  }

  void _openTransactionDetails(Transaction transaction) async {
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.transactionDetailsScreen,
        arguments: TransactionDetailsScreenArguments(transaction: transaction));
    if (result != null) {
      if (result is EditOperation) {
        _onFilterDelete();
        _onReload();
      }
      if (result is UpdateOperation) {
        _onReload(silently: true);
      }
    }
    /*
     * Available actions
     *
    if (result is RemoveAction) {
      onReload();
    }
    if (result is Transaction) {
      onReload();
    }
     */
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void onDateRangeChange(DateRange dateRangeItem) {
    _bloc.add(DateRangeChanged(dateRangeItem));
  }

  void _onReload({bool silently = false}) {
    _bloc.add(Refresh(silent: silently));
  }

  void _onToggleMode() {
    _bloc.add(ToggleMode());
  }
}
