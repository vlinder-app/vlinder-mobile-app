import 'package:atoma_cash/src/home_placeholder/home_placeholder.dart';

abstract class BaseResult {}

class SwitchToTabResult extends BaseResult {
  final TabId tabId;

  SwitchToTabResult(this.tabId);
}
