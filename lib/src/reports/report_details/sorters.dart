import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/src/reports/reports_list/bloc/bloc.dart';

class TransactionsSorter {
  static int _impactSort(Transaction b, Transaction a) =>
      a.carbonFootprint.compareTo(b.carbonFootprint);

  static int _expensesSort(Transaction a, Transaction b) =>
      a.amount.value.compareTo(b.amount.value);

  static int baseAmountSort(Transaction a, Transaction b) =>
      a.baseCurrencyAmount.value.compareTo(b.baseCurrencyAmount.value);

  static int Function(Transaction, Transaction) reportsSortForMode(Mode mode) {
    switch (mode) {
      case Mode.EXPENSES:
        return _expensesSort;
      case Mode.IMPACT:
        return _impactSort;
    }
  }
}
