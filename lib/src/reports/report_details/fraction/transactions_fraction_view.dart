import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/reports/reports_list/bloc/bloc.dart';
import 'package:atoma_cash/src/widgets/fraction_view.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:flutter/material.dart';

typedef OnFractionClick = void Function(Transaction transaction);

class TransactionsFractionView extends FractionView<Transaction> {
  static const double _kViewFractionMaxHeight = 136.0;
  static const double _kTransactionsFractionViewHeight = 181.0;

  final Mode mode;
  final List<Transaction> transactions;
  final double highestSpent;
  final double highestCarbonFootprint;
  final OnFractionClick onColumnClick;

  const TransactionsFractionView({
    Key key,
    @required this.mode,
    @required this.transactions,
    @required this.highestSpent,
    @required this.highestCarbonFootprint,
    this.onColumnClick,
  }) : super(key: key);

  @override
  String fractionColor(Transaction fraction) {
    return fraction.categories.first.color;
  }

  @override
  double height() => _kTransactionsFractionViewHeight;

  @override
  double fractionHeight(Transaction fraction) {
    switch (mode) {
      case Mode.EXPENSES:
        return (_kViewFractionMaxHeight *
            fraction.baseCurrencyAmount.value.abs() /
            highestSpent);
      case Mode.IMPACT:
        return _kViewFractionMaxHeight *
            fraction.carbonFootprint /
            highestCarbonFootprint;
      default:
        return 0.0;
    }
  }

  @override
  String fractionValue(BuildContext context, Transaction fraction) {
    switch (mode) {
      case Mode.EXPENSES:
        return fraction.baseCurrencyAmount
            .copyWith(value: fraction.baseCurrencyAmount.value.abs())
            .currencyShortFormatted();
      case Mode.IMPACT:
        return CarbonHelper.abridgedCarbonString(fraction.carbonFootprint,
            context: context);
      default:
        return "Undefined";
    }
  }

  @override
  List<Transaction> get fractions => transactions;

  @override
  String subtitle(BuildContext context, fraction) {
    if (fraction.counterparty?.name != null) {
      return fraction.counterparty.name;
    } else {
      return Localization.of(context)
          .unknownMerchantTransactionTitlePlaceholder;
    }
  }

  @override
  void onFractionClick(Transaction fraction) {
    onColumnClick(fraction);
  }
}
