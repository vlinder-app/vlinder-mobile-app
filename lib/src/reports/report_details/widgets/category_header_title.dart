import 'package:flutter/material.dart';

class CategoryHeaderTitle extends StatelessWidget {
  final String title;

  const CategoryHeaderTitle({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8.0),
      child: Text(
        title,
        style: TextStyle(
            fontSize: 20.0, fontWeight: FontWeight.w600, color: Colors.white),
      ),
    );
  }
}
