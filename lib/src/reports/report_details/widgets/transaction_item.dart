import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction_source_type.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/transaction/widgets/transaction_icon.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:flutter/material.dart';

import '../../models.dart';

class TransactionItem extends StatelessWidget {
  static const _iconSize = 54.0;

  final Transaction transaction;
  final Mode mode;
  final VoidCallback onClick;

  const TransactionItem(
      {Key key, @required this.transaction, @required this.mode, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FourRowListTile(
      forceThreeRow: true,
      leading: TransactionIcon(transaction),
      onClick: onClick,
      title: Text(
        _title(context),
        style: FourRowListTile.titleTextStyle,
        softWrap: false,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
      subtitle: Text(
        transaction.sourceType.toLocalizedString(context),
        style: FourRowListTile.subtitleTextStyle,
      ),
      trailing: Text(
        _valueForMode(context, mode),
        style: FourRowListTile.titleTextStyle,
      ),
      subTrailing: Text(
        _valueForMode(
            context, mode == Mode.IMPACT ? Mode.EXPENSES : Mode.IMPACT),
        style: FourRowListTile.subtitleTextStyle,
      ),
    );
  }

  String _valueForMode(BuildContext context, Mode mode) {
    switch (mode) {
      case Mode.EXPENSES:
        return transaction.baseOrMainAmountFormatted;
      case Mode.IMPACT:
        return CarbonHelper.carbonFootprintLocalizableValue(
            context, transaction.carbonFootprint);
      default:
        return "undefined";
    }
  }

  String _title(BuildContext context) {
    if (transaction.counterparty?.name != null) {
      return transaction.counterparty.name;
    } else {
      return Localization.of(context)
          .unknownMerchantTransactionTitlePlaceholder;
    }
  }

}
