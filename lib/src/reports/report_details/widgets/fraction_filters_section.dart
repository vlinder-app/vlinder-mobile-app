import 'dart:math';

import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/src/reports/report_details/bloc/report_details_state.dart';
import 'package:atoma_cash/src/reports/report_details/fraction/transactions_fraction_view.dart';
import 'package:atoma_cash/src/reports/widgets/filter_cell.dart';
import 'package:flutter/material.dart';

class FractionFiltersSection extends StatelessWidget {
  final ReportState state;
  final VoidCallback onDeleteFilter;
  final OnFractionClick onColumnClick;

  const FractionFiltersSection(
      {Key key,
      @required this.state,
      @required this.onDeleteFilter,
      this.onColumnClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (state.merchantNameFilter != null) {
      return FilterCell(
        text: state.merchantNameFilter,
        onDelete: onDeleteFilter,
      );
    } else {
      var transactionsList =
          state.aggregatedByMerchantTransactionsListWithBaseAmount;
      if (transactionsList.isNotEmpty) {
        return TransactionsFractionView(
          mode: state.mode,
          transactions: transactionsList,
          highestSpent: _extractHighestAmount(transactionsList),
          highestCarbonFootprint:
              _extractHighestCarbonFootprint(transactionsList),
          onColumnClick: onColumnClick,
        );
      } else {
        return Container();
      }
    }
  }

  double _extractHighestAmount(
          List<Transaction> transactions) =>
      transactions.fold(
          0.0,
          (result, transaction) =>
              max(result, transaction.baseCurrencyAmount.value.abs() ?? 0.0));

  double _extractHighestCarbonFootprint(List<Transaction> reports) =>
      reports.fold(
          0.0,
          (result, transaction) =>
              max(result, transaction.carbonFootprint ?? 0));
}
