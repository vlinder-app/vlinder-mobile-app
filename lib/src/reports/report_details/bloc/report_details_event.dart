import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:equatable/equatable.dart';

import '../../models.dart';

abstract class ReportEvent extends Equatable {
  const ReportEvent();
}

class Started extends ReportEvent {
  final Category category;
  final DateRange dateRange;
  final Mode mode;

  Started(this.category, this.dateRange, this.mode);

  @override
  List<Object> get props => [category, dateRange, mode];
}

class Refresh extends ReportEvent {
  final bool silent;

  Refresh({this.silent = false});

  @override
  List<Object> get props => [silent];
}

class ToggleMode extends ReportEvent {
  @override
  List<Object> get props => null;
}

class LastTransactionDateTimeSelected extends ReportEvent {
  final DateTime lastTransactionDateTime;

  LastTransactionDateTimeSelected(this.lastTransactionDateTime);

  @override
  List<Object> get props => [lastTransactionDateTime];
}

class DateRangeChanged extends ReportEvent {
  final DateRange item;

  DateRangeChanged(this.item);

  @override
  List<Object> get props => [item];
}

class FilterSelected extends ReportEvent {
  final String merchantName;

  FilterSelected(this.merchantName);

  @override
  List<Object> get props => [merchantName];
}
