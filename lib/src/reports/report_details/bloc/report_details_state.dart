import 'package:atoma_cash/extensions/double_extensions.dart';
import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/error/error.dart';
import 'package:atoma_cash/src/reports/report_details/filters.dart';
import 'package:atoma_cash/src/reports/report_details/sorters.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

import '../../models.dart';

@immutable
abstract class BaseReportState extends Equatable {
  const BaseReportState();
}

class ReportState extends BaseReportState {
  final DateRange dateRange;
  final Report report;
  final Mode mode;
  final bool isLoading;
  final Error error;
  final String merchantNameFilter;
  final Transaction lastTransaction;

  List<Transaction> get filteredTransactionsByMode =>
      Filters.filteredSortedReportsForMode(report.transactions, mode);

  List<Transaction> get aggregatedByMerchantTransactionsListWithBaseAmount =>
      _aggregatedByMerchantsTransactionsList(
          Filters.expenses(report.transactions, mode))
        ..sort(TransactionsSorter.baseAmountSort);

  ReportState(
      {@required this.dateRange,
      this.lastTransaction,
      this.merchantNameFilter,
      this.report,
      this.mode,
      this.isLoading,
      this.error});

  ReportState copyWith(
          {DateRange dateRangeItem,
          Transaction lastTransaction,
          String merchantNameFilter,
          Report report,
          Mode mode,
          bool isLoading,
          Error error}) =>
      ReportState(
          lastTransaction: lastTransaction ?? this.lastTransaction,
          merchantNameFilter: merchantNameFilter ?? this.merchantNameFilter,
          error: error ?? this.error,
          dateRange: dateRangeItem ?? this.dateRange,
          isLoading: isLoading ?? this.isLoading,
          mode: mode ?? this.mode,
          report: report ?? this.report);

  ReportState resetMerchantFilter() => ReportState(
      dateRange: this.dateRange,
      lastTransaction: this.lastTransaction,
      merchantNameFilter: null,
      report: this.report,
      mode: this.mode,
      isLoading: this.isLoading,
      error: this.error);

  @override
  List<Object> get props => [
        mode,
        dateRange,
        report,
        isLoading,
        error,
        merchantNameFilter,
        lastTransaction
      ];

  List<Transaction> _aggregatedByMerchantsTransactionsList(
      List<Transaction> transactions) {
    var filteredTransactionsList = transactions
        .where((element) => element.baseCurrencyAmount != null)
        .toList();
    Map<String, Transaction> aggregated = {};
    filteredTransactionsList.forEach((t) {
      String merchantName = t.counterparty?.name;
      if (aggregated.containsKey(merchantName)) {
        aggregated.update(
            merchantName,
            (oldTransaction) => oldTransaction.copyWith(
                baseCurrencyAmount: oldTransaction.baseCurrencyAmount.copyWith(
                    value: oldTransaction.baseCurrencyAmount.value +
                        t.baseCurrencyAmount.value),
                carbonFootprint: ((oldTransaction?.carbonFootprint ?? 0.0) +
                        (t.carbonFootprint ?? 0.0))
                    .toPrecision(CarbonHelper.carbonTrailingZerosCount)));
      } else {
        aggregated.putIfAbsent(merchantName, () => t);
      }
    });
    return aggregated.entries.map((f) => f.value).toList();
  }
}
