import 'dart:async';

import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/api/transactions/transactions.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/providers/currency_provider.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/change_view_mode_event.dart';
import 'package:bloc/bloc.dart';

import '../../models.dart';
import 'bloc.dart';

class ReportDetailsBloc extends Bloc<ReportEvent, ReportState> {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  BaseCurrencyProvider _baseCurrencyProvider = BaseCurrencyProvider();

  @override
  ReportState get initialState {
    return ReportState(
        dateRange: DateRange.month(), mode: Mode.EXPENSES, isLoading: true);
  }

  @override
  Stream<ReportState> mapEventToState(
    ReportEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapHomeStartedToState(
          event.mode, event.dateRange, event.category);
    }
    if (event is DateRangeChanged) {
      yield* _mapPeriodChangedToState(event.item);
    }
    if (event is ToggleMode) {
      yield* _mapToggleModeToState();
    }
    if (event is Refresh) {
      yield* _mapRefreshToState(event.silent);
    }
    if (event is FilterSelected) {
      yield* _mapFilterSelectedToState(event.merchantName);
    }
    if (event is LastTransactionDateTimeSelected) {
      yield* _mapLastTransactionDateTimeSelectedToState(
          event.lastTransactionDateTime);
    }
  }

  Stream<ReportState> _mapLastTransactionDateTimeSelectedToState(
      DateTime lastTransactionDateTime) async* {
    add(DateRangeChanged(
        state.dateRange.fromDateTime(lastTransactionDateTime)));
  }

  Stream<ReportState> _mapToggleModeToState() async* {
    yield state.copyWith(
        mode: state.mode == Mode.IMPACT ? Mode.EXPENSES : Mode.IMPACT);
    Analytics()
        .logEvent(ChangeViewModeEvent(state.mode, Source.CATEGORY_DETAILS));
  }

  Stream<ReportState> _mapRefreshToState(bool silentRefresh) async* {
    yield state.copyWith(isLoading: !silentRefresh);
    BaseResponse<Transaction> lastTransactionResponse =
        await _apiRemoteRepository.getLastTransaction();
    BaseResponse<Transactions> reportsResponse = await _apiRemoteRepository
        .getTransactions(
            period: state.dateRange.period,
            counterpartyNames: [state.merchantNameFilter],
            categoryIds: [state.report.category.id]);
    if (reportsResponse.isSuccess() && lastTransactionResponse.isSuccess()) {
      List<Transaction> transactions = reportsResponse.result.transactions;
      yield state.copyWith(
          isLoading: false,
          merchantNameFilter: state.merchantNameFilter,
          lastTransaction: lastTransactionResponse.result,
          report: state.report.copyWith(
            amount: _totalSpentForTransactions(transactions),
            carbonFootprint: _carbonTotal(transactions),
            transactions: transactions,
          ));
    } else {
      yield state.copyWith(error: reportsResponse.error, isLoading: false);
    }
  }

  Stream<ReportState> _mapPeriodChangedToState(DateRange item) async* {
    yield state.copyWith(dateRangeItem: item);
    add(Refresh());
  }

  Stream<ReportState> _mapFilterSelectedToState(String merchantName) async* {
    if (merchantName != null) {
      yield state.copyWith(merchantNameFilter: merchantName);
    } else {
      yield state.resetMerchantFilter();
    }
    add(Refresh());
  }

  Stream<ReportState> _mapHomeStartedToState(
      Mode mode, DateRange dateRange, Category category) async* {
    yield state.copyWith(
        dateRangeItem: dateRange,
        report: Report(category: category),
        mode: mode);
    add(Refresh());
  }

  Amount _totalSpentForTransactions(List<Transaction> transactions) {
    if (transactions.isNotEmpty) {
      Amount amount = transactions.first.baseCurrencyAmount;
      double totalSpent = transactions.fold(
          0.0,
          (previous, transaction) =>
              previous +
              (transaction.baseCurrencyAmount?.value?.isNegative ?? false
                  ? transaction?.baseCurrencyAmount?.value?.abs()
                  : 0.0));
      return amount.copyWith(value: totalSpent);
    } else {
      return _baseCurrencyProvider.baseCurrency.copyWith(value: 0.0);
    }
  }

  double _carbonTotal(List<Transaction> transactions) =>
      transactions.fold<double>(
          0.0,
          (previous, transaction) =>
              previous + (transaction?.carbonFootprint ?? 0.0));
}
