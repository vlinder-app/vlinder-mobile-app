import 'package:atoma_cash/src/home_placeholder/home_placeholder.dart';
import 'package:atoma_cash/src/reports/report_details/result/result.dart';
import 'package:flutter/material.dart';

@optionalTypeArgs
mixin ReportsPlaceholderMixin<T extends StatefulWidget> on State<T> {
  void onShowLastTransactionPeriodClick(
      BuildContext context, DateTime lastTransactionDateTime) {
    Navigator.of(context).pop(SwitchToTabResult(TabId.CATEGORIES));
    /*
     *   waiting for last transaction api for category
     *  _bloc.add(LastTransactionDateTimeSelected(lastTransactionDateTime));
     */
  }

  void onLastTransactionPeriodClick(BuildContext context) {
    Navigator.of(context).pop(SwitchToTabResult(TabId.ACTIVITY));
  }
}
