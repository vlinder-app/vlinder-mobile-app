import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PinSetupEvent extends Equatable {
  PinSetupEvent([List props = const []]);
}

class PinEntered extends PinSetupEvent {
  final String pin;
  final bool replaceExisting;

  PinEntered({@required this.pin, this.replaceExisting});

  @override
  String toString() {
    return 'PinEntered{pin: $pin, replaceExisting: $replaceExisting}';
  }

  @override
  List<Object> get props => [pin];
}
