import 'dart:async';

import 'package:atoma_cash/models/api/api/profile/response/profile.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/models/auth_api/auth/response/api_token.dart';
import 'package:atoma_cash/models/auth_api/auth/response/passcode_confirmation.dart';
import 'package:atoma_cash/models/auth_api/base_models/base_response.dart'
    as auth;
import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/resources/repository/remote/auth_remote_repository.dart';
import 'package:bloc/bloc.dart';

import './bloc.dart';

class PinSetupBloc extends Bloc<PinSetupEvent, PinSetupState> {
  @override
  PinSetupState get initialState => InitialPinSetupState();

  AuthRemoteRepository _authRemoteRepository;
  ApplicationLocalRepository _applicationLocalRepository;
  ApiRemoteRepository _apiRemoteRepository;
  int _failedPasscodeCount = passcodeValidationLimit;

  PinSetupBloc() {
    _authRemoteRepository = AuthRemoteRepository();
    _apiRemoteRepository = ApiRemoteRepository();
    _applicationLocalRepository = ApplicationLocalRepository();
  }

  @override
  Stream<PinSetupState> mapEventToState(
    PinSetupEvent event,
  ) async* {
    if (event is PinEntered) {
      yield* _mapPinEnteredToState(event);
    }
  }

  Stream<PinSetupState> _mapPinEnteredToState(PinEntered event) async* {
    yield LoadingPinState();
    auth.BaseResponse<PasscodeConfirmation> response =
        await _authRemoteRepository.verifyPasscode(
            passcode: event.pin, replaceExisting: event.replaceExisting);
    if (response.isSuccess()) {
      _applicationLocalRepository.savePasscode(passcode: event.pin);
      yield* _loadApiToken();
    } else {
      _failedPasscodeCount--;
      if (_failedPasscodeCount < 1) {
        yield PasscodeValidationLimitExceeded();
      } else {
        yield InitialPinSetupState(
            failedPasscodeAttempts: _failedPasscodeCount);
      }
    }
  }

  Stream<PinSetupState> _loadApiToken() async* {
    auth.BaseResponse<ApiToken> tokenResponse =
        await _authRemoteRepository.getToken();
    if (tokenResponse.isSuccess()) {
      yield* _handleSuccessPinInit();
    } else {
      yield* _failedPinStateSequence();
    }
  }

  Stream<PinSetupState> _handleSuccessPinInit() async* {
    BaseResponse<Profile> response = await _apiRemoteRepository.getProfile();
    if (response.isSuccess()) {
      Profile userProfile = response.result;
      yield SuccessPinState(
          isEmailConfirmationNeeded:
              _isEmailConfirmationNeededForProfile(userProfile));
    } else {
      yield* _failedPinStateSequence();
    }
  }

  Stream<PinSetupState> _failedPinStateSequence() async* {
    yield ErrorPinState();
    yield InitialPinSetupState(failedPasscodeAttempts: _failedPasscodeCount);
  }

  bool _isEmailConfirmationNeededForProfile(Profile userProfile) =>
      (userProfile.email?.isEmpty ?? true) || !userProfile.emailConfirmed;
}
