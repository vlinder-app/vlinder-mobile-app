import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';


@immutable
abstract class PinSetupState extends Equatable {
  PinSetupState([List props = const []]);
}

class InitialPinSetupState extends PinSetupState {
  final int failedPasscodeAttempts;

  InitialPinSetupState({this.failedPasscodeAttempts = 0});

  @override
  String toString() {
    return 'InitialPinSetupState{failedPasscodeAttempts: $failedPasscodeAttempts}';
  }

  @override
  List<Object> get props => [failedPasscodeAttempts];
}

class ErrorPinState extends PinSetupState {
  @override
  String toString() {
    return 'ErrorPinState{}';
  }

  @override
  List<Object> get props => null;
}

class SuccessPinState extends PinSetupState {
  final bool isEmailConfirmationNeeded;
  SuccessPinState({this.isEmailConfirmationNeeded});

  @override
  String toString() {
    return 'SuccessPinState{isEmailNeeded: $isEmailConfirmationNeeded}';
  }

  @override
  List<Object> get props => [];
}

class LoadingPinState extends PinSetupState {
  @override
  String toString() {
    return 'LoadingPinState{}';
  }

  @override
  List<Object> get props => [];
}

class PasscodeValidationLimitExceeded extends PinSetupState {
  @override
  String toString() {
    return 'PasscodeValidationLimitExceeded{}';
  }

  @override
  List<Object> get props => [];
}