import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/registration/pin/bloc/bloc.dart';
import 'package:atoma_cash/src/registration/pin/pin_screen.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bloc.dart';

abstract class IPinSetupScreenNavigationController {
  void onPinEnteredSuccessfully(bool isEmailConfirmationNeeded);

  void onForgotPinClicked();

  void onExceededPinSetupAttempts();
}

class PinSetupScreen extends StatefulWidget {
  final bool isUserHasPasscode;
  final bool replaceExisting;
  final IPinSetupScreenNavigationController _flowController;
  final bool shouldOverride;
  final int stepIndex;

  PinSetupScreen(
      {Key key,
      @required this.isUserHasPasscode,
      @required this.replaceExisting,
      @required IPinSetupScreenNavigationController flowController,
      @required this.stepIndex,
      this.shouldOverride = false})
      : _flowController = flowController,
        super(key: key);

  @override
  _PinSetupScreenState createState() => _PinSetupScreenState();
}

class _PinSetupScreenState extends State<PinSetupScreen> {
  final String dash = '-';
  PinSetupBloc _pinSetupBloc;
  PinBloc _pinBloc;

  @override
  void initState() {
    super.initState();
    _pinSetupBloc = PinSetupBloc();
    _pinBloc = PinBloc(validPin: dash);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => _pinSetupBloc,
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(kToolbarHeight),
          child: _appBarBuilder(), // StreamBuilder
        ),
        body: BlocListener(
          bloc: _pinSetupBloc,
          listener: (BuildContext context, PinSetupState state) {
            _stateChangesListener(context, state);
          },
          child: BlocBuilder(
              bloc: _pinSetupBloc,
              builder: (BuildContext context, PinSetupState state) {
                return _rootWidgetForState(state);
              }),
        ),
      ),
    );
  }

  bool _shouldUpdateAppBarActions(
          PinSetupState previousState, PinSetupState currentState) =>
      previousState is LoadingPinState && currentState is! LoadingPinState ||
      previousState is! LoadingPinState && currentState is LoadingPinState;

  Widget _appBarBuilder() => BlocBuilder(
        bloc: _pinSetupBloc,
        condition: _shouldUpdateAppBarActions,
        builder: (context, currentState) {
          return AppBar(actions: _appBarActions(currentState));
        },
      );

  List<Widget> _appBarActions(PinSetupState currentState) {
    if (currentState is LoadingPinState) {
      return [];
    }

    return [
      if (_isConfirmingExistingPasscode())
        Widgets.styledFlatButton(
            text: Localization.of(context).forgotButton,
            onPressed: widget._flowController.onForgotPinClicked)
    ];
  }

  void _stateChangesListener(BuildContext context, PinSetupState state) {
    if (state is ErrorPinState) {
      _showErrorSnackBar(context);
    }
    if (state is SuccessPinState) {
      widget._flowController
          .onPinEnteredSuccessfully(state.isEmailConfirmationNeeded);
    }
    if (state is PasscodeValidationLimitExceeded) {
      widget._flowController.onExceededPinSetupAttempts();
    }
  }

  Widget _rootWidgetForState(PinSetupState state) {
    if (state is LoadingPinState) {
      return Center(child: CircularProgressIndicator());
    }
    if (state is InitialPinSetupState) {
      return PinScreen(
        _subtitle(),
        title: _title(),
        description: _description(),
        filledPinCallback: _onFilledPin,
        pinBloc: _pinBloc,
        errorOnFailedPin: (pinState, context) {
          return _errorOnFailedPin(state, context);
        },
      );
    } else {
      return Container();
    }
  }

  String _title() {
    return _isConfirmingExistingPasscode()
        ? Localization.of(context).welcomeBackTitle
        : Localization.of(context).stepNumber(widget.stepIndex.toString());
  }

  String _subtitle() {
    return _isConfirmingExistingPasscode()
        ? Localization.of(context).enterPasscode
        : Localization.of(context).createPasscodeTitle;
  }

  String _description() {
    return _isConfirmingExistingPasscode()
        ? ""
        : Localization.of(context).useDifferentNumbersForYourSafety;
  }

  void _showErrorSnackBar(BuildContext context) {
    Widgets.failedSnackBar(
        context, Localization.of(context).legacyWrongPasscode);
  }

  String _errorOnFailedPin(PinSetupState state, BuildContext context) {
    if (state is InitialPinSetupState) {
      return (state.failedPasscodeAttempts == 0)
          ? (_isConfirmingExistingPasscode())
              ? ("")
              : (Localization.of(context).useDifferentNumbersForYourSafety)
          : Localization.of(context).wrongPasscodeAttemptsLeft(
              '${state.failedPasscodeAttempts}', '$passcodeValidationLimit');
    }
  }

  bool _isConfirmingExistingPasscode() =>
      widget.isUserHasPasscode && !widget.replaceExisting;

  void _onFilledPin(String pin) {
    _pinSetupBloc
        .add(PinEntered(pin: pin, replaceExisting: widget.replaceExisting));
  }
}
