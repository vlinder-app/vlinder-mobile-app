import 'package:atoma_cash/models/auth_api/auth/internal/jwt_token_data.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/registration/phone_confirmation/bloc/configuration.dart';
import 'package:atoma_cash/src/registration/widgets/HeaderWidget.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bloc.dart';

abstract class IPhoneConfirmationFlowController {
  void onPhoneConfirmed(JwtTokenData tokenData);

  void onExceededPhoneConfirmationAttempts();
}

class PhoneConfirmationScreen extends StatefulWidget {
  final String phone;
  final IPhoneConfirmationFlowController _flowController;

  PhoneConfirmationScreen(
      {Key key, this.phone, IPhoneConfirmationFlowController flowController})
      : _flowController = flowController,
        super(key: key);

  @override
  _PhoneConfirmationState createState() => _PhoneConfirmationState();
}

class _PhoneConfirmationState extends State<PhoneConfirmationScreen> {
  FocusNode _codeFocusNode;
  ScrollController _listViewScrollController;
  PhoneConfirmationBloc _confirmationBloc;
  final _codeTextController = TextEditingController();

  @override
  void initState() {
    _confirmationBloc = PhoneConfirmationBloc(widget.phone);
//    _codeFocusNode = FocusNode()..addListener(_onCodeFocusChanged);
    _listViewScrollController = ScrollController();
    _codeTextController.addListener(_onCodeChanged);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(),
      body: BlocListener(
        bloc: _confirmationBloc,
        listener: _blocListener,
        child: BlocBuilder(
          bloc: _confirmationBloc,
          builder: (BuildContext context, PhoneConfirmationState state) {
            return Container(
                margin: EdgeInsets.only(left: 24.0, right: 24.0),
                decoration: BoxDecoration(color: Colors.white),
                child: Form(
                  child: ListView(
                    controller: _listViewScrollController,
                    children: <Widget>[
                      Header(
                        Localization.of(context).step2,
                        subtitle:
                            Localization.of(context).confirmPhoneDescription,
                        description:
                            Localization.of(context).sentToPhone(widget.phone),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10.0),
                        child: TextFormField(
                          scrollPadding: EdgeInsets.only(bottom: 200.0),
                          autovalidate: true,
                          autocorrect: false,
                          buildCounter: (BuildContext context,
                                  {int currentLength,
                                  int maxLength,
                                  bool isFocused}) =>
                              null,
                          controller: _codeTextController,
                          decoration: UiHelper.styledInputDecoration(
                              Localization.of(context).phoneCodeHint),
                          focusNode: _codeFocusNode,
                          maxLength: 6,
                          keyboardType: TextInputType.numberWithOptions(
                              signed: false, decimal: false),
                          inputFormatters: [
                            WhitelistingTextInputFormatter.digitsOnly,
                          ],
                          maxLines: 1,
                          validator: (_) {
                            return _codeValidationErrorForState(state);
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 24.0),
                        child: _widgetForState(state),
                      ),
                      _resendCodeBuilder(state, context),
                      Container(
                        child: SizedBox(
                          height: 150.0,
                        ),
                      )
                    ],
                  ),
                ));
          },
        ),
      ),
    );
  }

  void _blocListener(BuildContext context, PhoneConfirmationState state) {
    if (state.isSuccess) {
      widget._flowController.onPhoneConfirmed(state.tokenData);
    }
    if (state.isExceededConfirmationAttempts) {
      widget._flowController.onExceededPhoneConfirmationAttempts();
    }
    if (state.isResendFailed) {
      Widgets.failedSnackBar(
          context, Localization.of(context).legacyUnableToSendCode);
    }
    if (state.isResendSuccess) {
      Widgets.notificationSnackBar(
          context, (Localization.of(context).codeSent));
    }
  }

  void _onCodeFocusChanged() {
    if (_codeFocusNode.hasFocus) {
      _listViewScrollController.animateTo(
          _listViewScrollController.position.minScrollExtent,
          duration: Duration(milliseconds: 1000),
          curve: Curves.easeOut);
    }
  }

  String _codeValidationErrorForState(PhoneConfirmationState state) {
    if (state.failedAttempts != 0) {
      return Localization.of(context).wrongCodeTypedAttempt(
          state.failedAttempts.toString(),
          '$phoneConfirmationAvailableAttempts');
    } else {
      return null;
    }
  }

  Widget _widgetForState(PhoneConfirmationState state) {
    return Widgets.raisedButtonWithLoadingIndicator(
        text: Localization.of(context).continueButton,
        isLoading: state.isSubmitting,
        onPressed:
            state.isCodeValid && !state.isSubmitting ? _onContinueClick : null);
  }

  Widget _resendCodeBuilder(
      PhoneConfirmationState state, BuildContext context) {
    if (state.remainingSeconds == null) {
      return Text('');
    } else {
      if (state.remainingSeconds == 0) {
        return _buildDidNotReceiveWidget(state);
      } else {
        return _buildTimerIndicator(state.remainingSeconds);
      }
    }
  }

  Widget _buildTimerIndicator(int remainingSeconds) {
    var dateTime = DateTime.fromMillisecondsSinceEpoch(
        Duration(seconds: remainingSeconds).inMilliseconds);
    return Center(
        child: Padding(
      padding: const EdgeInsets.only(top: 30.0, bottom: 30.0),
      child: _timerIndicatorText(dateTime.minute, dateTime.second),
    ));
  }

  Widget _buildDidNotReceiveWidget(PhoneConfirmationState state) {
    return Center(
      child: Padding(
          padding: const EdgeInsets.only(top: 25.0, bottom: 25.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                Localization.of(context).iDidntReceiveCode,
                style: TextStyle(
                    fontWeight: FontWeight.w400, color: Palette.mediumGrey),
              ),
              _resendWidgetForState(state),
            ],
          )),
    );
  }

  Widget _resendWidgetForState(PhoneConfirmationState state) {
    if (state.isResendLoading) {
      return Padding(
        padding: const EdgeInsets.only(top: 8.0, right: 10.0),
        child: Center(child: CircularProgressIndicator()),
      );
    } else {
      return Container(
        height: 25.0,
        child: FlatButton.icon(
          icon: Icon(
            Icons.replay,
            color: Palette.blue,
            size: 20.0,
          ),
          padding: EdgeInsets.only(left: 30.0, bottom: 0.0, top: 0.0),
          onPressed: _onResendButtonClick,
          label: Text(
            Localization.of(context).resendButton,
            style: TextStyle(color: Palette.blue, fontWeight: FontWeight.w600),
          ),
        ),
      );
    }
  }

  void _onCodeChanged() {
    _confirmationBloc.add(CodeChanged(code: _codeTextController.text));
  }

  void _onResendButtonClick() {
    _confirmationBloc.add(CodeResent());
  }

  void _onContinueClick() {
    _confirmationBloc.add(CodeSubmitted(_codeTextController.text));
  }

  Widget _timerIndicatorText(int minutes, int seconds) {
    String remainingTime =
        minutes > 0 ? '$minutes ${Localization.of(context).minutesShort} ' : '';
    remainingTime += '$seconds ${Localization.of(context).secondsShort}';

    return Text(
      Localization.of(context).verificationCodeCanBeResent(remainingTime),
      textAlign: TextAlign.center,
      style: TextStyle(fontWeight: FontWeight.w400, color: Palette.mediumGrey),
    );
  }

  @override
  void dispose() {
    _codeTextController.dispose();
    super.dispose();
  }
}
