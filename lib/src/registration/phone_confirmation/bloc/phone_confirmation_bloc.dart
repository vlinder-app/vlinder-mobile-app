import 'dart:async';

import 'package:atoma_cash/models/auth_api/auth/internal/jwt_token_data.dart';
import 'package:atoma_cash/models/auth_api/auth/response/phone_confirmation.dart';
import 'package:atoma_cash/models/auth_api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';
import 'package:atoma_cash/resources/repository/remote/auth_remote_repository.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/code_entered_event.dart';
import 'package:atoma_cash/src/analytics/models/events/code_resent_event.dart';
import 'package:atoma_cash/utils/jwt/jwt_token_parser.dart';
import 'package:atoma_cash/utils/registration/phone_confirmation_helper.dart';
import 'package:bloc/bloc.dart';

import './bloc.dart';

class PhoneConfirmationBloc
    extends Bloc<PhoneConfirmationEvent, PhoneConfirmationState> {
  final String _phone;
  PhoneConfirmationHelper _phoneConfirmationHelper;
  AuthRemoteRepository _authRemoteRepository;
  ApplicationLocalRepository _applicationLocalRepository;
  Timer _timer;
  int _remainingTimeInSeconds;
  int _failedAttempts = 0;

  PhoneConfirmationBloc(this._phone) {
    _phoneConfirmationHelper = PhoneConfirmationHelper();
    _authRemoteRepository = AuthRemoteRepository();
    _applicationLocalRepository = ApplicationLocalRepository();
    _checkExistingSmsRequest();
  }

  void _checkExistingSmsRequest() async {
    if (await _phoneConfirmationHelper.isContainExistingRequest(_phone)) {
      int remainingMillis =
          await _phoneConfirmationHelper.remainingSmsRequestMillis();
      int seconds = Duration(milliseconds: remainingMillis).inSeconds;
      _startCountDownTimer(seconds);
    }
  }

  @override
  PhoneConfirmationState get initialState => PhoneConfirmationState.initial();

  bool _isCodeValid(String code) => code.length == confirmationCodeLength;

  @override
  Stream<PhoneConfirmationState> mapEventToState(
    PhoneConfirmationEvent event,
  ) async* {
    if (event is CodeChanged) {
      yield* _mapCodeChangedInState(event.code);
    } else if (event is CodeSubmitted) {
      yield* _mapCodeSubmittedToState(event.code);
    } else if (event is CodeResent) {
      yield* _mapResendSmsToState(_phone);
    } else if (event is TimerTicked) {
      yield state.copyWith(remainingSeconds: event.seconds, isSuccess: false);
    } else if (event is HideCodeResendNotifications) {
      yield* _mapHideResendNotificationsToState();
    }
  }

  Stream<PhoneConfirmationState> _mapCodeSubmittedToState(String code) async* {
    yield state.copyWith(isSubmitting: true);
    Analytics().logEvent(CodeEnteredEvent());
    BaseResponse<PhoneConfirmation> response = await _authRemoteRepository
        .verifyPhone(phoneNumber: _phone, confirmationCode: code);
    if (response.isSuccess()) {
      _timer.cancel();
      JwtTokenData tokenData = _parsTokenData(response.result.verifyToken);
      _applicationLocalRepository.saveUserId(userId: tokenData.externalId);
      _applicationLocalRepository.saveEmail(email: tokenData.email);
      Analytics().setUserId(tokenData.externalId);
      yield PhoneConfirmationState.success(tokenData: tokenData);
      _phoneConfirmationHelper.reset();
    } else {
      _failedAttempts++;
      if (_failedAttempts >= phoneConfirmationAvailableAttempts) {
        _applicationLocalRepository.clearAll();
        yield state.copyWith(isExceededConfirmationAttempts: true);
      }
      yield state.copyWith(
          failedAttempts: _failedAttempts, isSubmitting: false);
    }
  }

  Stream<PhoneConfirmationState> _mapResendSmsToState(String phone) async* {
    yield state.copyWith(isResendLoading: true);
    Analytics().logEvent(CodeResentEvent());
    BaseResponse<PhoneConfirmation> response =
        await _authRemoteRepository.requestSMS(phoneNumber: phone);
    if (response.isSuccess()) {
      yield state.copyWith(isResendSuccess: true, isResendLoading: false);
      _phoneConfirmationHelper.savePhoneNumber(_phone);
    } else {
      yield state.copyWith(isResendFailed: true, isResendLoading: false);
    }
    this.add(HideCodeResendNotifications());
    _startCountDownTimer(
        Duration(milliseconds: PhoneConfirmationHelper.smsRequestTimeOutMillis)
            .inSeconds);
  }

  Stream<PhoneConfirmationState> _mapHideResendNotificationsToState() async* {
    yield state.copyWith(
        isResendFailed: false, isResendLoading: false, isResendSuccess: false);
  }

  Stream<PhoneConfirmationState> _mapCodeChangedInState(String code) async* {
    yield state.copyWith(isCodeValid: _isCodeValid(code), isSuccess: false);
  }

  void _startCountDownTimer(int seconds) {
    _remainingTimeInSeconds = seconds;
    _timer = Timer.periodic(Duration(seconds: 1), _timerHandler);
  }

  void _timerHandler(Timer timer) {
    if (this._remainingTimeInSeconds < 1) {
      timer.cancel();
    } else {
      _remainingTimeInSeconds--;
      this.add(TimerTicked(seconds: _remainingTimeInSeconds));
    }
  }

  JwtTokenData _parsTokenData(String token) {
    try {
      Map<String, dynamic> json = TokenParser.parseJwt(token);
      return JwtTokenData.fromJson(TokenParser.parseJwt(token));
    } catch (e) {
      print(e);
      return null;
    }
  }
}
