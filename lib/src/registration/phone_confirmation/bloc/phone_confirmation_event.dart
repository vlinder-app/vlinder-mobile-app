import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PhoneConfirmationEvent extends Equatable {
  PhoneConfirmationEvent([List props = const []]);
}

class TimerTicked extends PhoneConfirmationEvent {
  final int seconds;

  TimerTicked({@required this.seconds}) : super([seconds]);

  @override
  String toString() {
    return 'TimerTicked{seconds: $seconds}';
  }

  @override
  List<Object> get props => [seconds];
}

class CodeResent extends PhoneConfirmationEvent {
  @override
  String toString() {
    return 'CodeResent{}';
  }

  @override
  List<Object> get props => null;
}

class CodeChanged extends PhoneConfirmationEvent {
  final String code;

  CodeChanged({@required this.code}) : super([code]);

  @override
  String toString() {
    return 'CodeChanged{code: $code}';
  }

  @override
  List<Object> get props => null;
}

class CodeSubmitted extends PhoneConfirmationEvent {
  final String code;

  CodeSubmitted(this.code) : super([code]);

  @override
  String toString() {
    return 'CodeSubmitted{code: $code}';
  }

  @override
  List<Object> get props => [code];
}

class HideCodeResendNotifications extends PhoneConfirmationEvent {
  @override
  String toString() {
    return 'HideCodeResendNotifications{}';
  }

  @override
  List<Object> get props => null;
}
