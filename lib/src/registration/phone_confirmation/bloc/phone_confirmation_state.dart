import 'package:atoma_cash/models/auth_api/auth/internal/jwt_token_data.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

class PhoneConfirmationState {
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailed;
  final bool isCodeValid;
  final bool isResendSuccess;
  final bool isResendFailed;
  final bool isResendLoading;
  final int failedAttempts;
  final int remainingSeconds;
  final bool isExceededConfirmationAttempts;
  final JwtTokenData tokenData;

  PhoneConfirmationState(
      {@required this.isSubmitting,
      @required this.isSuccess,
      @required this.isFailed,
      @required this.isCodeValid,
      @required this.failedAttempts,
      @required this.remainingSeconds,
      @required this.isResendSuccess,
      @required this.isResendFailed,
      @required this.isExceededConfirmationAttempts,
      @required this.isResendLoading,
      @required this.tokenData});

  @override
  String toString() {
    return 'PhoneConfirmationState{isSubmitting: $isSubmitting, isSuccess: $isSuccess, isFailed: $isFailed, isCodeValid: $isCodeValid, isResendSuccess: $isResendSuccess, isResendFailed: $isResendFailed, isResendLoading: $isResendLoading, failedAttempts: $failedAttempts, remainingSeconds: $remainingSeconds, isExceededConfirmationAttempts: $isExceededConfirmationAttempts, tokenData: $tokenData}';
  }

  factory PhoneConfirmationState.initial() => PhoneConfirmationState(
      isSubmitting: false,
      isSuccess: false,
      isFailed: false,
      isCodeValid: false,
      failedAttempts: 0,
      isResendFailed: false,
      isResendSuccess: false,
      isResendLoading: false,
      isExceededConfirmationAttempts: false,
      tokenData: null,
      remainingSeconds: null);

  factory PhoneConfirmationState.success({JwtTokenData tokenData}) =>
      PhoneConfirmationState(
          isSubmitting: false,
          isSuccess: true,
          isFailed: false,
          isCodeValid: false,
          failedAttempts: 0,
          isResendFailed: false,
          isResendSuccess: false,
          isResendLoading: false,
          tokenData: tokenData,
          isExceededConfirmationAttempts: false,
          remainingSeconds: 0);

  PhoneConfirmationState copyWith(
      {bool isCodeValid,
      bool isSuccess,
      int remainingSeconds,
      int failedAttempts,
      bool isResendSuccess,
      bool isResendFailed,
      bool isResendLoading,
      bool isExceededConfirmationAttempts,
      bool isSubmitting,
      JwtTokenData tokenData}) {
    return PhoneConfirmationState(
        isExceededConfirmationAttempts: isExceededConfirmationAttempts ??
            this.isExceededConfirmationAttempts,
        isSubmitting: isSubmitting ?? this.isSubmitting,
        isSuccess: isSuccess ?? this.isSuccess,
        isFailed: this.isFailed,
        isResendLoading: isResendLoading ?? this.isResendLoading,
        isResendSuccess: isResendSuccess ?? this.isResendSuccess,
        isResendFailed: isResendFailed ?? this.isResendFailed,
        failedAttempts: failedAttempts ?? this.failedAttempts,
        isCodeValid: isCodeValid ?? this.isCodeValid,
        remainingSeconds: remainingSeconds ?? this.remainingSeconds,
        tokenData: tokenData ?? this.tokenData);
  }
}
