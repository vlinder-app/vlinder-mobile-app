import 'package:meta/meta.dart';

@immutable
class EmailConfirmationRequestState {
  final bool isEmailValid;
  final bool isButtonEnabled;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final bool isExceededEmailConfirmationLimit;
  final bool isEmailAlreadyUsed;

  EmailConfirmationRequestState(
      {@required this.isEmailValid,
      @required this.isSubmitting,
      @required this.isSuccess,
      @required this.isButtonEnabled,
      @required this.isExceededEmailConfirmationLimit,
        @required this.isFailure,
        @required this.isEmailAlreadyUsed});

  factory EmailConfirmationRequestState.empty() {
    return EmailConfirmationRequestState(
        isEmailValid: true,
        isSubmitting: false,
        isButtonEnabled: false,
        isSuccess: false,
        isExceededEmailConfirmationLimit: false,
        isFailure: false,
        isEmailAlreadyUsed: false);
  }

  factory EmailConfirmationRequestState.loading() {
    return EmailConfirmationRequestState(
        isEmailValid: true,
        isSubmitting: true,
        isButtonEnabled: true,
        isSuccess: false,
        isExceededEmailConfirmationLimit: false,
        isFailure: false,
        isEmailAlreadyUsed: false);
  }

  factory EmailConfirmationRequestState.success() {
    return EmailConfirmationRequestState(
        isEmailValid: true,
        isSubmitting: false,
        isButtonEnabled: true,
        isSuccess: true,
        isExceededEmailConfirmationLimit: false,
        isFailure: false,
        isEmailAlreadyUsed: false);
  }

  factory EmailConfirmationRequestState.failure() {
    return EmailConfirmationRequestState(
        isEmailValid: true,
        isSubmitting: false,
        isButtonEnabled: true,
        isSuccess: false,
        isExceededEmailConfirmationLimit: false,
        isFailure: true,
        isEmailAlreadyUsed: false);
  }

  EmailConfirmationRequestState copyWith(
      {bool isEmailValid,
      bool isButtonEnabled,
      bool isExceededEmailConfirmationLimit,
      bool isSuccess,
        bool isFailed,
        bool isSubmitting,
        bool isEmailAlreadyUsed}) {
    return EmailConfirmationRequestState(
        isExceededEmailConfirmationLimit: isExceededEmailConfirmationLimit ??
            this.isExceededEmailConfirmationLimit,
        isEmailValid: isEmailValid ?? this.isEmailValid,
        isSubmitting: this.isSubmitting,
        isButtonEnabled: isEmailValid ?? this.isEmailValid,
        isSuccess: isSuccess ?? this.isSuccess,
        isFailure: isFailed ?? this.isFailure,
        isEmailAlreadyUsed: isEmailAlreadyUsed ?? this.isEmailAlreadyUsed);
  }

  @override
  String toString() {
    return 'EmailConfirmationState{isEmailValid: $isEmailValid, isButtonEnabled: $isButtonEnabled, isSubmitting: $isSubmitting, isSuccess: $isSuccess, isFailure: $isFailure, isExceededEmailConfirmationLimit: $isExceededEmailConfirmationLimit, isEmailAlreadyUsed: $isEmailAlreadyUsed}';
  }


}
