import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class EmailConfirmationRequestEvent extends Equatable {
  EmailConfirmationRequestEvent([List props = const []]);
}

class EmailChanged extends EmailConfirmationRequestEvent {
  final String email;

  EmailChanged({@required this.email}) : super([email]);

  @override
  String toString() {
    return 'EmailChanged{email: $email}';
  }

  @override
  List<Object> get props => [email];
}

class EmailSubmitted extends EmailConfirmationRequestEvent {
  final String email;

  EmailSubmitted(this.email) : super([email]);

  @override
  String toString() {
    return 'EmailSubmitted{email: $email}';
  }

  @override
  List<Object> get props => [email];
}
