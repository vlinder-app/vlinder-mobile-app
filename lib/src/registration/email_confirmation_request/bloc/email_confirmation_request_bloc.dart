import 'dart:async';

import 'package:atoma_cash/models/api/api/profile/request/profile_model.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/utils/validators.dart';
import 'package:bloc/bloc.dart';

import './bloc.dart';

class EmailConfirmationRequestBloc
    extends Bloc<EmailConfirmationRequestEvent, EmailConfirmationRequestState> {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();

  @override
  EmailConfirmationRequestState get initialState => EmailConfirmationRequestState.empty();

  @override
  Stream<EmailConfirmationRequestState> mapEventToState(
      EmailConfirmationRequestEvent event,
  ) async* {
    if (event is EmailChanged) {
      yield* _mapEmailChangedToState(event.email);
    } else if (event is EmailSubmitted) {
      yield* _mapEmailSubmittedToState(event.email);
    }
  }

  Stream<EmailConfirmationRequestState> _mapEmailChangedToState(String email) async* {
    yield state.copyWith(
        isEmailValid: Validators.isValidEmail(email),
        isSuccess: false,
        isFailed: false);
  }

  Stream<EmailConfirmationRequestState> _mapEmailSubmittedToState(
      String email) async* {
    yield EmailConfirmationRequestState.loading();
    BaseResponse<Null> response =
        await _apiRemoteRepository.updateProfile(ProfileModel(email: email));
    if (response.isSuccess()) {
      yield* _confirmEmail();
    } else {
      yield EmailConfirmationRequestState.failure();
    }
  }

  Stream<EmailConfirmationRequestState> _confirmEmail() async* {
    BaseResponse<Null> response = await _apiRemoteRepository.confirmEmail();
    if (response.isSuccess()) {
      yield EmailConfirmationRequestState.success();
    } else {
      if (response.error.code == 429) {
        yield state.copyWith(isExceededEmailConfirmationLimit: true);
      } else if (response.error.code == 409) {
        yield state.copyWith(isEmailAlreadyUsed: true);
        yield EmailConfirmationRequestState.empty();
      } else {
        yield EmailConfirmationRequestState.failure();
      }
    }
  }
}
