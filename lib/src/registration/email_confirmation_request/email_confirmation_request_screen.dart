import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/registration/email_confirmation_request/email_confirmation_screen_configuration.dart';
import 'package:atoma_cash/src/registration/widgets/HeaderWidget.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bloc.dart';

abstract class IEmailConfirmationRequestFlowController {
  bool get isSkippable;

  void onDidSendEmailConfirmationRequest(String email);

  void onSkipEmailConfirmation();

  void onCancel();
}

class EmailConfirmationRequestScreen extends StatefulWidget {
  final IEmailConfirmationRequestFlowController _flowController;
  final IEmailConfirmationRequestScreenConfiguration _configuration;

  EmailConfirmationRequestScreen({
    Key key,
    @required IEmailConfirmationRequestFlowController flowController,
    @required IEmailConfirmationRequestScreenConfiguration configuration,
  })  : _flowController = flowController,
        _configuration = configuration,
        super(key: key);

  @override
  _EmailConfirmationRequestScreenState createState() =>
      _EmailConfirmationRequestScreenState();
}

class _EmailConfirmationRequestScreenState
    extends State<EmailConfirmationRequestScreen> {
  TextEditingController _emailEditingController;
  EmailConfirmationRequestBloc _emailConfirmationBloc;

  static const _actionButtonPadding = EdgeInsets.only(top: 24);

  @override
  void initState() {
    super.initState();
    _emailConfirmationBloc = EmailConfirmationRequestBloc();
    _emailEditingController = TextEditingController()
      ..text = widget._configuration.email()
      ..addListener(_onEmailChanged);
  }

  @override
  void dispose() {
    _emailEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
        ),
        body: BlocListener(
          bloc: _emailConfirmationBloc,
          listener: _blocListener,
          child: BlocBuilder(
              bloc: _emailConfirmationBloc, builder: _rootWidgetForState),
        ));
  }

  void _blocListener(
      BuildContext context, EmailConfirmationRequestState state) {
    if (state.isSuccess) {
      widget._flowController
          .onDidSendEmailConfirmationRequest(_emailEditingController.text);
    }
    if (state.isFailure) {
      _onFailedEmailConfirmation(context);
    }
    if (state.isExceededEmailConfirmationLimit) {
      _onExceedEmailConfirmationAttempts(context);
    }
    if (state.isEmailAlreadyUsed) {
      _onEmailAlreadyUsedError(context);
    }
  }

  Widget _rootWidgetForState(
      BuildContext context, EmailConfirmationRequestState state) {
    return Container(
      margin: EdgeInsets.only(left: 24.0, right: 24.0),
      decoration: BoxDecoration(color: Colors.white),
      child: Form(
        child: ListView(
          children: <Widget>[
            Header(
              widget._configuration.title(),
              subtitle: Localization.of(context).provideEmailTitle,
              description: widget._configuration.hint(context),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10.0),
              child: TextFormField(
                scrollPadding: EdgeInsets.only(bottom: 120.0),
                autofocus: true,
                controller: _emailEditingController,
                decoration: UiHelper.styledInputDecoration(
                    Localization.of(context).emailHint),
                autovalidate: true,
                autocorrect: false,
                keyboardType: TextInputType.emailAddress,
                maxLines: 1,
                validator: (_) {
                  return null;
                },
              ),
            ),
            Padding(
              padding: _actionButtonPadding,
              child: Widgets.raisedButtonWithLoadingIndicator(
                  text: Localization
                      .of(context)
                      .continueButton,
                  isLoading: state.isSubmitting,
                  onPressed: state.isButtonEnabled ? _onContinueClick : null),
            ),
            if (widget._flowController.isSkippable && !state.isSubmitting)
              Padding(
                  padding: _actionButtonPadding,
                  child: Widgets.styledFlatButton(
                      text: Localization
                          .of(context)
                          .notNowButton,
                      onPressed:
                      widget._flowController.onSkipEmailConfirmation)),
          ],
        ),
      ),
    );
  }

  void _onEmailChanged() {
    _emailConfirmationBloc
        .add(EmailChanged(email: _emailEditingController.text));
  }

  void _onContinueClick() {
    Widgets.hideKeyboardOnContext(context);
    _emailConfirmationBloc.add(EmailSubmitted(_emailEditingController.text));
  }

  void _onFailedEmailConfirmation(BuildContext context) {
    Widgets.failedSnackBar(
        context, Localization.of(context).unableToSendVerificationLink);
  }

  void _onExceedEmailConfirmationAttempts(BuildContext context) {
    Widgets.failedSnackBar(context,
        Localization
            .of(context)
            .thereAreTooManyEmailAttemptsTryIn24hours);
  }

  void _onEmailAlreadyUsedError(BuildContext context) {
    Widgets.failedSnackBar(context, Localization.of(context).emailIsUsed);
  }
}
