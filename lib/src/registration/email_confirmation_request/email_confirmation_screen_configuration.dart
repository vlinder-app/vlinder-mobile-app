import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/cupertino.dart';

abstract class IEmailConfirmationRequestScreenConfiguration {
  String title();

  String email();

  String hint(BuildContext context);
}

class Default extends IEmailConfirmationRequestScreenConfiguration {
  final String _email;

  Default({String email}) : _email = email;

  @override
  String hint(BuildContext context) =>
      Localization.of(context).provideEmailHint;

  @override
  String email() => _email ?? "";

  String title() => '';
}

class Accounts extends IEmailConfirmationRequestScreenConfiguration {
  final String _email;

  Accounts({String email}) : _email = email;

  @override
  String hint(BuildContext context) =>
      Localization.of(context).provideEmailBankHint;

  @override
  String email() => _email ?? "";

  @override
  String title() => '';
}
