import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/bank/webview/custom_app_bar.dart';
import 'package:atoma_cash/src/registration/restricted_area/bloc/bloc.dart';
import 'package:atoma_cash/src/registration/restricted_area/restricted_area_placeholder_result.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RestrictedAreaPlaceholderScreen extends StatefulWidget {
  final IPersonalDataSubmitter dataSubmitter;

  RestrictedAreaPlaceholderScreen(this.dataSubmitter, {Key key})
      : assert(dataSubmitter != null),
        super(key: key);

  @override
  _RestrictedAreaPlaceholderScreenState createState() =>
      _RestrictedAreaPlaceholderScreenState();
}

class _RestrictedAreaPlaceholderScreenState
    extends State<RestrictedAreaPlaceholderScreen> {
  RestrictedAreaPlaceholderBloc _bloc;
  TextEditingController _emailEditingController;
  TextEditingController _nameEditingController = TextEditingController();

  @override
  void initState() {
    _bloc = RestrictedAreaPlaceholderBloc(widget.dataSubmitter);
    _emailEditingController = TextEditingController()
      ..addListener(_onEmailChanged);
    super.initState();
  }

  @override
  void dispose() {
    _emailEditingController.dispose();
    _nameEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ModalStyleAppBar(
        AppBar(
          backgroundColor: Colors.transparent,
          leading: IconButton(
            icon: Icon(Icons.close),
            onPressed: _close,
          ),
        ),
      ),
      body: BlocListener(
        bloc: _bloc,
        listener: _blocListener,
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.only(left: 24.0, top: 26, right: 24.0),
            child: BlocBuilder(
              bloc: _bloc,
              builder: _rootContentBuilder,
            ),
          ),
        ),
      ),
    );
  }

  void _blocListener(
      BuildContext context, RestrictedAreaPlaceholderState state) {
    if (state.isSubmitted) {
      ExtendedNavigator.ofRouter<Router>()
          .pop(RestrictedAreaPlaceholderDataSubmitted());
    }
  }

  Widget _rootContentBuilder(
      BuildContext context, RestrictedAreaPlaceholderState state) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        _titleWidget(context),
        _emailWidget(context),
        _nameWidget(context),
        _actionWidget(context, state),
      ],
    );
  }

  Widget _titleWidget(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          Localization.of(context).restrictedAreaPlaceholderScreenTitle,
          style: FontBook.headerH3.copyWith(color: Palette.darkGrey),
        ),
        Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Text(
            Localization.of(context).restrictedAreaPlaceholderScreenSubtitle,
            style: FontBook.textBody1.copyWith(color: Palette.mediumGrey),
          ),
        )
      ],
    );
  }

  Widget _emailWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 28.0),
      child: TextFormField(
        scrollPadding: EdgeInsets.only(bottom: 120.0),
        autofocus: true,
        controller: _emailEditingController,
        decoration:
            UiHelper.styledInputDecoration(Localization.of(context).emailHint),
        autovalidate: true,
        autocorrect: false,
        keyboardType: TextInputType.emailAddress,
        maxLines: 1,
        validator: (_) {
          return null;
        },
      ),
    );
  }

  Widget _nameWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 24.0),
      child: TextFormField(
        scrollPadding: EdgeInsets.only(bottom: 120.0),
        autofocus: true,
        controller: _nameEditingController,
        decoration: UiHelper.styledInputDecoration(Localization.of(context)
            .restrictedAreaPlaceholderScreenNamePlaceholder),
        autovalidate: true,
        autocorrect: false,
        keyboardType: TextInputType.text,
        maxLines: 1,
        validator: (_) {
          return null;
        },
      ),
    );
  }

  Widget _actionWidget(
      BuildContext context, RestrictedAreaPlaceholderState state) {
    return Padding(
      padding: const EdgeInsets.only(top: 24.0),
      child: SizedBox(
        width: double.maxFinite,
        child: Widgets.raisedButtonWithLoadingIndicator(
            text: Localization.of(context).restrictedAreaPlaceholderScreenButtonTitle,
            isLoading: false,
            onPressed: state.isButtonEnabled ? _onSubmitClocked : null),
      ),
    );
  }

  void _onEmailChanged() {
    _bloc.add(EmailChanged(_emailEditingController.text));
  }

  void _onSubmitClocked() {
    _bloc.add(SubmitClicked(name: _nameEditingController.text));
  }

  void _close() {
    ExtendedNavigator.ofRouter<Router>().pop();
  }
}
