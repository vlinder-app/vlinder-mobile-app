abstract class RestrictedAreaPlaceholderResult {}

class RestrictedAreaPlaceholderDataSubmitted
    extends RestrictedAreaPlaceholderResult {
  RestrictedAreaPlaceholderDataSubmitted();
}
