import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
class RestrictedAreaPlaceholderState extends Equatable {
  final bool isSubmitted;
  final bool isButtonEnabled;

  RestrictedAreaPlaceholderState({this.isSubmitted, this.isButtonEnabled});

  factory RestrictedAreaPlaceholderState.initial() {
    return RestrictedAreaPlaceholderState(
        isSubmitted: false, isButtonEnabled: false);
  }

  factory RestrictedAreaPlaceholderState.submitted() {
    return RestrictedAreaPlaceholderState(
        isSubmitted: true, isButtonEnabled: false);
  }

  RestrictedAreaPlaceholderState copyWith({
    bool isSubmitted,
    bool isButtonEnabled,
  }) {
    return RestrictedAreaPlaceholderState(
      isSubmitted: isSubmitted ?? this.isSubmitted,
      isButtonEnabled: isButtonEnabled ?? this.isButtonEnabled,
    );
  }

  @override
  List<Object> get props => [isSubmitted, isButtonEnabled];
}
