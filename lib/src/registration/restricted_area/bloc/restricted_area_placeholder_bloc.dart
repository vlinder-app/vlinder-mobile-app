import 'package:atoma_cash/src/registration/restricted_area/bloc/bloc.dart';
import 'package:atoma_cash/utils/validators.dart';
import 'package:bloc/bloc.dart';

abstract class IPersonalDataSubmitter {
  Future<void> submitData(String email, {String name});
}

class RestrictedAreaPlaceholderBloc extends Bloc<RestrictedAreaPlaceholderEvent,
    RestrictedAreaPlaceholderState> {

  final IPersonalDataSubmitter _dataSubmitter;

  String _email;

  RestrictedAreaPlaceholderBloc(IPersonalDataSubmitter dataSubmitter)
      : _dataSubmitter = dataSubmitter;

  @override
  RestrictedAreaPlaceholderState get initialState =>
      RestrictedAreaPlaceholderState.initial();

  @override
  Stream<RestrictedAreaPlaceholderState> mapEventToState(
      RestrictedAreaPlaceholderEvent event) async* {
    if (event is EmailChanged) {
      yield* _mapEmailChangedToState(event.email);
    }
    if (event is SubmitClicked) {
      yield* _mapSubmitClickedToState(event.name);
    }
  }

  Stream<RestrictedAreaPlaceholderState> _mapEmailChangedToState(
      String email) async* {
    _email = email;
    yield RestrictedAreaPlaceholderState.initial().copyWith(
        isButtonEnabled: Validators.isValidEmail(email));
  }

  Stream<RestrictedAreaPlaceholderState> _mapSubmitClickedToState(
      String name) async* {
    await _dataSubmitter.submitData(_email, name: name);
    yield RestrictedAreaPlaceholderState.submitted();
  }
}
