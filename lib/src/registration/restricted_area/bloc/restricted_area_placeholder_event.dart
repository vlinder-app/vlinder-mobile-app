
import 'package:equatable/equatable.dart';

abstract class RestrictedAreaPlaceholderEvent extends Equatable {
  RestrictedAreaPlaceholderEvent();
}

class EmailChanged extends RestrictedAreaPlaceholderEvent {
  final String email;
  EmailChanged(this.email);

  @override
  List<Object> get props => [email];
}

class SubmitClicked extends RestrictedAreaPlaceholderEvent {
  final String name;
  SubmitClicked({this.name});

  @override
  List<Object> get props => [name];
}