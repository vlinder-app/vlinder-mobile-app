

import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/onboarding_lead_event.dart';
import 'package:atoma_cash/src/registration/restricted_area/bloc/bloc.dart';

class AmplitudePersonalDataSubmitter extends IPersonalDataSubmitter {
  @override
  Future<void> submitData(String email, {String name}) async {
    Analytics().logEvent(OnboardingLeadEvent(email, name: name));
  }
}