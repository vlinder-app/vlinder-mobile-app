import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  final String title;
  final String subtitle;
  final String description;
  final Color descriptionColor;

  const Header(this.title,
      {Key key,
      this.subtitle,
      this.description,
      this.descriptionColor = Palette.mediumGrey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 16.0, bottom: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          titleWidget(title),
          if (subtitle != null) subtitleWidget(subtitle),
          if (description != null) descriptionWidget(description)
        ],
      ),
    );
  }

  Widget titleWidget(String title) {
    return Text(title,
        style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 11.0,
            letterSpacing: 2.0,
            color: Palette.mediumGrey));
  }

  Widget subtitleWidget(String subtitle) {
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Text(subtitle,
          style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: 20.0,
              color: Colors.black)),
    );
  }

  Widget descriptionWidget(String description) {
    return Container(
      margin: EdgeInsets.only(top: 10.0),
      child: Text(description,
          style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 16.0,
              color: descriptionColor)),
    );
  }
}
