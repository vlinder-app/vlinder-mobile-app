import 'dart:async';
import 'dart:developer';

import 'package:android_play_install_referrer/android_play_install_referrer.dart';
import 'package:atoma_cash/models/environments/environment.dart';
import 'package:atoma_cash/resources/config/analytics/amplitude_config.dart';
import 'package:atoma_cash/resources/config/navigation/navigation.dart';
import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';
import 'package:atoma_cash/resources/repository/local/shared_storage_manager.dart';
import 'package:atoma_cash/resources/repository/remote/api_provider.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/resources/repository/remote/auth_api_provider.dart';
import 'package:atoma_cash/resources/repository/remote/auth_remote_repository.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/registration/authentication/navigation_event_listener.dart';
import 'package:atoma_cash/utils/deep_link/deep_link_handler.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:uni_links/uni_links.dart';

import './bloc.dart';

enum Flavor { PROD, DEV }

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState>
    implements EventsHandler {
  final Flavor flavor;
  StreamSubscription _sub;

  final ApplicationLocalRepository _applicationLocalRepository =
      ApplicationLocalRepository();

  final DeepLinkHandler _deepLinkHandler = DeepLinkHandler();

  final SharedStorageManager _sharedStorageManager = SharedStorageManager();

  final NavigationEventListener _navigationEventListener =
      NavigationEventListener();
  final AuthRemoteRepository _authRemoteRepository = AuthRemoteRepository();

  AuthenticationBloc(this.flavor) {
    _init(flavor);
    _authRemoteRepository.subscribeToEvents(_navigationEventListener);
  }

  @override
  AuthenticationState get initialState => Uninitialized();

  void _init(Flavor flavor) {
    _initPlatformStateForUriUniLinks();
    _initAnalytics(flavor);
    _initNetworkLayer(Environment.prod());
  }

  Future<void> _initPlatformStateForUriUniLinks() async {
    _sub = getUriLinksStream().listen((Uri uri) {
      _deepLinkHandler.handleUriFromStream(uri);
    }, onError: (Object err) {
      log("uri from stream failed - ${err.toString()}");
    });

    try {
      Uri _initialUri = await getInitialUri();
      _deepLinkHandler.handleInitialUri(_initialUri);
    } on PlatformException {
      log('Failed to get initial uri.');
    } on FormatException {
      log('Bad parse the initial link as Uri.');
    }
  }

  void _initAnalytics(Flavor flavor) =>
      Analytics().setApiKey(_analyticsApiKeyForFlavor(flavor));

  void _initNetworkLayer(Environment environment) {
    AuthApiProvider authApiProvider = AuthApiProvider(environment.authUri);
    ApiProvider apiProvider =
        ApiProvider(environment.apiUri, eventsHandler: this);
    AuthRemoteRepository(authApiProvider: authApiProvider);
    ApiRemoteRepository(apiProvider: apiProvider);
    _setRemoteRepositoriesLocalization();
  }

  @override
  Stream<AuthenticationState> mapEventToState(
    AuthenticationEvent event,
  ) async* {
    if (event is AppStarted) {
      yield* _mapAppStartedToState();
    }
    if (event is EnvironmentChanged) {
      yield* _mapEnvironmentChangedToState(event.environment);
    }
  }

  Stream<AuthenticationState> _mapAppStartedToState() async* {
    _getInstallReferrer();
    final bool isSignedIn = await _applicationLocalRepository.isSignedIn();
    if (isSignedIn) {
      await _loadSavedEnvironment();
      yield Authenticated();
    } else {
      bool isPreviewPassed = await _sharedStorageManager.getIsPreviewPassed();
      yield Unauthenticated(isPreviewPassed);
    }
  }

  Stream<AuthenticationState> _mapEnvironmentChangedToState(
      Environment environment) async* {
    _initNetworkLayer(environment);
    _applicationLocalRepository.saveEnvironment(environment);
  }

  Future<void> _loadSavedEnvironment() async {
    if (flavor == Flavor.DEV) {
      Environment environment =
          await _applicationLocalRepository.getEnvironment();
      if (environment != null) {
        add(EnvironmentChanged(environment));
      }
    }
  }

  void _getInstallReferrer() async {
    try {
      await AndroidPlayInstallReferrer.installReferrer;
    } catch (e) {
      print(e);
    }
  }

  void _setRemoteRepositoriesLocalization() {
    var locale = Intl.defaultLocale;
    ApiRemoteRepository().updateLocalization(locale);
    AuthRemoteRepository().updateLocalization(locale);
  }

  String _analyticsApiKeyForFlavor(Flavor flavor) {
    switch (flavor) {
      case Flavor.PROD:
        return AmplitudeConfig.kProdApiKey;
      case Flavor.DEV:
        return AmplitudeConfig.kDevApiKey;
      default:
        return AmplitudeConfig.kDevApiKey;
    }
  }

  @override
  Future<Function> close() {
    _sub.cancel();
    return super.close();
  }

  @override
  void onForceUpdateRequest() {
    Navigation.lockUiWithForceUpdateRequest();
  }

  @override
  void onMaintenanceMode() {
    Navigation.lockUiWithMaintenanceMode();
  }
}
