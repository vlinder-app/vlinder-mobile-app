import 'package:atoma_cash/models/environments/environment.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AuthenticationEvent extends Equatable {
  AuthenticationEvent([List props = const []]);
}

class AppStarted extends AuthenticationEvent {
  @override
  String toString() {
    return 'AppStarted{}';
  }

  @override
  List<Object> get props => [];
}

class EnvironmentChanged extends AuthenticationEvent {
  final Environment environment;

  EnvironmentChanged(this.environment);

  @override
  List<Object> get props => [environment];
}
