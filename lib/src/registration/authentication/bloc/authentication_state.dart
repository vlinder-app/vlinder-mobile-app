import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AuthenticationState extends Equatable {
  AuthenticationState([List props = const []]);
}

class Uninitialized extends AuthenticationState {
  @override
  String toString() => 'Uninitialized';

  @override
  List<Object> get props => null;
}

class Authenticated extends AuthenticationState {
  @override
  String toString() {
    return 'Authenticated{}';
  }

  @override
  List<Object> get props => null;
}

class Unauthenticated extends AuthenticationState {
  bool isPreviewPassed;

  Unauthenticated(this.isPreviewPassed);

  @override
  String toString() => 'Unauthenticated{isPreviewPassed: $isPreviewPassed}';

  @override
  List<Object> get props => [isPreviewPassed];
}
