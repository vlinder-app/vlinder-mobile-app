import 'package:atoma_cash/resources/config/navigation/navigation.dart';
import 'package:atoma_cash/resources/repository/remote/auth_remote_repository.dart';

class NavigationEventListener extends EventListener {
  @override
  void onSignOut() {
    Navigation.signOut();
  }
}
