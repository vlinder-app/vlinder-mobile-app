import 'package:meta/meta.dart';

@immutable
class ProfileState {
  final bool isFirstNameValid;
  final bool isLastNameValid;
  final bool isSuccess;
  final bool isFailed;
  final bool isSubmitted;
  final bool isButtonEnabled;

  bool get isFormValid => isFirstNameValid && isLastNameValid;

  ProfileState(
      {@required this.isFirstNameValid,
      @required this.isLastNameValid,
      @required this.isFailed,
      @required this.isSuccess,
      @required this.isButtonEnabled,
      @required this.isSubmitted});

  factory ProfileState.empty() {
    return ProfileState(
        isFirstNameValid: true,
        isLastNameValid: true,
        isFailed: false,
        isSuccess: false,
        isButtonEnabled: false,
        isSubmitted: false);
  }

  factory ProfileState.loading() {
    return ProfileState(
        isFirstNameValid: true,
        isLastNameValid: true,
        isFailed: false,
        isSuccess: false,
        isButtonEnabled: true,
        isSubmitted: true);
  }

  factory ProfileState.success() {
    return ProfileState(
        isFirstNameValid: true,
        isLastNameValid: true,
        isFailed: false,
        isSuccess: true,
        isButtonEnabled: true,
        isSubmitted: false);
  }

  factory ProfileState.failed() {
    return ProfileState(
        isFirstNameValid: true,
        isLastNameValid: true,
        isFailed: true,
        isSuccess: false,
        isButtonEnabled: true,
        isSubmitted: false);
  }

  ProfileState copyWith(
      {bool isFirstNameValid, bool isLastNameValid, bool isButtonEnabled}) {
    return ProfileState(
        isButtonEnabled: isButtonEnabled ?? this.isButtonEnabled,
        isSubmitted: isSubmitted ?? this.isSubmitted,
        isFailed: isFailed ?? this.isFailed,
        isSuccess: isSuccess ?? this.isSuccess,
        isFirstNameValid: isFirstNameValid ?? this.isFirstNameValid,
        isLastNameValid: isLastNameValid ?? this.isLastNameValid);
  }

  @override
  String toString() {
    return 'ProfileState{isFirstNameValid: $isFirstNameValid, isLastNameValid: $isLastNameValid, isSuccess: $isSuccess, isFailed: $isFailed, isSubmitted: $isSubmitted, isButtonEnabled: $isButtonEnabled}';
  }
}
