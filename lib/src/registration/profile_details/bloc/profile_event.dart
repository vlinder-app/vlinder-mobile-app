import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProfileEvent extends Equatable {
  ProfileEvent([List props = const []]);
}

class ProfileChanged extends ProfileEvent {
  final String firstName;
  final String lastName;

  ProfileChanged({@required this.firstName, @required this.lastName})
      : super([firstName, lastName]);

  @override
  String toString() {
    return 'ProfileChanged{firstName: $firstName, lastName: $lastName}';
  }

  @override
  List<Object> get props => [firstName, lastName];
}

class FirstNameChanged extends ProfileEvent {
  final String firstName;

  FirstNameChanged({@required this.firstName}) : super([firstName]);

  @override
  String toString() {
    return 'FirstNameChanged{firstName: $firstName}';
  }

  @override
  List<Object> get props => [firstName];
}

class LastNameChanged extends ProfileEvent {
  final String lastName;

  LastNameChanged({@required this.lastName}) : super([lastName]);

  @override
  String toString() {
    return 'LastNameChanged{lastName: $lastName}';
  }

  @override
  List<Object> get props => [lastName];
}

class Submitted extends ProfileEvent {
  final String firstName;
  final String lastName;

  Submitted({@required this.firstName, @required this.lastName})
      : super([firstName, lastName]);

  @override
  String toString() {
    return 'Submitted{firstName: $firstName, lastName: $lastName}';
  }

  @override
  List<Object> get props => [firstName, lastName];
}
