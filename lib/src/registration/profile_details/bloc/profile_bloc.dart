import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:atoma_cash/models/api/api/profile/request/profile_model.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/utils/validators.dart';

import './bloc.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ApiRemoteRepository _apiRemoteRepository;

  ProfileBloc() {
    _apiRemoteRepository = ApiRemoteRepository();
  }

  @override
  ProfileState get initialState => ProfileState.empty();

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    if (event is ProfileChanged) {
      yield* _mapProfileChangedToState(event.firstName, event.lastName);
    }
    if (event is FirstNameChanged) {
      yield* _mapFirstNameChangedToState(event.firstName);
    } else if (event is LastNameChanged) {
      yield* _mapLastNameChangedToState(event.lastName);
    } else if (event is Submitted) {
      yield* _mapSubmittedToState(event.firstName, event.lastName);
    }
  }

  Stream<ProfileState> _mapProfileChangedToState(
      String firstName, String lastName) async* {
    bool isButtonEnabled =
        Validators.isValidName(firstName) && Validators.isValidName(lastName);
    yield state.copyWith(isButtonEnabled: isButtonEnabled);
  }

  Stream<ProfileState> _mapFirstNameChangedToState(String firstName) async* {
    bool isFirstNameValid = Validators.isValidName(firstName);
    yield state.copyWith(
        isFirstNameValid: isFirstNameValid,
        isButtonEnabled: isFirstNameValid & state.isLastNameValid);
  }

  Stream<ProfileState> _mapLastNameChangedToState(String lastName) async* {
    bool isLastNameValid = Validators.isValidName(lastName);
    yield state.copyWith(
        isLastNameValid: isLastNameValid,
        isButtonEnabled: isLastNameValid && state.isFirstNameValid);
  }

  Stream<ProfileState> _mapSubmittedToState(
      String firstName, String lastName) async* {
    yield ProfileState.loading();
    Future.delayed(Duration(seconds: 1), () {});
    yield ProfileState.success();
  }
}
