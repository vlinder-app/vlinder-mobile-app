import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/registration/email_confirmation_request/email_confirmation_request_screen.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';

import 'bloc/bloc.dart';

class ProfileDetails extends StatefulWidget {
  @override
  _ProfileDetailsState createState() => _ProfileDetailsState();
}

class _ProfileDetailsState extends State<ProfileDetails> {
  final FocusNode _lastNameFocus = FocusNode();
  final FocusNode _firstNameFocus = FocusNode();
  ProfileBloc _profileBloc;
  TextEditingController _firstNameController, _lastNameController;

  @override
  void initState() {
    super.initState();
    _profileBloc = ProfileBloc();
    _firstNameController = TextEditingController()
      ..addListener(_onProfileChanged);
    _lastNameController = TextEditingController()
      ..addListener(_onProfileChanged);
  }

  _fieldFocusChange(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: true,
        appBar: AppBar(
          title: Text(Localization.of(context).legacyCreateAccountTitle),
          centerTitle: true,
        ),
        body: BlocListener(
          bloc: _profileBloc,
          listener: (BuildContext context, ProfileState state) {
            if (state.isSuccess) {
              _moveToEmailConfirmation();
            }
            if (state.isFailed) {}
          },
          child: BlocBuilder(
              bloc: _profileBloc,
              builder: (BuildContext context, ProfileState state) {
                return Container(
                  margin: EdgeInsets.only(left: 24.0, right: 24.0),
                  decoration: BoxDecoration(color: Colors.white),
                  child: Form(
                    child: ListView(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: 16.0),
                          child: Text(
                            Localization
                                .of(context)
                                .legacyCreateAccountDescription,
                            style: TextStyle(
                                color: Palette.darkGrey,
                                fontWeight: FontWeight.w600,
                                fontSize: 16.0),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 30.0),
                          child: TextFormField(
                            scrollPadding: EdgeInsets.only(bottom: 210.0),
                            decoration: UiHelper.styledInputDecoration(
                                Localization
                                    .of(context)
                                    .legacyFirstNameHint),
                            focusNode: _firstNameFocus,
                            autovalidate: true,
                            autocorrect: false,
                            controller: _firstNameController,
                            keyboardType: TextInputType.text,
                            maxLines: 1,
                            onFieldSubmitted: (term) {
                              _fieldFocusChange(
                                  context, _firstNameFocus, _lastNameFocus);
                            },
                            textCapitalization: TextCapitalization.sentences,
                            textInputAction: TextInputAction.next,
                            validator: (_) {
                              return null;
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 30.0),
                          child: TextFormField(
                            scrollPadding: EdgeInsets.only(bottom: 100.0),
                            decoration: UiHelper.styledInputDecoration(
                                Localization
                                    .of(context)
                                    .legacyLastNameHint),
                            focusNode: _lastNameFocus,
                            autovalidate: true,
                            autocorrect: false,
                            controller: _lastNameController,
                            textCapitalization: TextCapitalization.sentences,
                            textInputAction: TextInputAction.done,
                            keyboardType: TextInputType.text,
                            maxLines: 1,
                            validator: (_) {
                              return null;
                            },
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 24.0, bottom: 24.0),
                          child: Widgets.raisedButtonWithLoadingIndicator(
                              text: Localization
                                  .of(context)
                                  .continueButton,
                              isLoading: state.isSubmitted,
                              onPressed: state.isButtonEnabled
                                  ? _onContinueClick
                                  : null),
                        ),
                        Container(height: 250.0, color: Colors.transparent,),
                      ],
                    ),
                  ),
                );
              }),
        ));
  }

  void _onProfileChanged() {
    _profileBloc.add(ProfileChanged(
        firstName: _firstNameController.text,
        lastName: _lastNameController.text));
  }

  /*
   * temporary unused
   * 
  String _validationMessageForFirstName(ProfileState state) {
    if (state.isFirstNameValid) {
      return null;
    } else {
      return Localization
          .of(context)
          .incorrectFirstName;
    }
  }

  String _validationMessageForLastName(ProfileState state) {
    if (state.isLastNameValid) {
      return null;
    } else {
      return Localization
          .of(context)
          .incorrectLastName;
    }
  }

  void _onFirstNameChanged() {
    _profileBloc
        .add(FirstNameChanged(firstName: _firstNameController.text));
  }

  void _onLastNameChanged() {
    _profileBloc.add(LastNameChanged(lastName: _lastNameController.text));
  }

  */

  void _onContinueClick() {
    _profileBloc.add(Submitted(
        firstName: _firstNameController.text,
        lastName: _lastNameController.text));
  }

  void _moveToEmailConfirmation() {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => EmailConfirmationRequestScreen()),
            (Route<dynamic> route) => false);
  }

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _lastNameFocus.dispose();
    _firstNameFocus.dispose();
    super.dispose();
  }
}
