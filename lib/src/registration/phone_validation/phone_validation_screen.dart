import 'package:atoma_cash/models/api/api/geolocation/countries.dart';
import 'package:atoma_cash/models/environments/environment.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/country_selected_event.dart';
import 'package:atoma_cash/src/registration/authentication/bloc/authentication_bloc.dart';
import 'package:atoma_cash/src/registration/authentication/bloc/authentication_event.dart';
import 'package:atoma_cash/src/registration/phone_validation/models/retry_after.dart';
import 'package:atoma_cash/src/registration/restricted_area/bloc/amplitude_personal_data_submitter.dart';
import 'package:atoma_cash/src/registration/restricted_area/restricted_area_placeholder_result.dart';
import 'package:atoma_cash/src/registration/widgets/HeaderWidget.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:atoma_cash/utils/ui/adaptive.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bloc.dart';
import 'country_phone_search_delegate.dart';

abstract class IPhoneValidationFlowController {
  void onPhoneConfirmationCodeRequested(String phone);
}

class PhoneValidationScreen extends StatefulWidget {
  final IPhoneValidationFlowController _flowController;

  PhoneValidationScreen({
    Key key,
    IPhoneValidationFlowController flowController,
  })  : _flowController = flowController,
        super(key: key);

  @override
  _PhoneValidationScreenState createState() => _PhoneValidationScreenState();
}

class _PhoneValidationScreenState extends State<PhoneValidationScreen> {
  final _phoneController = TextEditingController();
  PhoneValidationBloc _bloc;
  String _countryPhoneCode = "";
  String _countryIso2 = "";
  Environment _currentEnvironment;

  _PhoneValidationScreenState();

  @override
  void initState() {
    _bloc = PhoneValidationBloc()..add(Started());
    _phoneController.addListener(_onPhoneChanged);
    _currentEnvironment = Environment.prod();
    WidgetsBinding.instance.addPostFrameCallback(
        (_) => _onEnvironmentChanged(_currentEnvironment));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(),
      body: BlocListener(
        bloc: _bloc,
        listener: _blocListener,
        child: BlocBuilder(
            bloc: _bloc,
            condition: (previous, current) => current is! RestrictedAreaState,
            builder: (BuildContext context, PhoneState state) {
              return Container(
                margin: EdgeInsets.only(left: 24.0, right: 24.0),
                decoration: BoxDecoration(color: Colors.white),
                child: Form(
                  child: ListView(
                    children: <Widget>[
                      Header(Localization.of(context).step1,
                          subtitle:
                              Localization.of(context).verifyPhoneDescription),
                      Padding(
                        padding: const EdgeInsets.only(top: 12.0),
                        child: Row(
                          children: <Widget>[
                            Flexible(
                              child: _countrySelectorWidgetTwo(context),
                              flex: Adaptive.isFourInchScreen(context) ? 8 : 5,
                            ),
                            Flexible(
                              flex: Adaptive.isFourInchScreen(context) ? 11 : 9,
                              child: _phoneNumberWidget(context),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: _phoneValidationErrorWidget(state),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 24.0),
                        child: _widgetForState(state),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 24.0),
                        child: _environmentSelection(context),
                      )
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }

  void _blocListener(BuildContext context, PhoneState state) {
    if (state is PhoneValidationState) {
      if (state.isSuccess) {
        widget._flowController.onPhoneConfirmationCodeRequested(
            _countryPhoneCode + _phoneController.text);
      }
      if (state.isSubmitting) {
        FocusScope.of(context).requestFocus(new FocusNode());
      }
      if (state.isReset) {
        _phoneController.clear();
      }
    }
    if (state is PhoneCodeLoadedState) {
      _countryPhoneCode = state.phoneCode;
      _countryIso2 = state.phoneCode;
    }

    if (state is RestrictedAreaState) {
      _showRestrictedAreaPlaceholder(context);
    }
  }

  Widget _environmentSelection(BuildContext context) {
    var flavor = BlocProvider.of<AuthenticationBloc>(context).flavor;
    if (flavor == Flavor.DEV) {
      return Widgets.styledFlatButton(
        text: "env: ${_currentEnvironment.environmentName}",
        onPressed: _onChangeEnvironmentClick,
      );
    }
  }

  void _onChangeEnvironmentClick() async {
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.environmentsScreen,
        arguments: EnvironmentsScreenArguments(
            currentEnvironment: _currentEnvironment));
    _handleEnvironmentChangedResult(result);
  }

  void _handleEnvironmentChangedResult(var result) {
    if (result != null) {
      if (result is Environment &&
          result.apiUri != _currentEnvironment.apiUri) {
        _currentEnvironment = result;
        _onEnvironmentChanged(_currentEnvironment);
        _bloc.add(Started());
      }
    }
  }

  void _onEnvironmentChanged(Environment environment) {
    var bloc = BlocProvider.of<AuthenticationBloc>(context);
    bloc.add(EnvironmentChanged(environment));
  }

  Widget _phoneNumberWidget(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0),
      child: TextFormField(
        controller: _phoneController,
        decoration: UiHelper.styledInputDecoration(
            Localization.of(context).phoneNumber),
        inputFormatters: [
          WhitelistingTextInputFormatter(RegExp(r"[+0-9]")),
        ],
        maxLength: 15,
        autovalidate: true,
        buildCounter: (BuildContext context,
                {int currentLength, int maxLength, bool isFocused}) =>
            null,
        keyboardType: TextInputType.phone,
        maxLines: 1,
      ),
    );
  }

  Widget _phoneValidationErrorWidget(PhoneValidationState state) {
    String phoneValidationError = _phoneValidationError(state);
    if (phoneValidationError != null) {
      return Text(
        phoneValidationError,
        style: TextStyle(color: Palette.red, fontSize: 12.0),
      );
    } else {
      return Container();
    }
  }

  Widget _countrySelectorWidgetTwo(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8.0),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            _onCountryClick(context);
          },
          child: InputDecorator(
            decoration: UiHelper.styledInputDecoration(
                Localization.of(context).countryHint),
            isEmpty: false,
            child: SizedBox(
              height: 20.0,
              child: DropdownButton<String>(
                isExpanded: true,
                hint: Text(
                  _countryPhoneCode,
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Palette.darkGrey),
                ),
                underline: Container(),
                onChanged: (s) {},
                items: [],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _onCountryClick(BuildContext context) async {
    if (_bloc.countries == null) {
      cantLoadCountriesSnackBar(context);
    } else {
      final Country selectedCountry = await showSearch(
          context: context,
          delegate: CountryPhoneSearchDelegate(_bloc.countries));
      if (selectedCountry != null) {
        Analytics().logEvent(CountrySelectedEvent(selectedCountry.iso2Code));
        _countryPhoneCode = selectedCountry.phoneCode;
        _countryIso2 = selectedCountry.iso2Code;
        _bloc.add(PhoneChanged(_countryPhoneCode, _phoneController.text));
      }
    }
  }

  String _phoneValidationError(PhoneValidationState state) {
    if (!state.isPhoneValid) {
      return Localization.of(context).incorrectPhoneNumber;
    } else if (state.isFailure) {
      return Localization.of(context).legacyUnableToSendValidationCode;
    } else if (state.isPhoneValidationError) {
      return Localization.of(context).legacyWrongPhoneNumber;
    } else if (state.isPhoneValidationError) {
      return null;
    } else if (state.isRetryLater) {
      return Localization.of(context).unableToSendPasscodeTryAfterMinutes(
          state.retryAfterDuration?.inMinutes ??
              RetryAfter.kRetryAfterDefaultMinutesCount);
    } else if (state.isCodeLimitExceeded) {
      return Localization.of(context).tooManyAttemptsOnPasscode;
    }
  }

  Widget _widgetForState(PhoneValidationState state) {
    return Widgets.raisedButtonWithLoadingIndicator(
        text: Localization.of(context).continueButton,
        isLoading: state.isSubmitting,
        onPressed: state.isButtonEnabled && !state.isSubmitting
            ? _requestSmsCode
            : null);
  }

  void cantLoadCountriesSnackBar(BuildContext context) {
    Scaffold.of(context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          duration: Duration(hours: 1),
          action: SnackBarAction(
            textColor: Colors.white,
            label: Localization.of(context).retryButton,
            onPressed: _updateCountries,
          ),
          content: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(Localization.of(context).cantLoadCountries),
            ],
          ),
          backgroundColor: Palette.red,
        ),
      );
  }

  void _showRestrictedAreaPlaceholder(BuildContext context) async {
    final result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
      Routes.restrictedAreaPlaceholderScreen,
      arguments: RestrictedAreaPlaceholderScreenArguments(
        dataSubmitter: AmplitudePersonalDataSubmitter(),
      ),
    );

    if (result != null) {
      if (result is RestrictedAreaPlaceholderDataSubmitted) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              duration: Duration(seconds: 10),
              content: Text(
                  Localization.of(context)
                      .restrictedAreaPlaceholderSubmittedMessage,
                  style: FontBook.textBody2Menu.copyWith(color: Colors.white)),
              backgroundColor: Palette.darkGrey,
            ),
          );
        _bloc.add(RestrictedAreaPersonalDataSubmitted());
      }
    }
  }

  void _updateCountries() {
    _bloc.add(Started());
  }

  void _requestSmsCode() {
    _bloc
        .add(Submitted(_countryPhoneCode, _phoneController.text, _countryIso2));
  }

  void _onPhoneChanged() {
    _bloc.add(PhoneChanged(_countryPhoneCode, _phoneController.text));
  }

  @override
  void dispose() {
    _phoneController.dispose();
    super.dispose();
  }
}
