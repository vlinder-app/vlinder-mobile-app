import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PhoneState extends Equatable {
  PhoneState([List props = const []]);
}

class LoadingState extends PhoneState {
  @override
  String toString() {
    return 'LoadingState{}';
  }

  @override
  List<Object> get props => null;
}

class PhoneCodeLoadedState extends PhoneState {
  final String phoneCode;
  final String countryIso2;

  PhoneCodeLoadedState(this.phoneCode, this.countryIso2);

  @override
  String toString() {
    return 'PhoneCodeLoadedState{phoneCode: $phoneCode, countryIso2: $countryIso2}';
  }

  @override
  List<Object> get props => [phoneCode];
}

@immutable
class PhoneValidationState extends PhoneState {
  final bool isPhoneValid;
  final bool isButtonEnabled;
  final bool isSubmitting;
  final bool isSuccess;
  final bool isFailure;
  final bool isPhoneValidationError;
  final bool isCodeLimitExceeded;
  final bool isRetryLater;
  final bool isReset;
  final Duration retryAfterDuration;
  final String phoneCode;

  PhoneValidationState(
      {@required this.isPhoneValid,
      this.phoneCode,
      @required this.isSubmitting,
      @required this.isSuccess,
      @required this.isButtonEnabled,
      @required this.isFailure,
      @required this.isRetryLater,
      @required this.isPhoneValidationError,
      @required this.retryAfterDuration,
      @required this.isCodeLimitExceeded,
      this.isReset = false})
      : super([
          isPhoneValid,
          isSubmitting,
          isSuccess,
          isButtonEnabled,
          isFailure,
          isRetryLater,
          isPhoneValidationError,
          isCodeLimitExceeded,
          retryAfterDuration,
          isReset,
        ]);

  factory PhoneValidationState.empty() {
    return PhoneValidationState(
        isPhoneValid: true,
        isSubmitting: false,
        isButtonEnabled: false,
        isSuccess: false,
        isFailure: false,
        isRetryLater: false,
        isPhoneValidationError: false,
        retryAfterDuration: null,
        isCodeLimitExceeded: false);
  }

  factory PhoneValidationState.loading() {
    return PhoneValidationState(
        isPhoneValid: true,
        isSubmitting: true,
        isButtonEnabled: true,
        isSuccess: false,
        isFailure: false,
        isRetryLater: false,
        retryAfterDuration: null,
        isCodeLimitExceeded: false,
        isPhoneValidationError: false);
  }

  factory PhoneValidationState.success() {
    return PhoneValidationState(
        isPhoneValid: true,
        isSubmitting: false,
        isButtonEnabled: true,
        isSuccess: true,
        isFailure: false,
        isRetryLater: false,
        retryAfterDuration: null,
        isCodeLimitExceeded: false,
        isPhoneValidationError: false);
  }

  factory PhoneValidationState.failure() {
    return PhoneValidationState(
        isPhoneValid: true,
        isSubmitting: false,
        isButtonEnabled: true,
        isSuccess: false,
        isFailure: true,
        isRetryLater: false,
        isCodeLimitExceeded: false,
        retryAfterDuration: null,
        isPhoneValidationError: false);
  }

  factory PhoneValidationState.retryAfter(Duration retryAfterDuration) {
    return PhoneValidationState(
        isPhoneValid: true,
        isSubmitting: false,
        isButtonEnabled: true,
        isSuccess: false,
        isFailure: false,
        isRetryLater: true,
        isCodeLimitExceeded: false,
        retryAfterDuration: retryAfterDuration,
        isPhoneValidationError: false);
  }

  factory PhoneValidationState.wrongPhoneFailure() {
    return PhoneValidationState(
        isPhoneValid: true,
        isSubmitting: false,
        isButtonEnabled: true,
        isSuccess: false,
        isFailure: false,
        isRetryLater: false,
        isCodeLimitExceeded: false,
        retryAfterDuration: null,
        isPhoneValidationError: true);
  }

  factory PhoneValidationState.codeLimitExceeded() {
    return PhoneValidationState(
        isPhoneValid: true,
        isSubmitting: false,
        isButtonEnabled: true,
        isSuccess: false,
        isFailure: false,
        isRetryLater: false,
        isCodeLimitExceeded: true,
        retryAfterDuration: null,
        isPhoneValidationError: false);
  }

  factory PhoneValidationState.restrictedArea() {
    return PhoneValidationState(
        isPhoneValid: true,
        isSubmitting: false,
        isButtonEnabled: false,
        isSuccess: false,
        isFailure: false,
        isRetryLater: false,
        isPhoneValidationError: false,
        retryAfterDuration: null,
        isCodeLimitExceeded: false);
  }

  PhoneValidationState copyWith(
      {bool isPhoneValid,
      bool isButtonEnabled,
      bool isSuccess,
      bool isPhoneValidationError,
      bool isRetryAfter,
      Duration retryAfterDuration,
      String phoneCode,
      bool isCodeLimitExceeded,
      bool isReset}) {
    return PhoneValidationState(
        phoneCode: phoneCode ?? this.phoneCode,
        retryAfterDuration: retryAfterDuration ?? this.retryAfterDuration,
        isCodeLimitExceeded: isCodeLimitExceeded ?? this.isCodeLimitExceeded,
        isPhoneValid: isPhoneValid ?? this.isPhoneValid,
        isSubmitting: this.isSubmitting,
        isRetryLater: this.isRetryLater,
        isButtonEnabled: isButtonEnabled ?? this.isButtonEnabled,
        isSuccess: isSuccess ?? this.isSuccess,
        isFailure: this.isFailure,
        isReset: isReset ?? this.isReset,
        isPhoneValidationError:
            isPhoneValidationError ?? this.isPhoneValidationError);
  }

  @override
  String toString() {
    return 'PhoneValidationState{isPhoneValid: $isPhoneValid, isButtonEnabled: $isButtonEnabled, isSubmitting: $isSubmitting, isSuccess: $isSuccess, isFailure: $isFailure, isPhoneValidationError: $isPhoneValidationError, isCodeLimitExceeded: $isCodeLimitExceeded, isRetryLater: $isRetryLater, retryAfterDuration: $retryAfterDuration, isReset: $isReset}';
  }

  @override
  List<Object> get props => [
        isCodeLimitExceeded,
        isPhoneValidationError,
        isSubmitting,
        isButtonEnabled,
        isSuccess,
        isFailure,
        isRetryLater,
        isPhoneValidationError,
        retryAfterDuration,
        phoneCode,
        isReset
      ];
}

class RestrictedAreaState extends PhoneState {
  RestrictedAreaState();

  @override
  List<Object> get props => null;
}
