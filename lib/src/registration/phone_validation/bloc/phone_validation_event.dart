import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PhoneValidationEvent extends Equatable {
  PhoneValidationEvent([List props = const []]);
}

class Started extends PhoneValidationEvent {
  @override
  String toString() {
    return 'Started{}';
  }

  @override
  List<Object> get props => null;
}

class PhoneChanged extends PhoneValidationEvent {
  final String phone;
  final String phoneCode;

  PhoneChanged(this.phoneCode, this.phone) : super([phone, phoneCode]);

  @override
  String toString() {
    return 'PhoneChanged{phone: $phone, phoneCode: $phoneCode}';
  }

  @override
  List<Object> get props => [phone, phoneCode];
}

class Submitted extends PhoneValidationEvent {
  final String phone;
  final String phoneCode;
  final String countryIso2;

  Submitted(this.phoneCode, this.phone, this.countryIso2);

  @override
  String toString() {
    return 'Submitted{phone: $phone, phoneCode: $phoneCode, countryIso2: $countryIso2}';
  }

  @override
  List<Object> get props => [phone, phoneCode];
}

class RestrictedAreaPersonalDataSubmitted extends PhoneValidationEvent {
  RestrictedAreaPersonalDataSubmitted();

  @override
  List<Object> get props => null;
}