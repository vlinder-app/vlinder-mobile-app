import 'dart:async';

import 'package:atoma_cash/models/api/api/geolocation/countries.dart';
import 'package:atoma_cash/models/api/api/geolocation/geolocation.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart' as api;
import 'package:atoma_cash/models/api/error_type/error_type.dart';
import 'package:atoma_cash/models/auth_api/auth/response/phone_confirmation.dart';
import 'package:atoma_cash/models/auth_api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/resources/repository/remote/auth_remote_repository.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/phone_entered_event.dart';
import 'package:atoma_cash/src/registration/phone_validation/models/retry_after.dart';
import 'package:atoma_cash/utils/registration/phone_confirmation_helper.dart';
import 'package:atoma_cash/utils/validators.dart';
import 'package:bloc/bloc.dart';

import './bloc.dart';

class PhoneValidationBloc extends Bloc<PhoneValidationEvent, PhoneState> {
  @override
  PhoneValidationState get initialState => PhoneValidationState.empty();

  AuthRemoteRepository _remoteRepository = AuthRemoteRepository();
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  PhoneConfirmationHelper _smsRequestHelper = PhoneConfirmationHelper();

  List<Country> countries;
  List<Country> availableCountries;

  bool _isPhoneValid(String phone) {
    if (phone.length < phoneValidationLimitLength) {
      return true;
    } else {
      return Validators.isValidPhone(phone);
    }
  }

  @override
  Stream<PhoneState> mapEventToState(
    PhoneValidationEvent event,
  ) async* {
    if (event is PhoneChanged) {
      yield* _mapPhoneChangedToState(event.phoneCode, event.phone);
    } else if (event is Submitted) {
      yield* _mapFormSubmitToState(
          event.phoneCode, event.phone, event.countryIso2);
    } else if (event is Started) {
      yield* _mapStartedToState();
    } else if (event is RestrictedAreaPersonalDataSubmitted) {
      yield* _mapRestrictedAreaPersonalDataSubmittedToState();
    }
  }

  List<Country> prepareCountiesListWithPhoneCodes(List<Country> countries) {
    List<Country> countriesWithCodes = [];
    countries.forEach((e) {
      e.countryCodes.forEach((c) {
        countriesWithCodes
            .add(Country(name: e.name, phoneCode: c, iso2Code: e.iso2Code));
      });
    });
    return countriesWithCodes;
  }

  String _phoneCodeForIso(String isoCode) {
    String phoneCode = "";
    countries.forEach((c) {
      if (c.iso2Code == isoCode) {
        phoneCode = c?.phoneCode ?? "";
      }
    });
    return phoneCode;
  }

  Stream<PhoneState> _mapStartedToState() async* {
    yield PhoneValidationState.loading();

    final allCountriesRequest = _apiRemoteRepository.getAllCountries();
    final availableCountriesRequest =
        _apiRemoteRepository.getAvailableCountries();

    var responses =
        await Future.wait([allCountriesRequest, availableCountriesRequest]);

    if (responses.every((request) => request.isSuccess())) {
      countries = prepareCountiesListWithPhoneCodes(responses[0].result);
      availableCountries = responses[1].result;
      api.BaseResponse<Geolocation> geolocationResponse =
          await _apiRemoteRepository.getGeolocation();
      if (geolocationResponse.isSuccess()) {
        final iso2Code = geolocationResponse.result.iso2Code;
        yield PhoneCodeLoadedState(_phoneCodeForIso(iso2Code), iso2Code);
      }
    } else {
      countries = null;
      availableCountries = null;
    }

    yield PhoneValidationState.empty();
  }

  Stream<PhoneValidationState> _mapPhoneChangedToState(
      String phoneCode, String phone) async* {
    if (state is PhoneValidationState) {
      yield (state as PhoneValidationState).copyWith(
          phoneCode: phoneCode,
          isPhoneValid: _isPhoneValid(phoneCode + phone),
          isButtonEnabled: Validators.isValidPhone(phoneCode + phone),
          isSuccess: false,
          isReset: false);
    }
  }

  Stream<PhoneState> _mapFormSubmitToState(
      String phoneCode, String phone, String countryIso2) async* {
    if (_isAvailableCountry(countryIso2)) {
      String phoneNumber = phoneCode + phone;
      Analytics().logEvent(PhoneEnteredEvent(phoneNumber));
      yield PhoneValidationState.loading();
      if (await _smsRequestHelper.isContainExistingRequest(phoneNumber)) {
        yield PhoneValidationState.success();
      } else {
        BaseResponse<PhoneConfirmation> response = await _remoteRepository
            .requestSMS(phoneNumber: phoneNumber, countryIso2: countryIso2);
        if (response.isSuccess()) {
          _smsRequestHelper.savePhoneNumber(phoneNumber);
          yield PhoneValidationState.success();
        } else {
          yield* _handleFailedCodeRequest(response.error);
        }
      }
    } else {
      yield RestrictedAreaState();
      yield PhoneValidationState.empty();
    }
  }

  Stream<PhoneState> _mapRestrictedAreaPersonalDataSubmittedToState() async* {
    yield PhoneValidationState.empty().copyWith(isReset: true);
  }

  Stream<PhoneValidationState> _handleFailedCodeRequest(Error error) async* {
    int code = error.code;
    if (code == 400) {
      yield PhoneValidationState.wrongPhoneFailure();
    } else if (code == 401 &&
        error.errorDetails.type == kPasscodeLimitExceeded) {
      yield PhoneValidationState.codeLimitExceeded();
    } else if (code == 429 && error.errorDetails.type == kRetryLater) {
      yield* _handleRetryAfterError(error);
    } else {
      yield PhoneValidationState.failure();
    }
  }

  Stream<PhoneValidationState> _handleRetryAfterError(Error error) async* {
    RetryAfter retryAfter;
    try {
      retryAfter = RetryAfter.fromJson(error.rawResponseData);
    } catch (e) {
      print(e);
    }
    Duration differenceDuration;
    if (retryAfter != null) {
      differenceDuration = retryAfter.retryAfter.difference(DateTime.now());
    }
    yield PhoneValidationState.retryAfter(differenceDuration);
  }

  bool _isAvailableCountry(String countryIso2) =>
      availableCountries.any((country) => country.iso2Code == countryIso2);
}
