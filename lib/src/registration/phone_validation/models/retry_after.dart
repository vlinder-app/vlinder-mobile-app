// To parse this JSON data, do
//
//     final retryAfter = retryAfterFromJson(jsonString);

import 'dart:convert';

RetryAfter retryAfterFromJson(String str) =>
    RetryAfter.fromJson(json.decode(str));

String retryAfterToJson(RetryAfter data) => json.encode(data.toJson());

class RetryAfter {
  static const kRetryAfterDefaultMinutesCount = 5;

  DateTime retryAfter;

  RetryAfter({
    this.retryAfter,
  });

  RetryAfter copyWith({
    DateTime retryAfter,
  }) =>
      RetryAfter(
        retryAfter: retryAfter ?? this.retryAfter,
      );

  factory RetryAfter.fromJson(Map<String, dynamic> json) => RetryAfter(
        retryAfter: json["retryAfter"] == null
            ? null
            : DateTime.parse(json["retryAfter"]),
      );

  Map<String, dynamic> toJson() => {
        "retryAfter": retryAfter == null ? null : retryAfter.toIso8601String(),
      };
}
