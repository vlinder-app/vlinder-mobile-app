import 'package:atoma_cash/models/api/api/geolocation/countries.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';
import 'package:sprintf/sprintf.dart';

class CountryPhoneSearchDelegate extends SearchDelegate<Country> {
  static const countryFlagUrl = "https://www.countryflags.io/%s/flat/48.png";

  final List<Country> values;

  List<Country> filterName = new List();

  CountryPhoneSearchDelegate(this.values);

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      if (query.isNotEmpty)
        IconButton(
          tooltip: 'Clear',
          icon: const Icon(
            (Icons.clear),
            color: Palette.mediumGrey,
          ),
          onPressed: () {
            query = '';
            showSuggestions(context);
          },
        ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    return _queryFilterWidget();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return _queryFilterWidget();
  }

  Widget _queryFilterWidget() {
    final suggestions = values
        .where((c) => c.name.toLowerCase().contains(query.toLowerCase()))
        .toList();

    return ListView.builder(
        itemCount: suggestions.length,
        itemBuilder: (BuildContext context, int index) {
          return new ListTile(
            leading: Image.network(
              sprintf(countryFlagUrl, [suggestions[index].iso2Code]),
              width: 32.0,
            ),
            trailing: Padding(
              padding: EdgeInsets.all(8.0),
              child: Text(
                suggestions[index].phoneCode,
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
            ),
            title: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                suggestions[index].name,
                style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.w600),
              ),
            ),
            onTap: () {
              close(context, suggestions[index]);
            },
          );
        });
  }
}
