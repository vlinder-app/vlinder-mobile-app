import 'package:atoma_cash/models/api/api/profile/response/profile.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/email_confirmed_event.dart';
import 'package:atoma_cash/src/registration/email_confirmation/bloc/bloc.dart';

class EmailProfileConfirmationBloc extends EmailConfirmationBloc {
  ApiRemoteRepository _apiRemoteRepository;

  EmailProfileConfirmationBloc() {
    _apiRemoteRepository = ApiRemoteRepository();
  }

  @override
  EmailConfirmationState get initialState => InitialState();

  @override
  Stream<EmailConfirmationState> mapEventToState(
      EmailConfirmationEvent event,
      ) async* {
    if (event is ResendClicked) {
      yield* _mapResendEventToState(event.email);
    }
    if (event is ContinueClicked) {
      yield* _mapContinueClickToState();
    }
  }

  Stream<EmailConfirmationState> _mapResendEventToState(String email) async* {
    yield LoadingState();
    BaseResponse<Null> response = await _apiRemoteRepository.confirmEmail();
    if (response.isSuccess()) {
      yield SuccessResendState();
    } else {
      if (response.error.code == 409) {
        yield EmailIsUsedState();
      } else if (response.error.code == 429) {
        yield ExceededConfrimationAttemptsState();
      } else {
        yield FailedResendState();
        yield InitialState();
      }
    }
  }

  Stream<EmailConfirmationState> _mapContinueClickToState() async* {
    yield LoadingState();
    BaseResponse<Profile> response = await _apiRemoteRepository.getProfile();
    if (response.isSuccess()) {
      Profile profile = response.result;
      if (profile.emailConfirmed ?? false) {
        Analytics().logEvent(EmailConfirmedEvent(Confirmed.yes));
        yield SuccessConfirmationState();
      } else {
        Analytics().logEvent(EmailConfirmedEvent(Confirmed.no));
        yield FailedConfirmationState();
        yield InitialState();
      }
    } else {
      Analytics().logEvent(EmailConfirmedEvent(Confirmed.no));
      yield FailedConfirmationState();
      yield InitialState();
    }
  }
}