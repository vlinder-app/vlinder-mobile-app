import 'package:atoma_cash/models/auth_api/auth/response/email_verification_response.dart';
import 'package:atoma_cash/models/auth_api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/auth_remote_repository.dart';
import 'package:atoma_cash/src/registration/email_confirmation/bloc/bloc.dart';

class EmailValidationBloc extends EmailConfirmationBloc {
  AuthRemoteRepository _authRemoteRepository;

  EmailValidationBloc() {
    _authRemoteRepository = AuthRemoteRepository();
  }

  @override
  EmailConfirmationState get initialState => InitialState();

  @override
  Stream<EmailConfirmationState> mapEventToState(
      EmailConfirmationEvent event) async* {
    if (event is Started) {
      yield* _mapStartedEventToState();
    }
    if (event is ResendClicked) {
      yield* _mapResendEventToState();
    }
    if (event is ContinueClicked) {
      yield* _mapContinueClickToState();
    }
  }

  Stream<EmailConfirmationState> _mapStartedEventToState() async* {
    yield* _mapResendEventToState();
  }

  Stream<EmailConfirmationState> _mapResendEventToState() async* {
    yield LoadingState();
    BaseResponse<EmailVerificationResponse> response =
        await _authRemoteRepository.requestEmailVerificationCode();
    if (response.isSuccess()) {
      yield SuccessResendState();
    } else {
      if (response.error.code == 429 || response.error.code == 401) {
        yield ExceededConfrimationAttemptsState();
      } else {
        yield FailedResendState();
      }
      yield InitialState();
    }
  }

  Stream<EmailConfirmationState> _mapContinueClickToState() async* {
    yield LoadingState();
    BaseResponse<EmailVerificationResponse> response =
        await _authRemoteRepository.checkIsEmailVerificationPassed();
    if (response.isSuccess()) {
      yield SuccessConfirmationState();
    } else {
      yield FailedConfirmationState();
      yield InitialState();
    }
  }
}
