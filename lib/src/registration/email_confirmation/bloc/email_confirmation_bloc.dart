import 'package:bloc/bloc.dart';

import './bloc.dart';

abstract class EmailConfirmationBloc
    extends Bloc<EmailConfirmationEvent, EmailConfirmationState> {}
