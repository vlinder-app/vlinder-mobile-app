import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

@immutable
abstract class EmailConfirmationEvent extends Equatable {
  EmailConfirmationEvent([List props = const []]);
}

class Started extends EmailConfirmationEvent {
  @override
  String toString() {
    return 'Started{}';
  }

  @override
  List<Object> get props => null;
}

class ResendClicked extends EmailConfirmationEvent {
  final String email;

  ResendClicked({@required this.email});

  @override
  String toString() {
    return 'ResendClicked{email: $email}';
  }

  @override
  List<Object> get props => [email];
}

class ContinueClicked extends EmailConfirmationEvent {
  @override
  String toString() {
    return 'ContinueClicked{}';
  }

  @override
  List<Object> get props => null;
}
