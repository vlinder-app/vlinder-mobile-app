import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class EmailConfirmationState {
  EmailConfirmationState([List props = const []]);
}

class InitialState extends EmailConfirmationState {
  @override
  String toString() {
    return 'InitialState{}';
  }
}

class LoadingState extends EmailConfirmationState {
  @override
  String toString() {
    return 'LoadingState{}';
  }
}

class SuccessResendState extends EmailConfirmationState {
  @override
  String toString() {
    return 'SuccessResendState{}';
  }
}

class SuccessConfirmationState extends EmailConfirmationState {
  @override
  String toString() {
    return 'SuccessConfirmationState{}';
  }
}

class FailedConfirmationState extends EmailConfirmationState {
  @override
  String toString() {
    return 'FailedConfirmationState{}';
  }
}

class FailedResendState extends EmailConfirmationState {
  @override
  String toString() {
    return 'FailedResendState{}';
  }
}

class ExceededConfrimationAttemptsState extends EmailConfirmationState {
  @override
  String toString() {
    return 'ExceededConfrimationAttemptsState{}';
  }
}

class EmailIsUsedState extends EmailConfirmationState {
  @override
  String toString() {
    return 'EmailIsUsedState{}';
  }
}
