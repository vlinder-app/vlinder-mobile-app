import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/registration/email_confirmation/bloc/bloc.dart';
import 'package:atoma_cash/src/registration/email_confirmation/email_confirmation_screen_configuration.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class IEmailConfirmationScreenFlowController {
  bool get isConfirmationRequired;

  void onEmailConfirmed();

  void onSkipEmailConfirmation();

  void onDestructiveActionClicked(String email);
}

class EmailConfirmationScreen extends StatefulWidget {
  final String email;
  final EmailConfirmationBloc _bloc;
  final IEmailConfirmationScreenFlowController _flowController;
  final IEmailConfirmationScreenConfiguration _configuration;
  final bool withInitialConfirmationRequest;

  EmailConfirmationScreen({
    Key key,
    this.email,
    this.withInitialConfirmationRequest = true,
    @required EmailConfirmationBloc bloc,
    @required IEmailConfirmationScreenFlowController flowController,
    @required IEmailConfirmationScreenConfiguration configuration,
  })  : _bloc = bloc,
        _flowController = flowController,
        _configuration = configuration,
        super(key: key);

  @override
  _EmailConfirmationScreenState createState() =>
      _EmailConfirmationScreenState();
}

class _EmailConfirmationScreenState extends State<EmailConfirmationScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _actionButtonTextStyle = TextStyle(color: Colors.white, fontSize: 13.0);
  EmailConfirmationBloc _emailConfirmationBloc;

  @override
  void initState() {
    super.initState();
    _emailConfirmationBloc = widget._bloc;
    if (widget.withInitialConfirmationRequest) {
      _emailConfirmationBloc.add(Started());
    }
  }

  void _onResendButtonClick() {
    _emailConfirmationBloc.add(ResendClicked(email: widget.email));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Palette.blue, // status bar color
        brightness: Brightness.dark, // status bar brightness
      ),
      body: BlocListener(
        bloc: _emailConfirmationBloc,
        listener: (BuildContext context, EmailConfirmationState state) {
          if (state is SuccessResendState) {
            Widgets.notificationSnackBar(
                context, Localization.of(context).confirmationLinkSent);
          }
          if (state is FailedResendState) {
            Widgets.failedSnackBar(
                context, Localization.of(context).unableToSendVerificationLink);
          }
          if (state is ExceededConfrimationAttemptsState) {
            _onExceedEmailConfirmationAttempts(context);
          }
          if (state is FailedConfirmationState) {
            Widgets.notificationSnackBar(context,
                Localization.of(context).legacyPleaseConfirmYourEmailAddress);
          }
          if (state is SuccessConfirmationState) {
            widget._flowController.onEmailConfirmed();
          }
          if (state is EmailIsUsedState) {
            Widgets.failedSnackBar(
                context, Localization.of(context).emailIsUsed);
          }
        },
        child: BlocBuilder(
            bloc: _emailConfirmationBloc,
            builder: (BuildContext context, EmailConfirmationState state) {
              return _rootWidgetForState(state);
            }),
      ),
    );
  }

  Widget _rootWidgetForState(EmailConfirmationState state) {
    if (state is LoadingState) {
      return Center(child: CircularProgressIndicator());
    } else {
      return _newRootWidgetTree();
    }
  }

  Widget _newRootWidgetTree() {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 13,
          child: Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(bottom: 30.0),
                child: Container(
                  decoration: BoxDecoration(
                      color: Palette.blue,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(20.0),
                          bottomRight: Radius.circular(20.0))),
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        flex: 3,
                        child: Center(
                          child: Container(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Stack(
                                  children: <Widget>[
                                    Image.asset(
                                      Assets.dotsPattern,
                                    ),
                                    Image.asset(
                                      Assets.okIcon,
                                    ),
                                  ],
                                  alignment: Alignment.center,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 40.0, left: 24.0, right: 24.0),
                                  child: Text(
                                    widget._configuration.message(context),
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 0.0),
                          child: _buttonsBar(),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Align(
                child: Padding(
                  padding: const EdgeInsets.only(
                      bottom: 10.0, left: 24.0, right: 24.0),
                  child: SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        child: Text(
                          Localization.of(context).continueButton,
                          style: TextStyle(
                              color: Palette.blue, fontWeight: FontWeight.w600),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0)),
                        color: Colors.white,
                        onPressed: _onContinueClick,
                        elevation: 5.0,
                      )),
                ),
                alignment: Alignment.bottomCenter,
              ),
            ],
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            color: Colors.white,
          ),
        )
      ],
    );
  }

  Widget _buttonsBar() {
    return Row(
      children: <Widget>[
        FlatButton.icon(
            onPressed: _onResendButtonClick,
            icon: Image.asset(
              Assets.resendIcon,
              width: 12.0,
              height: 12.0,
            ),
            label: Text(
              Localization.of(context).resendButton,
              style: _actionButtonTextStyle,
            )),
        _destructiveAction(),
      ],
      mainAxisAlignment: MainAxisAlignment.spaceAround,
    );
  }

  Widget _destructiveAction() {
    return FlatButton.icon(
        onPressed: _onDestructiveActionClicked,
        icon: Image.asset(
          widget._configuration.destructiveActionIconAsset(),
          width: 12.0,
          height: 12.0,
        ),
        label: Text(
          widget._configuration.destructiveActionTitle(context),
          style: _actionButtonTextStyle,
        ));
  }

  void _onDestructiveActionClicked() {
    widget._flowController.onDestructiveActionClicked(widget.email);
  }

  void _onExceedEmailConfirmationAttempts(BuildContext context) {
    Widgets.failedSnackBar(context,
        Localization.of(context).thereAreTooManyEmailAttemptsTryIn24hours);
  }

  void _onContinueClick() {
    if (widget._flowController.isConfirmationRequired) {
      _emailConfirmationBloc.add(ContinueClicked());
    } else {
      widget._flowController.onSkipEmailConfirmation();
    }
  }
}
