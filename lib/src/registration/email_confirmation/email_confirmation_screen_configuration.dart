import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/cupertino.dart';

abstract class IEmailConfirmationScreenConfiguration {
  String message(BuildContext context);

  String destructiveActionTitle(BuildContext context);

  String destructiveActionIconAsset();
}

class EmailProfileConfirmationConfiguration
    implements IEmailConfirmationScreenConfiguration {
  final String email;

  EmailProfileConfirmationConfiguration(this.email);

  @override
  String message(BuildContext context) =>
      Localization.of(context).weSentConfirmationLintToLink(email);

  @override
  String destructiveActionTitle(BuildContext context) =>
      Localization.of(context).changeEmailAddressButton;

  String destructiveActionIconAsset() => Assets.editIcon;
}

class EmailValidationConfiguration
    implements IEmailConfirmationScreenConfiguration {
  final String email;

  EmailValidationConfiguration(this.email);

  @override
  String message(BuildContext context) =>
      Localization.of(context).weSentRecoveryLintToLink(email);

  @override
  String destructiveActionTitle(BuildContext context) =>
      Localization
          .of(context)
          .backToSignInButton;

  String destructiveActionIconAsset() => Assets.backToSignInIcon;
}