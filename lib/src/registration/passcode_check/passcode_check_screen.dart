import 'package:atoma_cash/models/api/error/error.dart' as mc2api;
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';
import 'package:atoma_cash/src/registration/authentication/bloc/authentication_bloc.dart';
import 'package:atoma_cash/src/registration/passcode_check/bloc/bloc.dart';
import 'package:atoma_cash/src/registration/pin/bloc/bloc.dart';
import 'package:atoma_cash/src/registration/pin/bloc/pin_state.dart';
import 'package:atoma_cash/src/registration/pin/pin_screen.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'configuration.dart';

abstract class IPasscodeCheckNavigationController {
  void onPasscodeCheckPassed();

  void onStartPasscodeRecovery();

  void onExceededPasscodeCheckAttempts();
}

class PasscodeCheckScreen extends StatefulWidget {
  final IPasscodeCheckNavigationController _flowController;

  PasscodeCheckScreen(
      {Key key, IPasscodeCheckNavigationController flowController})
      : _flowController = flowController,
        super(key: key);

  @override
  _PasscodeCheckState createState() => _PasscodeCheckState();
}

class _PasscodeCheckState extends State<PasscodeCheckScreen> {
  PasscodeCheckBloc _passcodeCheckBloc;

  @override
  void initState() {
    super.initState();
    _passcodeCheckBloc = PasscodeCheckBloc();
    _passcodeCheckBloc.add(Launched());
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: _titleWidget(),
          centerTitle: true,
          actions: <Widget>[
            Widgets.styledFlatButton(
                text: Localization.of(context).forgotButton,
                onPressed: widget._flowController.onStartPasscodeRecovery),
          ],
        ),
        body: BlocListener(
          bloc: _passcodeCheckBloc,
          listener: (BuildContext context, PasscodeCheckState state) {
            _stateChangesListener(context, state);
          },
          child: BlocBuilder(
              bloc: _passcodeCheckBloc,
              builder: (BuildContext context, PasscodeCheckState state) {
                return _rootWidgetForState(state);
              }),
        ));
  }

  Widget _titleWidget() {
    var flavor = BlocProvider
        .of<AuthenticationBloc>(context)
        .flavor;
    if (flavor == Flavor.DEV) {
      return FutureBuilder<String>(
        builder: (context, snapshot) => Text("env: ${snapshot.data}"),
        future: _environmentName(),
        initialData: "",
      );
    }
  }

  Future<String> _environmentName() async {
    var env = await ApplicationLocalRepository().getEnvironment();
    return env.environmentName;
  }

  void _stateChangesListener(BuildContext context, PasscodeCheckState state) {
    if (state is SuccessState) {
      widget._flowController.onPasscodeCheckPassed();
    }
    if (state is SignOutState) {
      widget._flowController.onExceededPasscodeCheckAttempts();
    }
    if (state is PasscodeErrorState) {
      if (state?.error?.type == mc2api.ErrorType.NO_INTERNET_CONNECTION) {
        Widgets.failedSnackBar(
            context, Localization
            .of(context)
            .legacyYouAreOffline);
      } else {
        Widgets.failedSnackBar(
            context, Localization
            .of(context)
            .legacyPleaseTryLater);
      }
    }
  }

  Widget _rootWidgetForState(PasscodeCheckState state) {
    if (state is LoadingState) {
      return Center(child: CircularProgressIndicator());
    }
    if (state is InitialState) {
      return PinScreen(
        Localization.of(context).enterPasscode,
        title: Localization
            .of(context)
            .welcomeBackTitle,
        pinBloc: PinBloc(validPin: state.pin),
        successPinCallback: _onSuccessPin,
        failedPinCallback: _onFailedPin,
        errorOnFailedPin: (pinState, context) {
          return _errorOnFailedPin(pinState, context);
        },
      );
    } else {
      return Container();
    }
  }

  String _errorOnFailedPin(PinState state, BuildContext context) {
    if (state.failedAttempts == 0) {
      return "";
    } else {
      return Localization.of(context).wrongPasscodeAttemptsLeft(
          '${state.failedAttempts}', '$passcodeValidationLimit');
    }
  }

  void _onSuccessPin(String pin, BuildContext context) {
    _passcodeCheckBloc.add(SuccessPin());
  }

  void _onFailedPin(int failedAttemptsCount) {
    _passcodeCheckBloc.add(FailedPin(attemptsCount: failedAttemptsCount));
  }
}
