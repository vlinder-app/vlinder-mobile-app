import 'dart:async';

import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:bloc/bloc.dart';

import './bloc.dart';
import '../configuration.dart';

class PasscodeCheckBloc extends Bloc<PasscodeCheckEvent, PasscodeCheckState> {
  ApplicationLocalRepository _applicationLocalRepository;

  PasscodeCheckBloc() {
    _applicationLocalRepository = ApplicationLocalRepository();
  }

  @override
  PasscodeCheckState get initialState => LoadingState();

  @override
  Stream<PasscodeCheckState> mapEventToState(
    PasscodeCheckEvent event,
  ) async* {
    if (event is Launched) {
      yield* _mapLaunchedToState();
    } else if (event is SuccessPin) {
      yield* _mapSuccessPinToState();
    } else if (event is FailedPin) {
      yield* _mapFailedPinToState(event.attemptsCount);
    }
  }

  Stream<PasscodeCheckState> _mapFailedPinToState(
      int failedPasscodeCount) async* {
    if (failedPasscodeCount + 1 > passcodeValidationLimit) {
      await _applicationLocalRepository.clearAll();
      yield SignOutState();
    }
  }

  Stream<PasscodeCheckState> _mapLaunchedToState() async* {
    _initAnalytics();
    String pin = await _applicationLocalRepository.getPasscode();
    yield InitialState(pin: pin);
  }

  Stream<PasscodeCheckState> _mapSuccessPinToState() async* {
    yield SuccessState();
  }

  void _initAnalytics() async {
    String userId = await _applicationLocalRepository.getUserId();
    if (userId != null) {
      Analytics().setUserId(userId);
    }
  }
}
