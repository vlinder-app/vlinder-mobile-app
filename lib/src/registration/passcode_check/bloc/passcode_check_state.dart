import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:atoma_cash/models/api/error/error.dart' as mc2;
import 'package:meta/meta.dart';

@immutable
abstract class PasscodeCheckState extends Equatable {
  PasscodeCheckState([List props = const []]);
}

class InitialState extends PasscodeCheckState {
  final String pin;

  InitialState({@required this.pin}) : super([pin]);

  @override
  String toString() {
    return 'InitialState{pin: $pin}';
  }

  @override
  // TODO: implement props
  List<Object> get props => [pin];
}

class PasscodeErrorState extends PasscodeCheckState {
  final mc2.Error error;

  PasscodeErrorState(this.error);

  @override
  String toString() {
    return 'PasscodeErrorState{error: $error}';
  }

  @override
  List<Object> get props => [error];


}

class LoadingState extends PasscodeCheckState {
  @override
  String toString() {
    return 'LoadingState{}';
  }

  @override
  List<Object> get props => [];
}

class SuccessState extends PasscodeCheckState {
  SuccessState();

  @override
  String toString() {
    return 'SuccessState{}';
  }

  @override
  List<Object> get props => [];
}

class SignOutState extends PasscodeCheckState {
  @override
  String toString() {
    return 'SignOutState{}';
  }

  @override
  List<Object> get props => [];
}
