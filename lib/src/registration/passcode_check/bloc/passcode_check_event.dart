import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PasscodeCheckEvent {
  PasscodeCheckEvent([List props = const []]);
}

class Launched extends PasscodeCheckEvent {
  @override
  String toString() {
    return 'Launched{}';
  }
}

class SuccessPin extends PasscodeCheckEvent {
  @override
  String toString() {
    return 'SuccessPin{}';
  }
}

class FailedPin extends PasscodeCheckEvent {
  final int attemptsCount;

  FailedPin({@required this.attemptsCount});

  @override
  String toString() {
    return 'FailedPin{attemptsCount: $attemptsCount}';
  }
}
