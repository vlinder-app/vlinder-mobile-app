import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/registration/widgets/HeaderWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bloc.dart';

typedef SuccessPinCallback = void Function(String pin, BuildContext context);
typedef FailedPinCallback = void Function(int failedAttemptsCount);
typedef FilledPinCallback = void Function(String pin);
typedef ErrorOnFailedPin = String Function(
    PinState state, BuildContext context);

class PinScreen extends StatefulWidget {
  final String subtitle;
  final String title;
  final String description;
  final SuccessPinCallback successPinCallback;
  final FailedPinCallback failedPinCallback;
  final ErrorOnFailedPin errorOnFailedPin;
  final FilledPinCallback filledPinCallback;
  final PinBloc pinBloc;

  PinScreen(this.subtitle,
      {this.errorOnFailedPin,
      this.successPinCallback,
      this.failedPinCallback,
      this.filledPinCallback,
      @required this.pinBloc,
      this.title = "",
      this.description = ""});

  @override
  _PinScreenState createState() => _PinScreenState();
}

class _PinScreenState extends State<PinScreen> {
  static const double keyboardScaleRatio = 6.3;
  static const int columnsCount = 3;

  static const int deleteButtonIndex = 11;
  static const int actionButtonIndex = 9;
  static const int zeroButtonIndex = 10;

  PinBloc _pinBloc;

  @override
  void initState() {
    super.initState();
    _pinBloc = widget.pinBloc;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    final double _itemHeight =
        (_size.height - kToolbarHeight - 24) / keyboardScaleRatio;
    final double _itemWidth = _size.width / columnsCount;

    final double _itemMargin = 1 / (_itemWidth - _itemHeight) * 100;

    return BlocListener(
      bloc: _pinBloc,
      listener: (BuildContext context, PinState state) {
        if (state.isSuccess) {
          if (widget.successPinCallback != null) {
            widget.successPinCallback(state.pin, context);
          }
        }
        if (state.isPinFilled) {
          if (widget.filledPinCallback != null) {
            widget.filledPinCallback(state.pin);
          }
        }
        if (!state.isPinCorrect) {
          if (widget.failedPinCallback != null) {
            widget.failedPinCallback(state.failedAttempts);
          }
        }
      },
      child: BlocBuilder(
          bloc: _pinBloc,
          builder: (BuildContext context, PinState state) {
            return Container(
              margin: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 24.0),
              child: Column(
                children: <Widget>[
                  Container(
                      alignment: Alignment.centerLeft,
                      child: Header(
                        widget.title,
                        subtitle: widget.subtitle,
                        descriptionColor: _descriptionColor(state),
                        description: _descriptionTextForState(state),
                      )),
                  Spacer(
                    flex: 1,
                  ),
                  Expanded(
                    flex: 1,
                    child: Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: _bublesForState(state),
                      ),
                    ),
                  ),
                  Spacer(
                    flex: 1,
                  ),
                  Expanded(
                    flex: 13,
                    child: Center(
                      child: Container(
                        child: GridView.count(
                          physics: new NeverScrollableScrollPhysics(),
                          crossAxisCount: columnsCount,
                          childAspectRatio: (_itemWidth / _itemHeight),
                          children: List.generate(12, (index) {
                            return Container(
                              child: _pinItemForIndex(index, _itemMargin),
                            );
                          }),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
    );
  }

  Color _descriptionColor(PinState state) =>
      state.failedAttempts == 0 ? Palette.mediumGrey : Palette.red;

  String _descriptionTextForState(PinState state) =>
      (widget.errorOnFailedPin == null)
          ? (widget.description)
          : (widget?.errorOnFailedPin(state, context));

  void _onNumberTap(int number) {
    _pinBloc.add(PinNumberTapped(value: number));
  }

  void _onRemoveTap() {
    _pinBloc.add(BackspaceTapped());
  }

  Widget _pinItemForIndex(int index, double marginRatio) {
    switch (index) {
      case deleteButtonIndex:
        return _inkWellForIndex(_iconWidget(Assets.pinDelete), _onRemoveTap);
      case actionButtonIndex:
        return _inkWellForIndex(
            Text(''),
            () => {
                  // TODO: Add action tap handler
                });
      case zeroButtonIndex:
        return _inkWellForIndex(
            _numberWidget(0, marginRatio), () => {_onNumberTap(0)});
      default:
        int number = index + 1;
        return _inkWellForIndex(
            _numberWidget(number, marginRatio), () => {_onNumberTap(number)});
    }
  }

  List<Widget> _bublesForState(PinState state) {
    return List.generate(pinLength, (index) {
      return Container(
          padding: EdgeInsets.only(left: 15.0, right: 15.0),
          child: Image.asset(_imageResourceForPinState(state, index)));
    });
  }

  String _imageResourceForPinState(PinState state, int index) {
    if (!state.isPinCorrect) {
      return Assets.pinDotError;
    } else {
      return (state.pin.length > index)
          ? (Assets.pinDotActive)
          : (Assets.pinDotDisabled);
    }
  }

  Widget _numberWidget(int index, double marginRatio) {
    return Container(
      margin: EdgeInsets.all(marginRatio),
      decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle, // BoxShape.circle or BoxShape.retangle
          //color: const Color(0xFF66BB6A),
          boxShadow: [
            BoxShadow(
              offset: Offset(0.0, 2.0),
              color: Colors.grey.withAlpha(60),
              blurRadius: 2.0,
            ),
          ]),
      child: Center(
          child: Text(
        '$index',
        style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w500),
      )),
    );
  }

  Widget _iconWidget(String asset) {
    return Center(
        child: Image.asset(
      asset,
      height: 35.0,
      width: 35.0,
    ));
  }

  Widget _inkWellForIndex(Widget child, void onTap()) {
    return InkWell(
        borderRadius: BorderRadius.circular(40.0),
        onTap: onTap,
        child: SizedBox.expand(
          child: child,
        ));
  }
}
