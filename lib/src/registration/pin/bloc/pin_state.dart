import 'package:equatable/equatable.dart';
import 'package:atoma_cash/src/registration/pin/configuration.dart' as config;
import 'package:meta/meta.dart';

@immutable
abstract class PinBaseState extends Equatable {
  PinBaseState([List props = const []]);
}

class PinState extends PinBaseState {
  final String pin;
  final bool isPinCorrect;
  final int failedAttempts;
  final bool isSuccess;
  final bool isPinFilled;

  PinState(
      {@required this.pin,
      @required this.isPinCorrect,
      @required this.failedAttempts,
        @required this.isPinFilled,
      @required this.isSuccess})
      : super(
      [pin, isPinCorrect, failedAttempts, isSuccess, isPinFilled]);

  factory PinState.start() {
    return PinState(
        pin: '',
        isPinCorrect: true,
        failedAttempts: 0,
        isSuccess: false,
        isPinFilled: false);
  }

  factory PinState.success(String pin) {
    return PinState(
        pin: pin,
        isPinCorrect: true,
        failedAttempts: 0,
        isSuccess: true,
        isPinFilled: false);
  }

  factory PinState.error(int failedPinAttempts, String pin) {
    return PinState(
        pin: pin,
        isPinCorrect: false,
        failedAttempts: failedPinAttempts,
        isPinFilled: false,
        isSuccess: false);
  }

  PinState update(
      {String pin,
      bool isPinCorrect,
      int failedPinAttempts,
        bool isPinFilled,
      bool isSuccess}) {
    return copyWith(
        pin: pin,
        isPinCorrect: isPinCorrect,
        isPinFilled: isPinFilled,
        failedAttempts: failedPinAttempts,
        isSuccess: isSuccess);
  }

  PinState copyWith({String pin,
    bool isPinCorrect,
    int failedAttempts,
    bool isSuccess,
    bool isPinFilled}) {
    return PinState(
        pin: pin ?? this.pin,
        isPinFilled: isPinFilled ?? this.isPinFilled,
        isPinCorrect: isPinCorrect ?? this.isPinCorrect,
        failedAttempts: failedAttempts ?? this.failedAttempts,
        isSuccess: isSuccess ?? this.isSuccess);
  }

  @override
  List<Object> get props => [pin, isPinCorrect, isPinFilled, failedAttempts, isSuccess];
}

class PinSuccessState extends PinBaseState {
  final String pin;

  PinSuccessState({@required this.pin});

  @override
  List<Object> get props => [pin];
}

class PinFilledState extends PinBaseState {
  final String pin;

  PinFilledState({@required this.pin});

  @override
  List<Object> get props => [pin];
}
