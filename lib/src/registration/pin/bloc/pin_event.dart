import 'dart:math';

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PinEvent extends Equatable {
  PinEvent([List props = const []]);
}

class PinNumberTapped extends PinEvent {
  final int value;

  PinNumberTapped({this.value});

  @override
  String toString() {
    return 'PinNumberTapped{value: $value}';
  }

  @override
  List<Object> get props => [value];
}

class BackspaceTapped extends PinEvent {
  @override
  String toString() {
    return 'BackspaceTapped{}';
  }

  @override
  List<Object> get props => [];
}