import 'dart:async';

import 'package:bloc/bloc.dart';

import './bloc.dart';

class PinBloc extends Bloc<PinEvent, PinState> {
  final String validPin;
  String _pin = "";
  int _failedAttempts = 0;

  PinBloc({this.validPin});

  @override
  PinState get initialState {
    return PinState.start();
  }

  @override
  Stream<PinState> mapEventToState(
    PinEvent event,
  ) async* {
    if (event is PinNumberTapped) {
      yield* _mapPinNumberTappedToState(event.value);
    } else if (event is BackspaceTapped) {
      yield* _mapBackspaceTappedToState();
    }
  }

  Stream<PinState> _mapPinNumberTappedToState(int number) async* {
    if (_pin.length == pinLength - 1) {
      _pin += number.toString();
      yield state.copyWith(isPinFilled: true, pin: _pin);
      yield _pinFilled();
    } else if (_pin.length < pinLength) {
      _pin += number.toString();
      yield state.copyWith(pin: _pin);
    } else {
      _pin = number.toString();
      yield state.copyWith(pin: _pin, isPinCorrect: true);
    }
  }

  PinState _pinFilled() {
    if ((_pin == validPin) || (validPin == null)) {
      return PinState.success(_pin);
    } else {
      _failedAttempts++;
      return state.copyWith(
          failedAttempts: _failedAttempts,
          isPinCorrect: false,
          isPinFilled: false,
          pin: _pin);
    }
  }

  Stream<PinState> _mapBackspaceTappedToState() async* {
    if (_pin.length > 0) {
      _pin = _pin.substring(0, _pin.length - 1);
      yield state.copyWith(pin: _pin);
    }
  }
}
