import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/email_skip_event.dart';
import 'package:atoma_cash/src/registration/email_confirmation/bloc/bloc.dart';
import 'package:atoma_cash/src/registration/email_confirmation/bloc/email_profile_confirmation_bloc.dart';
import 'package:atoma_cash/src/registration/email_confirmation/bloc/email_validation_bloc.dart';
import 'package:atoma_cash/src/registration/email_confirmation/email_confirmation.dart';
import 'package:atoma_cash/src/registration/email_confirmation/email_confirmation_screen_configuration.dart';
import 'package:atoma_cash/src/registration/email_confirmation_request/email_confirmation_request_screen.dart';
import 'package:atoma_cash/src/registration/email_confirmation_request/email_confirmation_screen_configuration.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:auto_route/auto_route.dart';

enum EmailConfirmationFlow {
  AUTHENTICATION,
  SETTINGS,
  ACCOUNTS,
  PASSCODE_RECOVERY
}

class EmailConfirmationFlowController
    implements
        IEmailConfirmationRequestFlowController,
        IEmailConfirmationScreenFlowController {
  final EmailConfirmationFlow flow;
  final Function() onEmailConfirmedCallback;
  final Function() onEmailConfirmationCanceledCallback;

  static EmailConfirmationBloc emailConfirmationBloc(
      EmailConfirmationFlow flow) {
    switch (flow) {
      case EmailConfirmationFlow.PASSCODE_RECOVERY:
        return EmailValidationBloc();
      default:
        return EmailProfileConfirmationBloc();
    }
  }

  static IEmailConfirmationScreenConfiguration
      emailConfirmationScreenConfiguration(
          EmailConfirmationFlow flow, String email) {
    switch (flow) {
      case EmailConfirmationFlow.PASSCODE_RECOVERY:
        return EmailValidationConfiguration(email);
      default:
        return EmailProfileConfirmationConfiguration(email);
    }
  }

  bool get isExistingUser =>
      flow == EmailConfirmationFlow.SETTINGS ||
          flow == EmailConfirmationFlow.ACCOUNTS;

  bool get isSkippable => flow != EmailConfirmationFlow.PASSCODE_RECOVERY;

  bool get isConfirmationRequired =>
      flow != EmailConfirmationFlow.AUTHENTICATION;

  EmailConfirmationFlowController(this.flow,
      {this.onEmailConfirmedCallback,
        this.onEmailConfirmationCanceledCallback});

  @override
  void onCancel() {
    if (onEmailConfirmationCanceledCallback != null) {
      onEmailConfirmationCanceledCallback();
    } else {
      ExtendedNavigator.ofRouter<Router>().pop();
    }
  }

  @override
  void onSkipEmailConfirmation() {
    Analytics().logEvent(EmailSkipEvent());
    onCancel();
  }

  @override
  void onDidSendEmailConfirmationRequest(String email) {
    ExtendedNavigator.ofRouter<Router>().pushReplacementNamed(
        Routes.emailConfirmationScreen,
        arguments: EmailConfirmationScreenArguments(
            email: email,
            bloc: emailConfirmationBloc(flow),
            configuration: emailConfirmationScreenConfiguration(flow, email),
            flowController: this));
  }

  @override
  void onDestructiveActionClicked(String email) {
    switch (flow) {
      case EmailConfirmationFlow.AUTHENTICATION:
      case EmailConfirmationFlow.ACCOUNTS:
      case EmailConfirmationFlow.SETTINGS:
        _moveToEmailConfirmationRequestScreen(
            _emailConfirmationRequestScreenConfiguration(email));
        break;
      case EmailConfirmationFlow.PASSCODE_RECOVERY:
        onCancel();
        break;
    }
  }

  @override
  void onEmailConfirmed() {
    if (isExistingUser) {
      ExtendedNavigator.ofRouter<Router>().pop();
    }

    if (onEmailConfirmedCallback != null) {
      onEmailConfirmedCallback();
    }
  }

  void _moveToEmailConfirmationRequestScreen(
      IEmailConfirmationRequestScreenConfiguration configuration) {
    ExtendedNavigator.ofRouter<Router>().pushReplacementNamed(
        Routes.emailConfirmationRequestScreen,
        arguments: EmailConfirmationRequestScreenArguments(
            flowController: this, configuration: configuration));
  }

  IEmailConfirmationRequestScreenConfiguration
  _emailConfirmationRequestScreenConfiguration(String email) {
    switch (flow) {
      case EmailConfirmationFlow.ACCOUNTS:
        return Accounts(email: email);
      default:
        return Default(email: email);
    }
  }
}
