import 'package:atoma_cash/models/auth_api/auth/internal/jwt_token_data.dart';
import 'package:atoma_cash/resources/config/navigation/navigation.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/forgot_passcode_event.dart';
import 'package:atoma_cash/src/registration/email_confirmation_request/email_confirmation_screen_configuration.dart';
import 'package:atoma_cash/src/registration/flow_controllers/email_confirmation_flow_controller.dart';
import 'package:atoma_cash/src/registration/passcode_check/passcode_check_screen.dart';
import 'package:atoma_cash/src/registration/phone_confirmation/phone_confirmation_screen.dart';
import 'package:atoma_cash/src/registration/phone_validation/phone_validation_screen.dart';
import 'package:atoma_cash/src/registration/pin_setup/pin_setup.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';

enum AuthenticationFlow {
  REGULAR,
  PASSCODE_RECOVERY,
}

abstract class IAuthenticationBaseFlowController
    implements
        IPhoneValidationFlowController,
        IPhoneConfirmationFlowController,
        IPinSetupScreenNavigationController,
        IPasscodeCheckNavigationController {}

class AuthenticationFlowController
    implements IAuthenticationBaseFlowController {
  static const int regularFlowPasscodeSetupStepIndex = 3;
  static const int recoveryFlowEmailVerifiedPascodeSetupStepIndex = 4;
  final AuthenticationFlow flow;

  bool get _isPassCodeRecovery => flow == AuthenticationFlow.PASSCODE_RECOVERY;

  AuthenticationFlowController(this.flow);

  @override
  void onPhoneConfirmationCodeRequested(String phone) {
    ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.phoneConfirmationScreen,
        arguments: PhoneConfirmationScreenArguments(
            phone: phone, flowController: this));
  }

  @override
  void onPhoneConfirmed(JwtTokenData tokenData) {
    if (flow == AuthenticationFlow.PASSCODE_RECOVERY &&
        tokenData.isEmailVerified) {
      _moveToEmailValidation(
          EmailConfirmationFlow.PASSCODE_RECOVERY, tokenData.email, true);
    } else {
      _moveToPinSetupScreen(
          isUserHasPasscode: tokenData.hasPasscode,
          stepIndex: regularFlowPasscodeSetupStepIndex);
    }
  }

  @override
  void onExceededPhoneConfirmationAttempts() {
    _moveToPhoneValidationScreen();
  }

  @override
  void onPinEnteredSuccessfully(bool isEmailConfirmationNeeded) {
    if (isEmailConfirmationNeeded) {
      _startEmailConfirmation();
    } else {
      onPasscodeCheckPassed();
    }
  }

  @override
  void onForgotPinClicked() {
    onStartPasscodeRecovery();
  }

  @override
  void onExceededPinSetupAttempts() {
    _moveToPhoneValidationScreen();
  }

  @override
  void onExceededPasscodeCheckAttempts() {
    _moveToPhoneValidationScreen();
  }

  @override
  void onPasscodeCheckPassed() {
    _moveHome();
  }

  @override
  void onStartPasscodeRecovery() async {
    Analytics().logEvent(ForgotPasscodeEvent());
    AuthenticationFlowController flowController =
        AuthenticationFlowController(AuthenticationFlow.PASSCODE_RECOVERY);

    _moveTo(Routes.phoneValidationScreen,
        arguments:
            PhoneValidationScreenArguments(flowController: flowController));
  }

  void _moveToPinSetupScreen({bool isUserHasPasscode, int stepIndex}) {
    ExtendedNavigator.ofRouter<Router>().pushNamedAndRemoveUntil(
        Routes.pinSetupScreen, (Route<dynamic> route) => false,
        arguments: PinSetupScreenArguments(
            isUserHasPasscode: isUserHasPasscode,
            replaceExisting: _isPassCodeRecovery,
            flowController: this,
            stepIndex: stepIndex,
            shouldOverride: _isPassCodeRecovery));
  }

  void _startEmailConfirmation() {
    EmailConfirmationFlowController flowController =
        EmailConfirmationFlowController(EmailConfirmationFlow.AUTHENTICATION,
            onEmailConfirmedCallback: _moveHome,
            onEmailConfirmationCanceledCallback: _moveHome);

    _moveTo(Routes.emailConfirmationRequestScreen,
        arguments: EmailConfirmationRequestScreenArguments(
          flowController: flowController,
          configuration: Default(),
        ));
  }

  void moveToRecoveryEmailConfirmation(String email) {
    _moveToEmailValidation(
        EmailConfirmationFlow.PASSCODE_RECOVERY, email, false);
  }

  void moveToRecoveryPinSetup() {
    _moveToPinSetupScreen(
        isUserHasPasscode: true,
        stepIndex: recoveryFlowEmailVerifiedPascodeSetupStepIndex);
  }

  void _moveToEmailValidation(EmailConfirmationFlow confirmationFlow,
      String email, bool withInitialConfirmationRequest) {
    EmailConfirmationFlowController flowController =
        EmailConfirmationFlowController(confirmationFlow,
            onEmailConfirmedCallback: () {
      _moveToPinSetupScreen(
          isUserHasPasscode: true,
          stepIndex: recoveryFlowEmailVerifiedPascodeSetupStepIndex);
    }, onEmailConfirmationCanceledCallback: _onInterruptPasscodeRecovery);

    _moveTo(Routes.emailConfirmationScreen,
        arguments: EmailConfirmationScreenArguments(
            bloc: EmailConfirmationFlowController.emailConfirmationBloc(
                confirmationFlow),
            email: email ?? "",
            flowController: flowController,
            withInitialConfirmationRequest: withInitialConfirmationRequest,
            configuration: EmailConfirmationFlowController
                .emailConfirmationScreenConfiguration(
                    confirmationFlow, email)));
  }

  void _onInterruptPasscodeRecovery() {
    Navigation.signOut();
    _moveToPhoneValidationScreen();
  }

  void _moveHome() {
    _moveTo(Routes.home);
  }

  void _moveToPhoneValidationScreen() {
    AuthenticationFlowController flowController =
        AuthenticationFlowController(AuthenticationFlow.REGULAR);

    _moveTo(Routes.phoneValidationScreen,
        arguments:
            PhoneValidationScreenArguments(flowController: flowController));
  }

  void _moveTo(String routeName, {Object arguments}) {
    ExtendedNavigator.ofRouter<Router>().pushNamedAndRemoveUntil(
        routeName, (Route<dynamic> route) => false,
        arguments: arguments);
  }
}
