import 'package:atoma_cash/models/environments/environment.dart';
import 'package:equatable/equatable.dart';

abstract class EnvironmentsState extends Equatable {
  const EnvironmentsState();
}

class Loading extends EnvironmentsState {
  @override
  List<Object> get props => null;
}

class Loaded extends EnvironmentsState {
  final List<Environment> environments;

  Loaded(this.environments);

  @override
  List<Object> get props => [environments];
}

class Failed extends EnvironmentsState {
  final String error;

  Failed(this.error);

  @override
  List<Object> get props => [error];
}
