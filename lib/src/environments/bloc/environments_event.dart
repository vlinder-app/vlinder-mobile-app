import 'package:equatable/equatable.dart';

abstract class EnvironmentsEvent extends Equatable {
  const EnvironmentsEvent();
}

class Started extends EnvironmentsEvent {
  @override
  List<Object> get props => null;
}
