import 'dart:async';

import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/models/environments/environments.dart';
import 'package:atoma_cash/resources/repository/remote/environments_provider.dart';
import 'package:bloc/bloc.dart';

import 'bloc.dart';

class EnvironmentsBloc extends Bloc<EnvironmentsEvent, EnvironmentsState> {
  EnvironmentsProvider _environmentsProvider = EnvironmentsProvider();

  @override
  EnvironmentsState get initialState => Loading();

  @override
  Stream<EnvironmentsState> mapEventToState(
    EnvironmentsEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapAppStartedToState();
    }
  }

  Stream<EnvironmentsState> _mapAppStartedToState() async* {
    BaseResponse<Environments> response =
        await _environmentsProvider.getEnvironments();
    if (response.isSuccess()) {
      yield Loaded(response.result.environments);
    } else {
      yield Failed(response.error.toString());
    }
  }
}
