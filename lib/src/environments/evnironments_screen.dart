import 'package:atoma_cash/models/environments/environment.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/bloc.dart';

class EnvironmentsScreen extends StatefulWidget {
  final Environment currentEnvironment;

  const EnvironmentsScreen({Key key, this.currentEnvironment})
      : super(key: key);

  @override
  _EnvironmentsScreenState createState() => _EnvironmentsScreenState();
}

class _EnvironmentsScreenState extends State<EnvironmentsScreen> {
  EnvironmentsBloc _bloc;

  @override
  void initState() {
    _bloc = EnvironmentsBloc()..add(Started());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: Container(
          child: BlocBuilder(
            bloc: _bloc,
            builder: _blocBuilder,
          ),
        ));
  }

  Widget _blocBuilder(BuildContext context, EnvironmentsState state) {
    if (state is Failed) {
      return Container(
        margin: Dimensions.leftRight24Padding,
        child: Center(
          child: Text("Please, try again later! \n ${state.error}"),
        ),
      );
    }
    if (state is Loading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    if (state is Loaded) {
      return ListView.builder(
        itemBuilder: (context, index) {
          Environment environment = state.environments[index];
          return FourRowListTile(
            onClick: () => Navigator.of(context).pop(environment),
            title: Text(
              environment.environmentName,
              style: FourRowListTile.titleTextStyle,
            ),
            subtitle: Text(
              environment.apiUri,
              style: FourRowListTile.subtitleTextStyle,
            ),
            trailing: _environmentCheckMarkWidget(environment),
          );
        },
        itemCount: state.environments.length,
      );
    }
  }

  Widget _environmentCheckMarkWidget(Environment environment) {
    if (environment.apiUri == widget.currentEnvironment.apiUri) {
      return Icon(
        Icons.check,
        color: Palette.darkBlue,
      );
    } else {
      return null;
    }
  }
}
