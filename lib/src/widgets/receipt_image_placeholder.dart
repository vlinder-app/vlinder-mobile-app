import 'package:atoma_cash/utils/dimensions.dart';
import 'package:flutter/material.dart';

class ReceiptImagePlaceholder extends StatelessWidget {
  final VoidCallback onImageTap;
  final Widget child;

  const ReceiptImagePlaceholder({
    Key key,
    @required this.child,
    this.onImageTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.maxFinite,
      height: 184.0,
      margin: Dimensions.leftRight24Padding.copyWith(top: 16.0, bottom: 24.0),
      child: InkWell(
        onTap: onImageTap,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16.0),
          child: child,
        ),
      ),
    );
  }
}
