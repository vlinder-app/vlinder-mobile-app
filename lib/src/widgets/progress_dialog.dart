import 'package:atoma_cash/src/activity/widgets/dialogs/receipt_not_recognized_dialog.dart';
import 'package:flutter/material.dart';

class ProgressDialog extends StatelessWidget {
  final String text;
  final TextStyle style;

  const ProgressDialog(
      {Key key,
      @required this.text,
      this.style = const TextStyle(fontWeight: FontWeight.w600)})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: ReceiptNotRecognizedDialog.border,
      content: Container(
        height: 50.0,
        child: Row(
          children: <Widget>[
            CircularProgressIndicator(),
            Padding(
              padding: const EdgeInsets.only(left: 24.0),
              child: Text(
                text,
                style: style,
              ),
            )
          ],
        ),
      ),
    );
  }
}
