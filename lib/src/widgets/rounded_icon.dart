import 'package:atoma_cash/extensions/color_extensions.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class RoundedIcon extends StatelessWidget {
  static const _size = 40.0;

  final String iconStringColor;
  final Color iconColor;
  final String asset;
  final String iconUrl;
  final double size;

  const RoundedIcon(
      {Key key,
      this.iconStringColor,
      this.iconColor,
      this.asset,
      this.iconUrl,
      this.size = _size})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget icon;
    if (asset != null) {
      icon = Image.asset(asset);
    } else if (iconUrl != null) {
      icon = Padding(
        padding: const EdgeInsets.all(10.0),
        child: Image.network(iconUrl),
      );
    } else {
      icon = Image.asset(Assets.unknownCategoryWhiteIcon);
    }

    Color backgroundColor = Palette.mediumGrey;
    if (iconColor != null) {
      backgroundColor = iconColor;
    } else if (iconStringColor != null) {
      backgroundColor = HexColor.fromHex(iconStringColor);
    }

    return ClipOval(
      child: Container(
        color: backgroundColor,
        width: size,
        height: size,
        child: icon,
      ),
    );
  }
}
