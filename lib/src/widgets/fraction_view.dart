import 'package:flutter/material.dart';

import 'fraction_item.dart';

abstract class FractionView<T> extends StatelessWidget {
  static const double fullHeight = 206.0;
  static const double _kViewHeight = 161.0;

  final List<T> fractions;

  const FractionView({Key key, this.fractions}) : super(key: key);

  double fractionHeight(T fraction);

  String fractionValue(BuildContext context, T fraction);

  String fractionColor(T fraction);

  void onFractionClick(T fraction);

  String subtitle(BuildContext context, T fraction);

  double height() => _kViewHeight;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: height(),
          child: ListView.builder(
            itemCount: fractions.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              T fraction = fractions[index];
              return FractionItem(
                onClick: () => onFractionClick(fraction),
                title: fractionValue(context, fraction),
                subtitle: subtitle(context, fraction),
                color: fractionColor(fraction),
                fractionHeight: fractionHeight(fraction),
                edgeInsets: EdgeInsets.only(
                    left: 24.0,
                    right: index == fractions.length - 1 ? 24.0 : 0.0),
              );
            },
          ),
        ),
      ],
    );
  }
}
