import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class TotalSpendingCell extends StatelessWidget {
  static const subtitleTextStyle = TextStyle(
      fontSize: 14.0, fontWeight: FontWeight.w500, color: Palette.mediumGrey);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(16.0),
      padding: EdgeInsets.all(16.0),
      height: 140.0,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.0),
        boxShadow: [_rootContainerBoxShadow()],
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Total spending",
            style: subtitleTextStyle,
          ),
          Text(
            "\$ 1,767.34",
            style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.w600),
          ),
          Container(
            margin: EdgeInsets.only(top: 5.0, bottom: 5.0),
            child: LinearProgressIndicator(
              value: 0.7,
              backgroundColor: Palette.paleGrey,
              valueColor: AlwaysStoppedAnimation(Palette.blue),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "\$ 689 left of",
                style: subtitleTextStyle.copyWith(
                    fontWeight: FontWeight.w600, color: Colors.black),
              ),
              Row(
                children: <Widget>[
                  Text(
                    "\$ 2.447",
                    style:
                        subtitleTextStyle.copyWith(fontWeight: FontWeight.w600),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: Icon(Icons.camera, size: 18.0, color: Colors.grey,),
                  )
                ],
              )
            ],
          )
        ],
      ),
    );
  }

  BoxShadow _rootContainerBoxShadow() {
    return BoxShadow(
      offset: Offset(0.0, 3.0),
      color: Colors.grey.withAlpha(70),
      blurRadius: 5.0,
    );
  }
}
