import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class FourRowListTile extends StatelessWidget {
  static const double twoRowHeight = 75.0;
  static const double threeRowHeight = 92.0;
  static const double verticalItemInset = 3.0;

  static const EdgeInsets defaultContentPadding =
      EdgeInsets.only(top: 12.0, bottom: 12.0, left: 24.0, right: 24.0);

  static const TextStyle titleTextStyle = TextStyle(
    color: Colors.black,
    fontWeight: FontWeight.w600,
    fontSize: 14.0,
    height: 1.2,
  );

  static const TextStyle subtitleTextStyle = TextStyle(
    color: Palette.mediumGrey,
    fontWeight: FontWeight.w500,
    fontSize: 13.0,
    height: 1.3,
  );

  final Widget leading;
  final Widget title;
  final Widget subtitle;
  final Widget trailing;
  final Widget subTrailing;
  final Widget bottom;
  final VoidCallback onClick;
  final bool forceThreeRow;
  final EdgeInsets contentPadding;

  const FourRowListTile(
      {Key key,
      this.leading,
      this.title,
      this.subtitle,
      this.trailing,
      this.subTrailing,
      this.bottom,
      this.onClick,
      this.forceThreeRow = false,
      this.contentPadding = defaultContentPadding})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height:
          _isThreeRowCell() || forceThreeRow ? threeRowHeight : twoRowHeight,
      child: InkWell(
        onTap: onClick,
        child: Padding(
          padding: contentPadding,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              if (leading != null)
                Padding(
                  padding: const EdgeInsets.only(right: 16.0),
                  child: leading,
                ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Flexible(
                      child: Column(
                        mainAxisAlignment: _widgetsAlignment(),
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          title,
                          if (subtitle != null)
                            Padding(
                              padding: EdgeInsets.only(top: verticalItemInset),
                              child: subtitle,
                            ),
                          if (bottom != null)
                            Padding(
                              padding: EdgeInsets.only(top: verticalItemInset),
                              child: bottom,
                            ),
                        ],
                      ),
                    ),
                    Column(
                      mainAxisAlignment: _widgetsAlignment(),
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        if (trailing != null) trailing,
                        if (subTrailing != null) subTrailing
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  MainAxisAlignment _widgetsAlignment() {
    if (forceThreeRow || !_isThreeRowCell()) {
      return MainAxisAlignment.spaceEvenly;
    } else {
      return MainAxisAlignment.start;
    }
  }

  bool _isThreeRowCell() => bottom != null && subtitle != null;
}
