import 'package:atoma_cash/models/api/api/categories/period.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/activity/widgets/dialogs/option_cell.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:flutter/material.dart';

typedef OnPeriodTypeSelected = void Function(PeriodType periodType);

class PeriodTypeSelectorDialog extends StatelessWidget {
  static const kBorderRadius = 20.0;
  static const RoundedRectangleBorder border = const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(kBorderRadius),
          topRight: Radius.circular(kBorderRadius)));

  final OnPeriodTypeSelected onPeriodTypeSelected;

  const PeriodTypeSelectorDialog({
    Key key,
    @required this.onPeriodTypeSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: _periodsTypeListBuilder(context),
      )),
    );
  }

  List<Widget> _periodsTypeListBuilder(BuildContext context) =>
      PeriodType.values.map((periodType) {
        return OptionsCell(_periodTypeString(context, periodType),
            onClick: () => {onPeriodTypeSelected(periodType)});
      }).toList()
        ..add(_cancelCell(context));

  OptionsCell _cancelCell(BuildContext context) =>
      OptionsCell(Localization.of(context).cancelButton,
          onClick: () => Navigator.of(context).pop(),
          style: FourRowListTile.titleTextStyle.copyWith(color: Palette.blue));

  String _periodTypeString(BuildContext context, PeriodType type) {
    switch (type) {
      case PeriodType.TODAY:
        return Localization.of(context).today;
      case PeriodType.WEEK:
        return Localization.of(context).week;
      case PeriodType.MONTH:
        return Localization.of(context).month;
      case PeriodType.CUSTOM:
        return Localization.of(context).custom;
      default:
        return "Undefined period";
    }
  }
}
