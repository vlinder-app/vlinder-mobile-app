import 'package:atoma_cash/models/api/api/categories/date_range.dart';
import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/categories/period.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/select_filter_period_event.dart';
import 'package:atoma_cash/src/analytics/models/events/select_report_range_event.dart';
import 'package:atoma_cash/src/categories/list/period_list_item.dart';
import 'package:atoma_cash/src/widgets/periods_widget/period_type_selector_dialog.dart';
import 'package:flutter/material.dart';
import 'package:scroll_to_index/scroll_to_index.dart';

abstract class IPeriodsListener {
  void onRangeSelected(DateRange dateRangeItem);

  void onCustomPeriodClick(Period period);

  void onResetCustomPeriod();

  void onPeriodChanged(PeriodType type, DateRange selectedDateRange);
}

class PeriodsWidget extends StatefulWidget {
  final DateRange selectedItem;
  final IPeriodsListener listener;
  final GlobalKey periodsListViewKey;

  const PeriodsWidget(this.selectedItem,
      {Key key, @required this.listener, this.periodsListViewKey})
      : super(key: key);

  @override
  _PeriodsWidgetState createState() => _PeriodsWidgetState();
}

class _PeriodsWidgetState extends State<PeriodsWidget> {
  DateRangeCollections _dateRangeLists;
  AutoScrollController _autoScrollController = AutoScrollController();

  @override
  void initState() {
    _dateRangeLists = DateRangeCollections();
    super.initState();
  }

  @override
  void dispose() {
    _autoScrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(child: _periodWidgetForType(context, widget.selectedItem)),
        Container(
          width: 72.0,
          child: _calendarIconButton(context, widget.selectedItem),
        )
      ],
    );
  }

  Widget _calendarIconButton(BuildContext context,
      DateRange selectedDateRange) {
    return IconButton(
      icon: Image.asset(Assets.calendarIcon),
      onPressed: () => {_onCalendarButtonClicked(context, selectedDateRange)},
    );
  }

  void _onCalendarButtonClicked(BuildContext context,
      DateRange selectedDateRange) {
    showModalBottomSheet<void>(
        context: context,
        shape: PeriodTypeSelectorDialog.border,
        builder: (_) {
          return PeriodTypeSelectorDialog(
            onPeriodTypeSelected: (periodType) {
              Navigator.of(context).pop();
              widget.listener.onPeriodChanged(periodType, selectedDateRange);
            },
          );
        });
  }

  Widget _customPeriod(BuildContext context, DateRange selectedItem) {
    return Container(
      height: 40.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          PeriodListItem(
            isLast: false,
            item: selectedItem,
            isActive: true,
            onClick: (_) =>
                widget.listener.onCustomPeriodClick(selectedItem.period),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Material(
              type: MaterialType.transparency,
              child: IconButton(
                icon: Icon(
                  Icons.cancel,
                  color: Colors.white.withAlpha(200),
                ),
                onPressed: _onResetCustomPeriod,
              ),
            ),
          )
        ],
      ),
    );
  }

  void _onResetCustomPeriod() {
    widget.listener.onResetCustomPeriod();
  }

  Widget _periodWidgetForType(BuildContext context, DateRange selectedItem) {
    switch (selectedItem.periodType) {
      case PeriodType.TODAY:
        return _periodsListView(context, selectedItem, _dateRangeLists.days);
      case PeriodType.WEEK:
        return _periodsListView(context, selectedItem, _dateRangeLists.weeks);
      case PeriodType.MONTH:
        return _periodsListView(context, selectedItem, _dateRangeLists.months);
      case PeriodType.CUSTOM:
        return _customPeriod(context, selectedItem);
    }
  }

  void _scrollToSelectedItem(DateRange selectedItem, List<DateRange> periods) {
    int selectedIndex =
    periods.indexWhere((p) => p.period == selectedItem.period);
    if (selectedIndex != -1) {
      _autoScrollController.scrollToIndex(selectedIndex,
          preferPosition: AutoScrollPosition.middle);
    }
  }

  Widget _periodsListView(BuildContext context, DateRange selectedItem,
      List<DateRange> periods) {
    WidgetsBinding.instance.addPostFrameCallback(
            (_) => _scrollToSelectedItem(selectedItem, periods));
    return Container(
      height: 40.0,
      child: ListView.builder(
        itemCount: periods.length,
        scrollDirection: Axis.horizontal,
        reverse: true,
        controller: _autoScrollController,
        itemBuilder: (context, index) {
          bool isSelected = periods[index].period == selectedItem.period;
          return AutoScrollTag(
            index: index,
            key: ValueKey(index),
            controller: _autoScrollController,
            child: PeriodListItem(
              isLast: false,
              item: periods[index],
              isActive: isSelected,
              onClick: _onRangeSelected,
            ),
          );
        },
      ),
    );
  }

  void _onRangeSelected(DateRange item) {
    Analytics().logEvent(SelectReportRangeEvent(item.periodType));
    Analytics().logEvent(SelectFilterPeriodEvent(item.rangeString()));
    widget.listener.onRangeSelected(item);
  }

/*
   * Old way implementation with calendar popup button
   *
  Widget _calendarPopupMenuButton(
      BuildContext context, DateRange selectedDateRange) {
    PeriodType.values.map((f) {}).toList();
    return PopupMenuButton<PeriodType>(
      icon: Image.asset(Assets.calendarIcon),
      onSelected: (periodType) =>
          widget.listener.onPeriodChanged(periodType, selectedDateRange),
      itemBuilder: (BuildContext context) {
        return PeriodType.values.map((PeriodType type) {
          return PopupMenuItem<PeriodType>(
            value: type,
            child: Text(
              _periodTypeString(context, type),
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
          );
        }).toList();
      },
    );
  }

  String _periodTypeString(BuildContext context, PeriodType type) {
    switch (type) {
      case PeriodType.TODAY:
        return Localization.of(context).todayPeriod;
      case PeriodType.WEEK:
        return Localization.of(context).weekPeriod;
      case PeriodType.MONTH:
        return Localization.of(context).monthPeriod;
      case PeriodType.CUSTOM:
        return Localization.of(context).customPeriod;
      default:
        return "Undefined period";
    }
  }
   */
}
