import 'package:atoma_cash/extensions/color_extensions.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class FractionItem extends StatelessWidget {
  static const _itemWidth = 65.0;
  static const _fractionWidth = 56.0;
  static const _overlappingLayoutTitleBottomPadding = 6.0;
  static const _shortBarLayoutBottomPadding = 2.0;
  static const _minimumOverlappingBarHeight = 18.0;
  static const _subtitlePadding = 9.0;
  static const _barHeight = 136.0;
  static const _borderRadius = 12.0;

  final String color;
  final String title;
  final String subtitle;
  final double fractionHeight;
  final EdgeInsets edgeInsets;
  final VoidCallback onClick;

  bool get _isBarOverlapping =>
      fractionHeight >
      (_minimumOverlappingBarHeight + _overlappingLayoutTitleBottomPadding);

  const FractionItem({
    Key key,
    @required this.fractionHeight,
    @required this.color,
    @required this.edgeInsets,
    @required this.title,
    this.subtitle,
    this.onClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: _itemWidth,
      margin: edgeInsets,
      child: InkWell(
        onTap: onClick,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                alignment: Alignment.bottomCenter,
                width: _fractionWidth,
                height: _barHeight,
                child: _isBarOverlapping
                    ? _overlappingLayout()
                    : _shortBarLayout(),
              ),
              if (subtitle != null) _subtitle(),
            ]),
      ),
    );
  }

  Widget _overlappingLayout() => Stack(
        alignment: AlignmentDirectional.bottomCenter,
        children: <Widget>[
          _chartBar(),
          Padding(
            padding:
                EdgeInsets.only(bottom: _overlappingLayoutTitleBottomPadding),
            child: _title(Colors.white),
          ),
        ],
      );

  Widget _shortBarLayout() => Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: _shortBarLayoutBottomPadding),
              child: _title(Palette.mediumGrey),
            ),
            _chartBar(),
          ]);

  Widget _title(Color textColor) => Text(
        title,
        softWrap: false,
        maxLines: 1,
        overflow: TextOverflow.clip,
        style: TextStyle(
            fontWeight: FontWeight.w600, fontSize: 11.0, color: textColor),
      );

  Widget _chartBar() => Container(
    decoration: BoxDecoration(
            color: HexColor.fromHex(color),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(_borderRadius),
                topRight: Radius.circular(_borderRadius),
                bottomRight: Radius.circular(_borderRadius))),
        height: fractionHeight,
      );

  Widget _subtitle() => Padding(
      padding: EdgeInsets.only(top: _subtitlePadding),
      child: Text(
        subtitle,
        textAlign: TextAlign.center,
        softWrap: false,
        maxLines: 2,
        overflow: TextOverflow.ellipsis,
        style: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w600),
      ));
}
