import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/categories/spending.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class CategoryDetailsState extends Equatable {
  const CategoryDetailsState();
}

class Loading extends CategoryDetailsState {
  @override
  List<Object> get props => [];
}

class Loaded extends CategoryDetailsState {
  final Spending spending;
  final DateRange dateRangeItem;

  Loaded({@required this.spending, @required this.dateRangeItem});

  @override
  List<Object> get props => [spending, dateRangeItem];
}

class UserInteractionNeeded extends CategoryDetailsState {
  final UriModel uriModel;

  UserInteractionNeeded(this.uriModel);

  @override
  List<Object> get props => [uriModel];

  @override
  String toString() {
    return 'UserInteractionNeeded{uriModel: $uriModel}';
  }
}

class DataLoadFailed extends CategoryDetailsState {
  @override
  List<Object> get props => null;
}

class ErrorState extends CategoryDetailsState {
  final bool syncError;

  ErrorState({this.syncError = false});

  @override
  List<Object> get props => [syncError];

  @override
  String toString() {
    return 'ErrorState{syncError: $syncError}';
  }
}
