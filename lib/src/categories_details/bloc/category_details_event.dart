import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:equatable/equatable.dart';

abstract class CategoryDetailsEvent extends Equatable {
  const CategoryDetailsEvent();
}

class Started extends CategoryDetailsEvent {
  @override
  List<Object> get props => null;
}

class ManageAccountsEvent extends CategoryDetailsEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'AccountsEditEvent{}';
  }
}

class SyncEvent extends CategoryDetailsEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'SyncEvent{}';
  }
}

class BalancesReordered extends CategoryDetailsEvent {
  final int from;
  final int to;

  BalancesReordered(this.from, this.to);

  @override
  List<Object> get props => [from, to];

  @override
  String toString() {
    return 'BalancesReordered{from: $from, to: $to}';
  }
}

class DateRangeChanged extends CategoryDetailsEvent {
  final DateRange item;

  DateRangeChanged(this.item);

  @override
  List<Object> get props => [item];
}
