import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/categories/spending.dart';
import 'package:atoma_cash/models/api/api/categories/spending_by_category.dart';
import 'package:atoma_cash/src/categories/bloc/bloc.dart';
import 'package:atoma_cash/src/categories/list/categories_list_item.dart';
import 'package:atoma_cash/src/categories/period_stats/income_cell.dart';
import 'package:atoma_cash/src/categories/widgets/failed_data_load.dart';
import 'package:atoma_cash/src/categories/widgets/header.dart';
import 'package:atoma_cash/src/categories_details/categories_fraction/fraction_list.dart';
import 'package:atoma_cash/src/home/widgets/list/snapping_scroll_physics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CategoryDetailsScreen extends StatefulWidget {
  final String categoryId;
  final DateRange dateRange;

  const CategoryDetailsScreen(
      {Key key, @required this.categoryId, @required this.dateRange})
      : super(key: key);

  @override
  _CategoryDetailsScreenState createState() => _CategoryDetailsScreenState();
}

class _CategoryDetailsScreenState extends State<CategoryDetailsScreen>
    with AutomaticKeepAliveClientMixin
    implements IHeaderEventsListener {
  SnappingScrollPhysics _scrollPhysics =
      SnappingScrollPhysics(midScrollOffset: 240.0);

  double _periodsScrollOffset = 0.0;

  CategoriesBloc _bloc;

  @override
  void initState() {
    _bloc = CategoriesBloc()..add(Started());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Scaffold(
      body: BlocBuilder(
        bloc: _bloc,
        builder: _rootWidgetForState,
      ),
    );
  }

  Widget _rootWidgetForState(BuildContext context, CategoriesState state) {
    if (state is Loaded) {
      return _loadedWidget(context, state);
    }
    if (state is DataLoadFailed) {
      return FailedDataLoad(
        onReloadClick: _reloadData,
      );
    } else {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  Widget _loadedWidget(BuildContext context, Loaded state) {
    return CustomScrollView(
      shrinkWrap: false,
      physics: _scrollPhysics,
      slivers: <Widget>[
        HeaderWidget(
          state: state,
          listener: this,
        ),
        _periodStatsWidget(context, state.spending),
        SliverToBoxAdapter(
          child: Container(
            margin: EdgeInsets.only(top: 16.0, bottom: 16.0),
            child: CategoriesFractionView(
              total: state.spending.budget.spending.value,
              categories: state.spending.spendingByCategories,
              onManageClick: _onManageCategoriesClick,
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(
              _categoriesList(state.spending.spendingByCategories)),
        ),
        SliverToBoxAdapter(
          child: Container(
            height: 240.0,
          ),
        ),
      ],
    );
  }

  Widget _periodStatsWidget(BuildContext context, Spending spending) {
    return SliverToBoxAdapter(
      child: Container(
        margin: EdgeInsets.only(top: 8.0),
        child: Column(
          children: <Widget>[
            IncomeCell(
              income: spending.income,
            ),
          ],
        ),
      ),
    );
  }

  void _reloadData() {
    _bloc.add(Started());
  }

  void _onManageCategoriesClick() {}

  List<Widget> _categoriesList(List<SpendingByCategory> categories) {
    return categories.map<Widget>((c) => CategoriesListItem(c)).toList();
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void onDateRangeChange(DateRange dateRangeItem) {
    _bloc.add(DateRangeChanged(dateRangeItem));
  }

  @override
  void onReload() {
    _bloc.add(Started());
  }
}
