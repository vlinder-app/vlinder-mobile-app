import 'package:atoma_cash/resources/config/theme_provider.dart';
import 'package:atoma_cash/src/push_notifications/push_notification_service.dart';
import 'package:atoma_cash/src/push_notifications/push_notifications_client.dart';
import 'package:atoma_cash/src/registration/authentication/bloc/authentication_bloc.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/utils/localization_helper.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class App extends StatelessWidget {
  final Flavor flavor;

  const App({Key key, this.flavor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
      statusBarColor: Colors.transparent, //or set color with: Color(0xFF0000FF)
    ));
    PushNotificationService(client: PushNotificationsClient()).configure();
    return MaterialApp(
      localizationsDelegates: [
        LocalizationHelper.localizationsDelegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      builder: ExtendedNavigator<Router>(
        router: Router(),
        initialRoute: Routes.initial,
      ),
      localeListResolutionCallback:
          LocalizationHelper.localeListResolutionCallback,
      supportedLocales: LocalizationHelper.supportedLocales,
      theme: ThemeProvider.configureThemeData(),
    );
  }
}
