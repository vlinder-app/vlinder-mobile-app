import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/activity/widgets/dialogs/option_cell.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:flutter/material.dart';

class ReceiptNotRecognizedDialog extends StatelessWidget {
  static const kBorderRadius = 20.0;
  static const RoundedRectangleBorder border = const RoundedRectangleBorder(
      borderRadius: BorderRadius.all(Radius.circular(kBorderRadius)));

  final VoidCallback onTryAgainClick;
  final VoidCallback onAddManuallyClick;

  const ReceiptNotRecognizedDialog(
      {Key key, this.onTryAgainClick, @required this.onAddManuallyClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: Dimensions.alertDialogPadding,
      shape: border,
      title: _dialogTitleSection(context),
      content: Container(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: _receiptOptionsBuilder(context),
          )),
    );
  }

  Widget _dialogTitleSection(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(
          Localization.of(context).receiptNotRecognizedDialogTitle,
          style: TextStyles.alertDialogTitleStyle,
        ),
        Text(
          Localization.of(context).receiptNotRecognizedDialogSubtitle,
          style: TextStyles.alertDialogSubTitleStyle,
        ),
      ],
    );
  }

  List<Widget> _receiptOptionsBuilder(BuildContext context) {
    return <Widget>[
      OptionsCell(Localization.of(context).addManuallyButton,
          onClick: onAddManuallyClick),
      if (onTryAgainClick != null)
        OptionsCell(Localization.of(context).tryAgainButton,
            onClick: onTryAgainClick),
      OptionsCell(Localization.of(context).cancelButton,
          onClick: () => Navigator.of(context).pop(),
          style: FourRowListTile.titleTextStyle.copyWith(color: Palette.blue)),
    ];
  }
}
