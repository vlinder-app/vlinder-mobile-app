import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/activity/widgets/dialogs/option_cell.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:flutter/material.dart';

class ReceiptOptionsDialog extends StatelessWidget {
  static const kBorderRadius = 20.0;
  static const RoundedRectangleBorder border = const RoundedRectangleBorder(
      borderRadius: BorderRadius.only(
          topLeft: Radius.circular(kBorderRadius),
          topRight: Radius.circular(kBorderRadius)));

  final VoidCallback onLoadReceiptClick;
  final VoidCallback onAddManuallyClick;

  const ReceiptOptionsDialog(
      {Key key,
      @required this.onLoadReceiptClick,
      @required this.onAddManuallyClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
          height: 166.0,
          child: Column(
            children: _receiptOptionsBuilder(context),
          )),
    );
  }

  List<Widget> _receiptOptionsBuilder(BuildContext context) {
    return <Widget>[
      OptionsCell(Localization.of(context).loadReceiptPhotoButton,
          onClick: onLoadReceiptClick),
      OptionsCell(Localization.of(context).addManuallyButton,
          onClick: onAddManuallyClick),
      OptionsCell(Localization.of(context).cancelButton,
          onClick: () => Navigator.of(context).pop(),
          style: FourRowListTile.titleTextStyle.copyWith(color: Palette.blue)),
    ];
  }
}
