import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:flutter/material.dart';

class OptionsCell extends StatelessWidget {
  final String text;
  final TextStyle style;
  final VoidCallback onClick;

  const OptionsCell(this.text,
      {Key key, this.style = FourRowListTile.titleTextStyle, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onClick,
      child: Container(
          height: 55.0,
          child: Center(
            child: Text(
              text,
              style: style,
            ),
          )),
    );
  }
}
