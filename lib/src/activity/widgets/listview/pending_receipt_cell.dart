import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:atoma_cash/src/pending_receipts/models/receipt_image_event_type.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/src/widgets/rounded_icon.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

typedef OnProgressClick = void Function(String guid);
typedef OnReceiptClick = void Function(PendingReceipt receipt);

class PendingReceiptCell extends StatelessWidget {
  static const kProgressIndicatorSize = 24.0;
  static const kStateImageSize = 24.0;

  final PendingReceipt pendingReceipt;
  final OnProgressClick onProgressClick;
  final OnReceiptClick onReceiptClick;

  const PendingReceiptCell(
      {Key key,
      @required this.pendingReceipt,
      @required this.onProgressClick,
      this.onReceiptClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FourRowListTile(
      onClick: (onReceiptClick != null)
          ? (() => onReceiptClick(pendingReceipt))
          : null,
      forceThreeRow: true,
      title: Text(
        Localization.of(context).receiptPhotoCellTitle,
        style: FourRowListTile.titleTextStyle,
      ),
      subtitle: Text(
        pendingReceipt.type.toLocalizedString(context),
        style: FourRowListTile.subtitleTextStyle,
      ),
      leading: RoundedIcon(
        asset: Assets.receiptWhiteIcon,
        size: 54.0,
        iconColor: Palette.mediumGrey,
      ),
      trailing: _progressWidgetForReceipt(context, pendingReceipt),
    );
  }

  Widget _progressWidgetForReceipt(
      BuildContext context, PendingReceipt receipt) {
    switch (receipt.type) {
      case ReceiptImageEventType.Cancelled:
      case ReceiptImageEventType.UploadingFailed:
      case ReceiptImageEventType.RecognitionFailed:
        return _failedProgressIndicator(context);
      case ReceiptImageEventType.PartiallyRecognized:
        return _successProgressIndicator(context);
      default:
        return _progressIndicator(
            context, receipt.id, receipt.progressPercentage);
    }
  }

  Widget _progressIndicator(
          BuildContext context, String guid, int progressValue) =>
      InkWell(
        onTap: () => onProgressClick(guid),
        child: Container(
          child: CircularPercentIndicator(
            lineWidth: 2.0,
            radius: kProgressIndicatorSize,
            percent: (progressValue?.toDouble() ?? 0.0) / 100,
            progressColor: Palette.blue,
            center: Image.asset(Assets.stopSquareBlueIcon),
          ),
        ),
      );

  Widget _staticProgressIndicator(BuildContext context, String asset) =>
      Container(
        width: kStateImageSize,
        height: kStateImageSize,
        child: Image.asset(
          asset,
          fit: BoxFit.fill,
        ),
      );

  Widget _failedProgressIndicator(BuildContext context) =>
      _staticProgressIndicator(context, Assets.warningRoundRedIcon);

  Widget _successProgressIndicator(BuildContext context) =>
      _staticProgressIndicator(context, Assets.warningRoundBlueIcon);
}
