import 'package:atoma_cash/extensions/color_extensions.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction_source_type.dart';
import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/transaction/widgets/transaction_icon.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/utils/carbon/carbon.dart';
import 'package:flutter/material.dart';

class TransactionCell extends StatelessWidget {
  final Transaction transaction;
  final VoidCallback onClick;
  final bool withAttachmentIndicator;
  final bool amountInBase;
  final bool shouldColorizeCategory;

  const TransactionCell(
    this.transaction, {
    Key key,
    this.onClick,
    this.amountInBase = false,
    this.withAttachmentIndicator = true,
    this.shouldColorizeCategory = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FourRowListTile(
      onClick: onClick,
      title: Text(
        _title(context),
        style: FourRowListTile.titleTextStyle,
        overflow: TextOverflow.ellipsis,
        softWrap: false,
      ),
      leading: TransactionIcon(transaction),
      subtitle: _subtitle(context),
      trailing: _trailing(context, transaction),
      subTrailing: Text(
        transaction.carbonFootprint != null
            ? CarbonHelper.carbonFootprintLocalizableValue(
                context, transaction.carbonFootprint)
            : "",
        style: FourRowListTile.subtitleTextStyle,
        overflow: TextOverflow.ellipsis,
        softWrap: false,
      ),
      bottom: _bottomWidget(context),
    );
  }

  Widget _trailing(BuildContext context, Transaction transaction) {
    String textValue = amountInBase
        ? transaction.baseCurrencyAmount?.currencyFormatted() ??
            transaction.amount?.currencyFormatted()
        : transaction.amount?.currencyFormatted();
    return Text(
      textValue,
      style: FourRowListTile.titleTextStyle,
    );
  }

  Widget _subtitle(BuildContext context) {
    if (transaction.failedImageRequest ?? false) {
      return Text(
        Localization.of(context).transactionPhotoUploadFailed,
        style: FourRowListTile.subtitleTextStyle.copyWith(color: Palette.red),
        overflow: TextOverflow.ellipsis,
        softWrap: false,
      );
    } else {
      return Text(
        transaction.sourceType.toLocalizedString(context),
        style: FourRowListTile.subtitleTextStyle,
        overflow: TextOverflow.ellipsis,
        softWrap: false,
      );
    }
  }

  Widget _bottomWidget(BuildContext context) {
    final textColor =
        shouldColorizeCategory ? Palette.white : Palette.mediumGrey;
    return Container(
      decoration: BoxDecoration(
        color: _bottomWidgetBackgroundColor(),
        borderRadius: BorderRadius.all(Radius.circular(2.0)),
      ),
      child: Padding(
        padding: const EdgeInsets.only(left: 6, top: 1, right: 6, bottom: 3),
        child: Text(
          transaction.categories.first.name,
          style: FontBook.textBody2Menu.copyWith(color: textColor),
          overflow: TextOverflow.ellipsis,
          softWrap: false,
        ),
      ),
    );
  }

  String _title(BuildContext context) {
    if (transaction.counterparty?.name != null) {
      return transaction.counterparty.name;
    } else {
      return Localization.of(context)
          .unknownMerchantTransactionTitlePlaceholder;
    }
  }

  Color _bottomWidgetBackgroundColor() {
    if (shouldColorizeCategory) {
      return HexColor.fromHex(transaction.categories.first.color) ??
          Palette.paleGrey;
    } else {
      return Palette.paleGrey;
    }
  }
}
