import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class GroupSeparator extends StatelessWidget {
  final String text;

  const GroupSeparator(this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
          fontSize: 11.0,
          fontWeight: FontWeight.w700,
          letterSpacing: 2.0,
          color: Palette.mediumGrey),
    );
  }
}
