import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

abstract class ActivityEvent extends Equatable {
  const ActivityEvent();
}

class Started extends ActivityEvent {
  @override
  List<Object> get props => null;
}

class Refresh extends ActivityEvent {
  @override
  List<Object> get props => null;
}

class SilentUpdate extends ActivityEvent {
  @override
  List<Object> get props => null;
}

enum ReceiptSource { CAMERA, GALLERY }

class ReceiptAssetsSelected extends ActivityEvent {
  final List<Asset> assets;

  ReceiptAssetsSelected(this.assets);

  @override
  List<Object> get props => [assets];
}

class ReceiptFilesSelected extends ActivityEvent {
  final List<File> files;

  ReceiptFilesSelected(this.files);

  @override
  List<Object> get props => [files];
}

class Redraw extends ActivityEvent {
  @override
  List<Object> get props => null;
}

class PendingReceiptClicked extends ActivityEvent {
  final String receiptId;

  PendingReceiptClicked(this.receiptId);

  @override
  List<Object> get props => [receiptId];
}
