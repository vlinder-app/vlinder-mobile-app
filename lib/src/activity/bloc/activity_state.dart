import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/error/error.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class ActivityState extends Equatable {
  const ActivityState();
}

@immutable
class Loading extends ActivityState {
  @override
  List<Object> get props => null;
}

@immutable
class Loaded extends ActivityState {
  final List<Transaction> transactions;
  final bool transactionsLoading;
  final List<PendingReceipt> pendingReceipts;

  Loaded(
      {this.transactions,
      this.transactionsLoading = false,
      this.pendingReceipts});

  @override
  List<Object> get props =>
      [transactions, transactionsLoading, pendingReceipts];

  Loaded copyWith(
          {bool transactionsLoading,
          List<Transaction> transactions,
          List<PendingReceipt> pendingReceipts}) =>
      Loaded(
          pendingReceipts: pendingReceipts ?? this.pendingReceipts,
          transactionsLoading: transactionsLoading ?? this.transactionsLoading,
          transactions: transactions ?? this.transactions);

  @override
  String toString() {
    return 'Loaded{transactions: $transactions, '
        'transactionsLoading: $transactionsLoading, pendingReceipts: $pendingReceipts}';
  }
}

class ErrorState extends ActivityState {
  final Error error;

  ErrorState(this.error);

  @override
  List<Object> get props => [error];
}
