import 'dart:async';
import 'dart:io';

import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/api/transactions/transactions.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/providers/currency_provider.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/select_photos_event.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:atoma_cash/src/pending_receipts/pending_receipts_controller.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_container.dart';
import 'package:atoma_cash/src/transaction/transaction_receipt_image/transaction_image_controller.dart';
import 'package:bloc/bloc.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

import './bloc.dart';

class ActivityBloc extends Bloc<ActivityEvent, ActivityState>
    implements IReceiptUpdateListener {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  PendingReceiptController _pendingReceiptController =
      PendingReceiptController();

  List<Transaction> _transactions = [];
  List<PendingReceipt> _pendingReceipts = [];

  @override
  ActivityState get initialState => Loading();

  ActivityBloc() {
    BaseCurrencyProvider().updateBaseCurrencies();
    BaseCurrencyProvider().updateBaseCurrency();
    _pendingReceiptController.addListener(this);
  }

  Loaded _cachedLoadedState() =>
      Loaded(transactions: _transactions, pendingReceipts: _pendingReceipts);

  @override
  Stream<ActivityState> mapEventToState(
    ActivityEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapHomeStartedToState();
    }
    if (event is Refresh) {
      yield* _mapRefreshToState();
    }
    if (event is Redraw) {
      yield _cachedLoadedState();
    }
    if (event is PendingReceiptClicked) {
      yield* _mapPendingReceiptClickedToState(event.receiptId);
    }
    if (event is ReceiptFilesSelected) {
      yield* _mapReceiptFilesSelectedToState(event.files);
    }
    if (event is ReceiptAssetsSelected) {
      yield* _mapReceiptAssetsSelectedToState(event.assets);
    }
    if (event is SilentUpdate) {
      yield* _mapSilentUpdateState();
    }
  }

  Stream<ActivityState> _mapSilentUpdateState() async* {
    if (state is Loaded) {
      if (!(state as Loaded).transactionsLoading) {
        yield* _mapRefreshToState();
      }
    }
  }

  Stream<ActivityState> _mapHomeStartedToState() async* {
    BaseResponse<Transactions> transactionsResponse =
        await _apiRemoteRepository.getTransactions();
    if (transactionsResponse.isSuccess()) {
      _transactions = transactionsResponse.result.transactions;
      yield _cachedLoadedState();
    } else {
      yield ErrorState(transactionsResponse.error);
    }
  }

  Stream<ActivityState> _mapRefreshToState() async* {
    yield Loaded(
        transactions: _transactions,
        pendingReceipts: _pendingReceipts,
        transactionsLoading: true);
    BaseResponse<Transactions> transactionsResponse =
        await _apiRemoteRepository.getTransactions();
    if (transactionsResponse.isSuccess()) {
      _transactions = _fillTransactionsWithFailedImageRequests(
          transactionsResponse.result.transactions);
      yield _cachedLoadedState().copyWith(transactionsLoading: false);
    } else {
      yield ErrorState(transactionsResponse.error);
    }
  }

  List<Transaction> _fillTransactionsWithFailedImageRequests(
      List<Transaction> transactions) {
    List<Transaction> updatedTransactionsList = [];
    List<String> _failedImageRequests =
        TransactionImageController().failedImageTransactionsId;
    transactions.forEach((transaction) {
      if (_failedImageRequests.contains(transaction.id))
        updatedTransactionsList
            .add(transaction.copyWith(failedImageRequest: true));
      else
        updatedTransactionsList.add(transaction);
    });
    return updatedTransactionsList;
  }

  Stream<ActivityState> _mapPendingReceiptClickedToState(String id) async* {
    _pendingReceiptController.stopProcessing(id);
    yield _cachedLoadedState();
  }

  Stream<ActivityState> _mapReceiptFilesSelectedToState(
      List<File> files) async* {
    var list = List<ReceiptImageContainer>.generate(
        files.length, (index) => ReceiptImageContainer.fromFile(files[index]));
    _pendingReceiptController.scanReceipts(list);
  }

  Stream<ActivityState> _mapReceiptAssetsSelectedToState(
      List<Asset> assets) async* {
    Analytics().logEvent(SelectPhotosEvent(assets.length));
    var list = List<ReceiptImageContainer>.generate(assets.length,
        (index) => ReceiptImageContainer.fromAsset(assets[index]));
    _pendingReceiptController.scanReceipts(list);
  }

  @override
  void onReceiptsUpdate(List<PendingReceipt> receipts) {
    _pendingReceipts = receipts;
    add(Redraw());
  }

  @override
  void onSuccessTransaction(Transaction transaction, String guid) {
    add(Refresh());
  }
}
