import 'dart:async';
import 'dart:io';

import 'package:atoma_cash/extensions/color_extensions.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/activity/bloc/activity_bloc.dart';
import 'package:atoma_cash/src/activity/bloc/activity_event.dart';
import 'package:atoma_cash/src/activity/bloc/activity_state.dart';
import 'package:atoma_cash/src/activity/widgets/dialogs/receipt_options_dialog.dart';
import 'package:atoma_cash/src/activity/widgets/listview/group_separator.dart';
import 'package:atoma_cash/src/activity/widgets/listview/pending_receipt_cell.dart';
import 'package:atoma_cash/src/activity/widgets/listview/transaction_cell.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/open_activity_tab_event.dart';
import 'package:atoma_cash/src/analytics/models/events/scan_receipt_event.dart';
import 'package:atoma_cash/src/analytics/models/events/select_add_activity_method_event.dart';
import 'package:atoma_cash/src/home_placeholder/itab_listener.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:atoma_cash/src/permissions/permission_controller.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/src/transaction/transaction_details/transaction_details_screen.dart';
import 'package:atoma_cash/src/transaction/transaction_operations/transaction_operations_screen.dart';
import 'package:atoma_cash/utils/date_helper.dart';
import 'package:atoma_cash/utils/dimensions.dart';
import 'package:atoma_cash/utils/platform/platform_helper.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class ActivityScreen extends StatefulWidget implements ITabListener {
  final _ActivityScreenState _state = _ActivityScreenState();

  @override
  _ActivityScreenState createState() => _state;

  @override
  void onAppear() {
    _state.onAppear();
  }
}

class _ActivityScreenState extends State<ActivityScreen>
    with AutomaticKeepAliveClientMixin
    implements ITabListener {
  ActivityBloc _bloc;
  PermissionController _cameraPermissionController;
  PermissionController _galleryPermissionController;

  Completer<void> _refreshCompleter;

  @override
  void initState() {
    _bloc = ActivityBloc()..add(Started());
    _refreshCompleter = Completer<void>();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    _initCameraPermissionController();
    _initGalleryPermissionController();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Palette.blue,
        brightness: Brightness.dark,
        centerTitle: false,
        title: Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Text(
            Localization.of(context).activityTab,
            style: TextStyles.primaryActionBarStyle,
          ),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: IconButton(
              icon: Image.asset(Assets.addReceiptIcon),
              onPressed: _onPlusModalClicked,
            ),
          )
        ],
        bottom: PreferredSize(
          preferredSize: Size.fromHeight(60.0),
          child: _appBarBottomWidget(context),
        ),
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20.0))),
      ),
      body: BlocListener(
        bloc: _bloc,
        listener: _blocListener,
        child: BlocBuilder(
          bloc: _bloc,
          builder: _blocBuilder,
        ),
      ),
    );
  }

  void _initCameraPermissionController() {
    _cameraPermissionController =
        PermissionController.camera(context, _onCameraPermissionGranted);
  }

  void _initGalleryPermissionController() {
    _galleryPermissionController =
        PermissionController.gallery(context, _onGalleryPermissionGranted);
  }

  Widget _appBarBottomWidget(BuildContext context) {
    return Container(
      height: 50.0,
      color: Colors.white,
      child: Stack(
        children: <Widget>[
          Container(
            height: 25.0,
            decoration: BoxDecoration(
                color: Palette.blue,
                borderRadius:
                    BorderRadius.only(bottomLeft: Radius.circular(20.0))),
          ),
          _appBarBottomButtons(context),
        ],
      ),
    );
  }

  Widget _appBarBottomButtons(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 24.0, right: 24.0, bottom: 0.0),
      child: Row(
        children: <Widget>[
          Expanded(child: _scanReceiptButton(context)),
        ],
      ),
    );
  }

  Widget _scanReceiptButton(BuildContext context) {
    return RaisedButton(
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(Assets.qrReceiptIcon),
          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Text(
              Localization.of(context).scanReceiptButton,
              style: TextStyle(
                  fontSize: 15.0,
                  fontWeight: FontWeight.w600,
                  color: Palette.blue),
            ),
          ),
        ],
      ),
      elevation: 3.0,
      onPressed: _onScanReceiptClick,
      shape:
      RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
    );
  }

  void _onPlusModalClicked() {
    showModalBottomSheet<void>(
        context: context,
        shape: ReceiptOptionsDialog.border,
        builder: (_) {
          return ReceiptOptionsDialog(
            onAddManuallyClick: _onAddManuallyClick,
            onLoadReceiptClick: _onLoadReceiptClick,
          );
        });
  }

  Widget _transactionsPlaceholderWidget(BuildContext context) {
    return Container(
      child: Center(
        child: Text(
          Localization.of(context).activityTransactionPlaceholderText,
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 14.0, color: Palette.mediumGrey),
        ),
      ),
    );
  }

  Widget _blocBuilder(BuildContext context, ActivityState state) {
    if (state is Loading) {
      return _loadingStateBuilder(context);
    }
    if (state is Loaded) {
      return _loadedWidgetState(context, state);
    }
    if (state is ErrorState) {
      return _transactionsPlaceholderWidget(context);
    }

    return Container();
  }

  Widget _loadingStateBuilder(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget _loadedWidgetState(BuildContext context, Loaded state) {
    return RefreshIndicator(
        onRefresh: _onRefresh,
        child: CustomScrollView(slivers: [
          SliverToBoxAdapter(
            child:
                _pendingReceiptsListViewBuilder(context, state.pendingReceipts),
          ),
          _transactionsBuilder(context, state)
        ]));
  }

  DateTime _groupTrigger(Transaction transaction) {
    return DateHelper.defaultGroupTrigger(transaction.bookedAt);
  }

  Widget _transactionsBuilder(BuildContext context, Loaded state) {
    if ((state.transactions?.isEmpty ?? true) &&
        (state.pendingReceipts?.isEmpty ?? true)) {
      return SliverFillRemaining(
        child: _transactionsPlaceholderWidget(context),
      );
    } else {
      return SliverToBoxAdapter(
        child: _transactionListViewBuilder(context, state.transactions),
      );
    }
  }

  Widget _pendingReceiptsListViewBuilder(
      BuildContext context, List<PendingReceipt> receipts) {
    if (receipts.isEmpty) {
      return Container();
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: Dimensions.leftRight24Padding.copyWith(top: 24.0),
            child: GroupSeparator(
                Localization.of(context).pendingGroupTitle.toUpperCase()),
          ),
          ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: receipts.length,
              itemBuilder: (BuildContext context, int index) {
                PendingReceipt pendingReceipt = receipts[index];
                return PendingReceiptCell(
                  pendingReceipt: pendingReceipt,
                  onProgressClick: _onPendingReceiptProgressClick,
                  onReceiptClick: _onPendingReceiptClick,
                );
              }),
        ],
      );
    }
  }

  Widget _transactionListViewBuilder(
      BuildContext context, List<Transaction> transactions) {
    return GroupedListView<Transaction, DateTime>(
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      separator: Divider(
        height: 0.0,
        color: Colors.transparent,
        thickness: 0.0,
      ),
      padding: EdgeInsets.only(top: 16.0),
      groupBy: _groupTrigger,
      elements: transactions,
      sort: false,
      groupSeparatorBuilder: (DateTime dateTime) =>
          Padding(
            padding: const EdgeInsets.only(left: 24.0, right: 24.0),
            child: GroupSeparator(
                DateHelper.separatorStringForDateTime(context, dateTime)
                    .toUpperCase()),
          ),
      itemBuilder: (context, transaction) {
        return TransactionCell(
          transaction,
          onClick: () => _onTransactionClick(transaction),
        );
      },
    );
  }

  void _onScanReceiptClick() async {
    Analytics().logEvent(ScanReceiptEvent());
    if (PlatformHelper.isIOS(context)) {
      _cameraPermissionController.requestPermissions(context);
    } else {
      _scanReceiptUsingCamera();
    }
  }

  void _scanReceiptUsingCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    if (image == null) return;
    _scanReceiptFiles([image]);
  }

  void _loadReceiptUsingGallery() async {
    List<Asset> assetsList = List<Asset>();
    try {
      assetsList = await MultiImagePicker.pickImages(
          maxImages: 5,
          enableCamera: false,
          materialOptions: MaterialOptions(
              allViewTitle: Localization
                  .of(context)
                  .galleryRecentsTitle,
              actionBarTitle: Localization
                  .of(context)
                  .galleryAlbumsTitle,
              actionBarColor: Palette.blue.toHex()));
    } on Exception catch (e) {
      print(e.toString());
    }
    _scanReceiptAssets(assetsList);
  }

  void _scanReceiptAssets(List<Asset> assets) {
    _bloc.add(ReceiptAssetsSelected(assets));
  }

  void _scanReceiptFiles(List<File> files) {
    _bloc.add(ReceiptFilesSelected(files));
  }

  void _onPendingReceiptClick(PendingReceipt receipt) async {
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.pendingReceiptDetailsScreen,
        arguments: PendingReceiptDetailsScreenArguments(receipt: receipt));
    _handleResultActions(result);
  }

  void _onPendingReceiptProgressClick(String guid) {
    _bloc.add(PendingReceiptClicked(guid));
  }

  void _onCameraPermissionGranted() {
    _scanReceiptUsingCamera();
  }

  void _onGalleryPermissionGranted() {
    _loadReceiptUsingGallery();
  }

  void _onLoadReceiptClick() async {
    Analytics()
        .logEvent(SelectAddActivityMethodEvent(Methods.loadReceiptPhoto));
    Navigator.of(context).pop();
    _galleryPermissionController.requestPermissions(context);
  }

  void _onAddManuallyClick() async {
    Analytics().logEvent(SelectAddActivityMethodEvent(Methods.addManually));
    Navigator.of(context).pop();
    _fillAndCreateTransaction(null);
  }

  void _refreshTransactions() {
    _bloc.add(Refresh());
  }

  Future<void> _onRefresh() async {
    _refreshTransactions();
    return _refreshCompleter.future;
  }

  void _fillAndCreateTransaction(Transaction transaction) async {
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.transactionOperationsScreen,
        arguments: TransactionOperationsScreenArguments(
            transaction: transaction, operation: TransactionOperation.CREATE));
    _handleResultActions(result);
  }

  void _onTransactionClick(Transaction transaction) {
    _showTransactionDetails(transaction);
  }

  void _showTransactionDetails(Transaction transaction) async {
    var result = await ExtendedNavigator.ofRouter<Router>().pushNamed(
        Routes.transactionDetailsScreen,
        arguments: TransactionDetailsScreenArguments(transaction: transaction));
    _handleResultActions(result);
  }

  void _handleResultActions(dynamic result) {
    if (result != null) {
      if (result is RemoveAction) {
        _refreshTransactions();
      }
      if (result is EditOperation) {
        _refreshTransactions();
      }
      if (result is UpdateOperation) {
        _refreshTransactions();
      }
    }
  }

  void _blocListener(BuildContext context, ActivityState state) {
    if (state is Loaded) {
      if (!state.transactionsLoading) {
        _refreshCompleter?.complete();
        _refreshCompleter = Completer();
      }
    }
    if (state is ErrorState) {}
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void onAppear() {
    Analytics().logEvent(OpenActivityTabEvent());
    _bloc?.add(SilentUpdate());
  }
}
