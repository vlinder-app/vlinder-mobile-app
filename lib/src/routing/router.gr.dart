// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_route/auto_route.dart';
import 'package:atoma_cash/src/initial.dart';
import 'package:atoma_cash/src/home_placeholder/home_placeholder.dart';
import 'package:atoma_cash/src/categories_details/widgets/category_details_screen.dart';
import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/src/transaction/transaction_operations/transaction_operations_screen.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/src/transaction/transaction_details/transaction_details_screen.dart';
import 'package:atoma_cash/src/reports/report_details/report_details_screen.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/src/reports/models.dart';
import 'package:atoma_cash/src/reports/income_report/income_report_screen.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/src/transaction/category_select/category_select_screen.dart';
import 'package:atoma_cash/src/accounts/sync/connections_sync_screen.dart';
import 'package:atoma_cash/src/accounts/bank_security_disclaimer/bank_security_disclaimer.dart';
import 'package:atoma_cash/src/accounts/bank_security_disclaimer/bank_security_disclamer_config.dart';
import 'package:atoma_cash/src/accounts/account_details/account_details_screen.dart';
import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/src/permissions/request_permission_screen/request_permission_screen.dart';
import 'package:atoma_cash/src/permissions/request_permission_screen/request_permission_screen_configuration.dart';
import 'package:atoma_cash/src/registration/phone_validation/phone_validation_screen.dart';
import 'package:atoma_cash/src/registration/phone_confirmation/phone_confirmation_screen.dart';
import 'package:atoma_cash/src/registration/pin_setup/pin_setup.dart';
import 'package:atoma_cash/src/registration/passcode_check/passcode_check_screen.dart';
import 'package:atoma_cash/src/registration/email_confirmation_request/email_confirmation_request_screen.dart';
import 'package:atoma_cash/src/registration/email_confirmation_request/email_confirmation_screen_configuration.dart';
import 'package:atoma_cash/src/registration/email_confirmation/email_confirmation.dart';
import 'package:atoma_cash/src/registration/email_confirmation/bloc/email_confirmation_bloc.dart';
import 'package:atoma_cash/src/registration/email_confirmation/email_confirmation_screen_configuration.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_details/pending_receipt_details.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';
import 'package:atoma_cash/src/environments/evnironments_screen.dart';
import 'package:atoma_cash/models/environments/environment.dart';
import 'package:atoma_cash/src/registration/restricted_area/restricted_area_placeholder_screen.dart';
import 'package:atoma_cash/src/registration/restricted_area/bloc/restricted_area_placeholder_bloc.dart';
import 'package:atoma_cash/src/force_update/force_update_screen.dart';
import 'package:atoma_cash/src/maintenance/maintenance_screen.dart';

abstract class Routes {
  static const initial = '/';
  static const home = '/home';
  static const categoryDetailsScreen = '/category-details-screen';
  static const transactionOperationsScreen = '/transaction-operations-screen';
  static const transactionDetailsScreen = '/transaction-details-screen';
  static const reportDetailsScreen = '/report-details-screen';
  static const incomeReportScreen = '/income-report-screen';
  static const categorySelectScreen = '/category-select-screen';
  static const connectionsSyncScreen = '/connections-sync-screen';
  static const bankSecurityDisclaimer = '/bank-security-disclaimer';
  static const accountDetailsScreen = '/account-details-screen';
  static const requestPermissionScreen = '/request-permission-screen';
  static const phoneValidationScreen = '/phone-validation-screen';
  static const phoneConfirmationScreen = '/phone-confirmation-screen';
  static const pinSetupScreen = '/pin-setup-screen';
  static const passcodeCheck = '/passcode-check';
  static const emailConfirmationRequestScreen =
      '/email-confirmation-request-screen';
  static const emailConfirmationScreen = '/email-confirmation-screen';
  static const pendingReceiptDetailsScreen = '/pending-receipt-details-screen';
  static const environmentsScreen = '/environments-screen';
  static const restrictedAreaPlaceholderScreen =
      '/restricted-area-placeholder-screen';
  static const forceUpdateScreen = '/force-update-screen';
  static const maintenanceScreen = '/maintenance-screen';
  static const all = {
    initial,
    home,
    categoryDetailsScreen,
    transactionOperationsScreen,
    transactionDetailsScreen,
    reportDetailsScreen,
    incomeReportScreen,
    categorySelectScreen,
    connectionsSyncScreen,
    bankSecurityDisclaimer,
    accountDetailsScreen,
    requestPermissionScreen,
    phoneValidationScreen,
    phoneConfirmationScreen,
    pinSetupScreen,
    passcodeCheck,
    emailConfirmationRequestScreen,
    emailConfirmationScreen,
    pendingReceiptDetailsScreen,
    environmentsScreen,
    restrictedAreaPlaceholderScreen,
    forceUpdateScreen,
    maintenanceScreen,
  };
}

class Router extends RouterBase {
  @override
  Set<String> get allRoutes => Routes.all;

  @Deprecated('call ExtendedNavigator.ofRouter<Router>() directly')
  static ExtendedNavigatorState get navigator =>
      ExtendedNavigator.ofRouter<Router>();

  @override
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final args = settings.arguments;
    switch (settings.name) {
      case Routes.initial:
        return MaterialPageRoute<dynamic>(
          builder: (context) => Initial(),
          settings: settings,
        );
      case Routes.home:
        return MaterialPageRoute<dynamic>(
          builder: (context) => HomePlaceholder(),
          settings: settings,
        );
      case Routes.categoryDetailsScreen:
        if (hasInvalidArgs<CategoryDetailsScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<CategoryDetailsScreenArguments>(args);
        }
        final typedArgs = args as CategoryDetailsScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) => CategoryDetailsScreen(
              key: typedArgs.key,
              categoryId: typedArgs.categoryId,
              dateRange: typedArgs.dateRange),
          settings: settings,
        );
      case Routes.transactionOperationsScreen:
        if (hasInvalidArgs<TransactionOperationsScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<TransactionOperationsScreenArguments>(args);
        }
        final typedArgs = args as TransactionOperationsScreenArguments;
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              TransactionOperationsScreen(
                  key: typedArgs.key,
                  transaction: typedArgs.transaction,
                  operation: typedArgs.operation),
          settings: settings,
          transitionsBuilder: TransitionsBuilders.fadeIn,
        );
      case Routes.transactionDetailsScreen:
        if (hasInvalidArgs<TransactionDetailsScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<TransactionDetailsScreenArguments>(args);
        }
        final typedArgs = args as TransactionDetailsScreenArguments;
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              TransactionDetailsScreen(
                  key: typedArgs.key, transaction: typedArgs.transaction),
          settings: settings,
          opaque: false,
          transitionsBuilder: TransitionsBuilders.slideBottom,
          fullscreenDialog: true,
        );
      case Routes.reportDetailsScreen:
        if (hasInvalidArgs<ReportDetailsScreenArguments>(args)) {
          return misTypedArgsRoute<ReportDetailsScreenArguments>(args);
        }
        final typedArgs = args as ReportDetailsScreenArguments ??
            ReportDetailsScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (context) => ReportDetailsScreen(
              key: typedArgs.key,
              category: typedArgs.category,
              dateRange: typedArgs.dateRange,
              mode: typedArgs.mode),
          settings: settings,
        );
      case Routes.incomeReportScreen:
        if (hasInvalidArgs<IncomeReportScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<IncomeReportScreenArguments>(args);
        }
        final typedArgs = args as IncomeReportScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) => IncomeReportScreen(
              key: typedArgs.key,
              reports: typedArgs.reports,
              dateRange: typedArgs.dateRange),
          settings: settings,
        );
      case Routes.categorySelectScreen:
        if (hasInvalidArgs<CategorySelectScreenArguments>(args)) {
          return misTypedArgsRoute<CategorySelectScreenArguments>(args);
        }
        final typedArgs = args as CategorySelectScreenArguments ??
            CategorySelectScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (context) => CategorySelectScreen(
              key: typedArgs.key,
              allCategories: typedArgs.allCategories,
              selectedCategory: typedArgs.selectedCategory,
              primaryCategory: typedArgs.primaryCategory),
          settings: settings,
        );
      case Routes.connectionsSyncScreen:
        if (hasInvalidArgs<ConnectionsSyncScreenArguments>(args)) {
          return misTypedArgsRoute<ConnectionsSyncScreenArguments>(args);
        }
        final typedArgs = args as ConnectionsSyncScreenArguments ??
            ConnectionsSyncScreenArguments();
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              ConnectionsSyncScreen(key: typedArgs.key),
          settings: settings,
          opaque: false,
          transitionsBuilder: TransitionsBuilders.fadeIn,
          fullscreenDialog: true,
        );
      case Routes.bankSecurityDisclaimer:
        if (hasInvalidArgs<BankSecurityDisclaimerArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<BankSecurityDisclaimerArguments>(args);
        }
        final typedArgs = args as BankSecurityDisclaimerArguments;
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              BankSecurityDisclaimer(typedArgs.config),
          settings: settings,
          opaque: false,
          transitionsBuilder: TransitionsBuilders.slideBottom,
          fullscreenDialog: true,
        );
      case Routes.accountDetailsScreen:
        if (hasInvalidArgs<AccountDetailsScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<AccountDetailsScreenArguments>(args);
        }
        final typedArgs = args as AccountDetailsScreenArguments;
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              AccountDetailsScreen(typedArgs.account, key: typedArgs.key),
          settings: settings,
          opaque: false,
          transitionsBuilder: TransitionsBuilders.slideBottom,
          fullscreenDialog: true,
        );
      case Routes.requestPermissionScreen:
        if (hasInvalidArgs<RequestPermissionScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<RequestPermissionScreenArguments>(args);
        }
        final typedArgs = args as RequestPermissionScreenArguments;
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              RequestPermissionScreen(typedArgs.configuration),
          settings: settings,
          opaque: false,
          transitionsBuilder: TransitionsBuilders.slideBottom,
          fullscreenDialog: true,
        );
      case Routes.phoneValidationScreen:
        if (hasInvalidArgs<PhoneValidationScreenArguments>(args)) {
          return misTypedArgsRoute<PhoneValidationScreenArguments>(args);
        }
        final typedArgs = args as PhoneValidationScreenArguments ??
            PhoneValidationScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (context) => PhoneValidationScreen(
              key: typedArgs.key, flowController: typedArgs.flowController),
          settings: settings,
        );
      case Routes.phoneConfirmationScreen:
        if (hasInvalidArgs<PhoneConfirmationScreenArguments>(args)) {
          return misTypedArgsRoute<PhoneConfirmationScreenArguments>(args);
        }
        final typedArgs = args as PhoneConfirmationScreenArguments ??
            PhoneConfirmationScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (context) => PhoneConfirmationScreen(
              key: typedArgs.key,
              phone: typedArgs.phone,
              flowController: typedArgs.flowController),
          settings: settings,
        );
      case Routes.pinSetupScreen:
        if (hasInvalidArgs<PinSetupScreenArguments>(args, isRequired: true)) {
          return misTypedArgsRoute<PinSetupScreenArguments>(args);
        }
        final typedArgs = args as PinSetupScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) => PinSetupScreen(
              key: typedArgs.key,
              isUserHasPasscode: typedArgs.isUserHasPasscode,
              replaceExisting: typedArgs.replaceExisting,
              flowController: typedArgs.flowController,
              stepIndex: typedArgs.stepIndex,
              shouldOverride: typedArgs.shouldOverride),
          settings: settings,
        );
      case Routes.passcodeCheck:
        if (hasInvalidArgs<PasscodeCheckScreenArguments>(args)) {
          return misTypedArgsRoute<PasscodeCheckScreenArguments>(args);
        }
        final typedArgs = args as PasscodeCheckScreenArguments ??
            PasscodeCheckScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (context) => PasscodeCheckScreen(
              key: typedArgs.key, flowController: typedArgs.flowController),
          settings: settings,
        );
      case Routes.emailConfirmationRequestScreen:
        if (hasInvalidArgs<EmailConfirmationRequestScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<EmailConfirmationRequestScreenArguments>(
              args);
        }
        final typedArgs = args as EmailConfirmationRequestScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) => EmailConfirmationRequestScreen(
              key: typedArgs.key,
              flowController: typedArgs.flowController,
              configuration: typedArgs.configuration),
          settings: settings,
        );
      case Routes.emailConfirmationScreen:
        if (hasInvalidArgs<EmailConfirmationScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<EmailConfirmationScreenArguments>(args);
        }
        final typedArgs = args as EmailConfirmationScreenArguments;
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              EmailConfirmationScreen(
                  key: typedArgs.key,
                  email: typedArgs.email,
                  withInitialConfirmationRequest:
                      typedArgs.withInitialConfirmationRequest,
                  bloc: typedArgs.bloc,
                  flowController: typedArgs.flowController,
                  configuration: typedArgs.configuration),
          settings: settings,
          transitionsBuilder: TransitionsBuilders.fadeIn,
        );
      case Routes.pendingReceiptDetailsScreen:
        if (hasInvalidArgs<PendingReceiptDetailsScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<PendingReceiptDetailsScreenArguments>(args);
        }
        final typedArgs = args as PendingReceiptDetailsScreenArguments;
        return MaterialPageRoute<dynamic>(
          builder: (context) => PendingReceiptDetailsScreen(
              key: typedArgs.key, receipt: typedArgs.receipt),
          settings: settings,
          fullscreenDialog: true,
        );
      case Routes.environmentsScreen:
        if (hasInvalidArgs<EnvironmentsScreenArguments>(args)) {
          return misTypedArgsRoute<EnvironmentsScreenArguments>(args);
        }
        final typedArgs = args as EnvironmentsScreenArguments ??
            EnvironmentsScreenArguments();
        return MaterialPageRoute<dynamic>(
          builder: (context) => EnvironmentsScreen(
              key: typedArgs.key,
              currentEnvironment: typedArgs.currentEnvironment),
          settings: settings,
        );
      case Routes.restrictedAreaPlaceholderScreen:
        if (hasInvalidArgs<RestrictedAreaPlaceholderScreenArguments>(args,
            isRequired: true)) {
          return misTypedArgsRoute<RestrictedAreaPlaceholderScreenArguments>(
              args);
        }
        final typedArgs = args as RestrictedAreaPlaceholderScreenArguments;
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              RestrictedAreaPlaceholderScreen(typedArgs.dataSubmitter,
                  key: typedArgs.key),
          settings: settings,
          opaque: false,
          transitionsBuilder: TransitionsBuilders.fadeIn,
          fullscreenDialog: true,
        );
      case Routes.forceUpdateScreen:
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              ForceUpdateScreen(),
          settings: settings,
          opaque: false,
          transitionsBuilder: TransitionsBuilders.fadeIn,
          fullscreenDialog: true,
        );
      case Routes.maintenanceScreen:
        return PageRouteBuilder<dynamic>(
          pageBuilder: (context, animation, secondaryAnimation) =>
              MaintenanceScreen(),
          settings: settings,
          transitionsBuilder: TransitionsBuilders.fadeIn,
        );
      default:
        return unknownRoutePage(settings.name);
    }
  }
}

// *************************************************************************
// Arguments holder classes
// **************************************************************************

//CategoryDetailsScreen arguments holder class
class CategoryDetailsScreenArguments {
  final Key key;
  final String categoryId;
  final DateRange dateRange;
  CategoryDetailsScreenArguments(
      {this.key, @required this.categoryId, @required this.dateRange});
}

//TransactionOperationsScreen arguments holder class
class TransactionOperationsScreenArguments {
  final Key key;
  final Transaction transaction;
  final TransactionOperation operation;
  TransactionOperationsScreenArguments(
      {this.key, @required this.transaction, @required this.operation});
}

//TransactionDetailsScreen arguments holder class
class TransactionDetailsScreenArguments {
  final Key key;
  final Transaction transaction;
  TransactionDetailsScreenArguments({this.key, @required this.transaction});
}

//ReportDetailsScreen arguments holder class
class ReportDetailsScreenArguments {
  final Key key;
  final Category category;
  final DateRange dateRange;
  final Mode mode;
  ReportDetailsScreenArguments(
      {this.key, this.category, this.dateRange, this.mode});
}

//IncomeReportScreen arguments holder class
class IncomeReportScreenArguments {
  final Key key;
  final Reports reports;
  final DateRange dateRange;
  IncomeReportScreenArguments(
      {this.key, @required this.reports, @required this.dateRange});
}

//CategorySelectScreen arguments holder class
class CategorySelectScreenArguments {
  final Key key;
  final List<Category> allCategories;
  final Category selectedCategory;
  final Category primaryCategory;
  CategorySelectScreenArguments(
      {this.key,
      this.allCategories,
      this.selectedCategory,
      this.primaryCategory});
}

//ConnectionsSyncScreen arguments holder class
class ConnectionsSyncScreenArguments {
  final Key key;
  ConnectionsSyncScreenArguments({this.key});
}

//BankSecurityDisclaimer arguments holder class
class BankSecurityDisclaimerArguments {
  final BankSecurityDisclaimerConfiguration config;
  BankSecurityDisclaimerArguments({@required this.config});
}

//AccountDetailsScreen arguments holder class
class AccountDetailsScreenArguments {
  final Account account;
  final Key key;
  AccountDetailsScreenArguments({@required this.account, this.key});
}

//RequestPermissionScreen arguments holder class
class RequestPermissionScreenArguments {
  final IRequestPermissionScreenConfiguration configuration;
  RequestPermissionScreenArguments({@required this.configuration});
}

//PhoneValidationScreen arguments holder class
class PhoneValidationScreenArguments {
  final Key key;
  final IPhoneValidationFlowController flowController;
  PhoneValidationScreenArguments({this.key, this.flowController});
}

//PhoneConfirmationScreen arguments holder class
class PhoneConfirmationScreenArguments {
  final Key key;
  final String phone;
  final IPhoneConfirmationFlowController flowController;
  PhoneConfirmationScreenArguments({this.key, this.phone, this.flowController});
}

//PinSetupScreen arguments holder class
class PinSetupScreenArguments {
  final Key key;
  final bool isUserHasPasscode;
  final bool replaceExisting;
  final IPinSetupScreenNavigationController flowController;
  final int stepIndex;
  final bool shouldOverride;
  PinSetupScreenArguments(
      {this.key,
      @required this.isUserHasPasscode,
      @required this.replaceExisting,
      @required this.flowController,
      @required this.stepIndex,
      this.shouldOverride = false});
}

//PasscodeCheckScreen arguments holder class
class PasscodeCheckScreenArguments {
  final Key key;
  final IPasscodeCheckNavigationController flowController;
  PasscodeCheckScreenArguments({this.key, this.flowController});
}

//EmailConfirmationRequestScreen arguments holder class
class EmailConfirmationRequestScreenArguments {
  final Key key;
  final IEmailConfirmationRequestFlowController flowController;
  final IEmailConfirmationRequestScreenConfiguration configuration;
  EmailConfirmationRequestScreenArguments(
      {this.key, @required this.flowController, @required this.configuration});
}

//EmailConfirmationScreen arguments holder class
class EmailConfirmationScreenArguments {
  final Key key;
  final String email;
  final bool withInitialConfirmationRequest;
  final EmailConfirmationBloc bloc;
  final IEmailConfirmationScreenFlowController flowController;
  final IEmailConfirmationScreenConfiguration configuration;
  EmailConfirmationScreenArguments(
      {this.key,
      this.email,
      this.withInitialConfirmationRequest = true,
      @required this.bloc,
      @required this.flowController,
      @required this.configuration});
}

//PendingReceiptDetailsScreen arguments holder class
class PendingReceiptDetailsScreenArguments {
  final Key key;
  final PendingReceipt receipt;
  PendingReceiptDetailsScreenArguments({this.key, @required this.receipt});
}

//EnvironmentsScreen arguments holder class
class EnvironmentsScreenArguments {
  final Key key;
  final Environment currentEnvironment;
  EnvironmentsScreenArguments({this.key, this.currentEnvironment});
}

//RestrictedAreaPlaceholderScreen arguments holder class
class RestrictedAreaPlaceholderScreenArguments {
  final IPersonalDataSubmitter dataSubmitter;
  final Key key;
  RestrictedAreaPlaceholderScreenArguments(
      {@required this.dataSubmitter, this.key});
}
