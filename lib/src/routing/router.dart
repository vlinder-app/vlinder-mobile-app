import 'package:atoma_cash/src/accounts/account_details/account_details_screen.dart';
import 'package:atoma_cash/src/accounts/bank_security_disclaimer/bank_security_disclaimer.dart';
import 'package:atoma_cash/src/accounts/sync/connections_sync_screen.dart';
import 'package:atoma_cash/src/categories_details/widgets/category_details_screen.dart';
import 'package:atoma_cash/src/environments/evnironments_screen.dart';
import 'package:atoma_cash/src/force_update/force_update_screen.dart';
import 'package:atoma_cash/src/home_placeholder/home_placeholder.dart';
import 'package:atoma_cash/src/maintenance/maintenance_screen.dart';
import 'package:atoma_cash/src/pending_receipts/receipt_details/pending_receipt_details.dart';
import 'package:atoma_cash/src/permissions/request_permission_screen/request_permission_screen.dart';
import 'package:atoma_cash/src/registration/email_confirmation/email_confirmation.dart';
import 'package:atoma_cash/src/registration/email_confirmation_request/email_confirmation_request_screen.dart';
import 'package:atoma_cash/src/registration/passcode_check/passcode_check_screen.dart';
import 'package:atoma_cash/src/registration/phone_confirmation/phone_confirmation_screen.dart';
import 'package:atoma_cash/src/registration/phone_validation/phone_validation_screen.dart';
import 'package:atoma_cash/src/registration/pin_setup/pin_setup.dart';
import 'package:atoma_cash/src/registration/restricted_area/restricted_area_placeholder_screen.dart';
import 'package:atoma_cash/src/reports/income_report/income_report_screen.dart';
import 'package:atoma_cash/src/reports/report_details/report_details_screen.dart';
import 'package:atoma_cash/src/transaction/category_select/category_select_screen.dart';
import 'package:atoma_cash/src/transaction/transaction_details/transaction_details_screen.dart';
import 'package:atoma_cash/src/transaction/transaction_operations/transaction_operations_screen.dart';
import 'package:auto_route/auto_route.dart';
import 'package:auto_route/auto_route_annotations.dart';

import '../initial.dart';

@MaterialAutoRouter()
class $Router {
  @MaterialRoute(initial: true)
  Initial initial;
  HomePlaceholder home;
  CategoryDetailsScreen categoryDetailsScreen;
  @CustomRoute(transitionsBuilder: TransitionsBuilders.fadeIn)
  TransactionOperationsScreen transactionOperationsScreen;
  @CustomRoute(
      fullscreenDialog: true,
      opaque: false,
      transitionsBuilder: TransitionsBuilders.slideBottom)
  TransactionDetailsScreen transactionDetailsScreen;
  ReportDetailsScreen reportDetailsScreen;
  IncomeReportScreen incomeReportScreen;
  CategorySelectScreen categorySelectScreen;
  @CustomRoute(
      fullscreenDialog: true,
      opaque: false,
      transitionsBuilder: TransitionsBuilders.fadeIn)
  ConnectionsSyncScreen connectionsSyncScreen;
  @CustomRoute(
      fullscreenDialog: true,
      opaque: false,
      transitionsBuilder: TransitionsBuilders.slideBottom)
  BankSecurityDisclaimer bankSecurityDisclaimer;

  @CustomRoute(
      fullscreenDialog: true,
      opaque: false,
      transitionsBuilder: TransitionsBuilders.slideBottom)
  AccountDetailsScreen accountDetailsScreen;

  @CustomRoute(
      fullscreenDialog: true,
      opaque: false,
      transitionsBuilder: TransitionsBuilders.slideBottom)
  RequestPermissionScreen requestPermissionScreen;

  // Authentication
  PhoneValidationScreen phoneValidationScreen;
  PhoneConfirmationScreen phoneConfirmationScreen;
  PinSetupScreen pinSetupScreen;
  PasscodeCheckScreen passcodeCheck;
  EmailConfirmationRequestScreen emailConfirmationRequestScreen;
  @CustomRoute(transitionsBuilder: TransitionsBuilders.fadeIn)
  EmailConfirmationScreen emailConfirmationScreen;
  @MaterialRoute(fullscreenDialog: true)
  PendingReceiptDetailsScreen pendingReceiptDetailsScreen;
  EnvironmentsScreen environmentsScreen;
  @CustomRoute(
      fullscreenDialog: true,
      opaque: false,
      transitionsBuilder: TransitionsBuilders.fadeIn)
  RestrictedAreaPlaceholderScreen restrictedAreaPlaceholderScreen;
  @CustomRoute(
      fullscreenDialog: true,
      opaque: false,
      transitionsBuilder: TransitionsBuilders.fadeIn)
  ForceUpdateScreen forceUpdateScreen;
  @CustomRoute(transitionsBuilder: TransitionsBuilders.fadeIn)
  MaintenanceScreen maintenanceScreen;
}
