import 'package:atoma_cash/models/api/api/categories/period.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/material.dart';

class PeriodLocalization {
  static String periodLocalization(
      BuildContext context, PeriodType periodType, String prefix) {
    String prefix;
    switch (periodType) {
      case PeriodType.WEEK:
      case PeriodType.MONTH:
        prefix = Localization.of(context).legacyThisPeriod;
        break;
      default:
        {
          prefix = "";
        }
    }
    return "$prefix ${localizationForPeriodType(context, periodType)}";
  }

  static String localizationForPeriodType(
      BuildContext context, PeriodType periodType) {
    switch (periodType) {
      case PeriodType.TODAY:
        return Localization.of(context).today;
        break;
      case PeriodType.WEEK:
        return Localization.of(context).week;
        break;
      case PeriodType.MONTH:
        return Localization.of(context).month;
        break;
      case PeriodType.CUSTOM:
        return Localization.of(context).custom;
        break;
      default:
        {
          return "Undefined period";
        }
    }
  }
}
