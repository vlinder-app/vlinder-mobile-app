import 'dart:async';

import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/categories/spending.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:bloc/bloc.dart';

import './bloc.dart';

class CategoriesBloc extends Bloc<CategoriesEvent, CategoriesState> {
  ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  Spending _spending;
  DateRange _dateRangeItem;

  @override
  CategoriesState get initialState {
    _dateRangeItem = DateRange.month();
    return Loading();
  }

  @override
  Stream<CategoriesState> mapEventToState(
    CategoriesEvent event,
  ) async* {
    if (event is Started) {
      yield* _mapHomeStartedToState();
    }
    if (event is DateRangeChanged) {
      yield* _mapPeriodChangedToState(event.item);
    }
  }

  Stream<CategoriesState> _mapHomeStartedToState() async* {
    yield Loading();
    BaseResponse<Spending> balancesResponse =
    await _apiRemoteRepository.getSpending(_dateRangeItem.period);
    if (balancesResponse.isSuccess()) {
      _spending = balancesResponse.result;
      yield Loaded(spending: _spending, dateRangeItem: _dateRangeItem);
    } else {
      yield DataLoadFailed();
    }
  }

  Stream<CategoriesState> _mapPeriodChangedToState(DateRange item) async* {
    yield Loading();
    _dateRangeItem = item;
    yield* _mapHomeStartedToState();
  }

/*
  Stream<CategoriesState> _mapSyncEventToState() async* {
    yield Loading();
    BaseResponse<SyncStatus> syncBalanceResponse =
        await _apiRemoteRepository.syncBalances();
    if (syncBalanceResponse.isSuccess()) {
      String syncStatus = syncBalanceResponse.result.status;
      yield* _mapSyncStatusToState(syncStatus);
    } else {
      yield ErrorState(syncError: true);
    }
    yield Loaded(balances: _balances);
  }
   */
}
