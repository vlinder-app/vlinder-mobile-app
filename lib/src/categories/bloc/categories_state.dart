import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/categories/spending.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

@immutable
abstract class CategoriesState extends Equatable {
  const CategoriesState();
}

class Loading extends CategoriesState {
  @override
  List<Object> get props => [];
}

class Loaded extends CategoriesState {
  final Spending spending;
  final DateRange dateRangeItem;

  Loaded({@required this.spending, @required this.dateRangeItem});

  @override
  List<Object> get props => [spending, dateRangeItem];
}

class UserInteractionNeeded extends CategoriesState {
  final UriModel uriModel;

  UserInteractionNeeded(this.uriModel);

  @override
  List<Object> get props => [uriModel];

  @override
  String toString() {
    return 'UserInteractionNeeded{uriModel: $uriModel}';
  }
}

class DataLoadFailed extends CategoriesState {
  @override
  List<Object> get props => null;
}

class ErrorState extends CategoriesState {
  final bool syncError;

  ErrorState({this.syncError = false});

  @override
  List<Object> get props => [syncError];

  @override
  String toString() {
    return 'ErrorState{syncError: $syncError}';
  }
}
