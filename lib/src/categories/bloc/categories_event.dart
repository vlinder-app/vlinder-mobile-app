import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:equatable/equatable.dart';

abstract class CategoriesEvent extends Equatable {
  const CategoriesEvent();
}

class Started extends CategoriesEvent {
  @override
  List<Object> get props => null;
}

class ManageAccountsEvent extends CategoriesEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'AccountsEditEvent{}';
  }
}

class SyncEvent extends CategoriesEvent {
  @override
  List<Object> get props => null;

  @override
  String toString() {
    return 'SyncEvent{}';
  }
}

class BalancesReordered extends CategoriesEvent {
  final int from;
  final int to;

  BalancesReordered(this.from, this.to);

  @override
  List<Object> get props => [from, to];

  @override
  String toString() {
    return 'BalancesReordered{from: $from, to: $to}';
  }
}

class DateRangeChanged extends CategoriesEvent {
  final DateRange item;

  DateRangeChanged(this.item);

  @override
  List<Object> get props => [item];
}
