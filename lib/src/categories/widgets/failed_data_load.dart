import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/utils/ui/widgets.dart';
import 'package:flutter/material.dart';

class FailedDataLoad extends StatelessWidget {
  final String notificationText;
  final VoidCallback onReloadClick;

  const FailedDataLoad({Key key, this.notificationText, this.onReloadClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              notificationText ?? Localization.of(context).couldLoadData,
              style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 14.0,
                  color: Palette.darkGrey),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Widgets.styledFlatButton(
                  text: Localization.of(context).reloadButton,
                  onPressed: onReloadClick),
            ),
          ],
        ),
      ),
    );
  }
}
