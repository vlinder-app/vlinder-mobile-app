import 'package:atoma_cash/models/api/api/categories/budget.dart';
import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/categories/period.dart';
import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/categories/bloc/bloc.dart';
import 'package:atoma_cash/src/categories/widgets/period_selector_dialog.dart';
import 'package:atoma_cash/src/categories/widgets/total_spending.dart';
import 'package:atoma_cash/src/widgets/periods_widget/periods_widget.dart';
import 'package:atoma_cash/utils/platform/platform_helper.dart';
import 'package:flutter/material.dart';
import 'package:timeago/timeago.dart' as timeago;

abstract class IHeaderEventsListener {
  void onDateRangeChange(DateRange dateRangeItem);

  void onReload();
}

class HeaderWidget extends StatefulWidget {
  static const kHeaderHeight = 250.0;

  final IHeaderEventsListener listener;
  final Loaded state;

  const HeaderWidget({Key key, @required this.state, this.listener})
      : super(key: key);

  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget>
    implements IPeriodsListener {
  @override
  Widget build(BuildContext context) {
    return _headerAppBarWidget(context, widget.state);
  }

  Widget _headerAppBarWidget(BuildContext context, Loaded state) {
    return SliverAppBar(
      brightness: Brightness.dark,
      pinned: true,
      backgroundColor: Palette.blue,
      title: Text(
        Localization.of(context).categoriesHeader,
        style: TextStyle(
            color: Colors.white, fontSize: 20.0, fontWeight: FontWeight.w500),
      ),
      actions: _appBarActions(context, state),
      centerTitle: false,
      expandedHeight: HeaderWidget.kHeaderHeight,
      flexibleSpace: _flexibleSpace(context, state),
    );
  }

  List<Widget> _appBarActions(BuildContext context, Loaded state) {
    return [
      _calendarPopupMenuButton(context, state.dateRangeItem),
      Padding(
        padding: const EdgeInsets.only(right: 10.0, left: 10.0),
        child: IconButton(
          color: Colors.white,
          icon: Image.asset(Assets.updateIcon),
          onPressed: widget.listener.onReload,
        ),
      )
    ];
  }

  Widget _flexibleSpace(BuildContext context, Loaded state) {
    const EdgeInsets leftRightPadding =
        EdgeInsets.only(left: 16.0, right: 16.0);
    double statusBarHeight = MediaQuery.of(context).padding.top;
    double topMargin = kToolbarHeight +
        (PlatformHelper.isIOS(context) ? statusBarHeight / 2 : 0.0);
    return FlexibleSpaceBar(
      collapseMode: CollapseMode.parallax,
      background: Container(
        color: Colors.white,
        child: Container(
          decoration: BoxDecoration(
              color: Palette.blue,
              borderRadius:
                  BorderRadius.only(bottomLeft: Radius.circular(20.0))),
          margin: EdgeInsets.only(top: 0.0, bottom: 0.0),
          padding: EdgeInsets.only(top: topMargin, bottom: 0.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: leftRightPadding,
                child: _updatedLabel(state.spending.relevantAt),
              ),
              Padding(
                padding: leftRightPadding,
                child: _totalSpending(context, state.spending.budget),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: PeriodsWidget(
                  state.dateRangeItem,
                  listener: this,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  String _periodTypeString(BuildContext context, PeriodType type) {
    switch (type) {
      case PeriodType.TODAY:
        return Localization.of(context).today;
      case PeriodType.WEEK:
        return Localization.of(context).week;
      case PeriodType.MONTH:
        return Localization.of(context).month;
      case PeriodType.CUSTOM:
        return Localization.of(context).custom;
      default:
        return "Undefined period";
    }
  }

  void _onPeriodChanged(PeriodType type, DateRange selectedDateRange) {
    if (type == selectedDateRange.periodType) {
      return;
    }
    if (type == PeriodType.CUSTOM) {
      _showCustomPeriodDialog(null, null);
    } else {
      DateRange item;
      switch (type) {
        case PeriodType.TODAY:
          item = DateRange.day();
          break;
        case PeriodType.WEEK:
          item = DateRange.week();
          break;
        case PeriodType.MONTH:
          item = DateRange.month();
          break;
        default:
          item = DateRange.month();
      }
      widget.listener.onDateRangeChange(item);
    }
  }

  Widget _totalSpending(BuildContext context, Budget budget) {
    return TotalSpending(budget);
  }

  void _showCustomPeriodDialog(DateTime startDate, DateTime endDate) {
    if (startDate == null) {
      startDate = DateTime.now().subtract(Duration(days: 1));
    }

    if (endDate == null) {
      endDate = DateTime.now();
    }

    showDialog(
        context: context,
        builder: (_) {
          return PeriodSelectorDialog(
            startDate: startDate,
            endDate: endDate,
            onPeriodChanged: _onCustomPeriodSelected,
          );
        });
  }

  void _onCustomPeriodSelected(DateTime startDate, DateTime endDate) {
    DateRange customDateRangeItem = DateRange(
        period: Period(startDate: startDate, endDate: endDate),
        periodType: PeriodType.CUSTOM);
    widget.listener.onDateRangeChange(customDateRangeItem);
  }

  Widget _calendarPopupMenuButton(BuildContext context,
      DateRange selectedDateRange) {
    return PopupMenuButton<PeriodType>(
      icon: Image.asset(Assets.calendarIcon),
      onSelected: (periodType) =>
          _onPeriodChanged(periodType, selectedDateRange),
      itemBuilder: (BuildContext context) {
        return PeriodType.values.map((PeriodType type) {
          return PopupMenuItem<PeriodType>(
            value: type,
            child: Text(
              _periodTypeString(context, type),
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
          );
        }).toList();
      },
    );
  }

  Widget _updatedLabel(DateTime relevantAt) {
    return Text(
      timeago.format(relevantAt),
      style: TextStyle(
          fontSize: 13.0,
          fontWeight: FontWeight.w500,
          color: Colors.white.withAlpha(120)),
    );
  }

  @override
  void onCustomPeriodClick(Period period) {
    _showCustomPeriodDialog(period.startDate, period.endDate);
  }

  @override
  void onRangeSelected(DateRange dateRangeItem) {
    widget.listener.onDateRangeChange(dateRangeItem);
  }

  @override
  void onResetCustomPeriod() {
    widget.listener.onDateRangeChange(DateRange.month());
  }

  @override
  void onPeriodChanged(PeriodType type, DateRange selectedDateRange) {
    // TODO: implement onPeriodChanged
  }
}
