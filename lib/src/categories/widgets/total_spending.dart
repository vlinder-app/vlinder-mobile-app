import 'package:atoma_cash/models/api/api/categories/budget.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:flutter/material.dart';

class TotalSpending extends StatelessWidget {
  final Budget budget;
  final VoidCallback onSetBudgetClick;

  const TotalSpending(this.budget, {Key key, this.onSetBudgetClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _totalSpendingAmountColumn(
                context,
                budget.spending.currencyFormatted(),
                Localization.of(context).legacySpentLabel,
                CrossAxisAlignment.start),
            _budgetLeftWidget(context, budget)
          ],
        ),
        if (budget.amountLeft != null)
          Padding(
            padding: const EdgeInsets.only(top: 9.0, bottom: 10.0),
            child: LinearProgressIndicator(
              value: _indicatorValue(),
              valueColor: _isOverExpense()
                  ? AlwaysStoppedAnimation(Palette.red)
                  : AlwaysStoppedAnimation(Colors.white),
              backgroundColor:
              _isOverExpense() ? Colors.white : Colors.white.withAlpha(50),
            ),
          )
      ],
    );
  }

  bool _isOverExpense() => budget.overExpensePercentage > 0.0;

  double _indicatorValue() {
    return budget.overExpensePercentage > 0.0
        ? budget.overExpensePercentage / 100
        : budget.spentPercentage / 100;
  }

  Widget _budgetLeftWidget(BuildContext context, Budget budget) {
    if (budget.amountLeft != null) {
      return _totalSpendingAmountColumn(
          context,
          budget.amountLeft.currencyFormatted(),
          Localization
              .of(context)
              .legacyLeftLabel,
          CrossAxisAlignment.end);
    } else if (onSetBudgetClick != null) {
      return Container(
        height: 40.0,
        child: RaisedButton(
            onPressed: onSetBudgetClick,
            child: Text(
              "Set budget",
              style: TextStyle(fontWeight: FontWeight.w600),
            ),
            elevation: 5.0,
            textColor: Palette.blue,
            color: Colors.white,
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(15.0),
            )),
      );
    } else {
      return Container();
    }
  }

  Widget _totalSpendingAmountColumn(BuildContext context, String amount,
      String hint, CrossAxisAlignment alignment) {
    TextStyle amountTextStyle = TextStyle(
        color: Colors.white, fontSize: 24.0, fontWeight: FontWeight.w600);
    TextStyle hintTextStyle = TextStyle(
        color: Colors.white, fontSize: 14.0, fontWeight: FontWeight.w500);
    return Column(
      crossAxisAlignment: alignment,
      children: <Widget>[
        Text(
          amount,
          style: amountTextStyle,
        ),
        Text(
          hint,
          style: hintTextStyle,
        ),
      ],
    );
  }
}
