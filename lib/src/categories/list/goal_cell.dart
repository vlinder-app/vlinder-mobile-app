import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/categories/list/categories_base_item.dart';
import 'package:flutter/material.dart';

class GoalCell extends StatelessWidget {
  final String title, subtitle, trailing;
  final double progress;
  final VoidCallback onClick;

  const GoalCell(this.title, this.subtitle, this.trailing, this.progress, {Key key, this.onClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          CategoriesBaseItem(
            title: title,
            subtitle: subtitle,
            trailing: trailing,
            onClick: onClick,
          ),
          Container(
            margin: EdgeInsets.only(left: 24.0, right: 24.0),
            transform: Matrix4.translationValues(0.0, -5.0, 0.0),
            child: LinearProgressIndicator(
              value: progress,
              backgroundColor: Palette.paleGrey,
              valueColor: AlwaysStoppedAnimation(Palette.blue),
            ),
          )
        ],
      ),
    );
  }
}
