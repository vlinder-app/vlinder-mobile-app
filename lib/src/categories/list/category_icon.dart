import 'package:atoma_cash/extensions/color_extensions.dart';
import 'package:atoma_cash/models/api/api/categories/spending_by_category.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class CategoryIcon extends StatelessWidget {
  final SpendingByCategory category;

  const CategoryIcon(
    this.category, {
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CircularPercentIndicator(
      radius: 38.0,
      lineWidth: 2.0,
      percent: _indicatorValue(),
      center: Image.network(
        category.category.iconUrl,
        width: 25.0,
        height: 25.0,
      ),
      circularStrokeCap: CircularStrokeCap.butt,
      backgroundColor: _isOverExpense()
          ? HexColor.fromHex(category.category.color)
          : Palette.paleGrey,
      progressColor: _isOverExpense()
          ? Palette.red
          : HexColor.fromHex(category.category.color),
    );
  }

  bool _isOverExpense() {
    return category.budget.overExpensePercentage > 0;
  }

  double _indicatorValue() {
    double overExpensePercentage = category.budget.overExpensePercentage / 100;
    double spentPercentage = category.budget.spentPercentage / 100;
    return _isOverExpense()
        ? _indicatorValueRange(overExpensePercentage)
        : _indicatorValueRange(spentPercentage);
  }

  double _indicatorValueRange(double percentage) {
    if (percentage > 1.0) {
      return 1.0;
    } else if (percentage < 0.0) {
      return 0.0;
    } else {
      return percentage;
    }
  }
}
