import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CategoriesBaseItem extends StatelessWidget {
  final String title, subtitle, trailing, subTrailing;
  final Color subtitleColor;
  final Widget leading;
  final Widget bottom;
  final VoidCallback onClick;

  const CategoriesBaseItem(
      {Key key,
      @required this.title,
      @required this.trailing,
      this.subtitle,
      this.bottom,
      this.subTrailing,
      this.subtitleColor = Palette.mediumGrey,
      this.leading,
      this.onClick})
      : assert(title != null && trailing != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget subtitleWidget;
    if (subtitle != null) {
      subtitleWidget = Text(
        subtitle,
        overflow: TextOverflow.ellipsis,
        softWrap: false,
        maxLines: 1,
        style: FourRowListTile.subtitleTextStyle.copyWith(color: subtitleColor),
      );
    }

    Widget subtrailingWidget;
    if (subTrailing != null) {
      subtrailingWidget = Text(
        subTrailing,
        textAlign: TextAlign.end,
        style: FourRowListTile.subtitleTextStyle,
        overflow: TextOverflow.ellipsis,
        softWrap: false,
        maxLines: 1,
      );
    }

    return FourRowListTile(
      onClick: onClick,
      leading: leading,
      title: Text(
        title,
        style: FourRowListTile.titleTextStyle,
        overflow: TextOverflow.ellipsis,
        maxLines: 1,
      ),
      subtitle: subtitleWidget,
      trailing: Text(
        trailing,
        style: FourRowListTile.titleTextStyle,
      ),
      subTrailing: subtrailingWidget,
      bottom: bottom,
    );
  }
}
