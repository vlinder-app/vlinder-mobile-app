import 'package:atoma_cash/models/api/api/categories/spending_by_category.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/categories/categories_fraction/fraction_item.dart';
import 'package:flutter/material.dart';

class CategoriesFractionView extends StatelessWidget {
  static const double height = 206.0;
  static const double _kViewHeight = 161.0;
  static const double _kViewFractionMaxHeight = 136.0;

  final VoidCallback onManageClick;
  final double total;
  final List<SpendingByCategory> categories;

  const CategoriesFractionView(
      {Key key,
      @required this.total,
      @required this.categories,
      @required this.onManageClick})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 45.0,
          margin: EdgeInsets.only(left: 24.0, right: 24.0),
          child: Row(
            children: <Widget>[
              Text(
                Localization
                    .of(context)
                    .expenses
                    .toUpperCase(),
                style: TextStyle(
                    fontSize: 11.0,
                    letterSpacing: 2.0,
                    color: Palette.mediumGrey,
                    fontWeight: FontWeight.w700),
              ),
              InkWell(
                child: Text(
                  Localization.of(context).legacyManageButton,
                  textAlign: TextAlign.end,
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      color: Palette.blue,
                      fontSize: 13.0),
                ),
                onTap: onManageClick,
              )
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ),
        ),
        Container(
          height: _kViewHeight,
          child: ListView.builder(
            itemCount: categories.length,
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) {
              SpendingByCategory category = categories[index];
              double percentageOfTotal = _categoryPercentageOfTotal(index);
              return FractionItem(
                color: category.category.color,
                percentage: percentageOfTotal.toInt(),
                fractionHeight: _fractionHeight(index, percentageOfTotal),
                edgeInsets: EdgeInsets.only(
                    left: 24.0,
                    right: index == categories.length - 1 ? 24.0 : 0.0),
              );
            },
          ),
        ),
      ],
    );
  }

  double _categoryPercentageOfTotal(int index) {
    return categories[index].budget.spending.value * 100 / total;
  }

  double _fractionHeight(int index, double percentageOfTotal) {
    return _kViewFractionMaxHeight *
        _categoryPercentageOfTotal(index) /
        _categoryPercentageOfTotal(0);
  }
}
