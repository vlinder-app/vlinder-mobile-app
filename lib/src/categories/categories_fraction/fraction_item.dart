import 'package:atoma_cash/extensions/color_extensions.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class FractionItem extends StatelessWidget {
  static const _itemWidth = 38.0;

  final String color;
  final int percentage;
  final double fractionHeight;
  final EdgeInsets edgeInsets;

  const FractionItem({
    Key key,
    @required this.percentage,
    @required this.fractionHeight,
    @required this.color,
    @required this.edgeInsets,
  }) : super(key: key);

  String _percentageText(int percentage) =>
      percentage <= 0 ? "< 0 %" : "$percentage %";

  @override
  Widget build(BuildContext context) {
    return Container(
      width: _itemWidth,
      margin: edgeInsets,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
                color: HexColor.fromHex(color),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(8.0),
                    topRight: Radius.circular(8.0))),
            height: fractionHeight,
            margin: EdgeInsets.only(bottom: 12.0),
          ),
          Text(
            _percentageText(percentage),
            softWrap: false,
            maxLines: 1,
            overflow: TextOverflow.clip,
            style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 13.0,
                color: Palette.mediumGrey),
          )
        ],
      ),
    );
  }
}
