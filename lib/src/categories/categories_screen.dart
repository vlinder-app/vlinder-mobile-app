import 'package:atoma_cash/models/api/api/categories/date_range_filter.dart';
import 'package:atoma_cash/models/api/api/categories/spending.dart';
import 'package:atoma_cash/models/api/api/categories/spending_by_category.dart';
import 'package:atoma_cash/src/categories/bloc/bloc.dart';
import 'package:atoma_cash/src/categories/list/categories_list_item.dart';
import 'package:atoma_cash/src/categories/period_stats/income_cell.dart';
import 'package:atoma_cash/src/categories/widgets/failed_data_load.dart';
import 'package:atoma_cash/src/categories/widgets/header.dart';
import 'package:atoma_cash/src/home/widgets/list/snapping_scroll_physics.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/src/widgets/four_row_list_tile.dart';
import 'package:atoma_cash/utils/text_styles.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'categories_fraction/fraction_list.dart';

class CategoriesScreen extends StatefulWidget {
  @override
  _CategoriesScreenState createState() => _CategoriesScreenState();
}

class _CategoriesScreenState extends State<CategoriesScreen>
    with AutomaticKeepAliveClientMixin
    implements IHeaderEventsListener {
  SnappingScrollPhysics _scrollPhysics =
  SnappingScrollPhysics(midScrollOffset: 240.0);

  double _periodsScrollOffset = 0.0;

  CategoriesBloc _bloc;

  @override
  void initState() {
    _bloc = CategoriesBloc()..add(Started());
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder(
      bloc: _bloc,
      builder: _rootWidgetForState,
    );
  }

  Widget _rootWidgetForState(BuildContext context, CategoriesState state) {
    if (state is Loaded) {
      return _loadedWidget(context, state);
    }
    if (state is DataLoadFailed) {
      return FailedDataLoad(
        onReloadClick: _reloadData,
      );
    } else {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
  }

  double _bottomSpacerHeight(BuildContext context, Loaded state) {
    var safePadding = MediaQuery
        .of(context)
        .padding
        .top;

    final screenHeight = MediaQuery
        .of(context)
        .size
        .height -
        safePadding -
        kBottomNavigationBarHeight;

    double contentMargin = 40.0;

    double headerHeight = HeaderWidget.kHeaderHeight;
    double categoriesListHeight =
    _categoriesListHeight(state.spending.spendingByCategories);
    double contentHeight =
        categoriesListHeight + contentMargin + CategoriesFractionView.height;

    if (contentHeight < headerHeight) {
      return screenHeight - headerHeight - contentHeight;
    } else {
      double visibleContentHeight = screenHeight -
          headerHeight -
          kToolbarHeight -
          UiHelper.topSpacerHeight(context);
      double notVisibleContentHeight = contentHeight - visibleContentHeight;
      if (notVisibleContentHeight < 0) {
        return 0.0;
      } else if (notVisibleContentHeight < headerHeight) {
        return headerHeight - notVisibleContentHeight;
      }
    }
    return 0.0;
  }

  double _categoriesListHeight(List<SpendingByCategory> categories) {
    double summaryHeight = 0.0;
    categories.forEach((c) {
      summaryHeight += (c.budget.amountOverExpense.value > 0 &&
          c.budget.carbonFootprint != 0)
          ? FourRowListTile.twoRowHeight
          : FourRowListTile.threeRowHeight;
    });
    return summaryHeight;
  }

  Widget _loadedWidget(BuildContext context, Loaded state) {
    return CustomScrollView(
      shrinkWrap: false,
      physics: _scrollPhysics,
      slivers: <Widget>[
        HeaderWidget(
          state: state,
          listener: this,
        ),
        _periodStatsWidget(context, state.spending),
        SliverToBoxAdapter(
          child: Container(
            margin: EdgeInsets.only(top: 16.0, bottom: 16.0),
            child: CategoriesFractionView(
              total: state.spending.budget.spending.value,
              categories: state.spending.spendingByCategories,
              onManageClick: _onManageCategoriesClick,
            ),
          ),
        ),
        SliverList(
          delegate: SliverChildListDelegate(_categoriesList(
              state.spending.spendingByCategories, state.dateRangeItem)),
        ),
        SliverToBoxAdapter(
          child: Container(
            height: _bottomSpacerHeight(context, state),
            color: Colors.white,
          ),
        ),
      ],
    );
  }

  Widget _periodStatsWidget(BuildContext context, Spending spending) {
    return SliverToBoxAdapter(
      child: Container(
        margin: EdgeInsets.only(top: 8.0),
        child: Column(
          children: <Widget>[
            IncomeCell(
              income: spending.income,
            ),
          ],
        ),
      ),
    );
  }

  void _reloadData() {
    _bloc.add(Started());
  }

  void _onManageCategoriesClick() {}

  List<Widget> _categoriesList(List<SpendingByCategory> categories,
      DateRange dateRange) {
    return categories
        .map<Widget>((c) =>
        CategoriesListItem(
          c,
          onClick: () => _openCategoryDetails(c, dateRange),
        ))
        .toList();
  }

  void _openCategoryDetails(SpendingByCategory spendingByCategory,
      DateRange dateRange) {
    ExtendedNavigator.ofRouter<Router>().pushNamed(Routes.categoryDetailsScreen,
        arguments: CategoryDetailsScreenArguments(
            categoryId: spendingByCategory.category.id, dateRange: dateRange));
  }

  @override
  bool get wantKeepAlive => true;

  @override
  void onDateRangeChange(DateRange dateRangeItem) {
    _bloc.add(DateRangeChanged(dateRangeItem));
  }

  @override
  void onReload() {
    _bloc.add(Started());
  }
}
