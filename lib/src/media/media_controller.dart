import 'dart:io';

import 'package:atoma_cash/src/permissions/permission_controller.dart';
import 'package:atoma_cash/utils/platform/platform_helper.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

typedef OnFileSelect = void Function(File selectedFile);

class MediaController {
  final OnFileSelect onGalleryFileSelect;
  final OnFileSelect onCameraFileSelect;

  PermissionController _cameraPermissionController;
  PermissionController _galleryPermissionController;

  MediaController(BuildContext context,
      {@required this.onGalleryFileSelect, @required this.onCameraFileSelect}) {
    _cameraPermissionController =
        PermissionController.camera(context, _onCameraPermissionGranted);
    _galleryPermissionController =
        PermissionController.gallery(context, _onGalleryPermissionGranted);
  }

  void _onCameraPermissionGranted() {
    _scanReceiptUsingCamera();
  }

  void _onGalleryPermissionGranted() {
    _loadReceiptUsingGallery();
  }

  void _scanReceiptUsingCamera() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);
    if (image == null) return;
    onCameraFileSelect(image);
  }

  void _loadReceiptUsingGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (image == null) return;
    onGalleryFileSelect(image);
  }

  void getPictureFromGallery(BuildContext context) =>
      _galleryPermissionController.requestPermissions(context);

  void getPictureFromCamera(BuildContext context) {
    if (PlatformHelper.isIOS(context)) {
      _cameraPermissionController.requestPermissions(context);
    } else {
      _scanReceiptUsingCamera();
    }
  }
}
