import 'package:atoma_cash/src/preview/data_provider/slide_data_provider.dart';
import 'package:atoma_cash/src/preview/preview_screen.dart';
import 'package:atoma_cash/src/registration/authentication/bloc/bloc.dart';
import 'package:atoma_cash/src/registration/flow_controllers/authentication_flow_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'registration/passcode_check/passcode_check_screen.dart';
import 'registration/phone_validation/phone_validation_screen.dart';
import 'splash_screen/splash_screen.dart';

class Initial extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: BlocProvider.of<AuthenticationBloc>(context),
        builder: _homeBuilder);
  }

  Widget _homeBuilder(BuildContext context, AuthenticationState state) {
    if (state is Uninitialized) {
      return SplashScreen();
    } else if (state is Authenticated) {
      return PasscodeCheckScreen(
        flowController: _flowController(AuthenticationFlow.REGULAR),
      );
    } else if (state is Unauthenticated) {
      if (state.isPreviewPassed) {
        return PhoneValidationScreen(
          flowController: _flowController(AuthenticationFlow.REGULAR),
        );
      } else {
        return PreviewScreen(SlideDataProvider.previewSlides(context));
      }
    }
  }

  IAuthenticationBaseFlowController _flowController(AuthenticationFlow flow) =>
      AuthenticationFlowController(flow);
}
