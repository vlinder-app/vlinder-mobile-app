import 'package:atoma_cash/resources/config/font_book.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:atoma_cash/src/preview/data_provider/slide_data_model.dart';
import 'package:flutter/material.dart';

class PreviewSlideWidget extends StatelessWidget {
  static const double _imageSize = 236;

  final SlideDataModel slideData;

  PreviewSlideWidget({this.slideData});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Expanded(
          flex: 6,
          child: Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset(
                slideData.asset,
                width: _imageSize,
                height: _imageSize,
              )),
        ),
        Expanded(
          flex: 4,
          child: Column(children: <Widget>[
            Padding(
              padding:
              EdgeInsets.only(left: 24, top: 22, right: 24, bottom: 14),
              child: Text(
                slideData.title,
                textAlign: TextAlign.center,
                style: FontBook.headerH3.copyWith(color: Palette.darkGrey),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 24, right: 24),
              child: Text(
                slideData.hint,
                textAlign: TextAlign.center,
                style: FontBook.textBody1.copyWith(color: Palette.mediumGrey),
              ),
            )
          ]),
        ),
      ],
    );
  }
}
