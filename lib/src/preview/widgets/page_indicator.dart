import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class PageIndicator extends AnimatedWidget {
  final int numberOfPages;
  final Color normalColor;
  final Color selectedColor;
  final PageController pageController;

  final double dotSize;
  final double dotSpacing;

  final double expansionFactor;

  double get _currentPage {
    try {
      return pageController.page ?? pageController.initialPage.toDouble();
    } catch (Exception) {
      return pageController.initialPage.toDouble();
    }
  }

  PageIndicator(this.pageController,
      {Key key,
      @required this.numberOfPages,
      this.dotSize = 4,
      this.dotSpacing = 6,
      this.expansionFactor = 4,
      this.normalColor = Palette.paleGrey,
      this.selectedColor = Palette.blue})
      : super(listenable: pageController, key: key);

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      size: calculateSize(),
      painter: _dotsPainter(),
    );
  }

  CustomPainter _dotsPainter() {
    return _DotsPainter(
      count: numberOfPages,
      offset: _currentPage,
      dotSize: dotSize,
      spacing: dotSpacing,
      expansionFactor: expansionFactor,
      normalColor: normalColor,
      selectedColor: selectedColor,
      strokeWidth: 1.0,
    );
  }

  Size calculateSize() {
    return Size(
        ((dotSize + dotSpacing) * (numberOfPages - 1)) +
            (expansionFactor * dotSize),
        dotSize);
  }
}

class _DotsPainter extends CustomPainter {
  final double offset;

  final int count;

  final Paint dotPaint;

  final Color normalColor;

  final Color selectedColor;

  final double dotSize;

  final double spacing;

  final double expansionFactor;

  _DotsPainter({
    @required this.count,
    @required this.offset,
    @required this.dotSize,
    @required this.spacing,
    @required this.expansionFactor,
    @required this.normalColor,
    @required this.selectedColor,
    double strokeWidth,
  }) : dotPaint = Paint()
          ..color = normalColor
          ..style = PaintingStyle.fill
          ..strokeWidth = strokeWidth;

  double get distance => dotSize + spacing;

  @override
  void paint(Canvas canvas, Size size) {
    final int current = offset.floor();
    double drawingOffset = -spacing;
    final dotOffset = offset - current;

    for (int i = 0; i < count; i++) {
      Color color = normalColor;
      final activeDotWidth = dotSize * expansionFactor;
      final expansion = (dotOffset / 2 * ((activeDotWidth - dotSize) / .5));
      final xPos = drawingOffset + spacing;
      double width = dotSize;
      if (i == current) {
        color = Color.lerp(selectedColor, normalColor, dotOffset);
        width = activeDotWidth - expansion;
      } else if (i - 1 == current) {
        width = dotSize + expansion;
        color = Color.lerp(selectedColor, normalColor, 1.0 - dotOffset);
      }
      final yPos = size.height / 2;
      final rRect = RRect.fromLTRBR(
        xPos,
        yPos - dotSize / 2,
        xPos + width,
        yPos + dotSize / 2,
        Radius.circular(dotSize / 2),
      );
      drawingOffset = rRect.right;
      canvas.drawRRect(rRect, dotPaint..color = color);
    }
  }

  @override
  bool shouldRepaint(_DotsPainter oldDelegate) {
    return oldDelegate.offset != offset;
  }
}
