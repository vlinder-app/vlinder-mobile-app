import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/resources/repository/local/shared_storage_manager.dart';
import 'package:atoma_cash/src/analytics/analytics.dart';
import 'package:atoma_cash/src/analytics/models/events/click_get_started_event.dart';
import 'package:atoma_cash/src/analytics/models/events/preview_slide_changed_event.dart';
import 'package:atoma_cash/src/preview/data_provider/slide_data_provider.dart';
import 'package:atoma_cash/src/preview/widgets/page_indicator.dart';
import 'package:atoma_cash/src/preview/widgets/preview_slide_widget.dart';
import 'package:atoma_cash/src/registration/flow_controllers/authentication_flow_controller.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:atoma_cash/utils/ui/adaptive.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

class PreviewScreen extends StatefulWidget {
  final ISlideDataProvider slidesDataProvider;

  PreviewScreen(this.slidesDataProvider, {Key key}) : super(key: key);

  @override
  _PreviewScreenState createState() => _PreviewScreenState();
}

class _PreviewScreenState extends State<PreviewScreen>
    with TickerProviderStateMixin {
  PageController _slidesPageController = PageController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _slidesPageController.dispose();
    super.dispose();
  }

  void _onSlideChanged(int index) {
    Analytics().logEvent(PreviewSlideChanged());
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
              flex: _pageViewFlex(context),
              child: _pageView(context, widget.slidesDataProvider),
            ),
            Expanded(
              flex: _bottomContentFlex(context),
              child: Column(
                children: <Widget>[
                  PageIndicator(
                    _slidesPageController,
                    numberOfPages: widget.slidesDataProvider.itemsCount,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 24, top: 40, right: 24),
                    child: SizedBox(
                      width: double.maxFinite,
                      child: RaisedButton(
                          child:
                              Text(Localization.of(context).getStartedButton),
                          onPressed: () {
                            _onTapGetStarted(
                                widget.slidesDataProvider.itemsCount);
                          }),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _pageView(BuildContext context, SlideDataProvider dataProvider) {
    return Padding(
      padding: EdgeInsets.only(top: _pageViewPadding(context)),
      child: PageView.builder(
        controller: _slidesPageController,
        onPageChanged: _onSlideChanged,
        itemCount: 3,
        itemBuilder: (context, slideIndex) {
          final slideModel = dataProvider.slideAtIndex(slideIndex);
          return PreviewSlideWidget(slideData: slideModel);
        },
      ),
    );
  }

  void _onTapGetStarted(int numberOfPages) {
    Analytics().logEvent(ClickGetStartedEvent(_currentPage(numberOfPages) + 1));

    _saveIsPreviewPassed();

    ExtendedNavigator.ofRouter<Router>().pushNamedAndRemoveUntil(
        Routes.phoneValidationScreen, (Route<dynamic> route) => false,
        arguments: PhoneValidationScreenArguments(
            flowController:
                AuthenticationFlowController(AuthenticationFlow.REGULAR)));
  }

  void _saveIsPreviewPassed() async {
    await SharedStorageManager().saveIsPreviewPassed(true);
  }

  int _pageViewFlex(BuildContext context) {
    if (Adaptive.isFourInchScreen(context)) {
      return 80;
    } else {
      return 75;
    }
  }

  int _bottomContentFlex(BuildContext context) {
    if (Adaptive.isFourInchScreen(context)) {
      return 20;
    } else {
      return 25;
    }
  }

  double _pageViewPadding(BuildContext context) {
    if (Adaptive.isFourInchScreen(context)) {
      return double.minPositive;
    } else {
      return 44;
    }
  }

  int _currentPage(int numberOfPages) {
    return _slidesPageController.page != null
        ? _slidesPageController.page.round() % numberOfPages
        : 0;
  }
}
