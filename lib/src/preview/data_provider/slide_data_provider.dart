import 'package:atoma_cash/resources/config/assets.dart';
import 'package:atoma_cash/resources/localization/i18n.dart';
import 'package:atoma_cash/src/preview/data_provider/slide_data_model.dart';
import 'package:flutter/cupertino.dart';

abstract class ISlideDataProvider {
  int get itemsCount;

  SlideDataModel slideAtIndex(int index);
}

class SlideDataProvider extends ISlideDataProvider {
  @override
  int get itemsCount => _data.length;
  final List<SlideDataModel> _data;

  SlideDataProvider(List<SlideDataModel> data) : _data = data;

  factory SlideDataProvider.previewSlides(BuildContext context) {
    return SlideDataProvider([
      SlideDataModel(
        title: Localization.of(context).previewSlide1Title,
        hint: Localization.of(context).previewSlide1Hint,
        asset: Assets.previewSlide1,
      ),
      SlideDataModel(
        title: Localization.of(context).previewSlide2Title,
        hint: Localization.of(context).previewSlide2Hint,
        asset: Assets.previewSlide2,
      ),
      SlideDataModel(
        title: Localization.of(context).previewSlide3Title,
        hint: Localization.of(context).previewSlide3Hint,
        asset: Assets.previewSlide3,
      ),
    ]);
  }

  @override
  SlideDataModel slideAtIndex(int index) => _data[index];
}
