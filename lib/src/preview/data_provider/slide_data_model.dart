import 'package:flutter/material.dart';

class SlideDataModel {
  final String title;
  final String hint;
  final String asset;

  const SlideDataModel({
    @required this.title,
    @required this.hint,
    @required this.asset,
  });
}
