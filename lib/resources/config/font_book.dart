import 'package:flutter/cupertino.dart';

class FontBook {
  static const headerH1 = const TextStyle(
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    fontSize: 40.0,
    height: 1.5,
  );

  static const headerH2 = const TextStyle(
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    fontSize: 24.0,
    height: 1.3,
  );

  static const headerH3 = const TextStyle(
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    fontSize: 20.0,
    height: 1.4,
    letterSpacing: 0.15,
  );

  static const headerH4 = const TextStyle(
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    fontSize: 16.0,
    height: 1.5,
    letterSpacing: 0.15,
  );

  static const textButton = const TextStyle(
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    fontSize: 15.0,
    letterSpacing: 0.4,
  );

  static const textBody1 = const TextStyle(
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    fontSize: 15.0,
    height: 1.3,
    letterSpacing: 0.4,
  );

  static const textBody2Menu = const TextStyle(
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
    fontSize: 13.0,
    height: 1.5,
    letterSpacing: 0.25,
  );

  static const textCaption = const TextStyle(
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
    fontSize: 12.0,
    height: 1.0,
    letterSpacing: 0.4,
  );

  static const headerOverline = const TextStyle(
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
    fontSize: 11.0,
    height: 1.45,
    letterSpacing: 2.0,
  );

  static const textCharts = const TextStyle(
    fontWeight: FontWeight.w600,
    fontStyle: FontStyle.normal,
    fontSize: 11.0,
    height: 1.09,
    letterSpacing: 0.25,
  );
}
