class RequestHeaderKey {
  static const authorization = 'Authorization';
  static const acceptLanguage = 'Accept-Language';
  static const userAgent = 'User-Agent';
}
