class StorageIds {
  static final String refreshTokenKey = "refresh_token";
  static final String accessTokenKey = "access_token";
  static final String passcode = "passcode";
  static final String email = "email";
  static final String analyticsUserId = "analytcs_user_id";
  static final String environmentKey = "environment";

  static final String smsRequestTimestampKey = "sms_request_timestamp";
  static final String phoneNumberKey = "phone_number";
  static final String smsConfirmationFailedAttemptsCount =
      "sms_failed_attempts_count";
  static final String verifyIdentityRequestTimestampKey =
      "verify_identity_timestamp";
  static final String taxInfoRequestTimestampKey = "tax_info_timestamp";
  static final String postIdentVerificationKey = "post_ident_verification";
  static final String isPreviewPassedKey = "is_preview_passed";
}
