import 'package:atoma_cash/resources/config/palette.dart';
import 'package:flutter/material.dart';

class ThemeProvider {
  static ThemeData configureThemeData() {
    return ThemeData(
        fontFamily: "Manrope",
        primaryTextTheme: TextTheme(title: TextStyle(color: Palette.darkGrey)),
        brightness: Brightness.light,
        accentColor: Palette.blue,
        primaryColor: Palette.blue,
        backgroundColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
        buttonColor: Palette.red,
        appBarTheme: AppBarTheme(
            brightness: Brightness.light,
            color: Colors.white,
            elevation: 0.0,
            iconTheme: IconThemeData(color: Palette.darkGrey),
            actionsIconTheme: IconThemeData(color: Palette.darkGrey),
            textTheme: TextTheme(
                title: TextStyle(
                    letterSpacing: 1.0,
                    fontWeight: FontWeight.w500,
                    fontSize: 14.0,
                    color: Palette.darkGrey))),
        buttonTheme: ButtonThemeData(
          height: 48.0,
          buttonColor: Palette.blue,
          disabledColor: Palette.paleGrey,
          textTheme: ButtonTextTheme.primary,
        ));
  }
}
