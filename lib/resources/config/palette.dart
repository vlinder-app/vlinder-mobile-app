import 'dart:ui';

class Palette {
  static const blue = const Color(0xff3f46d2);
  static const mediumGrey = const Color(0xffa1a8bc);
  static const darkGrey = const Color(0xff323232);
  static const paleGrey = const Color(0xfff1f3f7);
  static const mediumBlue = const Color(0xff2a31bc);
  static const red = const Color(0xffee215e);
  static const green = const Color(0xff4dc79b);
  static const darkBlue = const Color(0xff1d217a);
  static const white = const Color(0xffffffff);
  static const blueForDarkBg = const Color(0xff9096ff);
  static const chartGradientColor = const Color(0xff4d55ff);
}
