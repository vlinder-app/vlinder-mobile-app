class ApplicationConfig {
  static final bool isDebug = bool.fromEnvironment("dart.vm.product") == false;
  static final String iosProxy = 'localhost:8888';
}
