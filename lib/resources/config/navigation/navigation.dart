import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';
import 'package:atoma_cash/src/push_notifications/push_notification_service.dart';
import 'package:atoma_cash/src/registration/flow_controllers/authentication_flow_controller.dart';
import 'package:atoma_cash/src/routing/router.gr.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';

class Navigation {
  static void moveToHome(BuildContext context) {
    ExtendedNavigator.ofRouter<Router>()
        .pushNamedAndRemoveUntil(Routes.home, (Route<dynamic> route) => false);
  }

  static void signOut() async {
    PushNotificationService().unsubscribe();
    await ApplicationLocalRepository().clearAll();
    ExtendedNavigator.ofRouter<Router>().pushNamedAndRemoveUntil(
        Routes.phoneValidationScreen, (Route<dynamic> route) => false,
        arguments: PhoneValidationScreenArguments(
            flowController:
                AuthenticationFlowController(AuthenticationFlow.REGULAR)));
  }

  static void lockUiWithForceUpdateRequest() {
    ExtendedNavigator.ofRouter<Router>().pushNamed(Routes.forceUpdateScreen);
  }

  static void lockUiWithMaintenanceMode() {
    ExtendedNavigator.ofRouter<Router>().pushNamedAndRemoveUntil(
        Routes.maintenanceScreen, (Route<dynamic> route) => false);
  }
}
