import 'dart:convert';

import 'package:atoma_cash/models/environments/environment.dart';
import 'package:atoma_cash/resources/config/storage_ids.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedStorageManager {
  static final SharedStorageManager _instance =
      SharedStorageManager._internal();

  SharedStorageManager._internal();

  List<String> get _persistentKeys => [StorageIds.isPreviewPassedKey];

  factory SharedStorageManager() {
    return _instance;
  }

  Future<void> saveEnvironment(Environment environment) async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString(
          StorageIds.environmentKey, json.encode(environment.toJson()));
    } catch (exc) {
      print(exc.toString());
    }
  }

  Future<Environment> getEnvironment() async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      String environmentJsonString =
          sharedPreferences.getString(StorageIds.environmentKey);
      if (environmentJsonString != null) {
        return Environment.fromJson(json.decode(environmentJsonString));
      }
      return null;
    } catch (exc) {
      print(exc.toString());
      return null;
    }
  }

  Future<void> saveSmsRequestTimestamp(int requestTimestamp) async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setInt(
          StorageIds.smsRequestTimestampKey, requestTimestamp);
    } catch (exc) {
      print(exc.toString());
    }
  }

  Future<int> getSmsRequestTimestamp() async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      int smsRequestTimeStamp =
          sharedPreferences.getInt(StorageIds.smsRequestTimestampKey);
      return smsRequestTimeStamp ?? 0;
    } catch (exc) {
      print(exc.toString());
      return 0;
    }
  }

  Future<void> removeSmsRequestTimestamp() async =>
      _removeKey(StorageIds.smsRequestTimestampKey);

  Future<void> saveFailedPhoneConfirmationAttempts(int failedAttempts) async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setInt(
          StorageIds.smsConfirmationFailedAttemptsCount, failedAttempts);
    } catch (exc) {
      print(exc.toString());
    }
  }

  Future<int> getFailedPhoneConfirmationAttempts() async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      int failedConfirmationCount = sharedPreferences
          .getInt(StorageIds.smsConfirmationFailedAttemptsCount);
      return failedConfirmationCount ?? 0;
    } catch (exc) {
      print(exc.toString());
      return 0;
    }
  }

  Future<void> removeFailedPhoneConfirmationAttempts() async =>
      _removeKey(StorageIds.smsRequestTimestampKey);

  Future<void> savePhoneNumber(String phoneNumber) async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setString(StorageIds.phoneNumberKey, phoneNumber);
    } catch (exc) {
      print(exc.toString());
    }
  }

  Future<String> getPhoneNumber() async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      String phoneNumber =
          sharedPreferences.getString(StorageIds.phoneNumberKey);
      return phoneNumber;
    } catch (exc) {
      print(exc.toString());
      return null;
    }
  }

  Future<void> removePhoneNumber() async =>
      _removeKey(StorageIds.phoneNumberKey);

  Future<void> clearAll() async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.clear();
    } catch (exc) {
      print(exc.toString());
    }
  }

  Future<void> clear() async {
    try {
      SharedPreferences preferences = await SharedPreferences.getInstance();
      for (String key in preferences.getKeys()) {
        if (!_persistentKeys.contains(key)) {
          preferences.remove(key);
        }
      }
    } catch (exc) {
      print(exc.toString());
    }
  }

  Future<void> _removeKey(String key) async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.remove(key);
    } catch (exc) {
      print(exc.toString());
    }
  }

  Future<void> saveVerifyIdentityRequestTimestamp(int requestTimestamp) async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setInt(
          StorageIds.verifyIdentityRequestTimestampKey, requestTimestamp);
    } catch (exc) {
      print(exc.toString());
    }
  }

  Future<int> getVerifyIdentityRequestTimestamp() async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      int smsRequestTimeStamp = sharedPreferences
          .getInt(StorageIds.verifyIdentityRequestTimestampKey);
      return smsRequestTimeStamp ?? 0;
    } catch (exc) {
      print(exc.toString());
      return 0;
    }
  }

  Future<void> saveTaxInfoRequestTimestamp(int requestTimestamp) async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setInt(
          StorageIds.taxInfoRequestTimestampKey, requestTimestamp);
    } catch (exc) {
      print(exc.toString());
    }
  }

  Future<int> getTaxInfoRequestTimestamp() async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      int smsRequestTimeStamp =
          sharedPreferences.getInt(StorageIds.taxInfoRequestTimestampKey);
      return smsRequestTimeStamp ?? 0;
    } catch (exc) {
      print(exc.toString());
      return 0;
    }
  }

  Future<void> saveIsPreviewPassed(bool isPreviewPassed) async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      sharedPreferences.setBool(StorageIds.isPreviewPassedKey, isPreviewPassed);
    } catch (exc) {
      print(exc.toString());
    }
  }

  Future<bool> getIsPreviewPassed() async {
    try {
      var sharedPreferences = await SharedPreferences.getInstance();
      bool isPreviewPassed =
          sharedPreferences.getBool(StorageIds.isPreviewPassedKey);
      return isPreviewPassed ?? false;
    } catch (exc) {
      print(exc.toString());
      return false;
    }
  }
}
