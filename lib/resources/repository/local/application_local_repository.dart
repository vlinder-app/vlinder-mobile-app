import 'package:atoma_cash/models/environments/environment.dart';
import 'package:atoma_cash/resources/repository/local/security_storage_manager.dart';
import 'package:atoma_cash/resources/repository/local/shared_storage_manager.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/resources/repository/remote/auth_remote_repository.dart';

class ApplicationLocalRepository {
  final SecurityStorageManager _securityStorageManager =
      SecurityStorageManager();
  final SharedStorageManager _sharedStorageManager = SharedStorageManager();

  Future<bool> isSignedIn() async {
    String accessToken = await _securityStorageManager.getAccessToken();
    String refreshToken = await _securityStorageManager.getRefreshToken();
    return (accessToken != null && refreshToken != null);
  }

  Future<void> saveEnvironment(Environment environment) async {
    _sharedStorageManager.saveEnvironment(environment);
  }

  Future<Environment> getEnvironment() async {
    return _sharedStorageManager.getEnvironment();
  }

  Future<void> saveVerifyIdentityRequestTimestamp(int requestTimestamp) async {
    _sharedStorageManager.saveVerifyIdentityRequestTimestamp(requestTimestamp);
  }

  Future<int> getVerifyIdentityRequestTimestamp() async {
    return _sharedStorageManager.getVerifyIdentityRequestTimestamp();
  }

  Future<void> saveSmsRequestTimestamp(int requestTimestamp) async {
    _sharedStorageManager.saveSmsRequestTimestamp(requestTimestamp);
  }

  Future<void> saveTaxInfoRequestTimestamp(int requestTimestamp) async {
    _sharedStorageManager.saveTaxInfoRequestTimestamp(requestTimestamp);
  }

  Future<int> getTaxInfoRequestTimestamp() async {
    return _sharedStorageManager.getTaxInfoRequestTimestamp();
  }

  Future<int> getSmsRequestTimestamp() async {
    return _sharedStorageManager.getSmsRequestTimestamp();
  }

  Future<void> savePhoneNumber(String phoneNumber) async {
    _sharedStorageManager.savePhoneNumber(phoneNumber);
  }

  Future<String> getPhoneNumber() async {
    return _sharedStorageManager.getPhoneNumber();
  }

  Future<String> getPasscode() async {
    return _securityStorageManager.getPasscode();
  }

  Future<void> savePasscode({String passcode}) async {
    _securityStorageManager.savePasscode(passcode: passcode);
  }

  Future<String> getAccessToken() async {
    return _securityStorageManager.getAccessToken();
  }

  Future<String> getUserId() async {
    return _securityStorageManager.getUserId();
  }

  Future<void> saveUserId({String userId}) async {
    _securityStorageManager.saveUserId(userId: userId);
  }

  Future<String> getEmail() async {
    return _securityStorageManager.getEmail();
  }

  Future<void> saveEmail({String email}) async {
    _securityStorageManager.saveEmail(email: email);
  }

  Future<void> clearAll() async {
    ApiRemoteRepository().resetRepository();
    AuthRemoteRepository().resetRepository();
    await _securityStorageManager.deleteAll();
    await _sharedStorageManager.clear();
  }
}
