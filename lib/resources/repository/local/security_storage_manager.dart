import 'package:atoma_cash/resources/config/storage_ids.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecurityStorageManager {
  final _storage = new FlutterSecureStorage();
  static final SecurityStorageManager _instance =
      SecurityStorageManager._internal();

  SecurityStorageManager._internal();

  factory SecurityStorageManager() {
    return _instance;
  }

  Future<String> getUserId() async {
    return await _storage.read(key: StorageIds.analyticsUserId);
  }

  Future<void> saveUserId({String userId}) async {
    await _storage.write(key: StorageIds.analyticsUserId, value: userId);
  }

  Future<String> getRefreshToken() async {
    return await _storage.read(key: StorageIds.refreshTokenKey);
  }

  Future<void> saveRefreshToken({String refreshToken}) async {
    await _storage.write(key: StorageIds.refreshTokenKey, value: refreshToken);
  }

  Future<String> getAccessToken() async {
    return await _storage.read(key: StorageIds.accessTokenKey);
  }

  Future<void> saveAccessToken({String accessToken}) async {
    await _storage.write(key: StorageIds.accessTokenKey, value: accessToken);
  }

  Future<String> getPasscode() async {
    return await _storage.read(key: StorageIds.passcode);
  }

  Future<void> savePasscode({String passcode}) async {
    await _storage.write(key: StorageIds.passcode, value: passcode);
  }

  Future<String> getEmail() async {
    return await _storage.read(key: StorageIds.email);
  }

  Future<void> saveEmail({String email}) async {
    await _storage.write(key: StorageIds.email, value: email);
  }

  Future<void> deleteAll() async {
    await _storage.deleteAll();
  }
}
