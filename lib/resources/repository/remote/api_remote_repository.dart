import 'dart:typed_data';

import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/models/api/api/accounts/accounts_types.dart';
import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/balances/balances.dart';
import 'package:atoma_cash/models/api/api/balances/currencies.dart';
import 'package:atoma_cash/models/api/api/balances/sync_status.dart';
import 'package:atoma_cash/models/api/api/banking/request/connection_sync_model.dart';
import 'package:atoma_cash/models/api/api/banking/response/connections.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/banners/banners.dart';
import 'package:atoma_cash/models/api/api/categories/categories.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint.dart';
import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint_info.dart';
import 'package:atoma_cash/models/api/api/categories/goals.dart';
import 'package:atoma_cash/models/api/api/categories/period.dart';
import 'package:atoma_cash/models/api/api/categories/savings.dart';
import 'package:atoma_cash/models/api/api/categories/spending.dart';
import 'package:atoma_cash/models/api/api/categories/spending_by_category.dart';
import 'package:atoma_cash/models/api/api/categories/transactions_type.dart';
import 'package:atoma_cash/models/api/api/fcm_registration/request/fcm_token_model.dart';
import 'package:atoma_cash/models/api/api/geolocation/countries.dart';
import 'package:atoma_cash/models/api/api/geolocation/geolocation.dart';
import 'package:atoma_cash/models/api/api/profile/request/profile_model.dart';
import 'package:atoma_cash/models/api/api/profile/response/profile.dart';
import 'package:atoma_cash/models/api/api/reports/offset_history.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/models/api/api/static_data/links.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/api/transactions/transactions.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/resources/repository/remote/api_provider.dart';
import 'package:dio/dio.dart';

class ApiRemoteRepository {
  ApiProvider _vlinderApiProvider;

  static final ApiRemoteRepository _instance = ApiRemoteRepository._internal();

  ApiRemoteRepository._internal();

  factory ApiRemoteRepository({ApiProvider apiProvider}) {
    _instance._vlinderApiProvider =
        apiProvider ?? _instance._vlinderApiProvider;
    return _instance;
  }

  void updateLocalization(String languageCode) {
    _vlinderApiProvider.updateLocalization(languageCode);
  }

  Future<void> updateAuthorization() async {
    return _vlinderApiProvider.updateAuthorizationHeader();
  }

  Future<BaseResponse<Profile>> getProfile() async {
    return _vlinderApiProvider.getProfile();
  }

  Future<BaseResponse<List<Country>>> getAllCountries() async {
    return _vlinderApiProvider.getAllCountries();
  }

  Future<BaseResponse<List<Country>>> getAvailableCountries() async {
    return _vlinderApiProvider.getAvailableCountries();
  }

  Future<BaseResponse<Links>> getLinks() async {
    return _vlinderApiProvider.getLinks();
  }

  Future<BaseResponse<Geolocation>> getGeolocation() async {
    return _vlinderApiProvider.getGeolocation();
  }

  Future<BaseResponse<Null>> updateProfile(ProfileModel profileModel) async {
    return _vlinderApiProvider.updateProfile(profileModel);
  }

  Future<BaseResponse<Null>> confirmEmail() async {
    return _vlinderApiProvider.confirmEmail();
  }

  Future<BaseResponse<Null>> registerFcmToken(FcmTokenModel tokenModel) async {
    return _vlinderApiProvider.registerFcmToken(tokenModel);
  }

  Future<BaseResponse<Null>> unregisterFcmToken(
      FcmTokenModel tokenModel) async {
    return _vlinderApiProvider.unregisterFcmToken(tokenModel);
  }

  Future<BaseResponse<UriModel>> authorizeBankAccount() async {
    return _vlinderApiProvider.authorizeBankAccounts();
  }

  Future<BaseResponse<Connections>> getBankConnections() async {
    return _vlinderApiProvider.getBankConnections();
  }

  Future<BaseResponse<UriModel>> syncConnection(
      ConnectionSyncModel syncModel) async {
    return _vlinderApiProvider.syncConnection(syncModel);
  }

  Future<BaseResponse<Banners>> getBanners() async {
    return _vlinderApiProvider.getBanners();
  }

  Future<BaseResponse<Null>> markBannerAsRead(String bannerId) async {
    return _vlinderApiProvider.markBannerAsRead(bannerId);
  }

  Future<BaseResponse<Accounts>> getBankAccounts() async {
    return _vlinderApiProvider.getAccounts();
  }

  Future<BaseResponse<AccountsTypes>> getAccountsTypes() async {
    return _vlinderApiProvider.getAccountsTypes();
  }

  Future<BaseResponse<Balances>> getBalances() async {
    return _vlinderApiProvider.getBalances();
  }

  Future<BaseResponse<Balances>> moveBalance(int fromIndex, int toIndex) async {
    return _vlinderApiProvider.moveBalance(fromIndex, toIndex);
  }

  Future<BaseResponse<SyncStatus>> syncBalances() async {
    return _vlinderApiProvider.syncBalances();
  }

  Future<BaseResponse<UriModel>> syncBankAccounts() async {
    return _vlinderApiProvider.syncBankAccounts();
  }

  Future<BaseResponse<Spending>> getSpending(Period period) async {
    return _vlinderApiProvider.getSpending(period);
  }

  Future<BaseResponse<Goals>> getPersonalGoals() async {
    return _vlinderApiProvider.getPersonalGoals();
  }

  Future<BaseResponse<Goals>> getCommunityGoals() async {
    return _vlinderApiProvider.getCommunityGoals();
  }

  Future<BaseResponse<EcoFootprint>> getEcoFootprint(String locationId) async {
    return _vlinderApiProvider.getEcoFootprint(locationId);
  }

  Future<BaseResponse<Amount>> getEcoFootprintForCategory(
      String categoryId, Amount amount) async {
    return _vlinderApiProvider.getEcoFootprintForCategory(categoryId, amount);
  }

  Future<BaseResponse<EcoFootprintInfo>> getEcoFootprintInfo(
      {String countryCode}) {
    return _vlinderApiProvider.getEcoFootprintInfo(countryCode: countryCode);
  }

  Future<BaseResponse<Savings>> getSavings(Period period) async {
    return _vlinderApiProvider.getSavings(period);
  }

  Future<BaseResponse<SpendingByCategory>> getSpendingByCategory(
      String categoryId,
      {Period period}) async {
    return _vlinderApiProvider.getSpendingByCategory(categoryId,
        period: period);
  }

  Future<BaseResponse<Reports>> getReports(Period period,
      {List<String> categoryIds}) async {
    return _vlinderApiProvider.getReports(period, categoryIds: categoryIds);
  }

  Future<BaseResponse<OffsetHistory>> getOffsetHistory() async {
    return _vlinderApiProvider.getOffsetHistory();
  }

  Future<BaseResponse<Category>> getCategory(String categoryId) async {
    return _vlinderApiProvider.getCategory(categoryId);
  }

  Future<BaseResponse<Categories>> getCategories() async {
    return _vlinderApiProvider.getCategories();
  }

  Future<BaseResponse<Null>> removeTransactionWithId(
      String transactionId) async {
    return _vlinderApiProvider.removeTransactionWithId(transactionId);
  }

  Future<BaseResponse<Transaction>> addTransaction(
      Transaction transaction) async {
    return _vlinderApiProvider.addTransaction(transaction);
  }

  Future<BaseResponse<Transaction>> patchTransaction(
      String transactionId, String patchBody) async {
    return _vlinderApiProvider.patchTransaction(transactionId, patchBody);
  }

  Future<BaseResponse<Amount>> getBaseCurrency() async {
    return _vlinderApiProvider.getBaseCurrency();
  }

  Future<BaseResponse<Currencies>> getCurrencies() async {
    return _vlinderApiProvider.getCurrencies();
  }

  Future<BaseResponse<Object>> setBaseCurrency(Amount currency) async {
    return _vlinderApiProvider.setBaseCurrency(currency);
  }

  Future<BaseResponse<Object>> checkAuthState() async {
    return _vlinderApiProvider.checkAuthState();
  }

  Future<BaseResponse<Uint8List>> getReceiptImage(String guid) async {
    return _vlinderApiProvider.getReceiptImage(guid);
  }

  String getPendingReceiptUriForGuid(String guid) =>
      _vlinderApiProvider.getPendingReceiptUriForGuid(guid);

  Future<BaseResponse<Object>> addTransactionReceiptImage(
      Uint8List fileBytes, String transactionId, String imageId,
      {CancelToken cancelToken}) async {
    return _vlinderApiProvider.addTransactionReceiptImage(
        fileBytes, transactionId, imageId,
        cancelToken: cancelToken);
  }

  Future<BaseResponse<Null>> confirmEmailWithToken(
      String confirmationToken) async {
    return _vlinderApiProvider.confirmEmailWithToken(confirmationToken);
  }

  Future<BaseResponse<Object>> removeTransactionReceiptImage(
      String imageId) async {
    return _vlinderApiProvider.removeTransactionReceiptImage(imageId);
  }

  Future<BaseResponse<Transactions>> getTransactions(
      {Period period,
      List<String> categoryIds,
      List<String> accountIds,
      List<String> counterpartyNames,
      int take,
      int skip}) async {
    return _vlinderApiProvider.getTransactions(
        period: period,
        accountIds: accountIds,
        categoryIds: categoryIds,
        counterpartyNames: counterpartyNames,
        take: take,
        skip: skip);
  }

  Future<BaseResponse<Transaction>> getLastTransaction(
      {TransactionsType transactionsType}) async {
    return _vlinderApiProvider.getLastTransaction();
  }

  Future<BaseResponse<Transaction>> scanReceipt(Uint8List fileBytes,
      {String imageId, CancelToken cancelToken}) async {
    return _vlinderApiProvider.scanReceipt(fileBytes,
        imageId: imageId, cancelToken: cancelToken);
  }

  void resetRepository() {
    _vlinderApiProvider.resetProvider();
  }
}
