import 'package:atoma_cash/models/auth_api/auth/request/api_token_request.dart';
import 'package:atoma_cash/models/auth_api/auth/request/email_verification.dart';
import 'package:atoma_cash/models/auth_api/auth/request/passcode_verification.dart';
import 'package:atoma_cash/models/auth_api/auth/request/phone_verification.dart';
import 'package:atoma_cash/models/auth_api/auth/response/api_token.dart';
import 'package:atoma_cash/models/auth_api/auth/response/email_verification_response.dart';
import 'package:atoma_cash/models/auth_api/auth/response/passcode_confirmation.dart';
import 'package:atoma_cash/models/auth_api/auth/response/phone_confirmation.dart';
import 'package:atoma_cash/models/auth_api/base_models/base_response.dart';
import 'package:atoma_cash/resources/config/application_config.dart';
import 'package:atoma_cash/resources/config/request_header_keys.dart';
import 'package:atoma_cash/utils/logging_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';

class AuthApiProvider {
  final String apiBaseUrl;
  final Dio _dio = Dio();

  var _additionalHeaders = Map<String, dynamic>();

  AuthApiProvider(this.apiBaseUrl) {
    _initDio();
  }

  void _initDio() {
    _dio.options.connectTimeout = 20000;
    _dio.options.receiveTimeout = 20000;
    _dio.options.baseUrl = apiBaseUrl;
    _setupInterceptor();
  }

  void updateLocalization(String languageCode) {
    _additionalHeaders[RequestHeaderKey.acceptLanguage] = languageCode;
  }

  Future<BaseResponse<PhoneConfirmation>> requestSMS(
      {String phoneNumber, String countryIso2}) async {
    String path = "/authority/account";
    AccountVerification phoneConfirmation =
        AccountVerification.withPhone(phoneNumber: phoneNumber, countryIso2: countryIso2);
    try {
      Response response =
          await _dio.post(path, data: phoneConfirmation.toJson());
      return new BaseResponse(response, (_) => PhoneConfirmation.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<PhoneConfirmation>> verifyPhone(
      String confirmationCode, String verifyToken) async {
    String path = "/authority/account/phone";
    AccountVerification phoneConfirmation =
        AccountVerification.withConfirmationCode(
            confirmationCode: confirmationCode, verifyToken: verifyToken);
    try {
      Response response =
          await _dio.post(path, data: phoneConfirmation.toJson());
      if (response.statusCode == 200) {
        PhoneConfirmation phoneConfirmation =
        PhoneConfirmation.fromJson(response.data);
        if (phoneConfirmation.authority == 'passcode') {
          return BaseResponse.withResult(phoneConfirmation);
        } else {
          return BaseResponse.withError(Error(code: 204));
        }
      } else {
        return BaseResponse.withDioError(null);
      }
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<EmailVerificationResponse>> requestEmailVerificationCode(
      String verifyToken) async {
    String path = "/authority/account/emailcode";
    EmailVerification emailVerificationRequest =
    EmailVerification.verification(verifyToken);

    try {
      Response response =
      await _dio.post(path, data: emailVerificationRequest.toJson());
      return BaseResponse(
          response, (_) => EmailVerificationResponse.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<EmailVerificationResponse>>
  checkIsEmailVerificationPassed(String verifyToken) async {
    String path = "/authority/account/email";
    EmailVerification emailVerificationRequest =
    EmailVerification.verification(verifyToken);

    try {
      Response response =
      await _dio.post(path, data: emailVerificationRequest.toJson());
      return BaseResponse(
          response, (_) => EmailVerificationResponse.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<PasscodeConfirmation>> verifyPasscode(
      {String passcode, bool replaceExisting, String verifyToken}) async {
    String path = "/authority/account/passcode";
    PasscodeVerification passcodeVerification =
    PasscodeVerification.verification(
        verifyToken, passcode, replaceExisting);
    try {
      Response response =
      await _dio.post(path, data: passcodeVerification.toJson());
      if (response.statusCode == 200) {
        PasscodeConfirmation confirmation =
        PasscodeConfirmation.fromJson(response.data);
        if (confirmation.authToken != null) {
          return BaseResponse.withResult(confirmation);
        } else {
          return BaseResponse.withError(Error(code: 204));
        }
      } else {
        return BaseResponse.withDioError(null);
      }
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<ApiToken>> getToken({String authToken}) async {
    String path = "/connect/token";
    TokenRequest tokenRequest = TokenRequest.bearer(authToken);
    try {
      Response response = await _dio.post(path,
          data: tokenRequest.toJson(),
          options:
          new Options(contentType: "application/x-www-form-urlencoded"));
      return new BaseResponse(response, (_) => ApiToken.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<ApiToken>> refreshToken({String token}) async {
    String path = "/connect/token";
    TokenRequest tokenRequest = TokenRequest.refresh(token);
    try {
      Response response = await _dio.post(path,
          data: tokenRequest.toJson(),
          options:
          new Options(contentType: "application/x-www-form-urlencoded"));
      return new BaseResponse(response, (_) => ApiToken.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  void _setupInterceptor() {
    _dio.interceptors.add(InterceptorsWrapper(
      onRequest: (RequestOptions options) {
        options.headers.addAll(_additionalHeaders);
        return options;
      },
    ));

    if (ApplicationConfig.isDebug) {
      _setupDebugInterceptor();
    }
  }

  void _setupDebugInterceptor() {
    _dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      LoggingInterceptor.printRequest(options);
      return options;
    }, onResponse: (Response response) {
      LoggingInterceptor.printResponse(response);
      return response;
    }, onError: (DioError e) {
//      _showErrorToast(e);
      LoggingInterceptor.printError(e);
      return e;
    }));
  }

  void _showErrorToast(DioError error) {
    String errorText =
        "Code: ${error?.response?.statusCode}, Type: ${error?.type
        ?.toString()}, Error: ${error?.response?.data?.toString()}";
    Fluttertoast.showToast(msg: errorText, toastLength: Toast.LENGTH_LONG);
  }
}
