import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/models/environments/environments.dart';
import 'package:dio/dio.dart';

class EnvironmentsProvider {
  final String apiBaseUrl = "https://env.vlinder.app/";
  final Dio _dio = Dio();
  var _additionalHeaders = Map<String, dynamic>();

  EnvironmentsProvider() {
    _initDio();
  }

  void _initDio() {
    _fillAdditionalHeaders();
    _dio.options.baseUrl = apiBaseUrl;
    _dio.options.headers = _additionalHeaders;
  }

  void _fillAdditionalHeaders() {
    _additionalHeaders.putIfAbsent("X-API-KEY", () => "vlinder");
  }

  Future<BaseResponse<Environments>> getEnvironments() async {
    String path = "";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse(response,
          creator: (_) => Environments.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    } catch (error) {
      print(error.toString());
      return BaseResponse.withDioError(null);
    }
  }
}
