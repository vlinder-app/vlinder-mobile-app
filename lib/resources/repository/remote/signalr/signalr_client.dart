import 'package:atoma_cash/resources/repository/local/application_local_repository.dart';
import 'package:atoma_cash/resources/repository/remote/signalr/isignalr_client.dart';
import 'package:logging/logging.dart';
import 'package:signalr_client/hub_connection.dart';
import 'package:signalr_client/signalr_client.dart';

class SignalrClient implements ISignalrClient {
  static const String _host = "https://api.vlinder.app/hubs/ReceiptImages";

  HubConnection _hubConnection;
  Logger _logger;

  SignalrClient() {
    Logger.root.level = Level.ALL;
    Logger.root.onRecord.listen((LogRecord rec) {
      print('${rec.level.name}: ${rec.time}: ${rec.message}');
    });
    _logger = Logger("SignalR_Client");
  }

  Future<String> _token() async {
    var token = await ApplicationLocalRepository().getAccessToken();
    return Future.value(token.split(" ")[1]);
  }

  @override
  Future<HubConnection> getHubConnection() async {
    if (_hubConnection == null) {
      final httpOptions = new HttpConnectionOptions(
          logger: _logger, accessTokenFactory: _token);

      _hubConnection = HubConnectionBuilder()
          .withUrl(_host, options: httpOptions)
          .configureLogging(_logger)
          .build();
      _hubConnection.serverTimeoutInMilliseconds = 100000;
      _hubConnection.onclose((error) {
        _logger.info("Connection Closed");
        print("Connection closed $error");
      });
    }

    if (_hubConnection.state != HubConnectionState.Connected) {
      await _hubConnection.start();
      print("Connection state '${_hubConnection.state}'");
      _logger.info("Connection state '${_hubConnection.state}'");
    }

    return _hubConnection;
  }
}
