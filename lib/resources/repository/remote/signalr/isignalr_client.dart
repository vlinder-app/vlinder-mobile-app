import 'package:signalr_client/signalr_client.dart';

abstract class ISignalrClient {
  Future<HubConnection> getHubConnection();
}
