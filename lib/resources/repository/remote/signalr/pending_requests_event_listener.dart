import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/resources/repository/remote/signalr/isignalr_client.dart';
import 'package:atoma_cash/src/pending_receipts/models/peding_receipt.dart';

typedef OnReceiptChanged = void Function(PendingReceipt receipt);

class PendingRequestsEventListener {
  static const String _kMethod = "ReceiveEvent";
  static const Duration _eventLimit = Duration(milliseconds: 50);

  final ISignalrClient signalrClient;
  final OnReceiptChanged onReceiptChangedListener;
  final ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();
  DateTime _lastEventDateTime;

  PendingRequestsEventListener(
      this.signalrClient, this.onReceiptChangedListener);

  Future<void> listenRequestsEvents() async {
    await _apiRemoteRepository.checkAuthState();
    var hubConnection = await signalrClient.getHubConnection();
    hubConnection.on(_kMethod, _handleServerEvent);
  }

  void _handleServerEvent(List<Object> objects) {
    if (objects != null && objects.isNotEmpty) {
      try {
        if (_lastEventDateTime == null ||
            DateTime.now().difference(_lastEventDateTime) > _eventLimit) {
          PendingReceipt pendingReceipt =
              PendingReceipt.fromJson(objects.first);
          _lastEventDateTime = DateTime.now();
          onReceiptChangedListener(pendingReceipt);
        }
      } catch (e) {
        print(e);
      }
    }
  }

  void dispose() async {
    var hubConnection = await signalrClient.getHubConnection();
    hubConnection.stop();
    hubConnection.off(_kMethod);
  }
}
