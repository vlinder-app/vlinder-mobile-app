import 'dart:convert';
import 'dart:typed_data';

import 'package:atoma_cash/models/api/api/accounts/accounts.dart';
import 'package:atoma_cash/models/api/api/accounts/accounts_types.dart';
import 'package:atoma_cash/models/api/api/balances/balances.dart';
import 'package:atoma_cash/models/api/api/balances/currencies.dart';
import 'package:atoma_cash/models/api/api/balances/patch_model.dart';
import 'package:atoma_cash/models/api/api/balances/sync_status.dart';
import 'package:atoma_cash/models/api/api/banking/request/connection_sync_model.dart';
import 'package:atoma_cash/models/api/api/banking/response/connections.dart';
import 'package:atoma_cash/models/api/api/banking/response/uri_response.dart';
import 'package:atoma_cash/models/api/api/banners/banners.dart';
import 'package:atoma_cash/models/api/api/categories/categories.dart';
import 'package:atoma_cash/models/api/api/categories/category.dart';
import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint.dart';
import 'package:atoma_cash/models/api/api/categories/eco_footprint/eco_footprint_info.dart';
import 'package:atoma_cash/models/api/api/categories/goals.dart';
import 'package:atoma_cash/models/api/api/categories/period.dart';
import 'package:atoma_cash/models/api/api/categories/spending.dart';
import 'package:atoma_cash/models/api/api/categories/spendings.dart';
import 'package:atoma_cash/models/api/api/categories/transactions_type.dart';
import 'package:atoma_cash/models/api/api/fcm_registration/request/fcm_token_model.dart';
import 'package:atoma_cash/models/api/api/geolocation/countries.dart';
import 'package:atoma_cash/models/api/api/geolocation/geolocation.dart';
import 'package:atoma_cash/models/api/api/profile/request/profile_model.dart';
import 'package:atoma_cash/models/api/api/profile/response/profile.dart';
import 'package:atoma_cash/models/api/api/reports/offset_history.dart';
import 'package:atoma_cash/models/api/api/reports/reports.dart';
import 'package:atoma_cash/models/api/api/static_data/links.dart';
import 'package:atoma_cash/models/api/api/transactions/transaction.dart';
import 'package:atoma_cash/models/api/api/transactions/transactions.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/models/api/error_type/error_type.dart';
import 'package:atoma_cash/models/auth_api/auth/response/api_token.dart';
import 'package:atoma_cash/models/auth_api/base_models/base_response.dart'
    as auth;
import 'package:atoma_cash/resources/config/request_header_keys.dart';
import 'package:atoma_cash/resources/repository/local/security_storage_manager.dart';
import 'package:atoma_cash/resources/repository/remote/auth_remote_repository.dart';
import 'package:atoma_cash/utils/logging_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http_parser/http_parser.dart';
import 'package:package_info/package_info.dart';

abstract class EventsHandler {
  void onForceUpdateRequest();

  void onMaintenanceMode();
}

class ApiProvider {
  Dio _dio = Dio();

  final String apiBaseUrl;
  final EventsHandler eventsHandler;

  var _additionalHeaders = Map<String, dynamic>();

  ApiProvider(this.apiBaseUrl, {this.eventsHandler}) {
    _initDio();
  }

  void _initDio() {
    _dio.options.baseUrl = apiBaseUrl;
    _dio.options.connectTimeout = 50000;
    _dio.options.receiveTimeout = 50000;
    _setUserAgentHeader();
    _updateAuthorizationHeader();
    _setupInterceptor();
  }

  void resetProvider() {
    _dio.options.headers.clear();
  }

  void _setUserAgentHeader() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    _additionalHeaders[RequestHeaderKey.userAgent] =
        "Vlinder/${packageInfo.version}";
  }

  void updateLocalization(String languageCode) {
    _additionalHeaders[RequestHeaderKey.acceptLanguage] = languageCode;
  }

  Future<void> updateAuthorizationHeader() async {
    await _updateAuthorizationHeader();
  }

  Future<void> _updateAuthorizationHeader() async {
    String accessToken = await SecurityStorageManager().getAccessToken();
    _dio.options.headers[RequestHeaderKey.authorization] = accessToken;
  }

  Future<BaseResponse<Profile>> getProfile() async {
    String path = "/api/Profile";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse(response, creator: (_) => Profile.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    } catch (error) {
      print(error.toString());
      return BaseResponse.withDioError(null);
    }
  }

  Future<BaseResponse<Uint8List>> getReceiptImage(String guid) async {
    String path = "/api/Transactions/receiptImage/$guid";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (data) =>
              Uint8List.fromList(utf8.encode(data)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    } catch (error) {
      print(error.toString());
      return BaseResponse.withDioError(null);
    }
  }

  Future<BaseResponse<Transaction>> addTransaction(
      Transaction transaction) async {
    String path = "/api/Transactions";
    try {
      Response response = await _dio.post(path, data: transaction.toJson());
      return new BaseResponse(response,
          creator: (_) => Transaction.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Transaction>> getLastTransaction(
      {TransactionsType transactionsType}) async {
    String path = "/api/Transactions/last";
    try {
      Response response = await _dio
          .get(path, queryParameters: {"transactionsType": transactionsType});
      return new BaseResponse(response,
          creator: (_) => Transaction.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Transaction>> patchTransaction(String transactionId,
      String patchBody) async {
    String path = "/api/Transactions/$transactionId";
    try {
      Response response = await _dio.patch(path, data: patchBody);
      return new BaseResponse(response,
          creator: (_) => Transaction.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Null>> removeTransactionWithId(
      String transactionId) async {
    String path = "/api/Transactions/$transactionId";
    try {
      Response response = await _dio.delete(path);
      return new BaseResponse(response, nullableResult: true);
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Null>> confirmEmail() async {
    String path = "/api/EmailConfirmation";
    try {
      Response response = await _dio.post(path);
      return new BaseResponse(response, nullableResult: true);
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Null>> registerFcmToken(FcmTokenModel tokenModel) async {
    String path = "/api/FcmRegistration";
    try {
      Response response = await _dio.post(path, data: tokenModel);
      return new BaseResponse(response, nullableResult: true);
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Null>> unregisterFcmToken(
      FcmTokenModel tokenModel) async {
    String path = "/api/FcmRegistration";
    try {
      Response response = await _dio.delete(path, data: tokenModel);
      return new BaseResponse(response, nullableResult: true);
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Null>> confirmEmailWithToken(
      String confirmationToken) async {
    String path = "/api/EmailConfirmation/Confirm";
    try {
      Response response = await _dio.post(path,
          queryParameters: {"confirmationToken": confirmationToken});
      return new BaseResponse(response, nullableResult: true);
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Null>> updateProfile(ProfileModel profileModel) async {
    String path = "/api/Profile";
    try {
      Response response = await _dio.patch(path, data: profileModel.toJson());
      return new BaseResponse(response, nullableResult: true);
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<List<Country>>> getAllCountries() async {
    String path = "/api/StaticData/countries?includeRegions=true";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => countriesFromJsonList((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Links>> getLinks() async {
    String path = '/api/StaticData/links';
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Links.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<List<Country>>> getAvailableCountries() async {
    String path = "/api/Globalization/countries";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => countriesFromJsonList((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Geolocation>> getGeolocation() async {
    String path = "/api/Tools/geolocation";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse(response,
          creator: (_) => Geolocation.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<UriModel>> syncBankAccounts() async {
    String path = "/api/Banking/sync";
    try {
      Response response = await _dio.post(path);
      return new BaseResponse(response, creator: (_) => UriModel.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<UriModel>> authorizeBankAccounts() async {
    String path = "/api/Banking/auth";
    try {
      Response response = await _dio.post(path);
      return new BaseResponse(response, creator: (_) => UriModel.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Accounts>> getAccounts() async {
    String path = "/api/Accounts";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Accounts.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<AccountsTypes>> getAccountsTypes() async {
    String path = "/api/Accounts/types";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => AccountsTypes.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Connections>> getBankConnections() async {
    String path = "/api/Banking/connections";
    try {
      Response response = await _dio.get(path);
      return BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Connections.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<UriModel>> syncConnection(
      ConnectionSyncModel syncModel) async {
    String path = "/api/Banking/sync";
    try {
      Response response = await _dio.post(path, data: syncModel.toJson());
      return new BaseResponse(response, creator: (_) => UriModel.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Banners>> getBanners() async {
    String path = "/api/Banners";
    try {
      Response response = await _dio.get(path);
      return BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Banners.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Null>> markBannerAsRead(String bannerId) async {
    String path = "/api/Banners/$bannerId/read";
    try {
      Response response = await _dio.put(path);
      return BaseResponse(response, nullableResult: true);
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Balances>> getBalances() async {
    String path = "/api/Balances";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => balancesFromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Balances>> moveBalance(int fromIndex, int toIndex) async {
    String path = "/api/Balances";
    try {
      Response response = await _dio.patch(path,
          data:
          patchModelToJson([PatchModel.moveBalances(fromIndex, toIndex)]));
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => balancesFromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<SyncStatus>> syncBalances() async {
    String path = "/api/Balances/sync";
    try {
      Response response = await _dio.post(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => SyncStatus.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Transactions>> getTransactions({Period period,
    List<String> categoryIds,
    List<String> accountIds,
    List<String> counterpartyNames,
    int take,
    int skip}) async {
    String path = "/api/Transactions";
    try {
      Response response = await _dio.get(path, queryParameters: {
        "startDate": period?.startDate,
        "endDate": period?.endDate,
        "take": take,
        "skip": skip,
        "accountIds": accountIds,
        "categoryIds": categoryIds,
        "counterpartyNames": counterpartyNames
      });
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Transactions.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Savings>> getSavings(Period period) async {
    String path = "/api/Savings";
    try {
      Response response = await _dio.get(path, queryParameters: {
        "startDate": period.startDate,
        "endDate": period.endDate
      });
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Savings.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<SpendingByCategory>> getSpendingByCategory(
      String categoryId,
      {Period period}) async {
    String path = "/api/Spending/byCategory/$categoryId";
    try {
      Response response = await _dio.get(path, queryParameters: {
        "startDate": period?.startDate,
        "endDate": period?.endDate
      });
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => SpendingByCategory.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Reports>> getReports(Period period,
      {List<String> categoryIds}) async {
    String path = "/api/Reports/categories";
    try {
      Response response = await _dio.get(path, queryParameters: {
        "startDate": period.startDate,
        "endDate": period.endDate,
        "categoryIds": categoryIds,
      });
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Reports.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<OffsetHistory>> getOffsetHistory() async {
    String path = "/api/reports/offset";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => OffsetHistory.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Spending>> getSpending(Period period) async {
    String path = "/api/Spending";
    try {
      Response response = await _dio.get(path, queryParameters: {
        "startDate": period.startDate,
        "endDate": period.endDate
      });
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Spending.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Goals>> getPersonalGoals() async {
    String path = "/api/Goals/personal";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Goals.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Goals>> getCommunityGoals() async {
    String path = "/api/Goals/community";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Goals.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Category>> getCategory(String categoryId) async {
    String path = "/api/Categories/$categoryId";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Category.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Categories>> getCategories() async {
    String path = "/api/Categories";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Categories.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<EcoFootprint>> getEcoFootprint(String locationId) async {
    String path = "/api/EcoFootprint";
    try {
      Response response = await _dio
          .get(path, queryParameters: {"locationId": locationId ?? ""});
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => EcoFootprint.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Amount>> getEcoFootprintForCategory(String categoryId,
      Amount amount) async {
    String path =
        "/api/EcoFootprint/category/$categoryId/${amount.currency}/${amount
        .value}";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Amount.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<EcoFootprintInfo>> getEcoFootprintInfo(
      {String countryCode}) async {
    String path = "/api/EcoFootprint/country";
    if (countryCode != null) {
      path += "/$countryCode";
    }

    try {
      Response response = await _dio.get(path);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => EcoFootprintInfo.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Object>> addTransactionReceiptImage(Uint8List fileBytes,
      String transactionId, String imageId,
      {CancelToken cancelToken}) async {
    String path = "/api/Transactions/receiptImage/$imageId";
    FormData formData = new FormData.fromMap({
      "file": MultipartFile.fromBytes(
        fileBytes,
        filename: "file.jpg",
        contentType: MediaType("image", "jpg"),
      ),
      "ImageId": imageId,
      "TransactionId": transactionId
    });
    try {
      Response response =
      await _dio.post(path, data: formData, cancelToken: cancelToken);
      return new BaseResponse.dynamic(response, nullableResult: true);
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Object>> removeTransactionReceiptImage(
      String imageId) async {
    String path = "/api/Transactions/receiptImage/$imageId";
    try {
      Response response = await _dio.delete(path);
      return new BaseResponse(response, nullableResult: true);
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Transaction>> scanReceipt(Uint8List fileBytes,
      {String imageId, CancelToken cancelToken}) async {
    String path = "/api/Transactions/ocr";
    FormData formData = new FormData.fromMap({
      "file": MultipartFile.fromBytes(
        fileBytes,
        filename: "file.jpg",
        contentType: MediaType("image", "jpg"),
      ),
      "ImageId": imageId
    });
    try {
      Response response =
      await _dio.post(path, data: formData, cancelToken: cancelToken);
      return new BaseResponse.dynamic(response,
          dynamicGenericsCreator: (_) => Transaction.fromJson((_)));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Currencies>> getCurrencies() async {
    String path = "/api/Currency";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse(response, creator: (_) => Currencies.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Amount>> getBaseCurrency() async {
    String path = "/api/Currency/base";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse(response, creator: (_) => Amount.fromJson(_));
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Object>> setBaseCurrency(Amount currency) async {
    String path = "/api/Currency/base";
    try {
      Response response = await _dio.post(path, data: currency.toJson());
      return new BaseResponse(response, nullableResult: true);
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  Future<BaseResponse<Object>> checkAuthState() async {
    String path = "/api/tools/authtest";
    try {
      Response response = await _dio.get(path);
      return new BaseResponse(response, nullableResult: true);
    } on DioError catch (error) {
      return BaseResponse.withDioError(error);
    }
  }

  String getPendingReceiptUriForGuid(String guid) =>
      guid != null ? "$apiBaseUrl/api/Transactions/receiptImage/$guid" : null;

  void _unlockDio() {
    _dio.unlock();
    _dio.interceptors.responseLock.unlock();
    _dio.interceptors.errorLock.unlock();
  }

  bool _isTokenExpired(DioError error) {
    var response = BaseResponse.withDioError(error);
    return response.error.code == 401 &&
        response.error?.errorDetails?.type == kTokenExpired;
  }

  void _setupInterceptor() {
    /*
    _dio.interceptors.add(LogInterceptor(
        responseBody: false,
        requestBody: false,
        requestHeader: false,
        responseHeader: false));
     */
    _dio.interceptors.add(InterceptorsWrapper(
        onRequest: (RequestOptions options) {
          options.headers.addAll(_additionalHeaders);
          LoggingInterceptor.printRequest(options);
          return options;
        },
        onResponse: (Response response) {
          LoggingInterceptor.printResponse(response);
          return response;
        },
        onError: _errorHandler));
  }

  dynamic _errorHandler(DioError error) async {
    LoggingInterceptor.printError(error);
    switch (error.response.statusCode) {
      case 401:
        return _handleExpiredToken(error);
      case 426:
        return _onForceUpdate(error);
      case 503:
        return _onMaintenanceMode(error);
      default:
        return error;
    }
  }

  dynamic _handleExpiredToken(DioError error) async {
    if (_isTokenExpired(error)) {
      RequestOptions options = error.response.request;
      // update token and repeat
      // Lock to block the incoming request until the token updated
      _lockDio();
      auth.BaseResponse<ApiToken> response =
      await AuthRemoteRepository().refreshToken();
      if (response.isSuccess()) {
        await _updateAuthorizationHeader();
        _unlockDio();
        return _dio.request(options.path,
            queryParameters: options.queryParameters,
            data: options.data,
            options: Options(method: options.method));
      }
    }
    _unlockDio();
    return error;
  }

  dynamic _onMaintenanceMode(DioError error) {
    _dio.close(force: true);
    _dio = Dio();
    _initDio();
    eventsHandler?.onMaintenanceMode();
    return error;
  }

  dynamic _onForceUpdate(DioError error) {
    _dio.close(force: true);
    eventsHandler?.onForceUpdateRequest();
    return error;
  }

  void _lockDio() {
    _dio.lock();
    _dio.interceptors.responseLock.lock();
    _dio.interceptors.errorLock.lock();
  }

  void _showErrorToast(DioError error) {
    String errorText =
        "Code: ${error?.response?.statusCode}, Type: ${error?.type?.toString()}, Error: ${error?.response?.data?.toString()}";
    Fluttertoast.showToast(
        msg: errorText, toastLength: Toast.LENGTH_LONG, timeInSecForIosWeb: 3);
  }
}
