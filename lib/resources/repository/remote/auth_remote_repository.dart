import 'package:atoma_cash/models/auth_api/auth/response/email_verification_response.dart';
import 'package:atoma_cash/models/auth_api/base_models/base_response.dart'
    as auth;
import 'package:atoma_cash/models/auth_api/models.dart';
import 'package:atoma_cash/resources/repository/local/security_storage_manager.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';
import 'package:atoma_cash/resources/repository/remote/auth_api_provider.dart';

abstract class EventListener {
  void onSignOut();
}

class AuthRemoteRepository {
  List<EventListener> _listeners = [];

  AuthApiProvider _authApiProvider;

  final securityStorageManager = SecurityStorageManager();
  String _token;

  static final AuthRemoteRepository _instance =
      AuthRemoteRepository._internal();

  AuthRemoteRepository._internal();

  factory AuthRemoteRepository({AuthApiProvider authApiProvider}) {
    _instance._authApiProvider = authApiProvider ?? _instance._authApiProvider;
    return _instance;
  }

  void resetRepository() {
    _token = null;
  }

  void subscribeToEvents(EventListener listener) {
    if (!_listeners.contains(listener)) {
      _listeners.add(listener);
    }
  }

  void unsubscribeFromEvents(EventListener listener) {
    if (_listeners.contains(listener)) {
      _listeners.remove(listener);
    }
  }

  void _notifySignOut() {
    _listeners.forEach((listener) => listener.onSignOut());
  }

  void updateLocalization(String languageCode) {
    _authApiProvider.updateLocalization(languageCode);
  }

  Future<auth.BaseResponse<PhoneConfirmation>> requestSMS(
      {String phoneNumber, String countryIso2}) async {
    var response = await _authApiProvider.requestSMS(
        phoneNumber: phoneNumber, countryIso2: countryIso2);
    _token = response?.result?.verifyToken ?? _token;
    return response;
  }

  Future<auth.BaseResponse<PhoneConfirmation>> verifyPhone(
      {String phoneNumber, String confirmationCode}) async {
    var response = await _authApiProvider.verifyPhone(confirmationCode, _token);
    _token = response?.result?.verifyToken ?? _token;
    return response;
  }

  Future<auth.BaseResponse<EmailVerificationResponse>>
      requestEmailVerificationCode() async {
    final response =
    await _authApiProvider.requestEmailVerificationCode(_token);
    _token = response?.result?.verifyToken ?? _token;
    return response;
  }

  Future<auth.BaseResponse<EmailVerificationResponse>>
      checkIsEmailVerificationPassed() async {
    final response =
    await _authApiProvider.checkIsEmailVerificationPassed(_token);
    _token = response?.result?.verifyToken ?? _token;
    return response;
  }

  Future<auth.BaseResponse<PasscodeConfirmation>> verifyPasscode(
      {String passcode, bool replaceExisting}) async {
    var response = await _authApiProvider.verifyPasscode(
        passcode: passcode,
        replaceExisting: replaceExisting,
        verifyToken: _token);
    _token = response?.result?.authToken ?? _token;
    return response;
  }

  Future<auth.BaseResponse<ApiToken>> getToken() async {
    var response = await _authApiProvider.getToken(authToken: _token);
    if (response.isSuccess()) {
      await _saveTokens(
          refreshToken: response?.result?.refreshToken,
          accessToken: response?.result?.accessToken,
          accessTokenType: response?.result?.tokenType);
    }
    return response;
  }

  Future<auth.BaseResponse<ApiToken>> refreshToken() async {
    var refreshToken = await securityStorageManager.getRefreshToken();
    var response = await _authApiProvider.refreshToken(token: refreshToken);
    if (response.isSuccess()) {
      _saveTokens(
          refreshToken: response?.result?.refreshToken,
          accessToken: response?.result?.accessToken,
          accessTokenType: response?.result?.tokenType);
    } else {
      _notifySignOut();
    }
    return response;
  }

  Future<void> _saveTokens(
      {String refreshToken, String accessToken, String accessTokenType}) async {
    await securityStorageManager.saveAccessToken(
        accessToken: '$accessTokenType $accessToken');
    await securityStorageManager.saveRefreshToken(refreshToken: refreshToken);
    await ApiRemoteRepository().updateAuthorization();
  }
}
