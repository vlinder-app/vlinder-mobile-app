import 'package:atoma_cash/models/api/api/balances/amount.dart';
import 'package:atoma_cash/models/api/api/balances/currencies.dart';
import 'package:atoma_cash/models/api/base_models/base_response.dart';
import 'package:atoma_cash/models/api/error/error.dart';
import 'package:atoma_cash/resources/repository/remote/api_remote_repository.dart';

class BaseCurrencyProvider {
  static final BaseCurrencyProvider _instance =
      BaseCurrencyProvider._internal();

  BaseCurrencyProvider._internal();

  final ApiRemoteRepository _apiRemoteRepository = ApiRemoteRepository();

  Amount _baseCurrency;
  List<Amount> _baseCurrencies = [];
  Amount _transactionCurrency;

  factory BaseCurrencyProvider() {
    return _instance;
  }

  Future<List<Amount>> get baseCurrencies async {
    if (_baseCurrencies.isEmpty) {
      return updateBaseCurrencies();
    } else {
      return _baseCurrencies;
    }
  }

  Amount get transactionCurrency => _transactionCurrency ?? _baseCurrency;

  void setTransactionCurrency(Amount transactionCurrency) =>
      _transactionCurrency = transactionCurrency;

  Amount get baseCurrency {
    if (_baseCurrency == null) {
      updateBaseCurrency();
      return Amount(currency: "Undefined", symbol: "Undefined");
    } else {
      return _baseCurrency;
    }
  }

  Future<List<Amount>> updateBaseCurrencies() async {
    BaseResponse<Currencies> _currenciesResponse =
        await _apiRemoteRepository.getCurrencies();
    if (_currenciesResponse.isSuccess()) {
      _baseCurrencies = _currenciesResponse.result.currencies;
      return _baseCurrencies;
    } else {
      print(_currenciesResponse.error);
      return null;
    }
  }

  Future<Amount> updateBaseCurrency() async {
    BaseResponse<Amount> _baseCurrencyResponse =
        await _apiRemoteRepository.getBaseCurrency();
    if (_baseCurrencyResponse.isSuccess()) {
      _baseCurrency = _baseCurrencyResponse.result;
      return _baseCurrency;
    } else {
      print(_baseCurrencyResponse.error);
      return null;
    }
  }

  Future<Error> setBaseCurrency(Amount baseCurrency) async {
    _baseCurrency = baseCurrency;
    BaseResponse<Object> _baseCurrencyResponse =
        await _apiRemoteRepository.setBaseCurrency(baseCurrency);
    if (_baseCurrencyResponse.isSuccess()) {
      return null;
    } else {
      return _baseCurrencyResponse.error;
    }
  }
}
