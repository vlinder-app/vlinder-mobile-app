import 'package:atoma_cash/extensions/double_extensions.dart';
import 'package:flutter_test/flutter_test.dart';

import 'currency_formatting_test_data.dart';

void main() {
  test('CurrencyAbridgedFormatter extension test: < 100', () {
    CurrencyFormattingTestDataModel.abridgedLessThenOneHundred
        .forEach((dataModel) {
      expect(dataModel.value.currencyAbridgedString(dataModel.ticker),
          dataModel.correctOutput);
    });
  });

  test('CurrencyAbridgedFormatter extension test: < 1000', () {
    CurrencyFormattingTestDataModel.abridgedLessThenOneThousand
        .forEach((dataModel) {
      expect(dataModel.value.currencyAbridgedString(dataModel.ticker),
          dataModel.correctOutput);
    });
  });

  test('CurrencyAbridgedFormatter extension test: > 1000', () {
    CurrencyFormattingTestDataModel.abridgedMoreThenOneThousand
        .forEach((dataModel) {
      expect(dataModel.value.currencyAbridgedString(dataModel.ticker),
          dataModel.correctOutput);
    });
  });
}
