import 'dart:core';

class CurrencyFormattingTestDataModel {
  double value;
  String correctOutput;
  String ticker;

  CurrencyFormattingTestDataModel(this.value, this.correctOutput, this.ticker);

  static final abridgedLessThenOneHundred = <CurrencyFormattingTestDataModel>[
    CurrencyFormattingTestDataModel(0.10, '€0.10', '€'),
    CurrencyFormattingTestDataModel(0.99, '€0.99', '€'),
    CurrencyFormattingTestDataModel(1.10, '\$1.10', '\$'),
    CurrencyFormattingTestDataModel(1.20, 'BTC1.20', 'BTC'),
    CurrencyFormattingTestDataModel(1.90, '€1.90', '€'),
    CurrencyFormattingTestDataModel(9.99, '€9.99', '€'),
    CurrencyFormattingTestDataModel(10.11, '€10.11', '€'),
    CurrencyFormattingTestDataModel(99.99, '€99.99', '€'),
  ];

  static final abridgedLessThenOneThousand = <CurrencyFormattingTestDataModel>[
    CurrencyFormattingTestDataModel(100.10, '€100', '€'),
    CurrencyFormattingTestDataModel(100.12, '€100', '€'),
    CurrencyFormattingTestDataModel(100.90, '€100', '€'),
    CurrencyFormattingTestDataModel(999.50, '€999', '€'),
  ];

  static final abridgedMoreThenOneThousand = <CurrencyFormattingTestDataModel>[
    CurrencyFormattingTestDataModel(1000.10, '€1.0K', '€'),
    CurrencyFormattingTestDataModel(1000.12, '€1.0K', '€'),
    CurrencyFormattingTestDataModel(1100, '€1.1K', '€'),
    CurrencyFormattingTestDataModel(1100.50, '€1.1K', '€'),
    CurrencyFormattingTestDataModel(1500.55, '€1.5K', '€'),
    CurrencyFormattingTestDataModel(12000, '€12.0K', '€'),
    CurrencyFormattingTestDataModel(12567.66, '€12.6K', '€'),
    CurrencyFormattingTestDataModel(123456.55, '123K', ''),
    CurrencyFormattingTestDataModel(1223656.55, '1.22M', ''),
  ];
}
