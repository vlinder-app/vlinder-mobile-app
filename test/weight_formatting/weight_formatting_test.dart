import 'package:atoma_cash/utils/weight_formatter.dart';
import 'package:flutter_test/flutter_test.dart';

import 'weight_formatting_test_data.dart';

void main() {
  test('abridged weight formatting test: < 100 g', () {
    WeightFormattingTestDataModel.abridgedLessThenOneHundredGram
        .forEach((dataModel) {
      expect(
          WeightFormatter.abridgedWeightString(dataModel.value,
              unitSymbol: dataModel.units),
          dataModel.correctOutput);
    });
  });

  test('abridged weight formatting test: < 100 kg', () {
    WeightFormattingTestDataModel.abridgedLessThenOneHundredKilogram
        .forEach((dataModel) {
      expect(
          WeightFormatter.abridgedWeightString(dataModel.value,
              unitSymbol: dataModel.units),
          dataModel.correctOutput);
    });
  });

  test('abridged weight formatting test: < 1 ton', () {
    WeightFormattingTestDataModel.abridgedLessThenOneTon.forEach((dataModel) {
      expect(
          WeightFormatter.abridgedWeightString(dataModel.value,
              unitSymbol: dataModel.units),
          dataModel.correctOutput);
    });
  });

  test('abridged weight formatting test: > 1 ton', () {
    WeightFormattingTestDataModel.abridgedMoreThenOneTon.forEach((dataModel) {
      expect(
          WeightFormatter.abridgedWeightString(dataModel.value,
              unitSymbol: dataModel.units),
          dataModel.correctOutput);
    });
  });
}
