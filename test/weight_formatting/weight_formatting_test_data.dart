class WeightFormattingTestDataModel {
  double value;
  String correctOutput;
  String units;

  WeightFormattingTestDataModel(this.value, this.correctOutput, {this.units});

  static final abridgedLessThenOneHundredGram = <WeightFormattingTestDataModel>[
    WeightFormattingTestDataModel(0.001, '<0.1 kg', units: 'kg'),
  ];

  static final abridgedLessThenOneHundredKilogram =
      <WeightFormattingTestDataModel>[
    WeightFormattingTestDataModel(0.21, '0.2'),
    WeightFormattingTestDataModel(10.125, '10.1'),
  ];

  static final abridgedLessThenOneTon = <WeightFormattingTestDataModel>[
    WeightFormattingTestDataModel(138.415, '138'),
    WeightFormattingTestDataModel(246.9, '246 kg', units: 'kg'),
  ];

  static final abridgedMoreThenOneTon = <WeightFormattingTestDataModel>[
    WeightFormattingTestDataModel(1000.123, '1.0 ton', units: 'ton'),
    WeightFormattingTestDataModel(1250.123, '1.2 ton'),
  ];
}
